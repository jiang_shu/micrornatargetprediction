\documentclass{beamer}
\usetheme{default}
\usepackage{natbib}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{circuitikz}
\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\graphicspath{{figures/}}
\usepackage{tikz}
\usetikzlibrary{shapes, arrows, calc, positioning}
\usepackage{float}
\usepackage{bm}
\usepackage{xcolor,colortbl}
\setbeamertemplate{footline}[frame number]

\newcommand{\heading}[1]{\textcolor{red}{\large #1}}
\newcommand{\mbf}[1]{\ensuremath{\boldsymbol{#1}}}
\newcommand{\mbb}[1]{\ensuremath{\mathbb{#1}}}
\newcommand{\mcal}[1]{\ensuremath{\mathcal{#1}}}
\definecolor{LightRed}{rgb}{1,0.5,0.5}
\newcommand{\clr}{\cellcolor{LightRed}}
\newcommand{\mrna}{\text{mRNA}~}
\newcommand{\micrna}{\text{microRNA}~}

\title[MicroRNA target prediction]{MicroRNA target prediction.}
\author[Asish Ghoshal]{Asish Ghoshal}

\begin{document}

\begin{frame}[plain]
  \titlepage
\end{frame}

\begin{frame}{MicroRNAs}
\begin{itemize}
    \item Small regulatory RNAs of length $\approx 22$ nucleotides.
    \item Responsible for post-transcriptional gene silencing.
    \item MicroRNAs downregulate protien production by pairing with the mRNAs inside a silencing complex.
    \item More that 60\% of genes are believed to be targeted by MicroRNAs \citep{friedman2009most}.
\end{itemize}
\end{frame}

\begin{frame}{Problem}
\begin{itemize}
  \item Given a microRNA and a mRNA, predict if the mRNA is a target of the microRNA.
  \item Predict the amount of downregulation, measured as the $\log_2$ fold change of protien production.
\end{itemize}
\end{frame}

\begin{frame}{Motivation}
  \begin{table}
    \scalebox{0.6}{
    \begin{tabular}{cccccccccccccccccccccccc}
      MicroRNA: & 3' & g & t & C & G & A & A & A & G & U & U & U & U & A & \clr C & \clr U & \clr A & \clr G & \clr A & \clr G & U & g & 5'\\
       & & &   & $\vert$ & : & $\vert$ & $\vert$ & $\vert$ & $\vert$ & $\vert$ &   & $\vert$ & $\vert$ & $\vert$ & $\vert$ & $\vert$ & $\vert$ & $\vert$ & $\vert$ & $\vert$ & : &  & \\
      3' UTR: & 5' & g & t & G & U & U & U & U & C & A & C & A & A & U & G & A & U & C & U & C & G & g & 3'
    \end{tabular}
    }
  \end{table}
  \begin{itemize}
    \item 15 matches.
    \item 2 G:U wobbles.
    \item Perfect complementarity at 5' end of MicroRNA (seed region) is an indicator of microRNA regulation.
    \item Perfect seed comlimentarity at the seed region neither sufficient nor necessary condition for microRNA regulation.
  \end{itemize}
\end{frame}

\begin{frame}{Current understanding of microRNA action}
\begin{itemize}
    \item Plants:
        \begin{itemize}
            \item Almost perfect pairing between \mrna and \micrna at the seed region.
            \item Results in cleavage of the \mrna. 
        \end{itemize}
    \item Animals:
        \begin{itemize}
            \item \micrna binds less tightly to the \mrna at the seed region.
            \item Insertions, deletions and mismatches in the seed region are commonly found.
            \item Act by transcript destabilization, translational repression, or both.
        \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Previous Approaches}
    \begin{itemize}
        \item Filtering approaches.
        \item Machine Learning.
    \end{itemize}
\end{frame}

\begin{frame}{Previous Approaches}
{\Large \emph{miRanda, \citet{enright2004microrna}}}
  \begin{itemize}
    \item Identify potential sites by using local alignment based on sequence complementarity and score target sites based on number of matches.
    \item G:U wobble pair is permitted but scores less.
    \item Computes the minimum free energy available for the microRNA (query sequence) and mRNA (reference sequence) pair.
    \item Filters out targets having scores less than a certain specified threshold and free energy above a specified threshold.
    \item Doesn't optimize for mRNA expression changes.
  \end{itemize}
\end{frame}

\begin{frame}{Previous Approaches}
{\Large\emph{\citet{grimson2007microrna}}}
  \begin{itemize}
    \item Four-class hierarchy of canonical seed types of differing efficiencies.
    \item Additional context features like AU content flanking the target site.
    \item Explicitly optimizes for mRNA expression changes.
  \end{itemize}
\end{frame}

\begin{frame}{Previous Approaches}
{\Large\emph{Machine Learning}}
\begin{itemize}
    \item Common Features:
    \begin{itemize}
        \item Thermodynamic features of target and microRNA: $\Delta G_{duplex}$, $\Delta \Delta G$ scores.
        \item Number of kinds of seed matches both canonical and non-canonical.
        \item Evolutionary conservation.
        \item AU composition of the non-seed flanking region.
    \end{itemize}
    \item Common Methods:
    \begin{itemize}
        \item Support Vector Regression \citep{betel2010comprehensive}.
        \item Gaussian Mixture models \citep{liu2010bayesian}.
        \item Naive Bayes \citep{yousef2007naive}.
    \end{itemize}
\end{itemize}
\end{frame}
    
\begin{frame}{State of the Art}
Benchmarked on the pSilac dataset.
\begin{itemize}
    \item Accuracy of around 60\%.
    \item Recall of around 10\%.
\end{itemize}
\end{frame}

\begin{frame}{MicroRNA target prediction}
    \begin{itemize}
        \item The seed match requirement is only able to explain 10\% of the true targets at an accuracy of 60\%.
        \item Conserved seed match further reduces the recall while marginally improving the accuracy.
        \item \citet{xu2014identifying} developed a model utilizing only $\Delta G_{duplex}$ and $\Delta \Delta G$ scores
             to get an accuracy of 60\% and recall of 10\%.
    \end{itemize}
\end{frame}

\begin{frame}{A functional data analysis approach}
    \begin{itemize}
        \item Compute $\Delta \Delta G$ curves for a \mrna-\micrna pair by using a sliding (overlapping) window.
        \item Classify the curves as a valid interaction (+1) or a invalid interaction (-1).
    \end{itemize}
\end{frame}

\begin{frame}{A functional data analysis approach}
    \begin{itemize}
        \item Data: discrete observations of infinitedimensional curves. $\{\mbf{x}_i \}_{i = 1}^{N}$.
        \item Use a basis function expansion using b-spline basis function to compute the smooth curves
        \begin{align*}
            x_i(t) = \sum_{j = 1}^{M} \psi_j(t)w_j
        \end{align*}
        \item Compute the weight matirx $W$ using least squares.
    \end{itemize}
\end{frame}

\begin{frame}{A functional data analysis approach}
    \begin{itemize}
        \item Use functional PCA to get the most important components.
        \begin{align*}
            f_{i1} = \int \xi_1(s)x_i(s)ds\\
            maximize\; \sum_if_{i1}^2 \;subject\; to\\
            ||\xi_1||^2 = 1
        \end{align*}
        \item Use the principal component scores as features to classify the curves using SVM.
    \end{itemize}
\end{frame}

\begin{frame}{Results}
    \begin{figure}[H]
    \centering
        \includegraphics[width=\linewidth]{pos_neg_plot}
    \label{fig:validation}
    \end{figure}
\end{frame}

\begin{frame}{Results}
    \begin{itemize}
        \item On artifically generated negative examples: 10-fold crossvalidation accuracy of $89\%$ and recall of $84\%$.
        \item On the TargetMiner dataset: 10-fold crossvalidation accuracy $70\%$ and recall of $30\%$.
        \item Didn't work on the test pSILAC dataset.
    \end{itemize}
\end{frame}

\begin{frame}{Results}
    \begin{itemize}
        \item Instead of learning one classifier for all genes, cluster curves and learn different classifier for each cluster.
        \item Cluster negative examples. Assign positive examples to clusters by gene.
        \item How do you assign positive examples when the same gene belongs to multiple clusters?
    \end{itemize}
\end{frame}

\begin{frame}{Results}
    \begin{figure}[H]
    \centering
        \includegraphics[width=\linewidth]{rplot}
    \label{fig:validation1}
    \end{figure}
\end{frame}

\begin{frame}{References}
\bibliographystyle{unsrtnat}
\bibliography{presentation}
\end{frame}


\end{document}
