#!/bin/bash
# No args: sync code only
# arg == data: sync data only
# otherwise treat arg as a file and sync that file.

SYNC_DATA=0
SYNC_CODE=0
ARGS="$#"

if [ "$ARGS" -gt 0 ]; then
    if [ "$1" == "data" ]; then
        SYNC_DATA=1
    else
        SYNC_FILE=$1
    fi
else
    SYNC_CODE=1
fi

for i in `seq 4 13`; do
    host="aghoshal@pdsl${i}.cs.purdue.edu"
    echo $host

    if [ "$SYNC_CODE" -eq 1 ]; then
        echo "Syncing code."
        ssh $host "mkdir -p ~/Workspace/MicroRnaTargetPrediction"
        rsync -avz code $host:~/Workspace/MicroRnaTargetPrediction/
    fi

    if [ "$SYNC_DATA" -eq 1 ];then
        echo "Syncing data."
        rsync -avz data/human $host:~/Workspace/MicroRnaTargetPrediction/data/
        rsync -avz data/mouse $host:~/Workspace/MicroRnaTargetPrediction/data/
        rsync -avz data/mirnas $host:~/Workspace/MicroRnaTargetPrediction/data/
    fi

    if [ "$SYNC_FILE" ]; then
        DST="~/Workspace/MicroRnaTargetPrediction/"
        echo "Syncing file ${SYNC_FILE} to ${DST}"
        rsync -avzR $SYNC_FILE $host:$DST
    fi
done
