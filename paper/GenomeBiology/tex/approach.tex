\subsection*{Approach}
\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=0.7\textwidth]{illustration}
    \caption{\csentence{Schematic of features.}
    A schematic of some of the features used in the model.
    The alternating blue and green regions denote the 13 consecutive windows
    around the target site (shown in red) where various thermodynamic 
    and sequence features are computed.
    The seed alignment pattern is computed by considering the mRNA nucleotides
    that are aligned with nucleotides from index 1 to 8 from the 5' end of the miRNA.
    The alignment is represented by a vector of 1 (match), 2 (mismatch), 3 (gap) and 4 (GU wobble).
    The relative position of the target site within an mRNA region is also used as a feature,
    as well as the length of the target site.}
    \label{fig:illustration}   
\end{figure*}

\begin{table*}[!htbp]
\caption{Comparison of the normalized candidate set size used by various methods.\label{tab:candidate_set}}
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline 
\rowcolor{LYellow}
 & mirSVR & PITA & TargetScan & STarMir & \bf{\name} \\
\hline 
Human & 1.256 & 3.078 & NA & 56.183 & 66.081 \\
\hline 
Mouse & 0.56 & 2.179 & 0.318 & 37.418 & 75.503 \\
\hline
\end{tabular}
\\[0.1in]
\begin{flushleft}
The normalized candidate set size
is obtained by dividing the candidate set size by the number of miRNA times the number of mRNAs. The normalized
candidate set size can be interpreted as the average number of candidate sites considered by the method for an mRNA-miRNA pair.
mirSVR, PITA and TargetScan only consider the 3' UTR region, so the normalized candidate set size is very low for those methods.
\end{flushleft}
\end{table*}

\begin{table*}[!htbp]
\caption{Attributes of data used for training and prediction in \name. \label{tab:data}}
\centering
\resizebox{2.0\columnwidth}{!}{%
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\hline 
\cellcolor{LYellow} & \cellcolor{LYellow} \# Positive examples (Seed:Seedless)& \cellcolor{LYellow}& \cellcolor{LYellow}&\cellcolor{LYellow} & \multicolumn{3}{c|}{\cellcolor{LYellow}\# Positive target sites in}\\
\multirow{-2}{*}{\cellcolor{LYellow}} & \multirow{-2}{*}{\cellcolor{LYellow}} & \multirow{-2}{*}{\cellcolor{LYellow}\# Negative examples} & \multirow{-2}{*}{\cellcolor{LYellow}\# mRNA} & \multirow{-2}{*}{\cellcolor{LYellow}\# miRNA} & \multicolumn{1}{c}{\cellcolor{LYellow}3' UTR} & \multicolumn{1}{c}{\cellcolor{LYellow}CDS} & \cellcolor{LYellow}5' UTR\\
\hline
HITS-CLIP (Mouse) & 861,208 (6\%:94\%) & 35,608,333 & 4,059 & 119 & 478,138 ($ \approx 56\%$) & 367,371 ($ \approx 43\%$) & 15,699 ($\approx 1\%$)\\
\hline
PAR-CLIP (Human) & 141,109 (8\%:92\%) & 2,659,748 & 1,211 & 35 & 80,775 ($\approx 57\%$)& 55,250($\approx 39\%$) & 5,084 ($\approx 4\%$)\\
\hline
\end{tabular}
}
\\[0.1in]
\begin{flushleft}
For both mouse and human data,
most of the positive miRNA target sites are found in the 3' UTR region followed by the CDS region with very few target sites
located in the 5' UTR region.
\end{flushleft}
\end{table*}

% this has already been explained in background
%CLIP methods \cite{chi2009argonaute, hafner2010transcriptome,
%kishore2011quantitative} identify short regions across the genome that harbor
%potential binding sites for miRNAs. 
While CLIP-seq datasets identify short mRNA regions
that are functional AGO-mRNA interaction sites \cite{chi2009argonaute, hafner2010transcriptome, kishore2011quantitative}, additional bioinformatic analysis
is needed to identify the miRNA-mRNA binding sites. In order to identify miRNAs that might
target those AGO-crosslinked regions, we followed the same general approach as previous methods
\cite{xu2014identifying, liu2013clip, betel2010comprehensive, kertesz2007role}.
The idea is to first generate a candidate set of mRNA binding 
sites for a list of mRNAs and miRNAs by enforcing a minimum threshold 
on the alignment score \footnote{\small{Loosely speaking ``alignment score'' is a
quantitative value that represents how well the miRNA is paired with the mRNA.
So the score depends on the lengths of exact matches and the degree of
mismatches.}}
% SC (4/8/15): We need to precisely define alignment score before using it.
% AG (04/11/15): Done
or the minimum free energy of the hybridization ($\delg$) of the
miRNA and mRNA and/or using seed-match constraints. In the next step, 
different methods use varied approaches to identify true
miRNA target sites within the generated candidate set of mRNA-miRNA interactions.
Previous works have used various criteria to generate the candidate set of mRNA target sites. For example, in mirSVR \cite{betel2010comprehensive}, the authors use the miRanda algorithm \cite{john2004human} to generate the initial candidate set. The miRanda algorithm
computes an optimal local alignment of the miRNA with an mRNA sequence,
by using various parameters for the overall alignment score, gap opening,
and gap extension penalties. The authors generate candidate sites involving
canonical seed matches, which they defined as ``sites that contain minimally a 6-mer perfect match at positions 2 to 7 of the miRNA'',
and non-canonical seed matches. However, for the latter, they only allow a single G:U wobble or a single 
mismatch in the seed region. In \cite{liu2013clip}, Liu {\em et al.} use 
two criteria for generating the candidate set. First, they use 
the RNAhybrid program \cite{kruger2006rnahybrid} 
to generate candidate sites by enforcing a threshold of --15 kcal/mol 
on the thermodynamic binding energy ($\delg$). Second, they constrain
the seed match alignment, without constraining the binding energy for the
match, to belong to one of the five seed classes of miRNA seeds, as defined in
\cite{bartel2009micrornas}. 
% (SC) the seed efficacy hierarchy being: 8mer > > 7mer-m8 > 7mer-A1 > > 6mer > no site, with the 6mer differing only slightly from no site at all -- I see 5 classes here but this paper talks about 6 classes: 
% (SC) http://nar.oxfordjournals.org/content/early/2012/11/23/nar.gks1138.full (Ref. 22) FYI
% AG 04/11/2015: Changed it to 5 seed classes.
It is easy to see that by starting out with a more restricted set of candidate target sites, a
method can achieve a higher true positive rate for identifying positive
target sites within this restrictive set. However, this would be at the cost of missing out on a large
number of positive target sites that are not present in the candidate set in the first place.

For our method, we select the least restrictive filter to have the most expansive superset
for the initial selection of possible target locations genome-wide.
Specifically, in our case we have the thermodynamic cut-off of --15 kcal/mol, and then, to
generate seed-sites, we constrain the seed match to be at least a 6-mer without using
any additional constraints. If a target region meets {\em either} of these two criteria,
then we include it in the candidate set.  Thus, we challenge our model by coming up with the most expansive set of potential target sites.
This shows up quantitatively in \Tref{tab:candidate_set},
where we see that the dataset that we evaluate on is the largest among all prior works.

Algorithm \ref{algo:candidate_set} describes our process of generating the candidate set.
Line 3 of the algorithm extracts all the candidate target regions for an mRNA-miRNA pair
for which the binding energy $(\delg)$ is less than --15 kcal/mol. Line 4 of the
algorithm extracts all target sites which have atleast a 6-mer seed match. Line 5 removes from the set of target sites extracted in step 3, those target sites that also have
a seed match. Finally, the algorithm returns a set of seed and seedless target sites for each mRNA-miRNA pair.

Since different methods use different numbers of miRNAs and mRNAs, we use a metric called
{\em ``normalized candidate set size''} to compare against other methods. The 
normalized candidate set size is defined to be the size of the candidate set divided
by the product of number of mRNAs and miRNAs used by the method. So, the normalized
candidate set size can be thought of as the average number of potential target sites
considered by a method for an mRNA-miRNA pair. \emph{The higher the number, the more general and less restrictive the method is.}

After generating the candidate set, we use CLIP-seq datasets to label the positive (that is, functional) sites. Specifically, if the site is contained within an AGO-crosslinked region for the mRNA, we label the mRNA fragment to be a (positive) target site.
%After generating the candidate set of target sites, we label each target site for an mRNA-miRNA pair
%to be a functional (positive) site if the site is contained within an AGO-crosslinked
%region for the mRNA as obtained from the CLIP datasets. 
Thus, \emph{miRNA-mRNA interactions are deemed positive (functional) if the target site is present in an AGO-crosslinked region and, additionally, either the binding energy of the miRNA-mRNA hybrid is below a
certain threshold or if there is a seed match.} In this paper, we use the term {\em ``seed match''} to refer to
a perfect pairing within nucleotides 1 to 8, of length at least 6, from the 5' end of the miRNA. We call the corresponding mRNA target site as a {\em ``seed site''}. On the other hand,
any pairing within nucleotides 1 to 8 from the 5' end of the miRNA that
does not involve at least a perfect 6-mer match is referred to as a {\em ``non-canonical seed match''}; the corresponding mRNA target site called a {\em ``seedless site''}.
\footnote{\small{It should be noted that our definition of a seed match is slightly different from what others have
used in the past. We used a slightly more general definition of a canonical seed match
to account for different types of canonical seed matches that are considered by various computational methods.
For example, Bartel {\em et al.} \cite{bartel2009micrornas} define three types of alignments 
involving perfect complementarity with nucleotides 2-7 from the  5' end
of the miRNA as canonical seed match.}} While our method makes no distinction between
a canonical and non-canonical seed match, we do present results for seed and seedless 
sites separately. This is in order to gain more insight into the subtleties of miRNA target recruitment as well as to compare the performance of our algorithm against other methods that generate predictions exclusively for
sites involving canonical seed matches. Notably, in the actual execution of our method, we use a principled approach for all target sites, whether, seed or seedless, by calling into play a novel metric that we call {\em seed enrichment metric}, which we describe later in the Methods Section.

Thus, given the notion of a functional miRNA-mRNA interaction, the learning problem becomes that of predicting whether a given region in an mRNA is targeted by a miRNA. 
%In the following sections we describe the different features we considered in
%developing a classifier and then describe the classifier model.

\begin{algorithm}
\caption{Candidate set generation for miRNA target prediction}\label{algo:candidate_set}
\begin{algorithmic}[1]
    \Require mRNA list $\mcal{M}$, miRNA list $\mcal{N}$
    \Ensure Candidate target locations, involving canonical seed matces $\mcal{O}^s$ and non-canonical
    seed matches $\mcal{O}$, for all mRNA miRNA pairs $\mcal{M} \times \mcal{N}$.
    \For{mRNA $m \in \mcal{M}$}
        \For{miRNA $n \in \mcal{N}$}
            \State $\mcal{O}_{mn} = \mcal{O}_{mn} \cup \{t : \delg(m[t], n) \leq -15 kcal/mol\}$
            \Comment{$t$ is a target site in $m$ as computed by RNAhybrid \cite{kruger2006rnahybrid}} 
            \State $\mcal{O}_{mn}^s = \mcal{O}_{mn}^s \cup \{t : 6 \leq | seedMatch(m[t], n) | \leq 8 \}$
            \Comment{$| seedMatch(m[t], n) |$ is the length of the seed match}
            \State $\mcal{O}_{mn} = \mcal{O}_{mn} \setminus \mcal{O}_{mn}^s$ 
        \EndFor
    \EndFor
    \State \Return $\{\mcal{O}_{mn}\}_{|\mcal{M}| \times |\mcal{N}|}$, $\{\mcal{O}_{mn}^s\}_{|\mcal{M}| \times |\mcal{N}|}$
\end{algorithmic}
\end{algorithm}
