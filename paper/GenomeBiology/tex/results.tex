\subsection*{Results}
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.45\textwidth]{v_clip_all_curves}
    \caption{The mean curves $(\mu(t))$, plus 1$\times$standard deviation ($\sigma(t)$), for various curves in the positive (red)
    and negative (black) mRNA-miRNA set for the human dataset (PAR-CLIP). The mean and standard deviations
    were computed for each index $-13 \leq t \leq 13$ over all examples.
    A window offset of 0 corresponds to the target site
    in the mRNA while other offsets correspond to positions of the moving window on either side of the target site
    The mean is computed over examples where the
    entire curve was available i.e. discarding the cases where the matching region was toward one end of the mRNA.}
    \label{fig:curves}
\end{figure}
By representing the features $\delg$, $\deldelg$, and local AU content as curves
(\Fref{fig:curves}), some interesting patterns immediately emerge.  For
instance, we see that all three features, on an average, have a characteristic
V-shape, where the value of the feature has a steep dip at the target site.
This is partly due to fact that the target site mostly has a smaller size than the
size of the consecutive windows that we use to compute values at regions flanking the target site.
% SC (04/10/15): CTM - don't use the term sliding window because there are no overlaps for the windows that we use of size 46 nt and 13 nt; use consecutive windows instead
For all the features, we do notice that there is significant overlap between the values of curves for positive and negative mRNA-miRNA
interactions --- observe the overlap in \fref{fig:curves} between the mean + standard deviation of the positive and the negative samples. What is interesting though, among the three features, the curves for local AU content have the most separability for the positive and
negative examples.

Another subtle difference between positive and negative
mRNA-miRNA interactions is that for AU content and $\delg$, the 
value of the curves gradually increases from the 5' to the 3' end. Also, the rate of increase is greater for the positive examples than for the negative examples.

Finally, we note that the difference in binding energy ($\delg$) is sharply lower
at the target site compared to that at the flanking regions. The 
difference becomes increasingly pronounced as we move from the 5' end to the 3' end of the mRNA.
So the curves for $\delg$ seem to suggest that the one of the factors
that determines if a miRNA will target a certain mRNA region is
governed not so much by the stability of the miRNA-mRNA duplex at the target region
but more by the difference in duplex stability between the target site and the target-flanking regions.
A larger difference between duplex stability at the site and flanking regions
translate to a greater preference for binding. Thus, given the differences between positive and
negative mRNA-miRNA interactions in terms of the various thermodynamic and sequence (AU) curves,
it only seemed natural to incorporate some sort of representation of these curves into our model.
Toward that end, we use non-parametric representations of the curves. These curves are represented as
linear combinations of cubic B-spline basis functions with only very general smoothness
assumptions. Specifically, for a B-spline, the assumption is that the second derivative of the curve exists and is continuous everywhere.
% SC - I abstracted some stuff out of what was written here. So, non-parametric is a misnomer here but correct. This is because we have parameters but we are not fitting with a quadratic equation or, for that matter, using any other order equation.
% AG (04/11/2015): That is sort of correct. 
% Non-parametric doesn't mean that there aren't any parameters but that the
% number of parameters grows with the input size. Here we don't choose the smooth curve to
% have a paramteric form (e.g. parabolic etc) but we instead represent the smooth curve
% as a linear combination of B-Spline basis functions, which can represent arbitrary curve.
In the next section, we describe the performance of our methods {\em vis-\`{a}-vis} competition.

\subsection*{Comparison against other methods}
For comparison with competition, we use the CLIP-seq datasets,
which have mRNA information, and coarser-grained (larger) nucleotide regions that include the actual AGO binding site, call that: $l_1$.
For our synthetic data, which is generated from the experimental data,
we have: (mRNA, miRNA, $l_2$), where the location $l_2$  is finer-grained and is localized within $l_1$.
We give competitive protocols a victory, if they predict a binding site as: (mRNA, miRNA, $l_3$),
if $l_3$ has at least a threshold amount of overlap with location $l_1$ (AGO-crosslinked region).
The threshold that we use for our evaluation is 90\%.
Note that this gives competition the benefit. As an example, let us consider the following scenario: if the actual synthetically-generated data is
(mRNA-a, miRNA-b, $l_2$) and the prediction from competition is (mRNA-a, miRNA-c, $l_2$),
we count that up as a victory for the competitive protocol. So, our
evaluation procedure does not penalize other methods if the identity of the miRNA
is different for a target site than what we have computed. 


\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=0.95\textwidth]{roc_all}
    \caption{ROC curves for Human (PAR-CLIP) and Mouse (HITS-CLIP). The figures in the first row
    are for target sites involving canonical seed matches while the second row shows results for
    non-canonical seed match target sites. The legend ``Human train'' in the ROC curves for mouse data
    indicates the model which was trained on human data while the mouse data was used as test dataset.
    Similarly the legend ``Mouse train'' in the ROC curves for human data indicates the model which
    was trained on mouse data while the human data was used as the test dataset. 
    MirSVR \cite{betel2010comprehensive}, PITA \cite{kertesz2007role}, TargetScan \cite{grimson2007microrna}
    only generate predictions for seed match sites in the 3' UTR region. Note that for seedless sites in humans, although 
    mirSVR appears to perform slightly better than MIRZA, it generates very few seedless target sites, thereby resulting
    in a very jagged ROC curve. The markers indicate points on the curve where the difference between the TPR and FPR is maximum.}
    \label{fig:results}
\end{figure*}

\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=0.95\textwidth]{roc_human_regions}
    \caption{ROC curves for 10-fold Crossvalidation performance of \name in different regions of
             the gene in the human dataset.}
    \label{fig:results_human_regions}
\end{figure*}

\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=0.8\textwidth]{plot_weights_all}
    \caption{Mean feature weights (along with standard deviation) learned by the linear SVM model for seed
    and seedless sites.
    \label{fig:plot_weights}}
\end{figure*}

\Fref{fig:results} shows the 10-fold crossvalidation as well as cross-species prediction performance
of our algorithm {\em vis-\`{a}-vis} mirSVR \cite{betel2010comprehensive},
PITA \cite{kertesz2007role}, TargetScan \cite{grimson2007microrna}, \cite{liu2013clip},
and MIRZA \cite{khorshid2013biophysical}, on the human and mouse datasets. Since mirSVR, TargetScan, and PITA, only consider the 3' UTR
region for making predictions, to have a fair comparison, we also validated our model only for those target sites that are present in the 3' UTR region.

From \Fref{fig:results}, it is clear that our method outperforms all competition for all the genic regions.
We note that the curves for \name are smoother because they have been averaged
over multiple hold-out datasets during crossvalidation and sub-sampling from the larger negative dataset.
% hold-out datasets being the random sub-samples
Further, another factor contributing to the smoothness of our ROC curves is the fact that our model
has very low variance at the cost of increased bias. This is further discussed in the Discussion Sub-section.

There are only a few methods that predict non-canonical target sites. We are able to get better performance
for these non-canonical site predictions as well. Also, the difference in performance between intra-species and inter-species prediction
is quite small, indicating that our method performs quite well in predicting across species, and by extrapolation, across multiple cell lines.

We also note that the prediction performance for 3' UTR sites is almost identical to those
of other sites (see \Fref{fig:results_human_regions})\footnote{For seed matches, the sparse 
5' UTR match sites shows slightly better performance, but considering the small size 
of this sample set, this is likely not statistically significant.}.
This goes to show that the inclusion of the categorical feature indicating 
the type of region, namely, 3' UTR, CDS, or 5' UTR, is able to explain the
differing efficacies of target sites in different regions,
and that other features like thermodynamic binding, accessibility, conservation, etc., have
the same predictive power in the three different regions.
% SC (4/10/15): I understand that for 5' for mouse, it is even better but we are ignoring that because that has so few sites. 
% AG (04/11/15): Yes.

\Fref{fig:plot_weights} shows the feature weights learned by our model for both canonical seed sites
and seedless sites. 
% From \qref{eq:logistic}, it is clear that for scalar covariates, which take positive 
% values, such as seed enrichment, conservation, etc., negative weights for the feature
% SC (4/10/15): Why do scalar covariates need to take positive values? It is not clear from that referred equation either. 
% correspond to low probability scores for the mRNA-miRNA interaction.
Negative weights for a scalar feature correspond to it being negatively 
correlated with the positive mRNA-miRNA interactions. In contrast, for functional covariates,
it is difficult to interpret the ``sign" of the weights for the B-spline basis functions. This is because the
coefficients of the basis functions control the shape of the curve, where, larger {\em absolute} weights correspond to higher predictive power.
\Tref{tab:weights} ranks the top 20 features in descending order of the absolute value of their weights. For
functional covariates, the number within square braces indicates the coefficient 
index in the B-spline basis function expansion of that feature.

It is immediately evident from \Tref{tab:weights} that for seed sites, most of the B-spline basis
function coefficients, with the exception of AU content, 
correspond to the ``seed'' curves. While, for the seedless sites, the ``site'' curves are
more effective in differentiating positive mRNA-miRNA interactions from negative ones.
That goes to show that when there is a seed match, the thermodynamic profile of
the mRNA seed region is what matters more in determining functional binding sites.

Local AU content is a strong differentiator of positive mRNA-miRNA interactions from negative ones. 
The weights learned by our model corroborate the conclusion from \Fref{fig:curves}
that since AU curves for positive and negative mRNA-miRNA interactions
have the least amount of overlap compared to other features like $\delg$, $\deldelg$, etc.,
they are strong indicators of miRNA-mediated downregulation. In fact,
local AU composition curves are among the top 5 features for both seed and seedless sites.
% and again for the seed sites the seed curves being more important than the AU site curves.
The fact that the local AU content is one the most important predictors for miRNA target prediction
has also been confirmed by mirSVR \cite{betel2010comprehensive} (Supplementary Figure S1) and, 
to some extent, by the Random Forest Model, described in \cite{xu2014identifying}. Notably, our representation
of local AU content is able to extract significant signal from the feature, which is otherwise
missed by scalar representations of the feature. Further, we are also able to capture other spatial 
characteristics of the feature \eg  the slope of the curve.

Another interesting observation is that for seed sites, accessibility ($\deldelg$) of
the target site is a better indicator of miRNA-mediated downregulation of mRNA
% SC (4/10/15) CTM: Why do you say miRNA downregulation. 
% I have corrected a few times but just making sure this is miRNA-mediated downregulation or target downregulation
than the thermodynamic stability of the miRNA target duplex ($\delg$). This is evident 
from the number of $\deldelg$ coefficients showing up in the top 20 features for seed sites. 
On the other hand, for seedless sites, $\delg$ coefficients dominate in the top 20. 
In \cite{kertesz2007role}, the authors argue that accessibility along with
binding energy is a better indicator of miRNA targeting than binding energy alone
($\delg$), but from our results that appears to be the case more
often for seed sites. One possible explanation for this might be that the binding free energy ($\delg$)
is mostly similar for different classes of seed matches (6-mers, 7-mers, and 8-mers).
So, for seed sites, accessibility of the target region ($\deldelg$) becomes the major discriminator between positive
and negative miRNA-mRNA interactions. Whereas for (non-canonical) seedless sites,
where there may be little base pairing at the seed region, the binding free energy 
becomes the limiting factor.
%that is the major indicator for miRNA-mRNA interactions.

We notice that our metric, seed enrichment, is also an important indicator
of mRNA downregulation. In fact, seed enrichment is among the top three features 
for seedless sites. We are able to get more signal from the seed enrichment
feature for non-canonical sites than for canonical sites. This is because there are only
a few different types of seed-match patterns for canonical seed sites, all with 
high values of enrichment. On the other hand, for non-canonical sites, seed enrichment 
varies greatly between different types of non-canonical interaction patterns.
\Fref{fig:seed_alignment} shows the proportion of various types seed-match patterns, canonical and non-canonical, in
the positive dataset for human and mouse. We see that the occurrence frequency of various
patterns in the human and mouse data is highly correlated, as indicated by the correlation
coefficient of $0.923$. This indicates that the various patterns of canonical and non-canonical
seed matches are conserved across species, rather than occurring merely by chance. What is also surprising
is that among the top 10 most frequently occurring patterns, only two are canonical seed matches,
namely, a 6-mer and a 7-mer match. Other frequently occurring seed-match patterns have long bulges,
as indicated by a series of gaps (denoted by 3s).
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.45\textwidth]{human_mouse_alignment_fraction}
    \caption{Scatter plot of frequencies of various types of seed alignment patterns in set of
    positive mRNA-miRNA interactions for Mouse (x-axis) and Human (y-axis). Among the top 10
    most frequently occurring patterns, only two, namely, the 6-mer and 7-mer,
    are canonical seed match patterns. In the labels for the top 10 most
    frequently occurring patterns, 1 indicates a match, 2 indicates a mismatch,
    3 a GAP, and 4 indicates a GU wobble.
    \label{fig:seed_alignment}}
\end{figure}
 
Our model is also able to learn the relative efficacy of target sites
located in the three different genic regions, namely, 3' UTR, 5' UTR, and CDS. 
The negative weights learned for the 3 categorical features has the following 
interpretation: the majority of target sites located in the CDS region are labeled 0
(\ie not true miRNA binding sites), while fewer target sites in the 5' UTR region,
and even fewer target sites in the 3' UTR region, are labeled 0. This corroborates with the fact that miRNA
targeting has the most efficacy in the 3' UTR region, as has been well established 
in previous works \cite{bartel2004micrornas,baek2008impact,bartel2009micrornas}.
% SC (4/10/15): Can you add a bunch for references for the studies that show
% the most efficacy in the 3' UTR region. Also, I get the rationale but it is
% counterintuitive that the weights are negative for all the 3 categorical
% features, 3' UTR, 5' UTR, and CDS.  CTM - The categorical features have value
% 0 and 1 and are not scaled to have zero mean, 1 Std. Dev. 
% AG (04/11/15): Added references.
In fact, this is one of the reasons why a lot of previous works 
\cite{betel2010comprehensive, kertesz2007role}
only consider the 3' UTR region to locate miRNA binding sites.
It should be noted that \Tref{tab:data}
indicates that a large number of positive target sites, around 40\% 
for both human and mouse datasets, are present in the CDS region. However, our model aggressively
tries to label those target sites as negative sites --- note the large negative weight for the CDS regions in \fref{fig:plot_weights}. This  
hints at one of two possibilities. First, a lot of the target sites reported by the CLIP
methods in the CDS region may be due to transient protein-binding events
and, thus, the level of downregulation due to such binding sites may not be significant 
\cite{baek2008impact, hafner2010transcriptome, schnall2011unusually}. Alternately, the mechanism of miRNA action in the CDS region is different from that in other regions
and that other features (or methods) might be needed to explain miRNA targeting in the CDS region.

Finally, we draw attention to the weights learned for conservation, site length, and the 
relative position of a site within one of the 3 regions, namely, 3' UTR, 5' UTR, and CDS \Tref{tab:weights}.
It is evident that conservation plays a positive, albeit small, role in determining true
miRNA binding sites. Again, for canonical seed sites, conservation of the seed region (seed consv)
is more important than conservation of the overall mRNA target site (consv). 
We note that since we use conservation as a feature (one among many used in our SVM classifier),
as opposed to using it as a filter, like some methods have done in the past 
\cite{bandyopadhyay2009targetminer, krek2005combinatorial},
we are also able to predict target sites that are {\em not} conserved.
The length of the target site is also strongly anti-correlated with the probability of a site
being a true binding site which shows that shorter mRNA-miRNA alignments, \ie  mRNA-miRNA alignments with
fewer gaps or bulges, are preferred. Finally, a negative, although small, weight
for the relative site location feature, \ie  relative positioning in the 3 categorical regions,  means that target sites located in the 5' end of a region are slightly preferred over those at the 3' end of the region.

\begin{table*}[!htbp]
\caption{Relative importance of features for seedless and seed sites.\label{tab:weights}}
\centering
\begin{minipage}{0.45\textwidth}
\centering
%\resizebox{!}{1.8in}{%
\begin{tabular}{|c|c|c|}
\hline
\rowcolor{LYellow}
Rank & Feature & Weight \\
\hline
1 & CDS & -0.452 \\
\hline
2 & Site length & -0.404 \\
\hline
3 & Seed enrichment & 0.364 \\
\hline
4 & $AU_{site} \: [10]$ & 0.198 \\
\hline
5 & $AU_{seed} \: [10]$ & 0.198 \\
\hline
6 & $AU_{seed} \: [9]$ & 0.192 \\
\hline
7 & 5' UTR & -0.165 \\
\hline
8 & Consv & 0.149 \\
\hline
9 & $\Delta \Delta G_{site} \: [10]$ & -0.148 \\
\hline
10 & Relative site location & -0.141 \\
\hline
11 & $AU_{site} \: [9]$ & 0.134 \\
\hline
12 & $\Delta G_{site} \: [15]$ & 0.118 \\
\hline
13 & $\Delta G_{site} \: [19]$ & 0.116 \\
\hline
14 & $\Delta G_{site} \: [14]$ & 0.113 \\
\hline
15 & $\Delta G_{site} \: [17]$ & 0.111 \\
\hline
16 & $\Delta G_{site} \: [16]$ & 0.098 \\
\hline
17 & $\Delta \Delta G_{site} \: [9]$ & -0.093 \\
\hline
18 & $AU_{seed} \: [8]$ & 0.089 \\
\hline
19 & $AU_{seed} \: [11]$ & 0.088 \\
\hline
20 & $\Delta G_{site} \: [12]$ & 0.078 \\
\hline
\end{tabular}
%}
\end{minipage}%
\begin{minipage}{0.45\textwidth}
\centering
%\resizebox{!}{1.8in}{%
\begin{tabular}{|c|c|c|}
\hline
\rowcolor{LYellow}
Rank & Feature & Weight \\
\hline
1 & Site length & -0.591 \\
\hline
2 & CDS & -0.436 \\
\hline
3 & $AU_{seed} \: [9]$ & 0.287 \\
\hline
4 & $\Delta \Delta G_{site} \: [10]$ & -0.250 \\
\hline
5 & $AU_{seed} \: [10]$ & 0.235 \\
\hline
6 & Seed enrichment & 0.210 \\
\hline
7 & $AU_{site} \: [9]$ & 0.208 \\
\hline
8 & $\Delta G_{seed} \: [9]$ & -0.195 \\
\hline
9 & $\Delta \Delta G_{seed} \: [9]$ & -0.193 \\
\hline
10 & $\Delta \Delta G_{seed} \: [10]$ & -0.190 \\
\hline
11 & $\Delta \Delta G_{site} \: [9]$ & -0.187 \\
\hline
12 & $AU_{site} \: [10]$ & 0.187 \\
\hline
13 & $\Delta G_{seed} \: [10]$ & -0.179 \\
\hline
14 & 5' UTR & -0.178 \\
\hline
15 & Seed consv & 0.175 \\
\hline
16 & Relative site location & -0.154 \\
\hline
17 & $\Delta G_{site} \: [12]$ & 0.100 \\
\hline
18 & $\Delta G_{site} \: [14]$ & 0.097 \\
\hline
19 & 3' UTR & -0.089 \\
\hline
20 & $AU_{seed} \: [3]$ & 0.088 \\
\hline
\end{tabular}
%}
\end{minipage}
\\[0.1in]
\begin{flushleft}
The rank is computed by
sorting by absolute value of weight in descending order. For functional covariates the numbers
in square braces indicate the coefficient index for the B-spline basis functions.
\end{flushleft}
\end{table*}

%%%%%%%% Discussion %%%%%%%%%%%%%%
\subsection*{Discussion}

The number of basis functions ($K$), controlling the smoothness of the various curves,
used in our model is the only other tunable parameter, apart from
the regularization parameter ($\lambda$). We set a very low value for $\lambda$
because of our use of a simple linear model that avoids overfitting anyway.
We choose the value of $K$ using 10-fold crossvalidation to maximize the
difference between TPR and FPR (see \Fref{fig:plot_basis}).
So in effect by fitting a smooth curve through noisy observations, and using
the coefficients of the basis functions as features, we reduce the 
dimensionality of the feature space used in our method.
Currently we use a single number $K$ for all the curves,
which is a simplification done to reduce the parameter space that needs to be
searched during training. 
% SC (04/15/2015), CFC: There is a way to  
%use different smoothness levels for different curves while using the same
%number of basis functions. This can be achieved by extracting the most important functional
%principal components for each functional feature. Then we can use 
%the projection scores of the features along the principal component directions
%as features into the classifier. We explored that avenue but were not able to
%find functional principal component libraries that scaled well to large datasets.
% SC (4/10/2015) Clarified the above line and this could serve as follow-on work

Our choice of using a linear model for identifying functional miRNA targets was
influenced by two factors. First, using a linear model helps in interpreting
the effect that various features have on the model and provide useful insights
into the actual mechanism of miRNA action. So, we trade accuracy for model interpretability 
by using a linear model that resulted in our model having very low variance but high bias.
For example, the misclassification
rate of our model remained close to 30\% both during training and during intra-species and
inter-species validation. Another beneficial effect of using the simple linear model
is the scalability of \name. The use of large candidate sets of mRNA-miRNA interactions 
resulted in a huge amount of data, 10.4 GB for mouse, 
% SC (4/8/15): Also give the number for humans. 
% AG (4/10/15): Resolved. 
to train and validate our model on. With a reduced set of mRNAs for humans, we still have a large amount of data, around 3.7 GB. 
We empirically find that our model scales well with these large amounts of data.
% SC (4/8/15): Say something more about how you built a scalable model. 
% AG (4/9/15): Resolved. 
The scalability of our model arises from several factors. We use a linear SVM model with stochastic gradient descent (during the training phase) which is easily parallelizable, we use SPARK \cite{zaharia:2010} as a scalable ML infrastructure, the predictions using SVM are all parallelized, and the data generation is also parallelized.   
% We note that it is possible to improve the accuracy of our model by using a non-linear SVM
% kernel and we plan on implementing that in the next version of our method.

Finally, we found that the TPR-FPR performance of STarMir \cite{liu2013clip} is much worse than that
reported in their paper. We contacted the authors multiple times regarding the issue
but were unsuccessful in eliciting a response.

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.45\textwidth]{basis_plot_human}
    \caption{Number of basis functions ($K$) computed using Gaussian Process regression.
    \label{fig:plot_basis}}
\end{figure}

\paragraph{Threats to validity}: 
In our approach we have made the assumption that functional miRNA target sites are those 
that are present within the AGO-crosslinked regions, as identified by CLIP-seq.
In that respect, our method has the most agreement with CLIP-seq data
among other computational approaches. However, since the identity of the miRNA
present in the CLIP region is unknown, it may happen that the miRNA predicted by
our model is different from the miRNA that was actually involved in the binding event.
Also, given AGO-crosslinked regions for various mRNAs,
we attempt to extract finer-grained target sites within AGO-crosslinked regions that may be
targeted by a set of miRNAs. Toward that end, we only consider the most abundantly expressed
miRNAs in a cell-line --- top-10 miRNA families for human and top-20 families for mouse.
This choice is as per the prior work \cite{chi2009argonaute} for mouse and \cite{kishore2011quantitative} for humans,
which state that those families accounted for most (95\% for human) of the miRNA sequece reads. 
% SC (4/8/15): Say we take the top-k, give value of k.
% AG (04/11/15): Done.
Thus, our method misses out on functional miRNA target sites that
may be outside AGO-crosslinked regions or fails to identify mRNA sites that are targeted 
by the miRNAs whose expression levels are low.

