\section*{Background}
MicroRNAs (miRNAs) are short 20-24 nucleotide (nt), endogenous RNAs that
modulate gene regulatory pathways \cite{jiang2009mir2disease,
bartel2004micrornas} and form the most widely studied class of non-coding RNAs
(ncRNAs).
%(CFC) CFC = candidate for chop; The first miRNA, lin-4, obtained from {\em C. elegans}, was found to  be an endogenous
% regulator of genes controlling developmental timing, acting via extensive,
% albeit imperfect, complementarity to its target gene’s (lin-41’s) 3’
% untranslated region (UTR) \cite{vella2004c, bagga2005regulation}.  
miRNAs mediate RNA interference (RNAi) by targeting the 3’ UTR of the mRNA, or in some
cases, other mRNA regions, such as the mRNA’s coding sequence (CDS) or its 5’ UTR
\cite{carthew2009origins}.  Following their biogenesis, miRNAs complex with
Argonaute (AGO) proteins, which are the catalytic  components of the RNA-induced
silencing complex (RISC) \cite{bartel2009micrornas}. This miRNA-RISC  complex
then  targets  its  cognate  mRNA  fragment.  These  interactions  result  in
mRNA repression,  destabilization,  or,  in  more  complex  ways,  contour  the
gene  expression  landscape  \cite{mukherji2011micrornas, hausser2014identification}.
There  are  over  two
thousand  miRNAs  that  have been  annotated  in humans
\cite{friedlander2014evidence}, displaying many-to-many associations with mRNA
targets.  Such associations are  speculated to  be  controlling  a  vast
majority  of  mammalian  genes \cite{friedman2009most},  involving  all
cellular  pathways  from development  to  pluripotency  and  oncogenesis
\cite{abbott2005let, li2009microrna, shao2010comprehensive, li2013comprehensive,
heinrich2012micrornas, poliseno2006micrornas}.
% SB (4/12/15): Break this list of references into smaller lists indicating specific aspects, such as, pluripotency. 
%(CFC)  potentially  in  the  form  of miRNA-mRNA networks \cite{liu2010microrna}, and often reciprocal in nature,
% where targets can in turn control the level and function of miRNAs
% \cite{pasquinelli2012micrornas}. 

Notwithstanding the biological importance of miRNAs, determining their targets
with high accuracy and exhaustively has remained elusive, with {\em in-silico}
predictions plagued by high false-positive and false-negative rates
\cite{ritchie2009predicting}.  This is due in many ways to the small size 
of miRNAs, which requires as few as 6 base pairs of complementarity for
functional miRNA targeting, as well as the diverse miRNA targetome \cite{clark2014argonaute}.  
As a machine learning task, the problem of miRNA target
prediction is that of link prediction in a bipartite graph, where vertices in
one set represent all possible target regions across all mRNAs while vertices
in the other set represent miRNAs. We can either predict if an edge exists (1/0)
between a pair of vertices representing an mRNA region and a miRNA
(classification), or we can predict the strength of the association
i.e., edge weights (regression).  In this paper we focus on the classification
problem of whether a miRNA targets an mRNA region. 
% Toward that end we use data 
% from CLIP experiments to train and evaluate the performance of our model. Additionally, we also
% generate probability scores for our prediction.

CLIP-seq, crosslinking via immunoprecipitation
followed by high-throughput sequencing, an elegant albeit lengthy biochemical
procedure, is a state-of-the-art-player in developing genome-scale regulatory
insights \cite{licatalosi2008hits, hafner2010transcriptome, konig2010iclip}.  
%(CFC) The technology allows a high-resolution investigation of the
% endogenous binding sites for RNA-binding proteins (RBP), examining the occupancy
% of the RISC-miRNA protein complexes on their cognate mRNA \cite{licatalosi2008hits, hafner2010transcriptome, konig2010iclip}.  
% (CFC) Three variants of CLIP-seq have been developed --- high-throughput sequencing CLIP (HITS-CLIP)
% \cite{licatalosi2008hits}; photoactivatable-ribonucleoside-enhanced CLIP
% (PAR-CLIP) \cite{hafner2010transcriptome}; and individual nucleotide resolution
% CLIP (iCLIP) \cite{konig2010iclip}; of which HITS-CLIP and PAR-CLIP are most
% commonly used.  
The technology allows target mRNAs to be identified within a
small window of resolution, beyond which, statistical models are needed to
exactly localize the MRE, that is, the miRNA recognition element or the binding
site.  This is even true for recent CLIP-seq variants \cite{konig2010iclip}, in order to account for background noise and
sequencing artifacts \cite{sugimoto2012analysis, friedersdorf2014advancing}.
Further, CLIP-seq has the advantage of profiling the native miRNA levels, as
opposed to supra-physiological levels obtained via miRNA transfection
experiments \cite{thomson2011experimental}, the latter being better suited for
developing small-interfering RNA (siRNA)-based therapeutics
\cite{gumienny2015accurate, kanasty2013delivery}.

While CLIP-seq can identify miRNAs and targets that form a part of the RISC
complex, it cannot decipher {\em which} miRNA form heteroduplex with {\em which} targets;
CLASH being an initial attempt in experimentally solving this problem
\cite{helwak2013mapping}.  Several computational methods have been developed to
decipher the specifics of miRNA-mRNA interactions, captured by CLIP-seq
\cite{betel2010comprehensive, liu2013clip, khorshid2013biophysical,
chou2013computational, corcoran2011paralyzer, majoros2013microrna}.  These
methods have contributed to understanding the diverse miRNA targetome,
biochemically captured by the CLIP technology.  The evolving knowledgebase of
this targetome has further supported the paradigm switch, wherein it is now
widely appreciated that the perfect complementarity between the miRNA seed and
mRNA 3’ UTR is neither necessary nor sufficient for miRNA regulation.

In this paper, we sought to leverage this ability of the CLIP-seq technology to
capture endogenous MREs to develop a unified method to understand the signatures
of miRNA-mRNA heteroduplexes --- whether standard, canonical seed matches, or
non-standard, non-canonical seedless matches \footnote{Through this paper, we will synonymously use the terms ``non-canonical match'', ``seedless match'', and ``non-canonical seedless match''.}.  
Specifically, in our system,
which we call \name, we use smooth B-spline, thermodynamic curves and sequence curves for adenosine-uracil (AU) content, in order to
extract enriched interaction features from the experimentally CLIPed (i.e.,
immunoprecipitated) regions\footnote{\name means ``discovery'' in Sanskrit.  The
word captures our enthusiasm in using functional data analysis techniques to
extend and refine the discovery of genomic targets modulated by these small,
albeit powerful regulatory RNA --- miRNA, which can chisel the process of gene
regulation, post-transcriptionally.  This in turn will accelerate the discovery
of novel disease biomarkers \cite{creemers2012circulating, jeffrey2008cancer}
that can cause network perturbations, \textit{in vivo}
\cite{barabasi2011network}, and facilitate the development of novel miRNA-based
therapeutics \cite{van2012developing}.}.  We then use a support vector machine (SVM)-based
machine learning (ML) system to learn the diverse signatures of this CLIPed,
miRNA targetome. Our improved performance (in terms of true positive and false positive rates) over all prior work arises from a combination of multiple factors, with the total benefit being greater than the sum of the constituents. These contributory factors are the use of an extensive set of features, converting noisy data points into smooth curves, converting the categorical feature of seed or seedless match into a numerical feature and treating both under one unified umbrella, and a careful consideration of the spatial nature of the miRNA-mRNA binding process into our classification scheme.


%(SC 04/14/2015)  CFC: So, how do miRNAs identify their targets?  
Early studies of target recognition
revealed near-perfect (contiguous) and conserved, Watson-Crick
complementarity at the 5’ miRNA end, which was called the “seed region”.  The seed is a 6–8 nt
substring within the first 8 nucleotides, starting from the 5’ miRNA end.
Typically, positions 2-7 from the 5’ end are considered to be the primary
(canonical) determinant of target specificity
\cite{wightman1993posttranscriptional, box2002micro, lewis2003prediction,
lewis2005conserved}.  However, given the large number of random occurrences of
any given hexamer in 3’ UTRs, a canonical “seed” match by itself is a poor
predictor for miRNA regulation \cite{moore2014mapping}.  
%(CFC) Specifically, the presence of a random hexamer
% in mRNA sequences makes the number of potential miRNA target sites in the
% transcriptome roughly 1 in 4000 nt for a 6 nt seed, far outnumbering the number
% of functional miRNA target sites \cite{moore2014mapping}.  
To complicate matters, non-canonical interactions involving “seedless sites”, where the
interactions are not nucleated by perfectly complementary miRNA seed regions and
yet effectively downregulate gene expression, have been described
\cite{vella2004c, lal2009mir, didiano2006perfect, shin2010expanding,
vo2010affinity, lu2010function, loeb2012transcriptome}.  Popular sequence
alignment heuristics such as BLAST (blastn) preclude the alignment of short
sequences with specific bulges or mismatch configurations
\cite{birmingham20063}.  So, given the earlier perceived rarity of non-canonical
matches, computational methods
for miRNA target prediction have traditionally focused on canonical (seed-based)
matches. Along the same lines, interactions with the 3’ UTR mRNA target region
have been primarily modeled, as opposed to the 5’ UTR or CDS, or non-coding mRNA
regions. In our work, we remove these two restrictions and find seedless matches
(in addition to the seed matches) throughout the gene regions. 

% (RS) -- Should this be in the related work section rather than the intro? %(SC) there is no separate related work in GB
\subsection*{Related Work} Among non-canonical prediction methods,  mirSVR
\cite{betel2010comprehensive} allows for a single GU wobble or a mismatch in
the 6-mer seed region.  For encoding the seed match pattern, mirSVR uses an
8-bit long vector, with “1” representing a match and “0” representing a
mismatch and then uses the bit-vector as a feature in their Support Vector
Regression (SVR) model.  Recent methods have expanded the target search to
other genic regions, such as the 5’ UTR and coding sequence (CDS) \cite{
liu2013clip, xu2014identifying}.  In this bracket, Liu \textit{et al.} generate
predictions for sites involving non-canonical seedless matches (seedless
sites), but they do not take into consideration the type of non-canonicality
for the examined seedless sites.  Instead, they use thermodynamic and mRNA
sequence features (e.g., local AU content) to generate predictions for the
seedless sites.  In doing so, they miss out on potential signal from the
non-canonical seedless match patterns that our findings indicate as enriched in
the identified functional miRNA-mRNA interactions.  One possible reason for
this, as pointed out by Xu \textit{et al.} \cite{xu2014characterization}, is
the difficulty in incorporating the large numbers of possible patterns of
insertions and deletions in the mRNA seed-matched region for differential
non-canonical seedless match patterns.  Computational methods have also
exclusively relied on thermodynamic features, such as the stability of the
miRNA-mRNA heteroduplex and the accessibility of the mRNA target region to
identify functional miRNA binding sites.  For example, Xu \textit{et al.}
\cite{xu2014characterization} only use binding energy and accessibility to
predict functional miRNA target sites.  

Another method, MIRZA \cite{khorshid2013biophysical} develops a rigorous
biophysical model via parameterizing the alignment between an mRNA segment and
a miRNA, interpreted as the binding energy between the two, and optimized using
CLIP data.  While MIRZA uses a novel model to incorporate canonical and
non-canonical matches in a unified manner, it does not take into account
secondary mRNA structures (the spatial configuration) in developing their
energy model --- mRNA secondary structures 
can potentially limit the target site accessibility to the docking
miRNA-RISC complex and therefore plays an important role in miRNA target
recognition \cite{kertesz2007role}.
Further, these approaches compute various thermodynamic
scores only at the target site region to summarize the thermodynamics of the
miRNA-mRNA interaction.  For example, Xu \textit{et al.}
\cite{xu2014characterization} observe that a certain normalized measure of site
accessibility, has a characteristic pattern around the target site region. Yet,
they do not exploit this observation in their model.  In contrast, while Liu
\textit{et al.} \cite{ liu2013clip} compute site accessibility in the target
region's vicinity in discrete chunks of 5, 10, 15, 20, 25, and 30 nt around the
target site region, they report only the accessibility computed at the target
site region as an important predictor of functional miRNA targets, failing to
capture the mRNA secondary structures around the target site defining its
accessibility.  Identifying this, prompted us to characterize binding energy
and accessibility as curves to model the spatial profile of the miRNA-mRNA
interaction.
%(CFC) not needed in this paper, although is useful to take into consideration for our next paper: Methods like microMUMMIE \cite{majoros2013microrna} combine deep-sequencing
% data, like T-to-C transition signal, with miRNA seed pairing and evolutionary
% conservation in a Hidden Markov Model (HMM) to unambiguously identify the
% identity of the miRNA involved in the RNA binding events.  But, in
% \cite{majoros2013microrna} the authors claim that imperfect seed matches
% accounted for only a small fraction of all targets.

Given the newly-discovered CLIP-based miRNA (expanded) targetome
and the limitations of the afore-mentioned computational approaches to make sense of this diverse targetome,
in this paper, we solve the classification problem of whether a miRNA targets an
mRNA region using our SVM-based system, \name. Our main contributions can be summarized as follows:

\begin{enumerate}

    \item We develop an efficient SVM classifier to identify the positive miRNA-mRNA
    interactions. Our classifier produces significantly better ROC curves than all
    prior work \cite{betel2010comprehensive, kertesz2007role, grimson2007microrna,
    liu2013clip, khorshid2013biophysical}
    when evaluated on CLIP-seq data, while also providing insights
    on which features are discriminating, and in which direction, that is, positive
    or negative interactions. Our Area-Under-the-Curve (AUC) values for the ROC curves for both human and mouse datasets are greater than that of all prior works, quantitatively 19.7\% and 22.0\% better for human (seed and seedless respectively) and 15.0\% and 22.8\% better for mouse (seed and seedless respectively).
% Specifically, we achieve a true positive
%    rate of around 70\% for both seed and seedless sites in humans at a false
%    positive rate of around 40\%, which is an improvement of around 20\% in
%    terms of area under the ROC curve when compared with mirSVR
%    \cite{betel2010comprehensive} and MIRZA \cite{khorshid2013biophysical}. We also find
%    similar improvements for mouse. 
    The classification performance of our model in
    inter-species validations while being slightly worse compared to intra-species
    validations, is still able to beat all prior methods.
    Our candidate dataset of miRNA-mRNA
    interactions is the largest among other computational approaches, which we achieve by employing
    the least strict filtering criteria on the original CLIP data. Finally, our method
    is able to predict significantly more non-canonical sites that are present within
    CLIP regions than prior computational approaches. 

    \item We characterize thermodynamic and sequence
    scores as ``curves" 
% (used as functional covariates in our SVM model) 
    and demonstrate how the shape of the curves discriminates between positive and
    negative miRNA-mRNA interactions. We compute curves at two levels of granularity
    for each of the thermodynamic and sequence features -  curves centered at the
    target site (we refer to them as “site curves”) and curves computed
    at a finer granularity and centered at the mRNA seed-matched region (called the ``seed curves’’). We demonstrate that a sum
    of 20 basis-splines (B-splines), each of degree 3, gives us satisfactory
    curve-fitting. Our use of B-splines enables us to fit relatively smooth curves over
    high dimensional, noisy data --- the scalar data points for thermodynamic and sequence scores. 

    \item We develop and incorporate in our model a novel metric
    called \textit{seed enrichment} that captures all patterns of seed matches,
    including multiple mismatches, GU wobbles (sequence-based imperfections), and
    long bulges (architectural imperfections) in forming the miRNA-mRNA heteroduplex.
    By doing so, we are able to adopt a unified approach toward modeling canonical
    and non-canonical heteroduplexes. This creates a numerical feature that makes it
    easier for our ML classifier (and other ML-based approaches) to use this feature
    for classification. We also demonstrate that a whole gamut of non-canonical seed
    matches, involving bulges on the mRNA, are enriched in the set of positive
    miRNA-mRNA interactions, in both humans and mice. In fact, the proportion of
    non-canonical matches is higher than that of canonical matches. This category of matches had been
    missed in much of prior work, \eg  \cite{kertesz2007role, grimson2007microrna}.
    It is now clear, through our current work plus other recent work \cite{liu2013clip, khorshid2013biophysical},
    that non-canonical matches should not be ignored in the study of miRNA-mRNA interactions. 
    However, this has to be balanced with the fact that the level of downregulation
    of gene expression is higher for seed matches \cite{khorshid2013biophysical} (see Figure 2). 

%    \item The classification
%    performance of our method is superior to prior techniques, both for canonical
%    and non-canonical seed-match sites, compared to all prior
%    state-of-the-art protocols. 

\end{enumerate}
