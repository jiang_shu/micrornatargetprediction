%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                          %%
%% The Abstract begins here                 %%
%%                                          %%
%% Please refer to the Instructions for     %%
%% authors on http://www.biomedcentral.com  %%
%% and include the section headings         %%
%% accordingly for your article type.       %%
%%                                          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{abstractbox}

\begin{abstract} % abstract

\parttitle{Background} MicroRNAs (miRNAs) are small regulatory RNA that mediate
RNA interference by binding to various mRNA target regions. There have been
several computational methods for target prediction of miRNAs to mRNAs.
However, these have considered all contributory features as scalar
representations, primarily, as thermodynamic or sequence-based features.
Further, a majority of these target canonical sites, where there are seed
matches exhibiting continuous base-pairing of nucleotides, spanning 6
nucleotides. Here, we present a machine-learning classification scheme, titled
\name, which captures the spatial profile of miRNA-mRNA interaction via smooth B-spline curves,
separately for various input features, such as thermodynamic and sequence features. Further, we use a principled approach to
uniformly model canonical {\em and} non-canonical seed matches, using a novel
seed enrichment metric. 

\parttitle{Results} We demonstrate that large number of seed-match patterns
have high enrichment values, conserved across species, and that majority of
miRNA binding sites involve non-canonical matches, corroborating recent
findings. Using spatial curves and popular categorical features, such as target
site length and location, we train a linear SVM model, utilizing experimental
CLIP data. Our model significantly outperforms all established methods, for
both canonical and non-canonical sites. We achieve this while using a much
larger candidate miRNA-mRNA interaction set than prior work.

\parttitle{Conclusions} We developed an efficient SVM-based model for miRNA
target prediction using recent CLIP data, demonstrating superior performance,
evaluated using ROC curves, specifically about 20\% better than the
state-of-the-art, for different species (human or mouse), or different target
types (canonical or non-canonical). 

% SB-written abstract
\begin{comment}
There have been several computational methods to determine the target binding sites of microRNAs (miRNAs) to messenger RNAs (mRNAs). However, these have largely considered all features as scalar features, such as, thermodynamic features. Further, they have mostly been targeted to finding the sites where there are canonical seed matches (continuous base pairing of nucleotide positions 2-7 from the 5' end of the miRNA). In this paper, we present a machine learning classification scheme, titled \name, that incorporates the spatial nature of miRNA-mRNA interaction by using curves (as opposed to scalar values) for various thermodynamic and sequence features to describe mRNA-miRNA interactions. We also use a principled, albeit simple, approach to uniformly model canonical and non-canonical seed matches, using a metric called {\em seed enrichment}. We show that large number of seed match patterns have high enrichment values and the majority of miRNA binding sites involve non-canonical seed matches. Using those features we train a linear SVM model, utilizing CLIP data, that significantly outperforms several established methods and some new methods for miRNA target prediction.
\end{comment}

% Original abstract
\begin{comment}
\parttitle{Motivation} 
Existing computational methods for microRNA target prediction use
various thermodynamic features and seed match rules to predict
miRNA binding sites in mRNAs. But, summarizing different thermodynamic
and sequence features at the target site (and additionally in the neighborhood
of the target sites) in a single scalar value misses the spatial nature
of miRNA-mRNA interaction. Also, traditionally methods have only considered
canonical seed matches (continuous base pairing of nucleotide positions 2-7 from 5' end of the miRNA)
and only a few types of non-canonical seed matches, but recent work show
that the overwhelming majority of miRNA-mRNA interactions involve non-canonical seed matches.


\parttitle{Results} 
In this paper we present a new machine learning method,
\name, that incorporates the spatial nature of miRNA-mRNA interaction
by using curves (as opposed to scalar values) for various thermodynamic and sequence features
to describe mRNA-miRNA interactions. 
We are able to demonstrate that such curves capture
richer information about mRNA-miRNA interactions and are able to better
discriminate the signatures of true miRNA binding sites. We also use a principled,
albeit simple, approach to uniformly model canonical and non-canonical seed matches, using
a metric called seed enrichment. We show that large number of seed match patterns
have high enrichment values and the majority of miRNA binding sites involve non-canonical seed matches.
Using those features we train a linear SVM model
, utilizing CLIP data, that significantly outperforms several established methods and some new methods
for miRNA target prediction.

\parttitle{Availability} 
All source code and data would be made publicly available
at https://github.com/asishgeek/Avishkar.
\end{comment}

\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                          %%
%% The keywords begin here                  %%
%%                                          %%
%% Put each keyword in separate \kwd{}.     %%
%%                                          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\begin{keyword}
%\kwd{sample}
%\kwd{article}
%\kwd{author}
%\end{keyword}

% MSC classifications codes, if any
%\begin{keyword}[class=AMS]
%\kwd[Primary ]{}
%\kwd{}
%\kwd[; secondary ]{}
%\end{keyword}

\end{abstractbox}

