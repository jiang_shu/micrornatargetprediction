\section{Methods}
\subsection{Data} 
We downloaded CLIP data for Humans published by \cite{kishore2011quantitative} from
Gene Expression Omnibus (series GSE28865). To be specific, we used the three
datasets having codes GSM714642, GSM714644 and GSM714646 which correspond to
samples from replicate A experiments involving AGO2 protein. The datasets identify
40 nucleotide long AGO binding sites for each mRNA. The combined data from all three
datasets contained 190,764 AGO binding sites across 10,159 different mRNAs. 
Following the same approach as \cite{kishore2011quantitative} we used
the 10 most abundantly expressed miRNA families, comprising of 44 different miRNAs, 
in Human HEK 293 cells to identify mRNA-miRNA binding sites. Since the total
number of potential binding sites of 44 miRNAs across 10159 mRNAs is enormous
and then generating features for all those binding sites is computationally expensive,
we randomly selected around 1,200 different mRNAs to train and evaluate our model.
% SC (3/28): What do you mean by initial analysis? Isn't this all that we use?
% AG (3/28): Well, I thought of mentioning that we intend on generating predictions
% for the entire data in due course of time but for the sake of this paper we have only
% considered 1,200 mRNAs.

For mouse, we downloaded HITS-CLIP data \citep{chi2009argonaute} from 
starBase database \citep{yang2011starbase, li2013starbase}. The data contained
locations of 11117 AGO-CLIP tags (actually CLIP tag clusters) in the mouse
genome (mm9 assembly) in a BED file. We mapped chromosome locations to mRNA locations
by first extracting nucleotide sequence for the corresponding chromosome locations
from the UCSC DAS server using the Rest API.
The UCSC DAS server provides access to genome annotation data for all current assemblies featured in the Genome Browser.
Then we use NCBI Blast to map those
sequences to mRNA names (and locations) using the RefSeq RNA database.
As in \cite{chi2009argonaute} we used 20 most abundant miRNA families
containing 119 miRNAs to identify mRNA-miRNA binding sites. We obtained
the miRNA names and sequences from the supplementary data provided by the authors \citep{chi2009argonaute}.
We then the generated the candidate set of mRNA-miRNA interactions using Algorithm \ref{algo:candidate_set}.
Following which we labeled each target location in the candidate set as 1 or 0 
depending on whether the candidate location for a mRNA was contained within 
a AGO-crosslinked region. The resulting data is summarized in \tref{tab:data}.

To incorporate evolutionary conservation of genome regions in Humans and Mouse
we downloaded PhastCons \citep{siepel2005phylogenetic, siepel2005evolutionarily}
conservations scores from UCSC Genome browser. For Mouse we used the conservation
scores obtained by alignment of 30 vertebrate genomes to the mouse genome (mm9 assembly).
Similarly,  Human conservation scores were obtained by 
alignment of 44 vertebrate genomes to the human genome (hg18 assembly).

To compute locations of various mRNA regions like 3' UTR, 5' UTR and CDS
we downloaded annotations for hg18 and mm9 assemblies from the UCSC
Table browser (RefSeq Genes track). Mature miRNA sequences were downloaded
from miRBase \citep{kozomara2013mirbase} website (http://www.mirbase.org/ftp.shtml).

\subsection{Thermodynamic features} 
Thermodynamic stability of the miRNA-mRNA
target duplex have been long identified as being an important predictor of true
binding sites of an miRNA \citep{kertesz2007role, bartel2009micrornas}. 
Thermodynamic stability of the miRNA-mRNA duplex is given by the free energy 
gained by binding of miRNA to the target site and is denoted by $\delg$.
Thermodynamic accessibility has also been argued to be an important predictor
of miRNA repression \citep{kertesz2007role}. Accessibility is defined as 
the ``difference between the free energy gained by the binding of 
the miRNA to the mRNA ($\delg$) and the free energy lost
by unpairing the target site nucleotides, $\Delta G_{open}$'' \citep{kertesz2007role}.
The target site nucleotides need to be unpaired to make the site accessible to
the RISC complex housing the miRNA, so $\deldelg$ measures the effective accessibility 
of a region.
We consider both the features in our model, but previous
works that consider these features
\citep{betel2010comprehensive, xu2014identifying},
compute the $\delg$ and $\deldelg$
values at the target site and regions flanking the target site, and use
the features as scalar covariates into a classification or regression model.
But, encapsulating the thermodynamic interaction in a single number
oversimplifies the spatial nature of mRNA-miRNA interaction. 
\cite{liu2013clip} use multiple numbers but their methodology has a problem as we discuss later.
% SC (3/28): Liu et al (2013) does not use a single number though. 
% AG (3/28): Yes, but I clarify later about how they use the different 
% numbers, as discrete features and that their features are significantly 
% different from our features becuase they don't move the window, they expand the window.
% Do I need to clarify more?
For example, as
shown in the illustration in \fref{fig:illustration}, the target site
might be surrounded by tight secondary structures, that make it
difficult for the miRNA housed within the RISC complex to interact with the
target site. So we had the idea that by characterizing the thermodynamic
interactions as curves, and taking into account the shape of the curves
in our model would improve the predictive power of our model.
So, we take a different approach to characterizing the thermodynamic stability of
miRNA-mRNA duplex and the accessibility of the target site. 
We consider the thermodynamic profile of mRNA-miRNA interaction
by taking into account $\delg$ and $\deldelg$ values at
the target site and use 13 consecutive windows both
upstream and downstream of the site region, of size 46 nucleotides each.
But rather than treating them as
separate features to be fed into a classifier, which discards the spatial nature of the phenomenon,
we fit a smooth curve through the noisy
observations to define what we call the {\bf ``thermodynamic curves''}.
The smoothed thermodynamic curves are used as
functional covariates in our model, where the word functional is used in the
statistical sense meaning that the features are infinite-dimensional functions
as opposed to being finite dimensional vectors.
The window length was chosen to be 46 nucleotides because
the AGO footprint on the mRNA spans around 46 nucleotides \citep{chi2009argonaute}.
The various curves used in our model that are centered at the target site
and computed at a resolution of 46 nucleotides are collectively referred
to as ``site curves'' in the paper.
Further, since the dimensionality of the curve is not known a priori,
we use basis functions to achieve a ``good enough'' fitting curve.
We experiment with different numbers of basis functions and settle on
the optimal number, 20, through our training phase.
Our results \fref{fig:curves} show that
computing thermodynamic profiles of mRNA and miRNA interactions
in terms of these curves, captures richer information than computing 
binding and accessibility energy at the target site alone - we are able to
discriminate the signatures of the binding sites better due to the use of curves.
Further, we are able to extract the relative importance of the curves
for the various thermodynamic features (as well as local AU content feature)
through our feature analysis phase.
 
We also compute the thermodynamic curves at a finer resolution, collectively 
referred to as ``seed curves'' in the paper, by considering a
window of size 9. The rationale behind this is that pairing of nucleotides
1-8 (from the 5' end) of the miRNA has been deemed to be much more functional than pairing at
other nucleotide regions. Thus we compute the binding ($\delg$) and
accessibility ($\deldelg$) curves centered at the mRNA seed matched region
i.e. nucleotides of the mRNA that are paired with the seed region of the miRNA. 
% SC (3/28): Then why do we consider the flanking regions as well - I presume here also we are taking 13 windows on each side.
% AG (3/29): Yes, otherwise how would be get seed curves?
For target sites where there is no seed match (i.e. a 6, 7 or 8-mer),
we pick the region within the target site that has the most favorable hybridization with
nucleotides 1-8 of the miRNA as the seed matched region.
It should be noted that the thermodynamic curves considered in this paper is fundamentally
different from those computed by \cite{liu2013clip} where they compute thermodynamic values
by keeping the window centered at the target site region but increase the window length
on either site of the target site region in increments of 5 nucleotides. In \cite{liu2013clip}
the different values computed for $\delg$ and $\deldelg$ are not considered as curves, but
rather as separate features into a classifier. The authors
do not provide any interpretation of the nature of information that such features capture
nor are they able to demonstrate the usefulness of those features; the only relevant
features as reported by the authors in their website http://sfold.wadsworth.org/starmirDB.php
appear to be thermodynamic features computed at the target site alone.
We note that we came up with the idea of using thermodynamic curves as features for a classifier
after the authors in \citep{xu2014characterization} showed that a normalized metric of
accessibility had a characteristic v-shape centered at the target region. 

\subsection{Seed match enrichment}
\cite{bartel2009micrornas} defined a hierarchy
of 6 different types of miRNA seeds that roughly correspond with the miRNA's
efficacy in downregulating mRNA targets. So a lot of computational approaches
for miRNA target prediction use the seed type as a categorical feature in their
model. Methods that take into account non-canonical seed matches,
e.g. \cite{betel2010comprehensive, xu2014identifying}, take a
non-principled approach by considering only one GU wobble or one mismatch
in the seed region \footnote{By non-canonical seed match we mean seed matches that do not
have, at least, a perfect 6-mer match within nucleotides 1 to 8 from the 5' end of the miRNA.
It must be noted that our definition of a seed match is slightly different from what others have
used in the past. We used a slightly more general definition of a canonical seed match
to account for different types of canonical seed matches that are considered by various computational methods.
For example \cite{bartel2009micrornas} define three types of alignments 
involving perfect complementarity with nucleotides 2-7 from the  5' end
of the miRNA as canonical seed match. While, our method makes no distiction between
a canonical and non-canonical seed match by using a feature called seed enrichment,
we train our model, and present results separately, for target sites involving canonical and non-canonical seed matches
(also referred to as seedless sites in this paper) to gain more insight into the process of miRNA repression
and also to compare the performance of our algorithm against other methods that generate predictions exclusively for
sites involving canonical seed matches.}.
In \cite{xu2014identifying} the authors state that
due to the difficulty of incorporating various patterns of insertions
and deletions that may occur in the seed match region, they only consider
one type of non-canonical seed match by allowing a single GU wobble.
Indeed, a model that enumerates all possible patterns of seed matches
and tries to learn the importance of each type of pattern in miRNA downregulation
would perform poorly because of the sheer number of possible patterns.
We circumvent that problem by representing the alignment of a miRNA with a mRNA as a vector
where each element takes 4 possible values corresponding to a match, mismatch,
gap and GU wobble respectively. We come up with a metric called {\bf ``seed enrichment''}
that captures, in a single numeric feature, the relative efficacy of various kinds of seed matches.
We observed that a vast number of seed matches, having long bulges (gaps) were enriched,
providing further justification for our consideration of non-canonical seed matches. 
This observation is also corroborated by \cite{helwak2013mapping}.

\paragraph{Enrichment score for each seed match.} We pre-compute
the number of occurrences of various seed match patterns in the positive mRNA-miRNA interaction dataset
and the corresponding seed enrichment score for each pattern as follows.
Let us consider the likelihood that a particular pattern of seed match, $\mbf{a}$,
is positively correlated with miRNA repression. 
To do this, we calculate the following probability for a given seed match pattern, $\mbf{a}$,
which has say $k$ occurrences among $n$ positive samples.
Let $\alpha$ be the probability that there
are $k$ occurrences of pattern $\mbf{a}$ among $n$ samples purely by chance.
As an example, for a region of length $|\mbf{a}|$ the expected number of pattern matches $\mbf{a}$ in $n$ samples,
purely by chance will be $0.25^{|\mbf{a}|} n$. Then, $\alpha$ is given as:
\begin{align}
\alpha = Binomial(k | n, 0.25^{|\mbf{a}|})
\label{eq:enrichment-score}
\end{align}
We call $1 - \alpha$ as our enrichment score. 

The advantage of our method is three fold. One, we are able to consider a lot of different types
of seed matches (both canonical and non-canonical) that are enriched in the set of positive mRNA-miRNA interactions
in a unified and principled manner. Second, since the overwhelming majority
of positive mRNA-miRNA interactions involve non-canonical seed matches,
we are able to generate high quality predictions for a lot of target sites that are missed by other methods. 
Finally, since machine learning methods typically handle numerical features better
than categorical features, especially those with high cardinality, our process of creating a numeric (probability) value allows us to get high accuracy
on predictions for non-canonical sites.

\Fref{fig:seed_alignment} shows the proportion of various types seed match patterns in
the positive dataset for human and mouse. We see that the occurrence frequency of various
patterns in the human and mouse data is highly correlated, as indicated by the correlation
coefficient of $0.923$, thereby indicating the various patterns of canonical and non-canonical
seed matches cut across species rather than occurring merely by chance. What is also surprising
is that among the top 10 most frequently occurring patterns only two are canonical seed matches,
viz. a 6-mer and a 7-mer match. Other frequently occurring seed match patterns have long bulges
as indicated by a series of gaps (3's).

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=\linewidth]{human_mouse_alignment_fraction}
    \caption{Scatter plot of frequencies of various types of seed alignment patterns in set of
    positive mRNA-miRNA interactions for Mouse (x-axis) and Human (y-axis). Among the top 10
    most frequently occurring patterns only two viz. the 6-mer and 7-mer
    are canonical seed match patterns.
    \label{fig:seed_alignment}}
\end{figure}

\subsection{Sequence features} 
To incorporate sequence features in our model
we consider the functional version of another popular feature: local AU composition
which is defined as the fraction of Adenine nucleotides (A) and Uracil  nucleotides (U)
in a block of mRNA. \citep{grimson2007microrna} showed that local AU content is
weakly correlated with reduced mRNA expression levels but by considering AU curves,
we are able to extract significant signal from this feature. In fact we show
later (\tref{tab:weights}) that local AU composition is a top feature in our model.
Again, like thermodynamic features, we compute the local AU content at two resolutions
site (window length 46) and seed (window length 9).

\subsection{Conservation} 
Evolutionary conservation has been used in the past
to reduce the false positive rate of computational mRNA target prediction
methods.  So we incorporate conservation scores of the overall target mRNA site, 
the seed match site and the off-seed site (nucleotide other than those that are aligned with
the seed region of the miRNA) as additional features into our model. The latter
two features are used only for sites containing canonical seed matches i.e.
either a perfect 6-mer, 7-mer or a 8-mer site.
% SC (3/28): The use of the conservation score as a feature will reduce the chance of us picking up the non-conserved sites?
% AG (3/29): We use the conservation score as a feature. For canonnical seed match site, we further use a fine grained
% version of the conservation score by computing the conservation score at the seed match site and the off-seed site.
% AG (04/02): Resolved over email.

\subsection{Miscellaneous features} 
Other features used in our model are (a)
the region in which the mRNA target site is present \emph{viz.} 3'UTR, 5'UTR and CDS,
(b) relative location of the target site within the region aforementioned, on a scale of 0 to 1
, where 0 indicates the 5' end of the region and 1 indicates the 3' end, 
and (c) the length of the target site. For long it had been believed that most of the miRNA
targets are located within the 3' UTR region of the mRNA, but recently CLIP methods and
some other computational methods e.g. \citep{xu2014identifying} have identified functional
miRNA targets in other gene regions like the 5' UTR region and the coding region sequence (CDS). So we
use a categorical feature to denote the type of region in which the target site is present
and learn weights (importance) for the three different gene regions. The relative
site location feature might help explain the fact that CLIP tags were enriched near
poly(A) sites (i.e. 3' end) and to a lesser degree near stop codons (5' end) than
in the middle of 3' UTR regions as reported in \citet{licatalosi2008hits}. Finally,
the feature ``site length'' accounts for the fact a perfect pairing between mRNA and miRNA,
and hence shorter target site length,
might be more preferable than alignments with long bulges on the mRNA (alignment of miRNA 
nucleotides to gaps) leading to longer target site lengths.

\subsection{Feature transformations}
By representing the features $\delg$, $\deldelg$ and local AU content
as curves (\fref{fig:curves}), some interesting patterns immediately emerge. 
For instance, we see that all three features, on an average, have a characteristic V-shape,
where the value of the feature has a steep dip at the target site. 
This is partly due to fact that the target site mostly has smaller size than
the size of the sliding window that we use to compute values at regions flanking the target site.
For all the features, we do notice that there is significant overlap between
the values of curves for positive and negative mRNA-miRNA interactions.
What is interesting though is that, the curves for local AU content
has the most separability among the three features for positive and negative examples.

Another subtle difference between positive and negative
mRNA-miRNA interactions is that for AU content and $\delg$, the 
value of the curves gradually increase from the 5' end to the 3' end and
the rate of increase is greater for the positive examples than the negative examples.

Finally, we note that the difference in binding energy ($\delg$) is sharply lower
at the target site compared to that at the flanking regions. The 
difference becomes increasingly pronounced as we move from the 5' end to the 3' end of the mRNA.
So the curves for $\delg$ seem to suggest that the one of the factors
that determines if a miRNA will target a certain mRNA region is
governed not so much by the stability of the miRNA-mRNA duplex at the target region
but more by the difference in duplex stability between the site and the flanking regions.
A larger difference between duplex stability at the site and flanking regions
indicates greater preference for binding. So given the differences between positive and
negative mRNA-miRNA interactions in terms of various thermodynamic and sequence (AU) curves,
it only seemed natural to incorporate some sort of representation of these curves into our model.
Towards that end we chose non-parametric representations of the curves in terms of
linear combinations of cubic B-spline basis functions with only very general smoothness
assumptions (specifically, the  second derivative of the curve exists and is continuous everywhere).
In the next paragraphs we describe various feature transformations and our
classifier model.

The list of features used in our model is summarized in \tref{tab:feature_summary}.

Let the training dataset be $\mathcal{D} = \{y_i, \mbf{x}_i\}_{i=1}^{N}$, where 
$y_i \in \{0, 1\}$ is the response variable,
$\mbf{x}_i$ is the feature list for the $i$-th training example.
The $i$-th feature list contains the following features:
$\mbf{\delg}_i^{site}$, $\mbf{\deldelg}_i^{site}$, $\mbf{au}_i^{site}$, 
$\mbf{\delg}_i^{seed}$, $\mbf{\deldelg}_i^{seed}$, $\mbf{au}_i^{seed}$, $\mbf{a}_i$, $\mbf{o}_i$,
where, the first three features are
vectors of length $2*W + 1$ centered around target site ($W$  is the number of windows
around either side of the site region and is chosen to be 13),
the next three features are essentially the same
as the first three except that they are computed around the seed match region
and at a smaller granularity by using a window of size 9.
% SC (3/28):  This previous enumeration of all the features seems superfluous because you have defined them in text before and in the table above. 
% AG (3/28): In the table, I have listed down the features after I have done the transformation, but here I formalize how I go from
% vectors in bold face letters, to functions. So I thought introducing the features as vectors to start with was necessary.
We use the zero value to replace missing values in each of the 6 vectors, e.g., when the target site is towards the beginning or the end
of the mRNA. We take each of the 6 vectors of supposedly noisy observations
and convert them to smooth curves by expanding the curves as a weighted sum of cubic B-spline basis functions,
e.g. $\delg_i^{site}(t) = \sum_{k=1}^{K} c_{ik} \psi_k(t)$. The coefficients $c_{ik}$ are estimated
by minimizing the least squares error on the discrete observations $\mbf{\delg}_i^{site}$. This is accomplished by
using the interpolation module in scipy (specifically LSQUnivariateSpline). The number of knots
for cubic B-spline interpolation is computed as $K - order + 2$ (order is 4 for cubic splines).
The number of basis functions K controls the smoothness of the curve, with smoothness 
decreasing with increasing K.
It should be noted that we use the same number of basis functions K ($\leq 2W + 1$),
and hence the same smoothness assumptions, for all 6 functional features
which makes our model slightly restrictive as opposed to a model which uses
different number of basis functions (and hence different smoothness assumptions) for
each of the 6 functional features. The choice was made to bound the size of the parameter space that has to be explored.
We compute similar curves for the other functional features e.g. $\deldelg_i^{site}(t)$ and so on.
Now, for each of the 6 functional features we use the B-spline coefficients as features in the SVM model.

The feature $\mbf{a}_i$ is the vector that represents the alignment between the first
8 nucleotides of the miRNA with the mRNA, in the $i$-th data point, as a vector of values, which can be 1 (match), 2 (mismatch),
3 (gap) or 4 (GU wobble) (refer to \fref{fig:illustration}). We calculate the enrichment score for each seed match pattern as described in Equation \ref{eq:enrichment-score}. Thus for the $i$-th data point we lookup the
precomputed enrichment score of the seed match pattern $\mbf{a}_i$ and use it as a feature in our SVM classifier.

\begin{table}
    \centering
    \resizebox{!}{2in}{%
    \begin{tabular}{|c|p{7 cm}|}
        \hline
        $\delg_{site}(t)$ & Thermodynamic binding curve centered at the target site obtained by fitting
                            a smooth curve through the vector observation $\mbf{\delg}^{site}$. \\
        \hline
        $\delg_{seed}(t)$ & Finer resolution thermodynamic binding curve centered at the seed match region obtained by fitting
                            a smooth curve through the vector observation $\mbf{\delg}^{seed}$. \\
        \hline
        $\deldelg_{site}(t)$ & Accessibility curve centered at the target site obtained by fitting
                            a smooth curve through the vector observation $\mbf{\deldelg}^{site}$. \\
        \hline
        $\deldelg_{site}(t)$ & Finer resolution accessibility curve centered at the seed match region obtained by fitting
                            a smooth curve through the vector observation $\mbf{\deldelg}^{seed}$. \\
        \hline
        $au_{site}(t)$ & Local AU content curve centered at the target site region obtained by fitting a smooth curve
                        through vector observation $\mbf{au}^{site}$. \\
        \hline
        $au_{seed}(t)$ & Finer resolution local AU content curve computed at the seed match region obtained by fitting a smooth curve
                        through vector observation $\mbf{au}^{seed}$. \\
        \hline
        seed enrichment & A scalar feature indicating the extent to which a seed match pattern in enriched in the set of
                            positive mRNA-miRNA interactions set on a scale of 0 to 1. \\
        \hline
        site conservation & The extent to which the mRNA site nucleotides are conserved across different species. \\
        \hline
        seed conservation & The extent to which the nucleotides in the mRNA site that are paired with the miRNA seed region
                            are conserved across different species. This is only used when there is a canonical seed match. \\
        \hline
        off seed conservation & Average conservation score of mRNA nucleotides that are not paired with the seed region 
                                of the miRNA. This is only used when there is a canonical seed match. \\
        \hline
        target site length & Length of the mRNA target site \\
        \hline
        target region & mRNA region where the target site is present, viz. 3' UTR, CDS or 5' UTR \\
        \hline
        relative position of target site & Relative position of a target site within one of the 3 regions above
                                            on a scale of 0 to 1, with 0 indicating the 5' end and 1 indicating the 3' end.\\
        \hline
    \end{tabular}
    }
    \caption{Summary of features used in our model. For functional features the domain of the function is in 
             $\{t: t \in \mathbb{Z}, -13 \leq t \leq 13 \}$.\label{tab:feature_summary} }
\end{table}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=\linewidth]{v_clip_all_curves}
    \caption{The mean curves $(\mu(t))$, plus 1$\times$standard deviation ($\sigma(t)$), for various curves in the positive (red)
    and negative (black) mRNA-miRNA set for the human dataset (PAR-CLIP). The mean and standard deviations
    were computed for each index $-13 \leq t \leq 13$ over all examples.
    A window offset of 0 corresponds to the target site
    in the mRNA while other offsets correspond to positions of the moving window on either side of the target site
    The mean is computed over examples where the
    entire curve was available i.e. discarding the cases where the matching region was toward one end of the mRNA.}
    \label{fig:curves}
% SC (3/28): In this curve at the top in the legend say (+ve samples) (-ve samples). Point out which is the 3' end and which is the 5' end. 
\end{figure}

\subsection{Model}
Let the training dataset be denoted by $\mathcal{\tilde{D}} = \{y_i, \mbf{\tilde{x}}_i\}_{i=1}^{N}$
after transforming the various features as mentioned in the previous section.
We are interested in learning a classifier $f(x_i)$ such that $y_i f(x_i) \geq 0$ (the response
variable, $y_i$,  for negative examples here is -1 instead of 0 to simplify notation). We learn a linear
classifier i.e. $f(\mbf{x}_i) = \mbf{w}^T\mbf{x}_i + b$ by minimizing the loss function given in \qref{eq:loss}
using stochastic gradient descent. The loss function in \qref{eq:loss} is the hinge loss and corresponds to a linear SVM.
The first term of \qref{eq:loss} penalizes data points that are misclassified (wrong side of the decision surface)
as well as correctly classified points that are too close to the decision surface i.e. points within some
margin of the decision surface. Thus, minimizing the loss functions results in a maximum margin decision surface
that best separates the two classes. The second term, called the regularization term, penalizes complex models
with large weights. 
\begin{align}
    L(\mbf{w}, \mbf{X}, \mbf{y}) = \frac{1}{n} \sum_{i=1}^{n} max(0, 1 - y_i f(\mbf{x}_i)) + \frac{\lambda}{2} ||\mbf{w}||_2^2
\label{eq:loss}
\end{align}
We used Apache Spark \citep{Zaharia:2010} running on a Yarn \citep{Vavilapalli:2013} cluster of 10 nodes to train our model.
The regularization parameter $\lambda$, which controls the trade-off between training misclassification rate and model complexity,
is set to a low value of $0.001$. 
Finally, we compute probability
scores for a test example, with feature vector $\mbf{x}^*$, using the weights learned from training according to
the logistic function given in \qref{eq:logistic}.
\begin{align}
    p(y^* = +1) = \frac{1}{1 + \exp(-\mbf{w}^T\mbf{x}^* - b)}
\label{eq:logistic}
\end{align}

\subsection{Comparison against other methods}
For comparison with competition, we use the CLIP datasets,
which has mRNA information, and coarser-grained (larger) nucleotide
regions that include the actual AGO binding site, call that $l_1$.
For our synthetic data, which is generated from the experimental data,
we have: (mRNA, miRNA, $l_2$) where the location $l_2$  is finer-grained and is within $l_1$.
We give competition a victory if they predict a binding site as (mRNA, miRNA, $l_3$)
if $l_3$ has at least a threshold amount of overlap with location $l_1$ (AGO-crosslinked region).
The threshold that we use for our evaluation is 90\%.
Note that this gives competition the benefit - if the actual synthetically generated data is
(mRNA-a, miRNA-b, $l_2$) and the prediction from competition is (mRNA-a, miRNA-c, $l_2$),
we count that up as a victory for the competitive protocol.

We downloaded target locations and their corresponding scores for each mRNA
as computed by mirSVR \citep{betel2010comprehensive}, PITA \citep{kertesz2007role},
TargetScan \citep{grimson2007microrna} and STarMir \citep{liu2013clip}.
When comparing performance against competition we only considered those mRNA, miRNA pairs
for which we generated data. Then we labelled each mRNA, miRNA interaction reported 
by other methods as 1 or 0 depending on whether the reported target location was contained
within an AGO cross-linked region for the mRNA in the CLIP datasets. 
Since, mirSVR reports sequences for hg19 assembly of the human genome
while we generate predictions using the hg18 assembly, we mapped mRNA target sites
from hg19 assembly to the hg18 assembly. 
Then we computed mean ROC (receiver operating characteristics) curves
for each method from the scores and the computed CLIP labels (1/0).
Note that TargetScan \citep{grimson2007microrna} does not generate predictions
for human.

We also evaluated the performance of our method against
another method called, MIRZA \citep{khorshid2013biophysical}, on the human dataset.
The biophysical model developed in \cite{khorshid2013biophysical} also considers
all possible canonical and non-canonical seed matches to identify miRNA target sites.
We downloaded the MIRZA tool from the website http://www.clipz.unibas.ch.
To generate ROC curves for MIRZA, we ran MIRZA on our candidate set of positive and negative
examples. Since MIRZA requires that all target sites be of the same length,
we made sure that each target site was expanded, or shrunk if necessary, to have
a length of 50 nucleotides. We averaged the ROC curve for MIRZA over 5 runs,
where in each run we randomly sub-sampled negative examples to have, roughly, the same
number of positive and negative examples. The ROC curve for each run was generated by
varying the threshold for the target quality score computed by MIRZA to compute the
true and false positive rates.



