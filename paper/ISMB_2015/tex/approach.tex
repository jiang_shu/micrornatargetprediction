\section{Approach}
As a machine learning task, the problem of miRNA target
prediction is that of link prediction in a bipartite graph, where vertices in
one set represent all possible target regions across all mRNAs while vertices
in the other set represent miRNAs. We can either predict if an edge exists (1/0)
between a pair of vertices representing an mRNA region and a miRNA
(classification), or we can predict the strength of the association
i.e. edge weights (regression).  In this paper we focus on the classification
problem of whether a miRNA targets a mRNA region. Towards that end we use data 
from CLIP experiments to train and evaluate the performance of our model. Additionally, we also
generate probability scores for our prediction.

CLIP methods \citep{chi2009argonaute, hafner2010transcriptome,
kishore2011quantitative} identify short regions across the genome that harbour
potential binding sites for miRNAs. While CLIP datasets identify short mRNA regions
that are functional AGO-mRNA interaction sites, additional bioinformatic analysis
is needed to identify the miRNAs-mRNA binding sites. In order to identify miRNAs that might
target those AGO-crosslinked regions we followed the same general approach as previous methods
\citep{xu2014identifying, liu2013clip, betel2010comprehensive, kertesz2007role}.
The general idea is to first generate a candidate set of mRNA binding 
sites for a list of mRNAs and miRNAs by enforcing a minimum threshold 
on the alignment score or the minimum free energy hybridization ($\delg$) of the
miRNA and mRNA and/or using seed match constraints. Then, 
different methods use different approaches to identify true
miRNA target sites within the candidate set of mRNA-miRNA interactions.
Previous works have used various criteria to generate the candidate set of mRNA target sites,
e.g. in mirSVR \cite{betel2010comprehensive} the authors use the Miranda \cite{john2004human}
algorithm to generate the initial candidate site. The Miranda algorithm
computes an optimal local alignment of the miRNA with an mRNA sequence,
by using various parameters for the overall alignment score, gap opening
and gap extension penalties, the authors generate candidate sites involving
canonical seed matches, which they defined as 
``sites that contain minimally a 6-mer perfect match at positions 2 to 7 of the miRNA'',
and non-canonical seed matches by allowing only a single G:U wobble or a single 
mismatch in the seed region. In \cite{liu2013clip} the authors use 
two criteria for generating the candidate set, first they use 
the RNAhybrid program \citep{kruger2006rnahybrid} 
to generate candidate sites by enforcing a threshold of -15 kcal/mol 
on the thermodynamic binding energy ($\delg$), and second they use constrain
the seed match, without constraining the binding energy, to belong to one of the 6
types of seed classes as defined in \citep{bartel2009micrornas}. It is easy to see
that by starting out with a more restricted set of candidate target sites, a
method can achieve a higher true positive rate for identifying positive
target sites within the candidate set, at the cost of missing out on a large
number of positive target sites that are not in the candidate set.

For our method, we select the least restrictive filter to have the most expansive superset
for the initial selection of possible target locations genome-wide.
Specifically, in our case we have the thermodynamic cut-off as -15 kcal/mol and to
generate seed-sites, we constrain the seed match to be at least a 6-mer without using
any additional constraints. If a target region meets either of these two criteria,
then we include it in the candidate set.  
Thus, we challenge our model by coming up with the most expansive set of potential target sites.
This shows up quantitatively in \tref{tab:candidate_set}
where we see that the dataset that we evaluate on is the largest among all prior works.
Algorithm \ref{algo:candidate_set} describes our process of generating the candidate set.
Line 3 of the algorithm extracts all the candidate target regions for a mRNA, miRNA pair
for which the binding energy $(\delg)$ is less than -15 kcal/mol. While, line 4 of the
algorithm extracts all target sites which have atleast a 6-mer seed match. In the following
line we remove from the set of target sites extracted in step 3 those target sites that also have
a seed match (extracted in step 4). Finally the algorithms returns a set of seed and seedless target sites
for each mRNA-miRNA pair.
% SC (3/27/15): Now give a 2-3 sentence description of Algorithm 1. Reviewers often skip pseudo code descriptions.
% AG (3/28/15): But I have already given that description a couple of lines before this where I
% talk about the cut-off and 6-mer match. The algorithm merely formalizes that notion so that there
% is no ambiguity. Don't you think another couple of lines of description is going to be redundant?

Since different methods use different numbers of miRNAs and mRNAs, we use a metric called
{\em ``normalized candidate set size''} to compare against other methods. The 
normalized candidate set size is defined to be the size of the candidate set divided
by the product of number of mRNAs and miRNAs used by the method. So the normalized
candidate set size can be thought of as the average number of potential target sites
considered by a method for a mRNA-miRNA pair. The higher the number is, the more general
and less restrictive the method is.
% AG (3/28/15): I think using both ``more expansive'' and ``less restrictive'' is redundant.
% So I stuck to ``more general'' and ``less restrictive''. If you think using both is also
% redundant, then I can remove one of the qualifiers.

After generating the
candidate set of target sites, we label each target site for a mRNA-miRNA pair
to be a functional (positive) site if the site is contained within an AGO-crosslinked
region for the mRNA as obtained from the CLIP datasets. Thus,
\emph{miRNA-mRNA interactions are
deemed functional if the target site is present in a AGO-crosslinked region and
additionally, either the binding energy of the miRNA-mRNA hybrid is below a
certain threshold or if there is a seed match}. Given this notion of a
functional miRNA-mRNA interaction, the learning problem now becomes that of
predicting whether a given region in a mRNA is targeted by a miRNA. In the
following sections we describe the different features we considered in
developing a classifier and then describe the classifier model.

\paragraph{Threats to validity}: 
In our approach we have made the assumption that functional miRNA target sites
are present only within the AGO-crosslinked regions as identified in the CLIP datasets.
Given AGO-crosslinked regions for various mRNAs
we try to extract finer grained target sites within AGO-crosslinked regions that may be
targeted by a set of miRNAs and in doing so we only consider the most abundantly expressed
miRNAs in a cell-line. Thus our method misses out on functional miRNA target sites that
may be outside AGO-crosslinked regions or fail to identify mRNA sites that are targeted 
by the miRNAs whose expressions level are low.

\begin{algorithm}
\caption{Candidate set generation for miRNA target prediction}\label{algo:candidate_set}
\begin{algorithmic}[1]
    \Require mRNA list $\mcal{M}$, miRNA list $\mcal{N}$
    \Ensure Candidate target locations, involving canonical seed matces $\mcal{O}^s$ and non-canonical
    seed matches $\mcal{O}$, for all mRNA miRNA pairs $\mcal{M} \times \mcal{N}$.
    \For{mRNA $m \in \mcal{M}$}
        \For{miRNA $n \in \mcal{N}$}
            \State $\mcal{O}_{mn} = \mcal{O}_{mn} \cup \{t : \delg(m[t], n) \leq -15 kcal/mol\}$
            \Comment{$t$ is a target site in $m$ as computed by RNAhybrid \citep{kruger2006rnahybrid}} 
            \State $\mcal{O}_{mn}^s = \mcal{O}_{mn}^s \cup \{t : 6 \leq \left \lvert seedMatch(m[t], n) \right \rvert \leq 8 \}$
            \Comment{$\left \lvert seedMatch(m[t], n) \right \rvert$ is the length of the seed match}
            \State $\mcal{O}_{mn} = \mcal{O}_{mn} \setminus \mcal{O}_{mn}^s$ 
        \EndFor
    \EndFor
    \State \Return $\{\mcal{O}_{mn}\}_{|\mcal{M}| \times |\mcal{N}|}$, $\{\mcal{O}_{mn}^s\}_{|\mcal{M}| \times |\mcal{N}|}$
\end{algorithmic}
\end{algorithm}

\begin{table*}[!htbp]
\centering
\resizebox{2.0\columnwidth}{!}{%
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\hline 
\cellcolor{LYellow}& \cellcolor{LYellow}& \cellcolor{LYellow}& \cellcolor{LYellow}&\cellcolor{LYellow} & \multicolumn{3}{c|}{\cellcolor{LYellow}\# Postive target sites in}\\
\multirow{-2}{*}{\cellcolor{LYellow}} & \multirow{-2}{*}{\cellcolor{LYellow}\# Positive examples} & \multirow{-2}{*}{\cellcolor{LYellow}\# Negative examples} & \multirow{-2}{*}{\cellcolor{LYellow}\# mRNA} & \multirow{-2}{*}{\cellcolor{LYellow}\# miRNA} & \multicolumn{1}{c}{\cellcolor{LYellow}3' UTR} & \multicolumn{1}{c}{\cellcolor{LYellow}CDS} & \cellcolor{LYellow}5' UTR\\
\hline
HITS-CLIP (Mouse) & 861,208 & 35,608,333 & 4,059 & 119 & 478,138 ($ \approx 56\%$) & 367,371 ($ \approx 43\%$) & 15,699 ($\approx 1\%$)\\
\hline
PAR-CLIP (Human) & 141,109 & 2,659,748 & 1,211 & 35 & 80,775 ($\approx 57\%$)& 55,250($\approx 39\%$) & 5,084 ($\approx 4\%$)\\
\hline
\end{tabular}
}
\caption{Attributes of data used for training and prediction in \name. For both mouse and human data,
most of the positive miRNA target sites are found in the 3' UTR region followed by the CDS region with very few target sites
located in the 5' UTR region. \label{tab:data}}
\end{table*}

\begin{table*}[!htbp]
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline 
\rowcolor{LYellow}
 & mirSVR & PITA & TargetScan & STarMir & \bf{\name} \\
\hline 
Human & 1.256 & 3.078 & NA & 56.183 & 66.081 \\
\hline 
Mouse & 0.56 & 2.179 & 0.318 & 37.418 & 75.503 \\
\hline
\end{tabular}
\caption{Comparison of the normalized candidate set size used by various methods. The normalized candidate set size
is obtained by dividing the candidate set size by the number of miRNA times the number of mRNAs. The normalized
candidate set size can be interpreted as the average number of candidate sites considered by the method for a mRNA-miRNA pair.
mirSVR, PITA and TargetScan only consider the 3' UTR region, so the normalized candidate set size is very low for those methods.
\label{tab:candidate_set}}
\end{table*}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=\linewidth]{illustration}
    \caption{A schematic of some of the features used in the model. The alternating blue
    and green regions denote the 13 consecutive windows around the target site (shown in red)
    where various thermodynamic and sequence features are computed. The seed alignment pattern
    is computed by considering the mRNA nucleotides that are aligned with nucleotides from index 1
    to 8 from the 5' end of the miRNA. The alignment is represented by a vector of 1 (match), 2 (mismatch),
    3 (gap) and 4 (GU wobble). The relative position of the target site within a mRNA region is also used
    as a feature, as well as the length of the target site.
    \label{fig:illustration}}   
\end{figure}

