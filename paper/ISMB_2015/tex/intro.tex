\section{Introduction}
MicroRNAs are endogenous 20-24 nucleotide (nt)
non-coding RNAs that modulate gene regulatory pathways \citep{jiang2009mir2disease}.
MicroRNAs (miRNAs) as well as short interfering RNAs (siRNAs) mediate RNA
interference (RNAi) by targeting the 3' untranslated region (UTR) of the mRNA,
or less frequently, other mRNA regions such as the coding sequence or
the 5' UTR \citep{carthew2009origins}.  Following their biogenesis,
miRNAs associate with Argonaute (AGO) family proteins; the latter
serving as the core of the RNA-induced silencing complex (RISC) \citep{bartel2009micrornas}.
The mature RISCs are then guided to their mRNA targets, typically resulting in
target mRNA repression or destabilization or both.
There are over two thousand miRNAs that have been annotated in humans \citep{friedlander2014evidence},
and since each miRNA can potentially target multiple mRNAs and each mRNA in turn
can be simultaneously targeted by mutiple miRNAs, it is speculated that as much as 60\% of mammalian
genes are affected by miRNAs \citep{friedman2009most}.

MiRNAs have been found to play a major role in development \citep{bartel2009micrornas},
brain functions \citep{shao2010comprehensive,li2013comprehensive},
and even cellular healing and pluripotency \citep{heinrich2012micrornas,poliseno2006micrornas}.
Thus abnormal changes in miRNA expression can cause
dysregulation of important biological pathways, and are involved in many
diseases such as cancers  and cardiovascular diseases \citep{hata2013functions}.
Therefore determination of the target mRNAs of a variety of miRNAs will enable the
deciphering of the etiology of myriad diseases.

In spite of such widespread gene regulation by miRNAs, the precise mechanism
of miRNA action is still not well understood, especially in animals.
In general, miRNAs act by binding to complementary regions in the target mRNAs
thereby inhibiting, or in some cases encouraging, protein production
through cleavage or destabilization the mRNA 
or by repression of mRNA translation \citep{brodersen2009revisiting,pillai2007repression}.
While pairing between miRNAs and mRNAs is mostly perfect in plants,
mRNA targets are regulated often through imperfect pairing in animals.
It has been observed in animals that functional mRNA-miRNA interactions can
occur through complementary base pairing with as little as 6 nucleotides of the miRNA,
which is known as the seed region of the miRNA (position 2-7 from the 5' end of the miRNA)
\citep{brennecke2005principles}. Alternately, such interactions may also
occur via imperfect base pairing at the seed region, referred to as non-canonical
seed match in literature, which are supplemented by additional base pairing 
at the 3' end of the miRNA (refer to \cite{bartel2009micrornas} for a good review).
Imperfect miRNA-mRNA hybrids with central bulges or mismatches (nt 9-12) enable
translation inhibition or exonucleolytic mRNA decay, while rare highly
complementary central pairing result in target modulation via slicing i.e.
mRNA cleavage \citep{brodersen2009revisiting,pillai2007repression}.

Recent advances in CLIP (cross-linking immunoprecipitation)
technology have allowed capturing miRNAs with their
cognate mRNAs under various conditions.  Immunoprecipitation of the AGO protein
followed by the high-throughput sequencing (HITS) of the cognate mRNA sequence
\citep{licatalosi2008hits} has afforded an experimental high-throughput approach to map genome-wide
miRNA targets experimentally with increasing resolution \citep{chi2009argonaute}. CLIP experiments
have the advantage of profiling the native miRNA levels as opposed to
supra-physiological levels obtained via miRNA transfection experiments \citep{thomson2011experimental}.
However, the resolution of CLIP experiments is not high enough to unambiguously
decipher miRNA targets. In HITS-CLIP experiments, for example, the footprint
size of the Ago protein is typically 30-50 nt. More recent techniques such as
iCLIP and CIMS analysis can potentially track the targets at nucleotide-level
resolution \citep{moore2014mapping}. 

Computational methods for MiRNA target prediction have traditionally focussed on
canonical seed matches in the 3' UTR region of mRNA to search for target sites,
with only a few methods incorporating non-canonical seed matches by allowing
for a single GU wobble or a mismatch in the seed region. For instance in
mirSVR \citep{betel2010comprehensive}, while the authors use a 8-bit long 
bit vector (1 representing a match and 0 representing a mismatch)
to encode the seed match pattern, they only allow for a single GU wobble
or a single mismatch in the 6-mer seed region. Recent methods
\citep{xu2014identifying, liu2013clip} have expanded the search to other 
areas in the gene like the 5' UTR, CDS and intronic regions. 
In \citet{liu2013clip}, the authors generate
predictions for sites involving non-canonical seed matches (seedless sites),
but they do not take into account the type of non-canonical seed match for seedless sites. 
Instead, they rely on other features to generate prediction for seedless sites, thereby
missing out on potential signal from non-canonical seed match patterns that are enriched in
the functional miRNA-mRNA interactions. One possible reason for that being the difficulty
to incorporate huge number of possible patterns of insertions and deletions in the seed region
as pointed out by \cite{xu2014characterization}.

Computational methods have also relied on thermodynamic features like stability 
of the miRNA-mRNA duplex and accessibility of the mRNA target region, along
with sequence features like AU content of the region flanking the target site
to infer functional mRNA-miRNA interactions \citep{grimson2007microrna,kertesz2007role}.
Yet previous methods compute various thermodynamic scores only at the target site region
to summarize the mRNA-miRNA interaction. In \cite{xu2014characterization} the authors
showed that a certain normalized measure of site accessibility, $Z_{\deldelg}$, 
has a characteristic pattern around the target site region. Yet they don't exploit that
fact in their model. In \citep{liu2013clip} the authors computed site accessibility 
in the neighborhood of the target region in discrete chunks of 5, 10, 15, 20, 25
and 30 nucleotides around the target site region, but the authors report that only
accessibility computed at the target site region is an important predictor
of functional miRNA target sites.

In this paper we develop a machine learning, called \name, to
solve the classification problem of predicting if a miRNA targets a mRNA region.
Our contributions are as follows:
\begin{itemize}
    \item We incorporate various thermodynamic and sequence ``curves'' at two 
    different resolutions, as functional covariates in our model (as opposed to multivariate or scalar covariates),
    to capture various patterns of curves that characterize functional mRNA-miRNA interactions.
    \item We develop and incorporate in our model a metric called seed enrichment that captures all patterns of seed matches,
    including multiple mismatches, GU wobbles and long bulges in the seed region. By doing so
    we are able to take a unified approach in modelling canonical and non-canonical seed matches. We also 
    show that a whole gamut of non-canonical seed matches, involving bulges on the mRNA, are enriched in
    the set of positive mRNA-miRNA interactions in both Humans and Mice.
    \item We are able to achieve superior performance, as measured by the true positive and false positive rates,
    over various existing and recent computational methods for miRNA target prediction and bring important 
    insights into various factors governing miRNA downregulation. Specifically, the area under the ROC curve (AUC)
    for our method is more than 20\% more than the closest competition for both target sites
    involving canonical and non-canonical seed matches. Moreover, we consider all possible non-canonical
    sites in our prediction and are hence able to predict a significant number of target sites that have
    non-canonical seed matches.
\end{itemize}

