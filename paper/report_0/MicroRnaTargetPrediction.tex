\documentclass{article}      % Specifies the document class

\usepackage{natbib}
\usepackage{fullpage}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{float}
\usepackage{graphicx}
\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\graphicspath{{figures/}}
\newcommand{\mrna}{\text{mRNA}~}
\newcommand{\micrna}{\text{microRNA}~}
\newcommand{\delg}{\ensuremath{\Delta G}~} % Delta G
\newcommand{\deldelg}{\ensuremath{\Delta \Delta G}~} %Delta Delta G
\newcommand{\mcal}[1]{\ensuremath{\mathcal{#1}}}
\newcommand{\mbf}[1]{\ensuremath{\mathbf{#1}}}
\newcommand{\mbb}[1]{\ensuremath{\mathbb{#1}}}
\DeclareMathOperator*{\argmin}{arg\,min}
\newtheorem{definition}{Definition}
\newcommand{\iref}[1]{Fig.~\ref{#1}}
\newcommand{\X}{\ensuremath{\mathbf{X}}}

\title{MicroRNA Target Prediction}  
\author{Asish Ghoshal}      

\bibliographystyle{apalike}

\begin{document}             % End of preamble and beginning of text.

\maketitle                   % Produces the title.
\section{Problem}
MicroRNA are a class of small (approximately 22 nucleotides) non-coding RNAs that are involved in gene regulation. It is believed that as
many as $60\%$ of the genes of humans and other mammals are regulated by MicroRNAs \citep{lewis2005conserved}. The mechanism in which
MicroRNAs target genes (mRNAs) in plants is well understood. In plants there is almost perfect pairing between \mrna and \micrna at the seed region,
defined as nucleotides 2-7 at the 5' end of the \micrna, which results in the \mrna being cleaved thereby
reducing the expression of the gene \citep{brennecke2005principles}. While in animals the \micrna binds less tightly to the \mrna
at the seed region - insertions, deletions and mismatches in the seed region are commonly found. So for animals a lot of computational approaches
have been developed to reliably predict \micrna target sites. The problem then can be formulated as follows: 
\begin{definition}
    Given a \mrna sequence $g_i =~ <g_{i,1}, g_{i,2}, ..., g_{i,N_i}>$ and a \micrna sequence $m_i =~ <m_{i,1}, m_{i,2}, ..., m_{i,M_i}>$ 
    compute the probability that the \mrna is a target of the microRNA.
\end{definition}

\section{Related Work}
Most computational approaches to predict \micrna target sites fall into two broad classes
viz. filtering approaches and machine learning approaches \citep{kleftogiannis2013we}. Filtering approaches start with a 
set of potential candidates and then filter out targets based on various features like evolutionary conservation,
various structural and thermodynamic criteria and so on; common examples being \citet{lewis2003prediction, enright2004microrna}.
While Machine Learning approaches use the aforementioned features and use
methods like Support Vector Regression \citep{betel2010comprehensive}, Gaussian Mixture models \citep{liu2010bayesian},
Naive Bayes \citep{yousef2007naive} among others to predict \micrna targets. What is common to most of the approaches is
that they look for seed matches. While earlier work focussed on canonical seed matches (perfect seed complementarity in
the 3' UTR region of the \mrna) later work have taken into account non-canonical seed types by allowing for a few GU wobbles in the seed
region and searching for target matches in the 5' untranslated region (5' UTR), promoters and coding sequences (CDSs) 
in addition to the 3' UTR region \citep{xu2014identifying}. 

The issue with most of the current approaches is this biased focus on the seed region - the majority of the current approaches
generate candidate sites by looking for seed matches allowing for one or two GU wobbles and then use different methods as described
above to further restrict candidate target sites. Also, a majority of the approaches do not account for insertions and deletions
in the seed region e.g. in the current state of the art \citep{xu2014identifying} the authors identify initial candidate 
sites by scanning the \mrna sequence for five different types of seed matches, four of which are canonical seed types and
only one being a non-canonical seed type that includes a single GU wobble. The authors also acknowledge that due to the difficulty
of modelling insertions and deletions in the seed region they considered only one non-canonical type of seed match which allows
for only one GU wobble in the seed region. The accuracy of their method was around 60\% and while the recall was higher than most
other methods, it was still very low.

\section{Approach}
In \citet{xu2014identifying} the authors identify various candidate sites by looking for seed matches for the five types of seeds, as discussed above,
in various regions of the genes viz. promoters, CDSs, 5' UTR and 3'UTR. Then they compute the binding energy
for the \micrna-target duple,x $\Delta G_{duplex}$, and the accessibility energy, $\Delta \Delta G$, ``which is the difference between
the free energy $\Delta G_{duplex}$ and the free energy required to unpair the target-site nucleotides, $\Delta G_{open}$". 
The $\Delta G$ and $\Delta \Delta G$ values indicate the thermodynamic stability of the \micrna-target duplex and is an important indicator
of whether the \micrna will attach to the \mrna at the site and regulate it. Then they compute the $\Delta G$ and $\Delta \Delta G$
scores for the entire \micrna-\mrna pair by taking a weighted sum of the $\Delta G$ and $\Delta \Delta G$ scores over all target sites.
The weights were computed by computing an empirical measure of signal to noise ratio rather than using any machine learning approach.
Finally, they use cutoff values for $\Delta G$ and $\Delta \Delta G$ for predicting \micrna targets. Thus, just by using $\Delta G$ and $\Delta \Delta G$
scores the authors were able to get better accuracy and recall than most existing approaches.

In this paper we develop a much more systematic and general approach to use the $\Delta G$ scores for the 
\micrna-target duplex to predict \micrna target sites. Rather than fixing seed types and looking for seed matches
we compute the \delg scores for each position of the \mrna by using a sliding window. If the length of the
\mrna is $l_n$ and the length of the \micrna is $l_m$,
then we use a window of length $2l_m$ and for every position of the window in the \mrna sequence
we compute the optimal \delg scores for the \micrna and \mrna (restricted to the window)
pair using dynamic programming. So we get \delg ``curves"
of length $l_n - 2l_m$ for each \micrna-\mrna pair. We can compute even more functional covariates such as 
the \deldelg scores, number of seed matches and non-seed matches by using the sliding window method.
Thus our training data consists of functions drawn from a reproducing kernel Hilbert space (RKHS). 
The advantage of the approach is that we don't fix the seed types and let ``the data
speak for itself". The main intuition behind this approach is that if there is a true binding site in a \mrna then owing
to the significant overlap between neighboring windows we are more likely to see the same optimal alignment and hence
the same \delg and \deldelg scores for nearby window positions (see \iref{fig:plot}). Also, by using functional data analysis approaches we
can discover, in theory, more complex relationships and correlations between the various functional covariates. This is
the first time functional data analysis methods are being used for \micrna target prediction.
\begin{figure}[H]
\centering
    \includegraphics[width=\linewidth]{plot2}
    \includegraphics[width=\linewidth]{plot1}
\caption{\delg curves for 100 positive (RED) and 100 negative samples (\micrna-\mrna pairs).
In the negative samples we can see that there are much more fluctuations, both coarse and finer, 
than the positive examples. The negative samples were generated by taking valid \micrna-\mrna target pairs
and then randomly shuffling and pairing \micrna and \mrna that were not paired in the training dataset. Only
the first 3000 and 1000 points of the curves are shown.%
\label{fig:plot}}
\end{figure}

\section{Method}
The data is given by $\mcal{D} = \{\X_i, y_i\}_{i=1}^{N}$, where $y_i \in \{+1, -1\}$ and $\X_i$ is the 
ith curve generated as described above. The optimal \delg score between a \micrna and an \mrna window is computed
by using the \emph{RNAhybrid} \citep{kruger2006rnahybrid} program.
We truncate each curve to a length of 3000. Curves having length less than 3000 are discarded. To compute
the smooth \delg function for a \micrna-\mrna pair from a set of discrete set of observations.

\section{Results}
A total of 170 samples of which 100 were negative samples were used to evaluate the performance of the algorithm.
The average accuracy and recall was computed by using 10-fold cross validation. The mean accuracy was {\bf 88.39286 \%}
while the mean recall was {\bf 84.28571 \%}. In comparison, the current state of the art has an accuracy of {\bf 64 \%}.

\begin{figure}[H]
\centering
    \includegraphics[width=\linewidth]{pos_neg_plot}
\caption{Performance of algorithm on the test dataset. The top figure shows the \delg curves for the correctly
predicted positive examples i.e. true \micrna-\mrna pairs while the bottom figure shows the \delg curves
for the correctly predicted negative examples. It can be seen that the model has correctly captured the
discriminative features in the curves. A \delg curve for a valid \micrna-\mrna pair starts out with lower \delg scores
towards the beginning and then climbs up while the \delg curve for an invalid \micrna-\mrna pair has lot more variation 
around a mean curve that pretty much remains constant.%
\label{fig:validation}}
\end{figure}

\bibliography{MicroRnaTargetPrediction}
\end{document}
