\documentclass{article}      % Specifies the document class

\usepackage{natbib}
\usepackage{fullpage}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{float}
\usepackage{graphicx}
\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\graphicspath{{figures/}}
\newcommand{\mrna}{\text{mRNA}~}
\newcommand{\micrna}{\text{microRNA}~}
\newcommand{\delg}{\ensuremath{\Delta G}~} % Delta G
\newcommand{\deldelg}{\ensuremath{\Delta \Delta G}~} %Delta Delta G
\newcommand{\mcal}[1]{\ensuremath{\mathcal{#1}}}
\newcommand{\mbf}[1]{\ensuremath{\mathbf{#1}}}
\newcommand{\mbb}[1]{\ensuremath{\mathbb{#1}}}
\DeclareMathOperator*{\argmin}{arg\,min}
\newtheorem{definition}{Definition}
\newcommand{\iref}[1]{Fig.~\ref{#1}}
\newcommand{\X}{\ensuremath{\mathbf{X}}}

\title{MicroRNA Target Prediction}  
\author{Asish Ghoshal}      

\bibliographystyle{apalike}

\begin{document}             % End of preamble and beginning of text.

\maketitle                   % Produces the title.
\section{Method}
Crosslinking immunoprecipitation (CLIP) methods like \citep{chi2009argonaute} have been used lately
to directly identify mRNA target sequences that contain miRNA binding sites. \citep{hafner2010transcriptome, kishore2011quantitative}
identified AGO crosslink-centered regions (CCRs) in mRNAs transcripts expressed in human HEK 293 cells. The authors
used a set of 10 most abundantly expressed miRNA families in HEK 293 cells to identify potential miRNAs that bound to the 
CCRs. In \citep{liu2013clip} the authors used raw data from the CLIP experiments and identified mRNA-miRNA interactions by using the following
approach:
\begin{itemize}
    \item The authors first extracted potential binding sites for a mRNA-miRNA pair by using a binding energy cutoff of -15 kcal/mol
    and seed match.
    \item Overlapping binding sites were merged based on a seed class hierarchy.
    \item mRNA-miRNA binding sites were then classfied as being valid or invalid based on whether or not the sites were contained in 
    a AGO crosslink-centered regions.
    \item The data thus generated was used for training and testing.
\end{itemize}
The following features were used by the authors to predict miRNA binding sites using logistic regression:
\begin{enumerate}
    \item seed type
    \item 3' base pairing
    \item site accessibility, which is a `` measure of structural accessibility as computed by the average of single-stranded probabilities of
    the nucleotides in the predicted binding site''.
    \item seed accessibility
    \item Up-stream accessibility for nucleotide blocks of size 5, 10, 15, 20, 25 and 30.
    \item Down-stream accessibility for nucleotide blocks of size 5, 10, 15, 20, 25 and 30.
    \item Up-stream AU composition for nucleotide blocks of size 5, 10, 15, 20, 25 and 30.
    \item Down-stream AU composition for nucleotide blocks of size 5, 10, 15, 20, 25 and 30.
    \item Site conservation
    \item Seed conservation
    \item $\Delta G_{hybrid}$ for mRNA-miRNA duplex.
    \item $\Delta G_{nucl}$ which is ``a measure of the potential of nucleation for miRNA:target hybridization''.
    \item $\Delta G_{total}$ which is ``a measure of the total energy change of the hybridization''.
\end{enumerate}

From the data published by \citep{liu2013clip} we extracted for each mRNA-miRNA pair, the target site location
and whether or not the location is present in an experimentally validated AGO CCR. Using a window length of size
46 nucleotides, we computed various features for the site location and the 13 consecutive windows upstream and downstream
from the site location. The features generated as such were used as functional covariates for predicting miRNA binding sites.
Specifically the training data is given by $\mcal{D} = \{\mbf{X}_i, y_i\}_{i=1}^{N}$ where $\mbf{X}_i$ are assumed to be
discrete observations of the functional covariates, $\mbf{X}_i(t)$, for the ith data point, and
$y_i \in \{0, 1\}$ based on whether the site is contained in within a CCR. Functional data
are obtained from the discrete observations by using $m$ orthogonal Bspline basis functions and using least squares to learn the weights of
the basis functions. We then use functional Principal Component Analysis (fPCA) to extract the first $k$ principal components for the
functional covariates. The principal component scores (projections of curves along the principal components)
are used as features for a SVM classifier. The functional covariates used in our model as follows:
\begin{enumerate}
    \item $\Delta G_{hybrid}$: The binding energy of the mRNA target region and miRNA.
    \item $\Delta \Delta G$: The difference between the binding energy of the mRNA-miRNA duplex and
        the energy required to unpair the nucleotides of the target region $\Delta G_{open}$.
    \item AU composition.
    \item Conservation of the mRNA site. The conservation score is generated for each nucleotide in the mRNA site. The number of
    basis functions and principal components for the conservation score is chosen independently from those of the other
    features that use upstream and downstream windows for computing the curves.
\end{enumerate}
We also used a scalar covariate to model different seed types. We represented the pairing at the seed region (nucleotides 1 to 8
at the 5' end of miRNA) as a vector with components $\{1,2,3,4\}$ where 1 represents a match, 2 a mismatch, 3 a gap and 4 a GU wobble.
We then computed the occurence frequencies for each of the seed match vector in the positive examples. Each seed match vector
was then given a score which was the difference between the occurence frequency of the seed match vector and the occurence frequency
of the background (i.e. $0.25^8 = 0.000015$). So seed matches that are occur more frequently in the positive examples 
than a random match are assigned higher scores.
For a mRNA-miRNA pair the score for its seed match vector is used as an additional feature in the SVM classifier.

\section{Results}
In the data published by \citep{liu2013clip} we observed that the site locations for a mRNA in the dataset
did not match the nucleotide sequence published by the authors. Doing a string search for the nucleotide sequence
of the site in the entire nucleotide sequence of the mRNA resulted in site locations that were offset by some amount.
The offset amount changed for each mRNA in the dataset. We thus used two datasets for our experiments, one where the site
locations were taken to be exactly the same as reported by the authors (no offset) and the other where the site location
was offset by a certain amount to match the nucleotide sequence of the site reported by the authors (offset dataset).
We also found that the other features reported by the authors like AU composition did not match their actual values.
\begin{figure}[H]
\centering
    \includegraphics[width=0.6\linewidth]{basis_roc_plot.pdf}
    \includegraphics[width=0.6\linewidth]{components_roc_plot.pdf}
\caption{Scatter plot of the false positive rate and true positive rate obtained by
changing the number of basis functions (top) and number of principal components (bottom).%
\label{fig:roc_offset}}
\end{figure}

\begin{figure}[H]
\centering
    \includegraphics[width=\linewidth]{basis_no_offset_roc_plot.pdf}
\caption{Scatter plot of the false positive rate and true positive rate obtained by
changing the number of basis functions for the data generated with and without using an offset.%
\label{fig:roc_no_offset}}
\end{figure}

\bibliography{MicroRnaTargetPrediction}
\end{document}
