Reading data from file
Done.
Preprocessing positive data.
Filtered out 29 items.
Filtered out 58 items.
Filtered out 37 items.
Filtered out 33 items.
Filtered out 38 items.
Filtered out 20 items.
Filtered out 32 items.
Filtered out 34 items.
Done (0.165100 sec).
Preprocessing negative data.
Filtered out 35 items.
Filtered out 52 items.
Filtered out 41 items.
Filtered out 36 items.
Filtered out 29 items.
Filtered out 25 items.
Filtered out 71 items.
Filtered out 23 items.
Done (0.152544 sec).
Positive data:  (469, 2000)
Negative data:  (438, 2000)
Beginning training.
Loading required package: splines
Loading required package: Matrix

Attaching package: ‘fda’

The following object is masked _by_ ‘.GlobalEnv’:

    smooth.fd

The following object is masked from ‘package:graphics’:

    matplot

Loading required package: splines
Loading required package: Matrix

Attaching package: ‘fda’

The following object is masked _by_ ‘.GlobalEnv’:

    smooth.fd

The following object is masked from ‘package:graphics’:

    matplot

Loading required package: splines
Loading required package: Matrix

Attaching package: ‘fda’

The following object is masked _by_ ‘.GlobalEnv’:

    smooth.fd

The following object is masked from ‘package:graphics’:

    matplot

Loading required package: splines
Loading required package: Matrix

Attaching package: ‘fda’

The following object is masked _by_ ‘.GlobalEnv’:

    smooth.fd

The following object is masked from ‘package:graphics’:

    matplot

Loading required package: splines
Loading required package: Matrix

Attaching package: ‘fda’

The following object is masked _by_ ‘.GlobalEnv’:

    smooth.fd

The following object is masked from ‘package:graphics’:

    matplot

Loading required package: splines
Loading required package: Matrix

Attaching package: ‘fda’

The following object is masked _by_ ‘.GlobalEnv’:

    smooth.fd

The following object is masked from ‘package:graphics’:

    matplot

Loading required package: splines
Loading required package: Matrix

Attaching package: ‘fda’

The following object is masked _by_ ‘.GlobalEnv’:

    smooth.fd

The following object is masked from ‘package:graphics’:

    matplot

Loading required package: splines
Loading required package: Matrix

Attaching package: ‘fda’

The following object is masked _by_ ‘.GlobalEnv’:

    smooth.fd

The following object is masked from ‘package:graphics’:

    matplot

Loading required package: splines
Loading required package: Matrix

Attaching package: ‘fda’

The following object is masked _by_ ‘.GlobalEnv’:

    smooth.fd

The following object is masked from ‘package:graphics’:

    matplot

Done (5.317057 sec).
Beginning prediction.
Smoothing test data
Done.
Projecting test curves along principal components
Done.
Beginning prediction.
Done.
Done (0.590109 sec).
Accuracy = 0.584615, Recall = 0.808511
Beginning training.
Done (1.135664 sec).
Beginning prediction.
Smoothing test data
Done.
Projecting test curves along principal components
Done.
Beginning prediction.
Done.
Done (0.568977 sec).
Accuracy = 0.456140, Recall = 0.553191
Beginning training.
Done (0.924343 sec).
Beginning prediction.
Smoothing test data
Done.
Projecting test curves along principal components
Done.
Beginning prediction.
Done.
Done (0.819810 sec).
Accuracy = 0.561404, Recall = 0.680851
Beginning training.
Done (1.154747 sec).
Beginning prediction.
Smoothing test data
Done.
Projecting test curves along principal components
Done.
Beginning prediction.
Done.
Done (0.564645 sec).
Accuracy = 0.500000, Recall = 0.659574
Beginning training.
Done (0.904807 sec).
Beginning prediction.
Smoothing test data
Done.
Projecting test curves along principal components
Done.
Beginning prediction.
Done.
Done (0.811620 sec).
Accuracy = 0.395833, Recall = 0.404255
Beginning training.
Done (0.901138 sec).
Beginning prediction.
Smoothing test data
Done.
Projecting test curves along principal components
Done.
Beginning prediction.
Done.
Done (1.040908 sec).
Accuracy = 0.533333, Recall = 0.510638
Beginning training.
Done (0.906538 sec).
Beginning prediction.
Smoothing test data
Done.
Projecting test curves along principal components
Done.
Beginning prediction.
Done.
Done (0.593745 sec).
Accuracy = 0.555556, Recall = 0.744681
Beginning training.
Done (0.918312 sec).
Beginning prediction.
Smoothing test data
Done.
Projecting test curves along principal components
Done.
Beginning prediction.
Done.
Done (0.582076 sec).
Accuracy = 0.454545, Recall = 0.531915
Beginning training.
Done (0.646521 sec).
Beginning prediction.
Smoothing test data
Done.
Projecting test curves along principal components
Done.
Beginning prediction.
Done.
Done (1.064467 sec).
Accuracy = 0.535714, Recall = 0.638298
Beginning training.
Done (0.957874 sec).
Beginning prediction.
Smoothing test data
Done.
Projecting test curves along principal components
Done.
Beginning prediction.
Done.
Done (0.582472 sec).
Accuracy = 0.509091, Recall = 0.608696
Mean Accuracy = 0.508623, Mean Recall = 0.614061
