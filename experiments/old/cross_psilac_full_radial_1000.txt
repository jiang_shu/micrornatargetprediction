Reading data from file
Done.
Preprocessing positive data.
Filtered out 103 items.
Filtered out 36 items.
Filtered out 44 items.
Filtered out 41 items.
Filtered out 56 items.
Filtered out 21 items.
Filtered out 24 items.
Filtered out 55 items.
Done (1.082043 sec).
Preprocessing negative data.
Filtered out 262 items.
Filtered out 212 items.
Filtered out 119 items.
Filtered out 100 items.
Filtered out 95 items.
Filtered out 80 items.
Filtered out 123 items.
Filtered out 45 items.
Done (1.472457 sec).
Positive data:  (5633, 1000)
Negative data:  (7769, 1000)
Beginning training.
Done.
Projecting test curves along principal components
Done.
Beginning prediction.
Done.
Done (5.777061 sec).
Accuracy = 0.522388, Recall = 0.310835
Beginning training.
Done (33.657118 sec).
Beginning prediction.
Smoothing test data
Done.
Projecting test curves along principal components
Done.
Beginning prediction.
Done.
Done (5.430013 sec).
Accuracy = 0.593985, Recall = 0.420959
Beginning training.
Done (33.596778 sec).
Beginning prediction.
Smoothing test data
Done.
Projecting test curves along principal components
Done.
Beginning prediction.
Done.
Done (5.870515 sec).
Accuracy = 0.583333, Recall = 0.422735
Mean Accuracy = 0.588076, Mean Recall = 0.374542
