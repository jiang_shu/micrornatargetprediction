from pylab import plot, xlabel, ylabel
from scipy import fft, arange

def plot_spectrum(y,Fs=1):
    '''
    Plots a Single-Sided Amplitude Spectrum of y(t)
    '''
    n = len(y) # length of the signal
    k = arange(n)
    T = n/Fs
    frq = k/T # two sides frequency range
    frq = frq[range(n/2)] # one side frequency range

    Y = fft(y)/n # fft computing and normalization
    Y = Y[range(n/2)]

    plot(frq,abs(Y),'g') # plotting the spectrum
    xlabel('Freq (Hz)')
    ylabel('|Y(freq)|')
