microRNA list
=============
key: microrna:list
Value: <set of micrornas>

mRNA list
=========
key: mrna:list 
Value: <set of mrnas>


mrna sequence
=============
key: seq:<mrna>
value: sequence

mrna features
=============
key: features:<assembly>:<mrna>
value: {'utr5_range' : "<start>-<end>",
        'utr3_range' : "<start>-<end>",
        'cds_range' : "<start>-<end>",
        'consv_seq' : "space separated list"
}

microRNA sequence
=================
key: seq:<microrna>
value: sequence

validated targets
=================
key: mirwalk:target:<microrna>
Value: Set of mrnas that the microrna targets

key: mirwalk:target:<mrna>
Value: Set of microrna that target the mrna.

key: mirwalk:target:list
Value: list of "<mrna>,<microrna>" pairs

negative targets
================
Proposed negative targets from mirWalk dataset.
Keys: mirwalk:neg:target*


