#!/bin/bash
for i in `seq 1 10`; do
    ./bash/spark-submit.sh \
        $(pwd)/python/cv_clip_spark.py \
        --dataset V-CLIP-LINEAR-TUNE --model FN \
        --subsample-runs 1 --cluster-assignment $(pwd)/../../data/V-CLIP/cluster${i}.csv \
        tune \
        --kernel 'LINEAR' --reg-param 0.00001 0.0001 0.001 0.01 0.1 \
        --cv-runs 5 \
        '/home/aghoshal/microrna/input/V-CLIP/v_clip_mrna_mirna_features_pos/*' \
        '/home/aghoshal/microrna/input/V-CLIP/v_clip_mrna_mirna_features_neg/*' \
        '/home/asish/Workspace/Academic/Research/Projects/micrornatargetprediction/data/V-CLIP/v_clip_all_seed_enrichment.csv' \
        /tmp/vclip_linear_tune_result_cluster_${i}.txt > >(tee /tmp/vclip_cv_linear_tune_result_cluster_${i}.log) 2>&1

    if [ "$?" -ne 0 ]; then
        echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
        exit 1
    fi
done
