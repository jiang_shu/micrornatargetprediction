#!/bin/bash
if [ "$USER" != 'aghoshal' ]; then
    echo -e "$COL_RED Must be run as user aghoshal $COL_NONE"
    exit 1
fi
ARGS=$@

BASEPATH='/home/asish/Workspace/Academic/Research/Projects/micrornatargetprediction'
REMOTE_BASEPATH='/home/aghoshal/Workspace/MicroRnaTargetPrediction'
R_SRC_PATH="$BASEPATH/code/src/R"
REMOTE_R_SRC_PATH="$REMOTE_BASEPATH/code/src/R"
SPARK_HOME="/home/aghoshal/spark-1.2.0-bin-hadoop2.4"

# Zip dependencies
echo -e "$COL_GREEN Zipping up python dependencies $COL_NONE"
cd $BASEPATH/code/src/python/; zip /tmp/dependencies.zip * > /dev/null 2>&1
if [ "$?" -ne 0 ]; then
    echo -e "$COL_RED Failed. $COL_NONE"
    exit 1
else
    echo -e "$COL_GREEN Done. $COL_NONE"
fi

SPARK_OPTIONS="--master yarn-client --num-executors 10 --driver-memory 6g --executor-memory 7g --executor-cores 4 --conf spark.akka.frameSize=30 --conf spark.scheduler.mode=FAIR --py-files /tmp/dependencies.zip"

CMD="export R_SRC_PATH=$R_SRC_PATH; export SPARK_YARN_USER_ENV='JAVA_HOME=/usr/lib/jvm/java-7-oracle,R_SRC_PATH=$REMOTE_R_SRC_PATH'; $SPARK_HOME/bin/spark-submit $SPARK_OPTIONS $ARGS"
echo -e "$COL_RED $CMD $COL_NONE"
bash -c "$CMD"
