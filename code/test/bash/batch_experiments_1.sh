#!/bin/bash

if [ "$?" -ne 0 ]; then
    echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
    exit 1
fi

# 10 fold CV Mouse CDS (Seedless)
#./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
#    --dataset HITS-CLIP-CDS-CV --model FN --train-iters 200 --region CDS\
#    crossvalidate \
#    '/home/aghoshal/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_pos/*'\
#    '/home/aghoshal/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_neg/*'\
#    ~/Workspace/Academic/Research/Projects/micrornatargetprediction/data/HITS-CLIP/hits_clip_all_seed_enrichment.csv\
#    /tmp/output_hits_clip_cds_cv.txt > >(tee /tmp/output_hits_clip_cds_cv.log) 2>&1

#if [ "$?" -ne 0 ]; then
#    echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
#    exit 1
#fi

# 10 fold CV Moouse 5' UTR (Seedless)
./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
    --dataset HITS-CLIP-5UTR-CV --model FN --train-iters 200 --region 5UTR --subsample-runs 5\
    crossvalidate \
    '/home/aghoshal/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_pos/*'\
    '/home/aghoshal/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_neg/*'\
    ~/Workspace/Academic/Research/Projects/micrornatargetprediction/data/HITS-CLIP/hits_clip_all_seed_enrichment.csv\
    /tmp/output_hits_clip_5utr_cv.txt > >(tee /tmp/output_hits_clip_5utr_cv.log) 2>&1

if [ "$?" -ne 0 ]; then
    echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
    exit 1
fi

# 10 fold CV Mouse 3' UTR (Seedless)
./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
    --dataset HITS-CLIP-3UTR-CV --model FN --train-iters 200 --region 3UTR --subsample-runs 5\
    crossvalidate \
    '/home/aghoshal/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_pos/*'\
    '/home/aghoshal/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_neg/*'\
    ~/Workspace/Academic/Research/Projects/micrornatargetprediction/data/HITS-CLIP/hits_clip_all_seed_enrichment.csv\
    /tmp/output_hits_clip_3utr_cv.txt > >(tee /tmp/output_hits_clip_3utr_cv.log) 2>&1
