#!/bin/bash
if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters"
    exit
fi


split -d -n l/10 $1 $2

for file in `ls ${2}*`
do
	mv "$file" "$file.csv"
done

i=4;
for file in `ls ${2}*`
do
	str=$3"@pdsl"$i".cs.purdue.edu";
    ssh $str "mkdir -p ~/Workspace/MicroRnaTargetPrediction/data/splits/$2"
	rsync -avz $file $str:~/Workspace/MicroRnaTargetPrediction/data/splits/$2/$file
    rm $file
	i=$[$i +1];
	echo "Setup file on $str";
done
echo "Finished splitting up file"

# Execute data generation command
file_ind=0;
for i in `seq 4 13`; do
	host="${3}@pdsl${i}.cs.purdue.edu";
    file="${2}0${file_ind}.csv"
    file_ind=$[$file_ind +1];
    echo "$host -> $file"
    cmd="sh -c 'cd ~/Workspace/MicroRnaTargetPrediction/code/src/python; export PYTHONPATH=\"\${PYTHONPATH}:./\"; nohup ../../../env/bin/python ../../test/python/generate_clip_data.py ~/Workspace/MicroRnaTargetPrediction/data/splits/$2/$file ~/Workspace/MicroRnaTargetPrediction/data/V-CLIP/vclip_micrornas.txt hg19 V-CLIP > ../../../data/data_gen_$file.log 2>&1 &'"
    echo $cmd
    ssh -n -f $host "$cmd"
done
