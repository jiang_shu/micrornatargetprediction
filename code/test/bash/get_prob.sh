if [ "$#" -ne 2 ]; then
    echo -e "${COL_RED}Usage: $0 <hdfs path> <output path>${COL_NONE}"
    exit 1
fi

HDFS_PATH="$1"
OUTPUT_PATH="$2"
HADOOP_PATH=/home/aghoshal/hadoop-2.5.1

LS_CMD="$HADOOP_PATH/bin/hdfs dfs -ls "

for file in `${LS_CMD} $HDFS_PATH | grep "prob" | sed -e "s/  */ /g" | cut -d" " -f8`; do
    echo -e "${COL_PURPLE}Processing path: $file ${COL_NONE}"
    for part in `${LS_CMD} ${file} | grep "part" | sed -e "s/  */ /g" | cut -d" " -f8`; do
        DST="${OUTPUT_PATH}/${file##*\/}_${part##*\/}"
        echo -e "${COL_GREEN}Copying $part to $DST ${COL_NONE}"
        $HADOOP_PATH/bin/hdfs dfs -copyToLocal $part $DST
    done
done


