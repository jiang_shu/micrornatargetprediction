#!/bin/bash
gamma[1]=0.001
gamma[2]=0.01
gamma[3]=0.0001
gamma[4]=0.01
gamma[5]=0.1
gamma[6]=0.01
gamma[7]=0.01
gamma[8]=0.1
gamma[9]=0.1
gamma[10]=0.1
for i in `seq 1 10`; do
    g=${gamma[$i]}
    ./bash/spark-submit.sh \
        $(pwd)/python/cv_clip_spark.py \
        --dataset V-CLIP-CLUSTER-${i} --model FN \
        --subsample-runs 1 --cluster-assignment $(pwd)/../../data/V-CLIP/cluster${i}.csv \
        crossvalidate \
        --model-params 20 1.0 0.001 'RBF' $g \
        --cv-runs 5 \
        '/home/aghoshal/microrna/input/V-CLIP/v_clip_mrna_mirna_features_pos/*' \
        '/home/aghoshal/microrna/input/V-CLIP/v_clip_mrna_mirna_features_neg/*' \
        '/home/asish/Workspace/Academic/Research/Projects/micrornatargetprediction/data/V-CLIP/v_clip_all_seed_enrichment.csv' \
        /tmp/vclip_cv_result_cluster_${i}.txt > >(tee /tmp/vclip_cv_result_cluster_${i}.log) 2>&1

    if [ "$?" -ne 0 ]; then
        echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
        exit 1
    fi
done

