#!/bin/bash

if [ "$?" -ne 0 ]; then
    echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
    exit 1
fi

# 10 fold CV Mouse CDS (Seedless)
./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
    --dataset HITS-CLIP-SEEDLESS-LINEAR-CV --model FN --subsample-runs 5 --train-iters 200 \
    crossvalidate \
    '/home/aghoshal/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_pos/*'\
    '/home/aghoshal/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_neg/*'\
    /home/asish/Workspace/Academic/Research/Projects/micrornatargetprediction/data/HITS-CLIP/hits_clip_all_seed_enrichment.csv\
    /tmp/output_hits_clip_seedless_cv.txt > >(tee /tmp/output_hits_clip_seedless_cv.log) 2>&1

if [ "$?" -ne 0 ]; then
    echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
    exit 1
fi

# 10 fold CV Human CDS (Seed)
./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
    --dataset V-CLIP-SEED-LINEAR-CV --model FN --subsample-runs 5 --train-iters 200 --seed-sites\
    crossvalidate \
    '/home/aghoshal/microrna/input/V-CLIP/v_clip_mrna_mirna_features_pos/*'\
    '/home/aghoshal/microrna/input/V-CLIP/v_clip_mrna_mirna_features_neg/*'\
    /home/asish/Workspace/Academic/Research/Projects/micrornatargetprediction/data/V-CLIP/v_clip_all_seed_enrichment.csv\
    /tmp/output_v_clip_seed_cv.txt > >(tee /tmp/output_v_clip_seed_cv.log) 2>&1

if [ "$?" -ne 0 ]; then
    echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
    exit 1
fi

# 10 fold CV Human CDS (Seed)
./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
    --dataset V-CLIP-SEEDLESS-LINEAR-CV --model FN --subsample-runs 5 --train-iters 200 \
    crossvalidate \
    '/home/aghoshal/microrna/input/V-CLIP/v_clip_mrna_mirna_features_pos/*'\
    '/home/aghoshal/microrna/input/V-CLIP/v_clip_mrna_mirna_features_neg/*'\
    /home/asish/Workspace/Academic/Research/Projects/micrornatargetprediction/data/V-CLIP/v_clip_all_seed_enrichment.csv\
    /tmp/output_v_clip_seedless_cv.txt > >(tee /tmp/output_v_clip_seedless_cv.log) 2>&1
