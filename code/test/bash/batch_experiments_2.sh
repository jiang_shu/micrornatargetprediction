#!/bin/bash

if [ "$?" -ne 0 ]; then
    echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
    exit 1
fi

# 10 fold CV Human CDS (Seedless)
./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
    --dataset V-CLIP-CDS-CV --model FN --train-iters 200 --region CDS\
    crossvalidate \
    '/home/aghoshal/microrna/input/V-CLIP/v_clip_mrna_mirna_features_pos/*'\
    '/home/aghoshal/microrna/input/V-CLIP/v_clip_mrna_mirna_features_neg/*'\
    /home/asish/Workspace/Academic/Research/Projects/micrornatargetprediction/data/V-CLIP/v_clip_all_seed_enrichment.csv\
    /tmp/output_v_clip_cds_cv.txt > >(tee /tmp/output_v_clip_cds_cv.log) 2>&1

if [ "$?" -ne 0 ]; then
    echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
    exit 1
fi

# 10 fold CV Human 5' UTR (Seedless)
./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
    --dataset V-CLIP-5UTR-CV --model FN --train-iters 200 --region 5UTR --subsample-runs 5\
    crossvalidate \
    '/home/aghoshal/microrna/input/V-CLIP/v_clip_mrna_mirna_features_pos/*'\
    '/home/aghoshal/microrna/input/V-CLIP/v_clip_mrna_mirna_features_neg/*'\
    /home/asish/Workspace/Academic/Research/Projects/micrornatargetprediction/data/V-CLIP/v_clip_all_seed_enrichment.csv\
    /tmp/output_v_clip_5utr_cv.txt > >(tee /tmp/output_v_clip_5utr_cv.log) 2>&1

if [ "$?" -ne 0 ]; then
    echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
    exit 1
fi

# 10 fold CV Human 3' UTR (Seedless)
./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
    --dataset V-CLIP-3UTR-CV --model FN --train-iters 200 --region 3UTR --subsample-runs 5\
    crossvalidate \
    '/home/aghoshal/microrna/input/V-CLIP/v_clip_mrna_mirna_features_pos/*'\
    '/home/aghoshal/microrna/input/V-CLIP/v_clip_mrna_mirna_features_neg/*'\
    /home/asish/Workspace/Academic/Research/Projects/micrornatargetprediction/data/V-CLIP/v_clip_all_seed_enrichment.csv\
    /tmp/output_v_clip_3utr_cv.txt > >(tee /tmp/output_v_clip_3utr_cv.log) 2>&1
