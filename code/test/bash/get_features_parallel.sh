#!/bin/bash

#python get_features_clip_new.py ../../../data/HITS-CLIP/hits_clip_mrna_mirna_1013_ip-_data.txt /tmp/seed_enrichment.csv ../../../data/HITS-CLIP/hits_clip_mrna_mirna_1013_ip-_features.txt mm9 | tee /tmp/log.txt

if [ "$#" -ne 1 ]; then
    echo "Usage: get_features_parallel <dataset>, where dataset is one of HITS-CLIP/V-CLIP"
    exit
fi

DATASET=$1
if [[ "$DATASET" != HITS-CLIP* ]] && [ "$DATASET" != "V-CLIP" ]; then
    echo "Dataset should either be HITS-CLIP* or V-CLIP"
    exit 1
fi

if [[ "$DATASET" == HITS-CLIP* ]]; then
    ASSEMBLY="mm9"
else
    ASSEMBLY="hg19"
fi

echo "Using Assembly: $ASSEMBLY"

CODE_PATH="~/Workspace/MicroRnaTargetPrediction/code/src/python"
DATA_PATH="../../../data/${DATASET}"

for i in `seq 4 13`; do
	host="aghoshal@pdsl${i}.cs.purdue.edu";
    DATA_FILE="$DATA_PATH/mrna_mirna_data.txt"
    OUTPUT_FILE="$DATA_PATH/mrna_mirna_features.txt"
    LOG_FILE="$DATA_PATH/features_gen.log"

    echo "Starting feature generation on $host for dataset $DATASET" 

    CMD="cd $CODE_PATH; export PYTHONPATH=\"\${PYTHONPATH}:./\"; nohup ../../../env/bin/python get_features_clip_new.py $DATA_FILE /dev/null $OUTPUT_FILE $ASSEMBLY > $LOG_FILE 2>&1 &"

    ssh -n -f $host "sh -c '$CMD'"
done
