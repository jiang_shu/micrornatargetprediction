#!/bin/bash

# 10 fold CV Human 3' UTR (Seed)
#./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
#    --dataset V-CLIP-3UTR-SEED --model FN --train-iters 200 --region 3UTR --seed-sites\
#    crossvalidate \
#    '/microrna/input/V-CLIP/v_clip_mrna_mirna_features_pos/*'\
#    '/microrna/input/V-CLIP/v_clip_mrna_mirna_features_neg/*'\
#    ~/Workspace/Academic/Research/Projects/micrornatargetprediction/data/V-CLIP/v_clip_all_seed_enrichment.csv\
#    /tmp/output_vclip_cv_3utr_seed.txt > >(tee /tmp/output_vclip_cv_3utr_seed.log) 2>&1

if [ "$?" -ne 0 ]; then
    echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
    exit 1
fi

# 10 fold CV Mouse 3' UTR (Seed)
#./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
#    --dataset HITS-CLIP-3UTR-SEED --model FN --train-iters 200 --region 3UTR --seed-sites\
#    crossvalidate \
#    '/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_pos/*'\
#    '/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_neg/*'\
#    ~/Workspace/Academic/Research/Projects/micrornatargetprediction/data/HITS-CLIP/hits_clip_all_seed_enrichment.csv\
#    /tmp/output_hits_clip_cv_3utr_seed.txt > >(tee /tmp/output_hits_clip_cv_3utr_seed.log) 2>&1

if [ "$?" -ne 0 ]; then
    echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
    exit 1
fi

# Train on Mouse and Predict on Human 3' UTR (Seed)
#./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
#    --dataset V-CLIP-TEST-3UTR-SEED --model FN --train-iters 200  --region 3UTR --seed-sites\
#    predict \
#    '/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_pos/*'\
#    '/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_neg/*'\
#    '/microrna/input/V-CLIP/v_clip_mrna_mirna_features_pos/*'\
#    '/microrna/input/V-CLIP/v_clip_mrna_mirna_features_neg/*'\
#    ~/Workspace/Academic/Research/Projects/micrornatargetprediction/data/HITS-CLIP/hits_clip_common_seed_enrichment.csv\
#    /tmp/output_train_hits_clip_test_vclip_3utr_seed.txt > >(tee /tmp/output_train_hits_clip_test_vclip_3utr_seed.log) 2>&1

if [ "$?" -ne 0 ]; then
    echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
    exit 1
fi

# Train on Human 3' UTR (Seed) and predict on Mouse 3' UTR (Seed)
#./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
#    --dataset HITS-CLIP-TEST-3UTR-SEED --model FN --train-iters 200  --region 3UTR --seed-sites\
#    predict \
#    '/microrna/input/V-CLIP/v_clip_mrna_mirna_features_pos/*'\
#    '/microrna/input/V-CLIP/v_clip_mrna_mirna_features_neg/*'\
#    '/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_pos/*'\
#    '/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_neg/*'\
#    ~/Workspace/Academic/Research/Projects/micrornatargetprediction/data/V-CLIP/v_clip_common_seed_enrichment.csv\
#    /tmp/output_train_vclip_test_hits_clip_3utr_seed.txt > >(tee /tmp/output_train_vclip_test_hits_clip_3utr_seed.log) 2>&1


# 10 fold CV Mouse (Seed)
./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
    --dataset HITS-CLIP-SEED-CV --model FN --train-iters 200 --seed-sites\
    crossvalidate \
    '/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_pos/*'\
    '/microrna/input/HITS-CLIP/hits_clip_mrna_mirna_features_neg/*'\
    ~/Workspace/Academic/Research/Projects/micrornatargetprediction/data/HITS-CLIP/hits_clip_all_seed_enrichment.csv\
    /tmp/output_hits_clip_cv_seed.txt > >(tee /tmp/output_hits_clip_cv_seed.log) 2>&1

if [ "$?" -ne 0 ]; then
    echo -e "$COL_RED !!! FAILED !!! $COL_NONE"
    exit 1
fi

# 10 fold CV Human 3' UTR (Seed)
./bash/spark-submit.sh $(pwd)/python/cv_clip_spark.py \
    --dataset V-CLIP-SEED-CV --model FN --train-iters 200 --seed-sites\
    crossvalidate \
    '/microrna/input/V-CLIP/v_clip_mrna_mirna_features_pos/*'\
    '/microrna/input/V-CLIP/v_clip_mrna_mirna_features_neg/*'\
    ~/Workspace/Academic/Research/Projects/micrornatargetprediction/data/V-CLIP/v_clip_all_seed_enrichment.csv\
    /tmp/output_vclip_cv_seed.txt > >(tee /tmp/output_vclip_cv_seed.log) 2>&1
