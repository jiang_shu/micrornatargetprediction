'''
    Generate the following features for a mrna:
        <mrna> <5'utr range> <3' range> <cds range>
        <conservation seq>
'''
import sys
import numpy as np
from batch_gzip_file_reader import BatchGZipFileReader


ASSEMBLY = 'mm9'
PHASTCONS_PATH = '../../../data/phast_cons/' + ASSEMBLY
FILE_SUFFIX = {'mm9': 'data',
               'hg18': 'phastCons44way.wigFix'}


def get_chromosome_consv_seq(chromosome):
    print "Getting conservation sequence for chromosome: ", chromosome
    fname = "%s/%s.%s.gz" % (PHASTCONS_PATH, chromosome, FILE_SUFFIX[ASSEMBLY])
    consv = {}
    current_stretch = []
    count = 0
    start_ind = None
    next_start = None
    buff = []
    reader = BatchGZipFileReader(fname, 10000)
    remaining = True
    while remaining:
        remaining = reader.read(buff)
        for line in buff:
            line = line.strip()
            if line.startswith("fixedStep"):
                line = line.split()
                next_start = int(line[2].split("=")[1])
                if not start_ind:
                    start_ind = next_start
                    continue
                if count + start_ind + 1 != next_start:
                    consv[(start_ind, start_ind + count)] =\
                        np.array(current_stretch)
                    del current_stretch[:]
                    count = 0
                    start_ind = next_start
            else:
                current_stretch.append(float(line))
                count += 1
        del buff[:]
    if count > 0:
        consv[(start_ind, start_ind + count)] =\
            np.array(current_stretch)

    return consv


def get_consv_for_range(start, end, consv):
    consv_range = []
    last_end = start
    found_end = False
    for key in sorted(consv.keys()):
        if start >= key[0] and end <= key[1]:
            # (start, end) is completely with a bucket
            consv_range = consv[key][start - key[0]: end - key[0]]
            found_end = True
            return consv_range
        elif start >= key[0] and start <= key[1] and end > key[1]:
            # start within bucket but end is outside.
            consv_range.append(consv[key][start - key[0]:])
            last_end = key[1]
        elif end >= key[0] and end <= key[1] and start < key[0]:
            # end is within bucket but start is outside.
            found_end = True
            consv_range.append(np.zeros(key[0] - last_end))
            consv_range.append(consv[key][:end - key[0]])
            break
        elif key[0] > start and key[1] < end:
            consv_range.append(np.zeros(key[0] - last_end))
            consv_range.append(consv[key])
            last_end = key[1]
    if not found_end:
        consv_range.append(np.zeros(end - last_end))
    if consv_range:
        consv_range = np.hstack(consv_range)
        return consv_range
    return None


def write_features(fname, output_fname, header):
    with open(fname, 'rU') as ifp, open(output_fname, 'w') as ofp:
        current_chrom = None
        consv = None
        for line in ifp:
            line = line.strip().split("\t")
            chrom = line[header['chrom']]
            if chrom != current_chrom:
                current_chrom = chrom
                consv = get_chromosome_consv_seq(current_chrom)
            mrna_name = line[header['name']]
            strand = line[header['strand']]
            cdsStart, cdsEnd = int(line[header['cdsStart']]),\
                int(line[header['cdsEnd']])
            exonStarts, exonEnds = line[header['exonStarts']],\
                line[header['exonEnds']]
            exonStarts = [int(x) for x in exonStarts.strip(",").split(",")]
            exonEnds = [int(x) for x in exonEnds.strip(",").split(",")]
            mrna_consv = []
            exon_length = 0
            utr5_end, utr3_start = 0, 0

            for s, e in zip(exonStarts, exonEnds):
                consv_range = get_consv_for_range(s, e, consv)
                if consv_range is not None:
                    mrna_consv.append(consv_range)
                if s <= cdsStart:
                    if e <= cdsStart:
                        utr5_end += e - s
                    else:
                        utr5_end += cdsStart - s
                if cdsEnd <= e:
                    if cdsEnd <= s:
                        utr3_start += e - s
                    else:
                        utr3_start += e - cdsEnd
                exon_length += e - s

            utr5_range = (0, utr5_end)
            utr3_range = (exon_length - utr3_start, exon_length)
            cds_range = (utr5_range[1], utr3_range[0])

            if strand == '-':
                utr5_range = (0, utr3_start)
                utr3_range = (exon_length - utr5_end, exon_length)
                cds_range = (exon_length - cds_range[1],
                             exon_length - cds_range[0])
            if mrna_consv:
                mrna_consv = np.hstack(mrna_consv)
            else:
                print "WARNING: Length of conserved region, 0, doesn't match %d"\
                    " for mrna: %s" %\
                    (exon_length, mrna_name)
                continue
            if np.size(mrna_consv) != exon_length:
                print "WARNING: Length of conserved region, %d, doesn't match %d"\
                    " for mrna: %s" %\
                    (np.size(mrna_consv), exon_length, mrna_name)
            ofp.write("%s,%d-%d,%d-%d,%d-%d,%s\n" %
                      (mrna_name, utr5_range[0], utr5_range[1],
                       utr3_range[0], utr3_range[1],
                       cds_range[0], cds_range[1],
                       " ".join([str(x) for x in mrna_consv])
                       )
                      )


def main():
    header_file = '../../../data/mouse/mm9_ref_gene_header'
    fname = '../../../data/mouse/mm9_ref_genes'
    header = {}
    with open(header_file, 'rU') as fp:
        line = fp.readline()
        line = line.strip().split("\t")
        for i in range(len(line)):
            header[line[i]] = i

    output_fname = '../../../data/mouse/mrna_mm9_features.txt'
    write_features(fname, output_fname, header)


if __name__ == '__main__':
    sys.exit(main())
