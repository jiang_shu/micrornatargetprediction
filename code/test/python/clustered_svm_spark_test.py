import sys
import numpy as np
from clustered_svm_spark import ClusterSvmModel
from pyspark import SparkContext
from record import Record
import time
import matplotlib.pyplot as plt
from tempfile import NamedTemporaryFile


DEFAULT_COL_MAP = {1: 'r', 0: 'b'}
SVM_NUM_PARTITIONS = 10
RDD_PARTITIONS = 72


def update_error(l, p, error, total):
    e = 1 if l != p else 0
    error.add(e)
    total.add(1)


def get_misclassification_error(sc, model, rdd):
    rdd = model.predict(rdd)
    misclassified = sc.accumulator(0)
    total = sc.accumulator(1)
    rdd.foreach(lambda (x, p): update_error(x.label(), p, misclassified, total))
    return misclassified.value/float(total.value)


def label_func_1(x):
    return 1.0 if x[0]**2 - 20 > 0 else 0.0


def label_func_2(x):
    return 1.0 if (x[0] - 20)**2 + (x[1] - 20)**2 <= 4 else 0.0


def label_func_3(x):
    return 1.0 if np.abs(x[0] + 10) + np.abs(x[1] + 10) <= 3 else 0.0


def get_labelled_data(label_func, cluster_id,
                      m=np.array([0.0, 0.0]), s=1.0):
    data = np.random.randn(60000, 2)
    data *= s
    data += m
    labels = [label_func(x) for x in data]
    labelled_data = []
    count = 0
    for l, d in zip(labels, data):
        labelled_data.append(Record(d, label=l, record_id=(cluster_id, count)))
        count += 1
    return labelled_data


def plot_data(data, col_map=DEFAULT_COL_MAP):
    colors = [col_map[int(x.label())] for x in data]
    area = np.pi * 4
    data_arr = np.vstack(data)
    plt.scatter(data_arr[:, 0], data_arr[:, 1], c=colors, s=area, alpha=0.6)


def generate_training_data(sc, plot=False):
    labeled_data_1 = get_labelled_data(label_func_1, 0, s=5)
    labeled_data_2 = get_labelled_data(label_func_2, 1,
                                       m=np.array([20, 20]), s=2)

    labeled_data_3 = get_labelled_data(label_func_3, 2, m=np.array([-10, -10]))

    labeled_data = labeled_data_1 + labeled_data_2 + labeled_data_3
    labeled_data_rdd = sc.parallelize(labeled_data, RDD_PARTITIONS)

    if plot is True:
        temp_file = NamedTemporaryFile(mode='w', prefix='cluster_svm_test_',
                                       suffix='.pdf', delete=False)
        plot_data(labeled_data_1)
        plot_data(labeled_data_2, col_map={1: 'g', 0: 'y'})
        plot_data(labeled_data_3, col_map={1: 'm', 0: 'k'})
        plt.savefig(temp_file, format='pdf', bbox_inches='tight')
        print "Saved plot to:", temp_file.name

    return labeled_data_rdd


def cluster_assignment_func(x):
    return x.rid()[0]


def main():
    use_threads = False

    if len(sys.argv) >= 2 and sys.argv[1] == 'use_threads':
        print "Using threads"
        use_threads = True

    sc = SparkContext(appName="Cluster SVM Test")
    model = ClusterSvmModel(3, cluster_assignment_func, gamma=0.1)
    model1 = ClusterSvmModel(3, cluster_assignment_func, gamma=0.1)

    rdd = generate_training_data(sc, plot=False)
    rdd = rdd.cache()
    t1 = time.time()
    model.train(sc, rdd, num_partitions=SVM_NUM_PARTITIONS,
                use_threads=use_threads)
    t2 = time.time()
    training_time = (t2 - t1)

    t1 = time.time()
    error = get_misclassification_error(sc, model, rdd)
    t2 = time.time()
    pred_time = t2 - t1
    rdd.unpersist()

    print "Training time: %.3f" % training_time
    print "Prediction time: %.3f" % pred_time
    print "Training error:", error

if __name__ == '__main__':
    sys.exit(main())
