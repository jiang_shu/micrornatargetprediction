import sys
import numpy as np
from sklearn.cross_validation import KFold
from train import train
from predict import predict
import time
from set_model_params import set_model_params


def usage():
    print '''%s <postive mfe file> <positive mfe_open file> <positive seed file>
        <negative mfe file> <negative mfe_open file> <negative seed file>
    ''' % sys.argv[0]


def compute_performance(y_actual, y_pred):
    ''' Compute accuracy and recall '''
    pos_pred = np.where(y_pred == 1)[0]
    actual_pos = np.where(y_actual == 1)[0]
    pos_matches = np.intersect1d(pos_pred,
                                 actual_pos)
    accuracy = 0
    if np.size(pos_pred) > 0:
        accuracy = float(np.size(pos_matches))/float(np.size(pos_pred))
    recall = float(np.size(pos_matches))/float(np.size(actual_pos))
    return (accuracy, recall)


def crossvalidate(pos_mfe, pos_mfe_open, pos_seed,
                  neg_mfe, neg_mfe_open, neg_seed):

    set_model_params()
    len_pos = np.size(pos_mfe, 0)
    len_neg = np.size(neg_mfe, 0)
    pos_y = np.repeat(1, len_pos)
    neg_y = np.repeat(-1, len_neg)
    pos_kf = KFold(len_pos, 10)
    neg_kf = KFold(len_neg, 10)
    pos_ind = [(k, v) for k, v in pos_kf]
    neg_ind = [(k, v) for k, v in neg_kf]
    acc = []
    rec = []
    for ind in zip(pos_ind, neg_ind):
        pos_train_ind = ind[0][0]
        pos_test_ind = ind[0][1]
        neg_train_ind = ind[1][0]
        neg_test_ind = ind[1][1]

        print "Beginning training."
        t1 = time.time()

        (svm_model, pca_obj) = train(pos_mfe[pos_train_ind],
                                     pos_mfe_open[pos_train_ind],
                                     pos_seed[pos_train_ind],
                                     neg_mfe[neg_train_ind],
                                     neg_mfe_open[neg_train_ind],
                                     neg_seed[neg_train_ind])

        t2 = time.time()
        print "Done (%f sec)." % (t2 - t1)

        test_mfe = np.vstack((pos_mfe[pos_test_ind], neg_mfe[neg_test_ind]))
        test_mfe_open = np.vstack((pos_mfe_open[pos_test_ind],
                                   neg_mfe_open[neg_test_ind]))
        test_seed = np.vstack((pos_seed[pos_test_ind], neg_seed[neg_test_ind]))

        print "Beginning prediction."
        t1 = time.time()

        y_pred = predict(test_mfe, test_mfe_open, test_seed, svm_model, pca_obj)
        y_actual = np.hstack((pos_y[pos_test_ind], neg_y[neg_test_ind]))
        accuracy, recall = compute_performance(y_actual, y_pred)

        t2 = time.time()
        print "Done (%f sec)." % (t2 - t1)

        print "Accuracy = %f, Recall = %f" % (accuracy, recall)
        acc.append(accuracy)
        rec.append(recall)

    acc = np.array(acc)
    rec = np.array(rec)
    print "Mean Accuracy = %f, Mean Recall = %f" % (np.mean(acc),
                                                    np.mean(rec))


def main():
    if len(sys.argv) != 7:
        usage()
        return 1

    print "Reading data from file"
    t1 = time.time()
    pos_mfe = np.genfromtxt(sys.argv[1], delimiter=',')
    pos_mfe_open = np.genfromtxt(sys.argv[2], delimiter=',')
    pos_seed = np.genfromtxt(sys.argv[3], delimiter=',')
    neg_mfe = np.genfromtxt(sys.argv[4], delimiter=',')
    neg_mfe_open = np.genfromtxt(sys.argv[5], delimiter=',')
    neg_seed = np.genfromtxt(sys.argv[6], delimiter=',')
    t2 = time.time()
    print "Done (%f sec)." % (t2 - t1)
    print "Positive data: ", pos_mfe.shape
    print "Negative data: ", neg_mfe.shape
    crossvalidate(pos_mfe, pos_mfe_open, pos_seed,
                  neg_mfe, neg_mfe_open, neg_seed)


if __name__ == '__main__':
    sys.exit(main())
