"""
    Given a positive and negative dataset. Computes
    the Functional Principal Components for each of the functional
    features.
"""
from fpca_spark import FPCA
from pyspark import SparkContext
import sys
import pickle
import numpy as np


OFFSET = 5
INDEX = dict(delg_site=0, delg_seed=1,
             deldelg_site=2, deldelg_seed=3,
             au_site=4, au_seed=5)
RDD_PARTITIONS = 80
RANDOM_SEED = 13


def parse_str_array(str_arr, delimiter=" ", conv_func=float):
    a = np.array([conv_func(x) for x in str_arr.split(delimiter)])
    return np.nan_to_num(a)


def preprocess_data(rdd):
    rdd = rdd.map(lambda x: x.strip().split(",")[OFFSET: (OFFSET + 6)])
    rdd = rdd.map(lambda x: [parse_str_array(y) for y in x])
    return rdd


def get_fpca_model(rdd, key):
    if key not in INDEX:
        raise Exception("Wrong key %s" % key)
    data_rdd = rdd.map(lambda x: x[INDEX[key]])
    model = FPCA()
    model.fit(data_rdd, compute_explained_variance_ratio=True)
    return model


def compute_fpca(sc, pos_file, neg_file, output_file):
    '''
        Computes Functional principal components.
        @sc: SparkContext
        @pos_file: path to positive file
        @neg_file: path to negative file
        @output_file: path to file where the functional PCA models are written
        to.
    '''
    pos_file = pos_file
    neg_file = neg_file
    pos_rdd = sc.textFile(pos_file)
    neg_rdd = sc.textFile(neg_file)
    pos_count = pos_rdd.count()
    neg_count = neg_rdd.count()
    print "Positive count:", pos_count
    print "Negative count:", neg_count
    print "Sampling from negative dataset."
    sample_frac = pos_count / float(neg_count)
    neg_rdd = neg_rdd.sample(False, sample_frac, RANDOM_SEED)
    data_rdd = pos_rdd.union(neg_rdd)
    data_rdd = preprocess_data(data_rdd)
    data_rdd = data_rdd.repartition(RDD_PARTITIONS)
    data_rdd = data_rdd.cache()

    for key in ['delg_site', 'delg_seed', 'deldelg_site', 'deldelg_seed',
                'au_site', 'au_seed']:

        fname = output_file + ".%s" % key
        print "Writing model for %s to %s" % (key, fname)
        with open(fname, 'w') as ofp:
            model = get_fpca_model(data_rdd, key)
            pickle.dump(model, ofp)
        print "Done."

    print "All done"


def main():
    if len(sys.argv) != 4:
        print "Usage: %s <positive file> <negative file> <output_file>" %\
            sys.argv[0]
        return 1

    sc = SparkContext(appName="Functional PCA")
    compute_fpca(sc, sys.argv[1], sys.argv[2], sys.argv[3])
    return 0


if __name__ == '__main__':
    sys.exit(main())
