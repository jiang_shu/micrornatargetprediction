import sys
import numpy as np
from pipeline import Pipeline
import preprocess
from parallelize import run_func_parallely
from sklearn.cross_validation import KFold
from train import train
from predict import predict
import time
from set_model_params import set_model_params


def usage():
    print '''%s <postive mfe file> <positive mfe_open file> <positive seed file>
        <negative mfe file> <negative mfe_open file> <negative seed file>
    ''' % sys.argv[0]


def compute_performance(y_actual, y_pred):
    ''' Compute accuracy and recall '''
    pos_pred = np.where(y_pred == 1)[0]
    actual_pos = np.where(y_actual == 1)[0]
    pos_matches = np.intersect1d(pos_pred,
                                 actual_pos)
    accuracy = 0
    if np.size(pos_pred) > 0:
        accuracy = float(np.size(pos_matches))/float(np.size(pos_pred))
    recall = float(np.size(pos_matches))/float(np.size(actual_pos))
    return (accuracy, recall)


def preprocess_data(data_file, len_atleast=None, len_atmost=None):
    print "Reading data from file"
    with open(data_file, 'rU') as fp:
        data = fp.readlines()
    print "Done."

    if len_atmost == 'max_len':
        pipeline = (Pipeline())\
            .add_step(preprocess.remove_unwanted_chars, delimiter=',')\
            .add_step(preprocess.get_slice,
                      from_index=4, delimiter=' ')\
            .add_step(lambda x: len(x))
        data_len = run_func_parallely(pipeline.execute,
                                      data,
                                      kwparams={'for_each': True},
                                      combine_func=lambda l: [x for sublist in l
                                                              for x in sublist])
        max_data_len = max(data_len)
        print "Maximum data length is ", max_data_len
    else:
        max_data_len = len_atmost

    pipeline = (Pipeline())\
        .add_step(preprocess.remove_unwanted_chars, delimiter=',')\
        .add_step(preprocess.get_slice,
                  from_index=4, delimiter=' ', as_numeric=True)\
        .add_step(preprocess.filter_by_length, atleast=len_atleast,
                  atmost=max_data_len)\
        .add_step(preprocess.fit_length, desired_len=max_data_len)

    print "Preprocessing data."
    t1 = time.time()
    data = run_func_parallely(pipeline.execute,
                              data,
                              kwparams={'for_each': True})
    t2 = time.time()
    print "Done (%f sec)." % (t2 - t1)
    return data


def crossvalidate(pos_mfe, pos_mfe_open, pos_seed,
                  neg_mfe, neg_mfe_open, neg_seed):
    set_model_params(num_basis=40, num_principal_components=5)
    len_pos = np.size(pos_mfe, 0)
    len_neg = np.size(neg_mfe, 0)
    pos_y = np.repeat(1, len_pos)
    neg_y = np.repeat(-1, len_neg)
    pos_kf = KFold(len_pos, 10)
    neg_kf = KFold(len_neg, 10)
    pos_ind = [(k, v) for k, v in pos_kf]
    neg_ind = [(k, v) for k, v in neg_kf]
    acc = []
    rec = []
    for ind in zip(pos_ind, neg_ind):
        pos_train_ind = ind[0][0]
        pos_test_ind = ind[0][1]
        neg_train_ind = ind[1][0]
        neg_test_ind = ind[1][1]

        print "Beginning training."
        t1 = time.time()

        (svm_model, pca_obj) = train(pos_mfe[pos_train_ind],
                                     pos_mfe_open[pos_train_ind],
                                     pos_seed[pos_train_ind],
                                     neg_mfe[neg_train_ind],
                                     neg_mfe_open[neg_train_ind],
                                     neg_seed[neg_train_ind])

        t2 = time.time()
        print "Done (%f sec)." % (t2 - t1)

        test_mfe = np.vstack((pos_mfe[pos_test_ind], neg_mfe[neg_test_ind]))
        test_mfe_open = np.vstack((pos_mfe_open[pos_test_ind],
                                   neg_mfe_open[neg_test_ind]))
        test_seed = np.vstack((pos_seed[pos_test_ind], neg_seed[neg_test_ind]))

        print "Beginning prediction."
        t1 = time.time()

        y_pred = predict(test_mfe, test_mfe_open, test_seed, svm_model, pca_obj)
        y_actual = np.hstack((pos_y[pos_test_ind], neg_y[neg_test_ind]))
        accuracy, recall = compute_performance(y_actual, y_pred)

        t2 = time.time()
        print "Done (%f sec)." % (t2 - t1)

        print "Accuracy = %f, Recall = %f" % (accuracy, recall)
        acc.append(accuracy)
        rec.append(recall)

    acc = np.array(acc)
    rec = np.array(rec)
    print "Mean Accuracy = %f, Mean Recall = %f" % (np.mean(acc),
                                                    np.mean(rec))


def main():
    if len(sys.argv) != 7:
        usage()
        return 1

    print "Reading data from file"
    t1 = time.time()
    atmost = 1000
    pos_mfe = preprocess_data(sys.argv[1], len_atmost=atmost)
    pos_mfe_open = preprocess_data(sys.argv[2], len_atmost=atmost)
    pos_seed = preprocess_data(sys.argv[3], len_atmost=atmost)
    neg_mfe = preprocess_data(sys.argv[4], len_atmost=atmost)
    neg_mfe_open = preprocess_data(sys.argv[5], len_atmost=atmost)
    neg_seed = preprocess_data(sys.argv[6], len_atmost=atmost)
    t2 = time.time()
    print "Done (%f sec)." % (t2 - t1)
    print "Positive data: ", pos_mfe.shape
    print "Negative data: ", neg_mfe.shape
    crossvalidate(pos_mfe, pos_mfe_open, pos_seed,
                  neg_mfe, neg_mfe_open, neg_seed)


if __name__ == '__main__':
    sys.exit(main())
