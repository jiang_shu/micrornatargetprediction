'''
This reads validated mrna-mirna pairs
and emits mrna and mirna that are not paired.
'''
import sys


def main():
    data = set([])
    all_mrna = set([])
    all_mirna = set([])
    with open('../../../data/validated_targets.txt', 'rU') as fp:
        for line in fp:
            (mrna, mirna) = line.strip().split(",")
            data.add((mrna, mirna))
            all_mrna.add(mrna)
            all_mirna.add(mirna)
    with open('../../../data/negative_targets.txt', 'w') as fp:
        for mrna in all_mrna:
            for mirna in all_mirna:
                if (mrna, mirna) not in data:
                    fp.write("%s,%s\n" % (mrna, mirna))


if __name__ == '__main__':
    sys.exit(main())
