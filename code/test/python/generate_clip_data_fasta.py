'''
    From clip tag locations in mrna. Generate data for mrna-mirna pair.
'''
from pipeline import Pipeline
from parallelize import run_func_parallely
from batch_fasta_file_reader import BatchFastaFileReader
from batch_file_reader import BatchFileReader
from redisdao import RedisDao
from functools import partial
import subprocess
import re
import sys


OUTPUT_PATH = '../../../data/'
ERR_MISSING_MRNA = -1
ERR_MISSING_SEQ = -2
# RNAhybrid executabe
RNAHYBRID = "../../third-party/bin/RNAhybrid"


def usage():
    print "%s <clip data (fasta)> <microRNA file> <dataset name>" % sys.argv[0]


def invoke_rna_hybrid(mrna_seq, mirna_seq,
                      energy_threshold=None,
                      helix_constraint="-f 2,7"):
    executable = RNAHYBRID
    # TODO: Change the dataset type (3utr_human). But do we need to change
    # if we are not interested in the p-value?
    args = "-c -b 1 -s 3utr_human"
    if helix_constraint:
        args += " " + helix_constraint
    if energy_threshold:
        args += " -e %0.3f" % energy_threshold
    cmd = "%s %s %s %s" % (executable, args, mrna_seq, mirna_seq)
    output = subprocess.check_output(cmd, shell=True)
    return output


def parse_rna_hybrid_output(outputs):
    # Parse output to get the following features:
    #   Mean free energy (mfe) of duplex.
    #   Start and end position of match
    mfe_list = []
    loc_list = []
    for output in outputs.split('\n'):
        output = output.strip()
        output = output.split(":")
        if not output or not output[0]:
            continue
        mfe = float(output[4])
        if not mfe < 0.0:
            continue
        start = int(output[6])
        match_len = len(output[7])
        end = start + match_len
        mfe_list.append(mfe)
        loc_list.append((start, end))

    return (mfe_list, loc_list)


def is_overlapping(loc, other_loc):
    ''' Returns True of @loc overlaps @other_loc '''
    if loc[0] <= other_loc[1] and loc[0] >= other_loc[0]:
        return True
    elif loc[1] <= other_loc[1] and loc[1] >= other_loc[0]:
        return True
    return False


def contains(loc, other_loc):
    ''' Returns True of @loc is contained within @other_loc '''
    if loc[0] >= other_loc[0] and loc[1] <= other_loc[1]:
        return True
    return False


def get_labeled_target_locations(micro_rna, mrna=None, clip_locations=None):
    '''
        Gets potential target locations in @mrna due to @micro_rna
        If the locations overlap a clip location in clip_locations then
        return 1 for that location otherwise return 0 for that location
    '''
    potential_locations = []
    output = invoke_rna_hybrid(mrna._seq, micro_rna._seq,
                               energy_threshold=-15.0,
                               helix_constraint=None)
    _, locations = parse_rna_hybrid_output(output)
    potential_locations += locations
    output = invoke_rna_hybrid(mrna._seq, micro_rna._seq)
    _, locations = parse_rna_hybrid_output(output)
    potential_locations += locations
    labeled_locations = []

    for loc in potential_locations:
        label = 0
        for other_loc in clip_locations:
            if contains(loc, other_loc):
                label = 1
                break
        labeled_locations.append((loc, label))
    return labeled_locations


def get_location(dao, seq_record):
    name = seq_record.name
    seq = str(seq_record.seq)
    seq = re.sub("T", "U", seq)
    name = name.strip().split("|")
    mrna_name = name[0].split('.')[0]

    mrna = dao.get_messenger_rna_from_name(mrna_name, assembly="hg18")
    if not mrna:
        print "%s not found" % mrna_name
        return ERR_MISSING_MRNA

    start = mrna._seq.find(seq)

    if start < 0:
        print "Sequence not found in %s" % mrna_name
        return ERR_MISSING_SEQ

    end = start + 40
    return (mrna, (start, end))


def get_mrna_clip_locations(clip_fasta_file):
    dao = RedisDao()
    reader = BatchFastaFileReader(clip_fasta_file, 1000)
    mrna_clip_locations = {}

    records = []
    missing_sequence = 0
    missing_mrna = 0
    # Read fasta records
    while reader.read(records):
        for seq_record in records:
            ret = get_location(dao, seq_record)
            if ret == ERR_MISSING_MRNA:
                missing_mrna += 1
                continue
            elif ret == ERR_MISSING_SEQ:
                missing_sequence += 1
                continue

            mrna, location = ret[0], ret[1]
            if mrna in mrna_clip_locations:
                mrna_clip_locations[mrna].append(location)
            else:
                mrna_clip_locations[mrna] = [location]
        del records[:]

    for seq_record in records:
        ret = get_location(dao, seq_record)
        if ret == ERR_MISSING_MRNA:
            missing_mrna += 1
            continue
        elif ret == ERR_MISSING_SEQ:
            missing_sequence += 1
            continue

        mrna_name, location = ret[0], ret[1]
        if mrna in mrna_clip_locations:
            mrna_clip_locations[mrna].append(location)
        else:
            mrna_clip_locations[mrna] = [location]
    del records[:]

    print "Missing mRNAs: ", missing_mrna
    print "Missing sequences: ", missing_sequence

    return mrna_clip_locations


def generate_data(micro_rnas, mrna_clip_locations, dataset):
    output_fname = "%s/clip_%s.txt" % (OUTPUT_PATH, dataset)
    with open(output_fname, 'w') as fp:
        for mrna in mrna_clip_locations.keys():
            func = partial(get_labeled_target_locations,
                           mrna=mrna,
                           clip_locations=mrna_clip_locations[mrna])
            func.__name__ = 'generate_data'
            pipeline = (Pipeline())\
                .add_step(func)
            labeled_locs = run_func_parallely(
                pipeline.execute,
                micro_rnas,
                kwparams={'for_each': True},
                combine_func=lambda l: [x for sublist in l
                                        for x in sublist])
            if len(labeled_locs) != len(micro_rnas):
                raise Exception("Expected len: %d, got %d" % (len(micro_rnas,
                                                                  labeled_locs)
                                                              ))
            for micro_rna, labeled_loc in zip(micro_rnas, labeled_locs):
                for loc in labeled_loc:
                    fp.write('%s,%s,%d-%d,%d\n' % (mrna, micro_rna,
                                                   loc[0][0],
                                                   loc[0][1],
                                                   loc[1])
                             )


def main():
    if len(sys.argv) != 4:
        usage()
        return 1

    clip_fasta_file = sys.argv[1]
    microrna_file = sys.argv[2]
    dataset = sys.argv[3]

    mrna_clip_locations = get_mrna_clip_locations(clip_fasta_file)
    reader = BatchFileReader(microrna_file)
    micro_rna_names = []
    reader.read(micro_rna_names)
    micro_rnas = []
    dao = RedisDao()
    for name in micro_rna_names:
        name = name.strip()
        micro_rna = dao.get_micro_rna_from_name(name)
        if micro_rna:
            micro_rnas.append(micro_rna)

    generate_data(micro_rnas, mrna_clip_locations, dataset)


if __name__ == '__main__':
    sys.exit(main())
