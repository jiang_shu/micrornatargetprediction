import unittest
import numpy as np
from preprocess_spark import filter_rows
from functools import partial
from record import Record


SITE_REGION_TYPES = ['3UTR', '5UTR', 'CDS']


def generate_data_row(seed_site=False, with_nan=False, data_mean=0.0,
                      label=None, record_id=None):

    delg = [np.random.randn(27) + data_mean, np.random.randn(27) + data_mean]
    deldelg = [np.random.randn(27) + data_mean, np.random.randn(27) + data_mean]
    au = [np.random.randn(27) + data_mean, np.random.randn(27) + data_mean]
    if with_nan is True:
        delg[0][np.random.randint(0, 27, 13)] = np.nan
        delg[1][np.random.randint(0, 27, 13)] = np.nan
        deldelg[0][np.random.randint(0, 27, 13)] = np.nan
        deldelg[1][np.random.randint(0, 27, 13)] = np.nan
        au[0][np.random.randint(0, 27, 13)] = np.nan
        au[1][np.random.randint(0, 27, 13)] = np.nan

    consv = np.random.rand(3)
    if not seed_site:
        consv[1], consv[2] = np.nan, np.nan
    rel_site_loc, seed_enrichment = np.random.rand(2)

    site_region = SITE_REGION_TYPES[np.random.randint(0, 3)]
    site_len = np.random.randint(22, 40)
    feature_list = [delg, deldelg, au, consv, site_region, rel_site_loc,
                    seed_enrichment, site_len]

    return Record(feature_list, label=label, record_id=record_id)


class CvClipSparkTest(unittest.TestCase):

    def setUp(self):
        # Set the seed so that we can have deterministic tests.
        np.random.seed(282629734)
        self.data = []
        for i in range(10):
            self.data.append(generate_data_row(label=1,
                                               record_id='mRNA:%d' % i))
        for i in range(10):
            self.data.append(generate_data_row(seed_site=True,
                                               label=1,
                                               record_id='mRNA:%d' % i))

    def test_seedless_all(self):

        # Test seedless sites and all region
        filter_func = partial(filter_rows, seed_sites=False, region=None)
        result = filter(lambda x: x is not None, map(filter_func, self.data))
        self.assertEqual(len(result), 10)
        for i in range(10):
            self.assertEqual(id(result[i]), id(self.data[i]))

    def test_seedless_3utr(self):
        # Test seedless sites and 3UTR
        filter_func = partial(filter_rows, seed_sites=False,
                              region=set(['3UTR']))
        result = filter(lambda x: x is not None, map(filter_func, self.data))
        for row in result:
            self.assertEqual(row[-4], '3UTR')
            self.assertTrue(type(row[3]), float)
            self.assertTrue(type(row[3]), float)

    def test_seed_cds(self):
        # Test seed sites and CDS
        filter_func = partial(filter_rows, seed_sites=True,
                              region=set(['CDS']))
        result = filter(lambda x: x is not None, map(filter_func, self.data))
        for row in result:
            self.assertTrue(not np.isnan(row[3][1]))
            self.assertTrue(not np.isnan(row[3][2]))
            self.assertEqual(row[-4], 'CDS')


if __name__ == '__main__':
    unittest.main()
