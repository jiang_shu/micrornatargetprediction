import sys
import ast


def main():
    params_file = '../../data/experiments/crossvalidation/params.txt'
    result_file = '../../data/experiments/crossvalidation/results.txt'

    params = []
    result = []

    with open(params_file, 'rU') as fp:
        for line in fp:
            line = line.strip()
            if not line:
                continue
            m = ast.literal_eval(line)
            params.append(m)

    with open(result_file, 'rU') as fp:
        for line in fp:
            line = line.strip()
            if not line:
                continue
            m = ast.literal_eval(line)
            result.append(m)

    print "therm_components,au_components,therm_basis,"\
        "au_basis,acc,recall,tpr,fpr"

    for p, r in zip(params, result):
        print "%d,%d,%d,%d,%f,%f,%f,%f" % (
            p['therm_components'],
            p['au_components'],
            p['therm_basis'],
            p['au_basis'],
            r[0], r[1], r[2], r[3]
        )

if __name__ == '__main__':
    sys.exit(main())
