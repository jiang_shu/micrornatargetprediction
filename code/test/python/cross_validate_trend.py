from __future__ import print_function
import sys
import numpy as np
from pipeline import Pipeline
import preprocess
from parallelize import run_func_parallely
import itertools
import rpy2
from rpy2.robjects.numpy2ri import numpy2ri
rpy2.robjects.numpy2ri.activate()
from r_interface import create_r_func
from set_model_params import set_model_params
import time
from batch_file_reader import BatchFileReader
from sklearn.metrics import confusion_matrix
import os.path


BATCH_READERS = {}  # Used for string batch_reader objects


def log(*args, **kwargs):
    out = sys.stdout
    if "out" in kwargs and kwargs["out"] == 'stderr':
        out = sys.stderr
    print("INFO: ", *args, file=out)


def get_batch_reader(fname, max_lines):
    global BATCH_READERS
    if fname not in BATCH_READERS:
        BATCH_READERS[fname] = (BatchFileReader(fname, max_lines), True)
    return BATCH_READERS[fname]


def read_data_from_file(fname, max_lines):
    global BATCH_READERS
    log("Reading atmost %d lines of data from file %s" % (max_lines, fname))
    data = []
    reader, lines_remaining = get_batch_reader(fname, max_lines)

    if not lines_remaining:
        log("Reached end of file.")
        return None

    lines_remaining = reader.read(data)

    if not lines_remaining:
        BATCH_READERS[fname] = (None, False)

    log("Done.")

    pipeline = (Pipeline())\
        .add_step(preprocess.remove_unwanted_chars, delimiter=',')\
        .add_step(preprocess.get_slice,
                  from_index=4, delimiter=' ', as_numeric=True)\

    data = run_func_parallely(pipeline.execute,
                              data,
                              kwparams={'for_each': True},
                              combine_func=lambda l: [x for sublist in l
                                                      for x in sublist])
    data = [np.array(x) for x in data]
    return data


def get_deldelg(mfe, mfe_helix, mfe_open, mfe_seed):
    deldelg = mfe
    seed_indices = (mfe_seed == 6)
    deldelg[seed_indices] = mfe_helix[seed_indices]
    deldelg -= mfe_open
    return deldelg


def get_deldelg_func(data):
    return get_deldelg(data[0], data[1], data[2], data[3])


def get_filename(dataset, example_type, ext,
                 args, *additional_args):

    params = list(args) + list(additional_args)

    if not (example_type == 'pos' or example_type == 'neg'):
        raise Exception('example_type should either be pos/neg')

    data_path = '../../../data'
    filename = data_path + "/%s_%s" % (example_type, dataset)
    for param in params:
        filename += "_%s" % str(param)
    filename += ext
    if not os.path.exists(filename):
        raise Exception('%s doesnt exist.' % filename)

    return filename


def get_data(dataset, example_type, ext, *args, **kwargs):
    max_lines = sys.maxsize

    if "max_lines" in kwargs:
        max_lines = kwargs["max_lines"]

    mfe_file = get_filename(dataset, example_type, ext, args, "mfe")
    mfe_helix_file = get_filename(dataset, example_type,
                                  ext, args, "mfe_helix")

    mfe_open_file = get_filename(dataset, example_type,
                                 ext, args, "mfe_open")

    seed_file = get_filename(dataset, example_type,
                             ext, args, "seed")

    mfe = read_data_from_file(mfe_file, max_lines)
    if not mfe:
        return None
    mfe_helix = read_data_from_file(mfe_helix_file, max_lines)
    if not mfe_helix:
        return None
    mfe_open = read_data_from_file(mfe_open_file, max_lines)
    if not mfe_open:
        return None
    seed = read_data_from_file(seed_file, max_lines)
    if not seed:
        return None

    data = zip(mfe, mfe_helix, mfe_open, seed)

    pipeline = (Pipeline())\
        .add_step(get_deldelg_func)

    data = run_func_parallely(pipeline.execute,
                              data,
                              kwparams={'for_each': True},
                              combine_func=lambda l: [x for sublist in l
                                                      for x in sublist])
    return data


def dist_func(data, function=None):
    dist = function(data[0], data[1])
    return dist


def get_scores(data):
    log("Getting scores for %d items." % len(data))
    t1 = time.time()
    r_dist_func = create_r_func('../R/getDistance.R')
    pipeline = (Pipeline())\
        .add_step(dist_func, function=r_dist_func)

    data = run_func_parallely(pipeline.execute,
                              data,
                              kwparams={'for_each': True},
                              combine_func=lambda l: [x for sublist in l
                                                      for x in sublist])
    data = np.array(data)
    t2 = time.time()
    log("Done (%f sec)." % (t2 - t1))
    return data


def len_func(data):
    data1_size = np.size(data[0])
    data2_size = np.size(data[1])
    return (np.abs(data1_size - data2_size)/float(np.max((data1_size,
                                                          data2_size))))


def get_len_diff(data):
    pipeline = (Pipeline())\
        .add_step(len_func)\

    diff = run_func_parallely(pipeline.execute,
                              data,
                              kwparams={'for_each': True},
                              combine_func=lambda l: [x for sublist in l
                                                      for x in sublist])
    return np.array(diff)


def get_votes(test_data, train_data, lambda1, lambda2):
    ''' Get votes for each data in test_data by the train_data.'''
    test_size = np.size(test_data)
    train_size = np.size(train_data)

    # Pair up training and test examples.
    mat = [(x1, x2) for x1, x2 in itertools.product(test_data,
                                                    train_data)]
    log("Total size of test x training data is: ", len(mat))
    size_diff = get_len_diff(mat)

    # We dont want to get scores for all possible training
    # test examples becuase of significant differences in length
    # between a lot of the training and test examples.
    # Find the threshold of length difference to get votes for.
    t = np.linspace(0, 1, 100)
    y = np.exp(-lambda2*t)
    diff_thresh = t[np.where(y < 0.1)[0][0]]
    log("Diff threshold is : %.4f" % diff_thresh)

    # Find indices for which to get votes for
    valid_indices = (size_diff <= diff_thresh)
    log("Number of valid indices: %d" % np.size(np.where(valid_indices)))

    # Compute cosine distance scores.
    mat = np.array(mat)
    scores = np.zeros(test_size * train_size)
    scores[valid_indices] = get_scores(mat[valid_indices])

    # Reshape the scores matrix such that we have all the scores
    # for each test example in a row
    scores = scores.reshape((test_size, train_size))
    size_diff = size_diff.reshape((test_size, train_size))

    votes = np.exp(-(lambda1*scores) - (lambda2*size_diff))
    votes = np.sum(votes, 1)
    return votes


def predict(pos_train_data, neg_train_data, test_data):
    set_model_params()
    lambda1 = 6.1
    lambda2 = 9.6

    pos_votes = get_votes(test_data, pos_train_data, lambda1, lambda2)
    neg_votes = get_votes(test_data, neg_train_data, lambda1, lambda2)

    pos_pred = pos_votes > neg_votes
    pos_pred = pos_pred.astype(int)
    pos_prob = pos_votes / neg_votes

    return pos_pred, pos_prob


def main():
    pos_train_data = get_data('targetminer', 'pos', '.txt', 'train',
                              max_lines=300)
    neg_train_data = get_data('targetminer', 'neg', '.txt', 'train',
                              max_lines=300)
    pos_probs = np.array([])
    neg_probs = np.array([])
    pos_pred = np.array([])
    neg_pred = np.array([])
    conf = np.array([[0, 0], [0, 0]])

    tp = 0.0
    fp = 0.0
    tn = 0.0
    fn = 0.0

    pos_train_size = np.size(pos_train_data)
    neg_train_size = np.size(neg_train_data)

    if pos_train_size > neg_train_size:
        pos_train_data = pos_train_data[0:neg_train_size]
    else:
        neg_train_data = neg_train_data[0:pos_train_size]

    log('Positive training data size: %d' % np.size(pos_train_data))
    log('Negative training data size: %d' % np.size(neg_train_data))

    while True:
        pos_test_data = get_data('psilac', 'pos', '.txt',
                                 max_lines=500)
        neg_test_data = get_data('psilac', 'neg', '.txt',
                                 max_lines=500)
        if pos_test_data is None and neg_test_data is None:
            break
        y_true = np.array([])
        y_pred = np.array([])

        if pos_test_data:
            test_data_size = np.size(pos_test_data)
            y_true = np.repeat(1, test_data_size)

            y_pred, y_prob = predict(pos_train_data, neg_train_data,
                                     pos_test_data)
            pos_pred = np.append(pos_pred, y_pred)
            pos_probs = np.append(pos_probs, y_prob)
            c = confusion_matrix(y_true, y_pred)
            conf += c

        if neg_test_data:
            test_data_size = np.size(neg_test_data)
            y_true = np.repeat(0, test_data_size)

            y_pred, y_prob = predict(pos_train_data, neg_train_data,
                                     neg_test_data)
            neg_pred = np.append(neg_pred, y_pred)
            neg_probs = np.append(neg_probs, y_prob)
            c = confusion_matrix(y_true, y_pred)
            conf += c

        tp += conf[1][1]
        tn += conf[0][0]
        fp += conf[0][1]
        fn += conf[1][0]

        log("Accuracy: %.3f" % (tp/(tp + fp)), out='stderr')
        log("Recall: %.3f" % (tp/(tp + fn)), out='stderr')

    np.savetxt('../../../data/psilac_pos_pred.txt',
               pos_pred, fmt='%.0f', delimiter=",")
    np.savetxt('../../../data/psilac_neg_pred.txt',
               neg_pred, fmt='%.0f', delimiter=",")
    np.savetxt('../../../data/psilac_pos_probs.txt',
               pos_probs, fmt='%.3f', delimiter=",")
    np.savetxt('../../../data/psilac_neg_probs.txt',
               neg_probs, fmt='%.3f', delimiter=",")
    np.savetxt('../../../data/psilac_confusion.txt',
               conf, fmt='%.0f', delimiter=",")

    return 0


if __name__ == '__main__':
    sys.exit(main())
