import sys
from scipy.misc import comb
from itertools import izip
from r_interface import create_r_func
from set_model_params import set_model_params
from cluster import cluster
from parallelize import run_func_parallely
from pipeline import Pipeline
import numpy as np
import rpy2.robjects as robjects
import rpy2
from rpy2.robjects.numpy2ri import numpy2ri
rpy2.robjects.numpy2ri.activate()


def flatten_list(l):
    return [item for sublist in l for item in sublist]


def get_array(line):
    return np.array([float(x) for x in line[4].split(" ")])


def smooth_data(data):
    r_smooth_data = create_r_func('smoothFd.R')
    return r_smooth_data(data)


def get_data(mfe_file, mfe_helix_file, mfe_open_file):
    data_id = []
    data = []
    with open(mfe_file) as mfe_fp,\
         open(mfe_helix_file) as mfe_helix_fp,\
         open(mfe_open_file) as mfe_open_fp:
        for mfe, mfe_helix, mfe_open in izip(mfe_fp, mfe_helix_fp, mfe_open_fp):
            mfe = mfe.strip().split(',')
            mfe_helix = mfe_helix.split(',')
            mfe_open = mfe_open.split(',')

            data_id.append(mfe[0])

            mfe = get_array(mfe)
            mfe_helix = get_array(mfe_helix)
            mfe_open = get_array(mfe_open)

            data_point = np.array(mfe)
            helix_non_neg_ind = np.where(mfe_helix != 0)
            data_point[helix_non_neg_ind] = mfe_helix[helix_non_neg_ind]
            data_point = data_point - mfe_open
            data.append(data_point)

    return (data_id, data)


def main():
    pos_id, pos_data = get_data('../../../data/pos_psilac_mfe_1000_70.txt',
                                '../../../data/pos_psilac_mfe_helix_1000_70.txt',
                                '../../../data/pos_psilac_mfe_open_1000_70.txt')
    neg_id, neg_data = get_data('../../../data/neg_psilac_mfe_1000_70.txt',
                                '../../../data/neg_psilac_mfe_helix_1000_70.txt',
                                '../../../data/neg_psilac_mfe_open_1000_70.txt')
    smoothed_pos_data = []
    smoothed_neg_data = []
    set_model_params(num_basis=40, num_principal_components=5)

    pipeline = Pipeline().add_step(smooth_data)

    smoothed_pos_data = run_func_parallely(pipeline.execute,
                                           pos_data,
                                           kwparams={'for_each': True},
                                           combine_func=flatten_list)


    smoothed_neg_data = run_func_parallely(pipeline.execute,
                                           neg_data,
                                           kwparams={'for_each': True},
                                           combine_func=flatten_list)


    pos_data_coeffs = map(lambda x: np.array(x[0]),
                          smoothed_pos_data) # Get the coeffs
    pos_data_coeffs = np.hstack(pos_data_coeffs)
    pos_data_coeffs = pos_data_coeffs.T

    neg_data_coeffs = map(lambda x: np.array(x[0]),
                          smoothed_neg_data) # Get the coeffs
    neg_data_coeffs = np.hstack(neg_data_coeffs)
    neg_data_coeffs = neg_data_coeffs.T

    (pos_cluster_assignments, neg_cluster_assignments) = cluster(
        pos_id, pos_data_coeffs, neg_id, neg_data_coeffs, by='ALL')

    cluster_map = {} # For a cluster id store num of positive and num negative
    for cluster_id in pos_cluster_assignments:
        count = cluster_map.get(cluster_id, 0)
        cluster_map[cluster_id] = count + 1

    for cluster_id in neg_cluster_assignments:
        counts = cluster_map.get(cluster_id, (0, 0))
        if type(counts) == tuple:
            counts = (counts[0], counts[1] + 1)
        else:
            counts = (counts, 1)
        cluster_map[cluster_id] = counts

    num_pos = pos_data_coeffs.shape[0]
    num_neg = neg_data_coeffs.shape[0]
    num_total =  num_pos + num_neg
    background = float(num_pos)/num_total
    print "Num of positive samples: ", num_pos
    print "Num of negative samples: ", num_neg
    print "Total: ", num_total

    for cluster_id in sorted(cluster_map.keys()):
        value = cluster_map[cluster_id]
        if type(value) == tuple:
            pos, neg = value[0], value[1]
        else:
            pos = value
            neg = 0
        total = pos + neg
        pos_frac = float(pos)/total
        print "ID: %d, positive= %d/%d (%.4f), background = %.4f" % (
            cluster_id, pos, total, pos_frac, background)

    np.savetxt('../../../data/pos_psilac_cluster_1000_70.txt',
               pos_cluster_assignments, fmt='%.0f', delimiter=',')
    np.savetxt('../../../data/neg_psilac_cluster_1000_70.txt',
               neg_cluster_assignments, fmt='%.0f', delimiter=',')


if __name__ == '__main__':
    sys.exit(main())
