from redisdao import RedisDao
import sys
import re


missing_seq = 0
missing_mrna = set([])
missing_mirna = set([])
mrna_cache = {}
mirna_name_map = {}


def usage():
    print "%s <data file> <output file> <mirna old/new map file> "\
        "<data assembly> <target assembly>" % sys.argv[0]


def get_mrna(dao, mrna_name, assembly, target_assembly):
    mrna = dao.get_messenger_rna_from_name(mrna_name, assembly=assembly)
    if assembly == target_assembly:
        return mrna

    target_mrna = dao.get_messenger_rna_from_name(mrna_name,
                                                  assembly=target_assembly)

    if (mrna is not None and target_mrna is not None) and\
            (str(mrna._seq) == str(target_mrna._seq)):
        return target_mrna

    return None


def get_mirna_name_map(fname):
    mirna_name_map = {}
    with open(fname, 'rU') as fp:
        for line in fp:
            old_name, new_name = line.strip().split(",")
            mirna_name_map[old_name] = new_name

    return mirna_name_map


def find_all(s, pattern):
    """
        Returns all (overlapping) occurences of @pattern within @s.
        Returns 1 based indices.
    """
    pattern_len = len(pattern)
    matches = re.finditer(r'(.)(?=(%s))' % pattern, s)
    return [(m.end(1) + 1, m.end(1) + pattern_len) for m in matches]


def str_locs(locs):
    return " ".join(["%s-%s" % (l[0], l[1]) for l in locs])


def write_location(line, ofp, assembly, target_assembly, dao):

    global missing_mirna
    global missing_mrna
    global missing_seq

    line = line.strip()
    if not line:
        return

    mrna_name, mirna_name, seq, score = line.strip().split(",")

    if mrna_name not in mrna_cache:
        mrna_cache[mrna_name] = get_mrna(dao, mrna_name,
                                         assembly, target_assembly)

    if mrna_cache[mrna_name] is None:
        missing_mrna.add(mrna_name)
        return

    if mirna_name.endswith("*"):
        mirna_name = mirna_name_map[mirna_name]

    if mirna_name is None:
        missing_mirna.add(mirna_name)
        return

    seq_locs = find_all(str(mrna_cache[mrna_name]._seq), seq)
    if len(seq_locs) <= 0:
        missing_seq += 1
        return

    ofp.write("%s,%s,%s,%s\n" % (mrna_name,
                                 mirna_name,
                                 str_locs(seq_locs),
                                 score))


def get_locations(fname, output_file, mirna_map_file,
                  assembly, target_assembly):
    global mirna_name_map
    mirna_name_map = get_mirna_name_map(mirna_map_file)
    count = 0
    with open(fname, 'rU') as fp,\
            open(output_file, 'w') as ofp,\
            RedisDao() as dao:

        for line in fp:
            try:
                write_location(line, ofp, assembly, target_assembly, dao)
                count += 1
            except:
                print "Encountered error while processing line", count + 1
                raise

    print "Missing mrnas:", len(missing_mrna)
    print "Missing mirnas:", len(missing_mirna)
    print "Missing sequences:", missing_seq


def main():
    if len(sys.argv) != 6:
        usage()
        return 1

    fname = sys.argv[1]
    output_file = sys.argv[2]
    mirna_map_file = sys.argv[3]
    assembly = sys.argv[4]
    target_assembly = sys.argv[5]

    get_locations(fname, output_file, mirna_map_file, assembly, target_assembly)
    return 0


if __name__ == '__main__':
    sys.exit(main())
