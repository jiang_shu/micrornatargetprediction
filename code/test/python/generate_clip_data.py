'''
    From clip tag locations in mrna. Generate data for mrna-mirna pair.
'''
import subprocess
import sys
import os
import os.path
from tempfile import NamedTemporaryFile
import argparse
from pyspark import SparkContext
from sparkdao import SparkDao
from avishkarconf import AvishkarConf


ERR_MISSING_MRNA = -1
ERR_MISSING_SEQ = -2
ERR_REVERSE_SEQUENCE = -3


class LocationWithAttribute:
    def __init__(self, loc, energy, seed_type):
        self._loc = loc
        self._energy = energy
        self._seed_type = seed_type

    def __eq__(self, other):
        return self._loc == other._loc

    def __ne__(self, other):
        return self._loc != other._loc

    def __hash__(self):
        return hash(self._loc)

    def to_str(self):
        seed_type =\
            "None" if self._seed_type is None else ("%d-%d" % self._seed_type)
        return "%d-%d,%.3f,%s" % (
            self._loc[0], self._loc[1], self._energy, seed_type
        )

    def __str__(self):
        return self.to_str()

    @staticmethod
    def from_str(str_obj):
        tokens = str_obj.strip().split(",")
        (loc, energy, seed_type) = tokens
        loc = tuple(map(int, loc.split(",")))
        energy = float(energy)
        seed_type =\
            None if seed_type == "None" else tuple(map(int, loc.split(",")))
        return LocationWithAttribute(loc, energy, seed_type)

    def is_preferable(self, other):
        # Returns True if self is prefferable over @other LocationWithAttribute
        # object
        # Seed site is preferred over seed-less site
        # A seed-match of greater size is preferred
        # A site with lower energy is preferred.
        if other._seed_type is None and self._seed_type is not None:
            return True
        if other._seed_type is not None and \
                self._seed_type is not None and \
                ((self._loc[1] - self._loc[0]) >
                 (other._loc[1] - other._loc[0])):
            return True
        if self._energy[1] < other._energy:
            return True
        return False


def parse_arguments():
    argparser = argparse.ArgumentParser(
        description="Generate mRNA-miRNA data from CLIP data which is"
        " later used for training and validation. "
    )
    argparser.add_argument(
        "assembly", choices=['mm9', 'hg19', 'hg18'],
        help="The sequence assembly to use for mRNA."
    )
    argparser.add_argument(
        "clip_dataset",
        help="The CLIP dataset to use. This should correspond to "
        "the filename in HDFS that stores the CLIP locations."
    )
    argparser.add_argument(
        "output_file",
        help="File to write the output to."
    )
    argparser.add_argument(
        "--mirna-regex", default=None, dest='mirna_regex',
        help="Regular experssion to use for miRNA names to filter miRNA. "
    )
    argparser.add_argument(
        "--mirna-names", nargs='*', default=[], dest='mirna_names',
        help="miRNA names to filter."
    )
    args = argparser.parse_args()
    return args


def invoke_rna_hybrid(executable, mrna_seq, mirna_seq,
                      energy_threshold=None,
                      helix_constraint=None):

    # TODO: Change the dataset type (3utr_human). But do we need to change
    # if we are not interested in the p-value?
    args = "-c -s 3utr_human -m %d" % len(mrna_seq)
    if helix_constraint is not None:
        args += " -f %d,%d" % (helix_constraint[0], helix_constraint[1])
    if energy_threshold is not None:
        args += " -e %0.3f" % energy_threshold

    if os.name == 'nt':
        # We are running on windows, so use files.
        fp = NamedTemporaryFile(mode='w', delete=True)
        fp.write(">mrna\n")
        fp.write("%s\n" % mrna_seq)
        fp.flush()
        cmd = "%s %s -t %s %s" % (executable, args, fp.name, mirna_seq)
        output = subprocess.check_output(cmd, shell=True)
        fp.close()
    else:
        cmd = "%s %s %s %s" % (executable, args, mrna_seq, mirna_seq)
        output = subprocess.check_output(cmd, shell=True)

    return output


def get_match_len_and_type(mrna_mismatch, mrna_match,
                           mirna_mismatch, mirna_match):
    # Returns the length of mrna that is actually aligned (excluding gaps)
    # Returns the start and end position of mirna match from the 5' end until
    # nucleotide position 8.
    # Note: The returned indices are 1 based and matches are perfect (i.e.
    # no GU wobbles)

    max_align_len = [mrna_mismatch, mrna_match, mirna_mismatch, mirna_match]
    max_align_len = max(map(lambda x: len(x), max_align_len))
    mrna_mismatch += " " * (max_align_len - len(mrna_mismatch))
    mrna_match += " " * (max_align_len - len(mrna_match))
    mirna_mismatch += " " * (max_align_len - len(mirna_mismatch))
    mirna_match += " " * (max_align_len - len(mirna_match))

    mrna_align_len = 0
    for i in range(max_align_len):
        # If both mismatch and match are not gaps then increment
        # the mrna align length.
        if mrna_mismatch[i] == " " and mrna_match[i] == " ":
            continue
        mrna_align_len += 1

    mirna_start = 0
    mirna_start_offset = 0  # accounts for gaps at the beginning of miRNA.
    mirna_end = 0
    # We are looking for a block of matches from the 5' end of the miRNA
    # without any GU wobbles.
    # Ignore GU wobbles at the beginning
    for i in range(1, max_align_len):
        if (mirna_mismatch[-i] == " " and mirna_match[-i] == " "):
            mirna_start_offset += 1
            continue
        break

    for i in range(1 + mirna_start_offset, max_align_len):
        # If we have a Gap or mismatch or a GU wobble then increment the start
        if mirna_match[-i] == " " or\
                (mirna_match[-i] == "G" and mrna_match[-i] == "U") or\
                (mirna_match[-i] == "U" and mrna_match[-i] == "G"):
            mirna_start += 1
        # The moment we have a match break.
        if mirna_match[-i] != " ":
            break

    mirna_start += 1

    mirna_end = mirna_start
    for i in range(mirna_start + mirna_start_offset, 9 + mirna_start_offset):
        if mirna_match[-i] == " " or\
                (mirna_match[-i] == "G" and mrna_match[-i] == "U") or\
                (mirna_match[-i] == "U" and mrna_match[-i] == "G"):
            break
        mirna_end += 1

    mirna_end -= 1

    return (mrna_align_len, (mirna_start, mirna_end))


def contains(loc, other_loc):
    ''' Returns True if @loc is contained within @other_loc '''
    if loc[0] >= other_loc[0] and loc[1] <= other_loc[1]:
        return True
    return False


def parse_rna_hybrid_output(outputs, enforce_seed_match=False):
    # Parse output to get the following features:
    #   Mean free energy (mfe) of duplex.
    #   Start and end position of match
    #   GU wobbles are not considered.
    loc_attr_list = []
    outputs = outputs.split('\n')
    for output in outputs:
        output = output.strip()
        output = output.split(":")
        if not output or not output[0]:
            continue
        mfe = float(output[4])
        if not mfe < 0.0:
            continue
        mrna_mismatch, mrna_match, mirna_match, mirna_mismatch =\
            output[7], output[8], output[9], output[10]
        mrna_align_len, seed_type = get_match_len_and_type(
            mrna_mismatch, mrna_match, mirna_mismatch, mirna_match
        )

        start = int(output[6])
        end = start + mrna_align_len

        if not enforce_seed_match:
            # We have no seed match constraints
            loc_attr_list.append(
                LocationWithAttribute((start, end), mfe, None)
            )
        else:
            if seed_type[1] - seed_type[0] + 1 >= 6:
                loc_attr_list.append(
                    LocationWithAttribute((start, end), mfe, seed_type)
                )

    return loc_attr_list


def resolve_ovarlapping_locations(loc_attr_list):
    # Resolved LocationWithAttribute objects
    resolved_loc_attr = {}
    for loc_attr in loc_attr_list:
        if loc_attr in resolved_loc_attr and \
                resolved_loc_attr[loc_attr].is_preferable(loc_attr):
                continue
        resolved_loc_attr[loc_attr] = loc_attr
        return sorted(resolved_loc_attr.values(), key=lambda x: x._loc)


def get_labeled_target_locations(avishkar_conf,
                                 micro_rna, mrna, clip_locations):
    '''
        Gets potential target locations in @mrna due to @micro_rna
        If the locations overlap/is contained in a clip location
        in clip_locations then
        return 1 for that location otherwise return 0 for that location
    '''
    executable = avishkar_conf.rnahybrid_executable()
    output = invoke_rna_hybrid(executable, mrna._seq, micro_rna._seq,
                               energy_threshold=-15.0)
    loc_attrs = parse_rna_hybrid_output(output)
    constraints = [(1, 6), (2, 7), (3, 8)]
    for cons in constraints:
        output = invoke_rna_hybrid(executable, mrna._seq, micro_rna._seq,
                                   energy_threshold=-1.0,
                                   helix_constraint=cons)
        loc_attrs += parse_rna_hybrid_output(output, enforce_seed_match=True)
    loc_attrs = resolve_ovarlapping_locations(loc_attrs)
    candidate_set = []
    for loc_attr in loc_attrs:
        label = 0
        for other_loc in clip_locations:
            if contains(loc_attr._loc, other_loc):
                label = 1
                break
        candidate_set.append((label, loc_attr))

    print "%s, %s: locations: %d" % (mrna._name, micro_rna._name,
                                     len(candidate_set))
    return (mrna, micro_rna, candidate_set)


def process_partition(conf, itr):
    for x in itr:
        mrna, mirna, clip_locations = x[0], x[1], x[2]
        yield get_labeled_target_locations(conf, mirna, mrna, clip_locations)


class CandidateSetGenerator:
    def __init__(self, conf, mrna_clip_rdd, mirna_rdd):
        """
            Generate candidate set for each mRNA-miRNA pair
            given an RDD of mRNA CLIP data (@mrna_clip_rdd)
            and an RDD of miRNA objects (@mirna_rdd).
            Returns (@MessengerRNA, @MicroRNA, List<LabelledLocation>)
            where LabelledLocation is a tuple of:
                (loc, label, mfe, seed match type)
        """
        self._conf = conf
        self._mrna_clip_rdd = mrna_clip_rdd
        self._mirna_rdd = mirna_rdd

    def generate(self):
        # TODO: Repartition the data for even distribution across nodes?
        # TODO: Partition data such that the total length of transcript
        # on each node remains approximately the same.
        combined_rdd = self._mrna_clip_rdd.cartesian(self._mirna_rdd)\
            .map(lambda (x, y): (x[0], y, x[1]))

        conf = self._conf
        return combined_rdd.mapPartitions(
            lambda itr: process_partition(conf, itr)
        )


def convert_to_csv(mrna, mirna, candidate_sets):
    for (label, loc_attr) in candidate_sets:
        yield "%d,%s,%s,%s" % (label, mrna._name, mirna._name, str(loc_attr))


def main():
    args = parse_arguments()
    if len(args.mirna_names) == 0:
        args.mirna_names = None
    sc = SparkContext(appName="Avishkar:: Candidate set generation")

    avishkar_conf = AvishkarConf()
    spark_dao = SparkDao(avishkar_conf, sc)
    mrna_clip_rdd = spark_dao.get_mrna_clip_data(
        args.assembly, args.clip_dataset
    )
    mirna_rdd = spark_dao.get_mirna_sequences(
        mirna_regex=args.mirna_regex, mirna_names=args.mirna_names
    )
    generator = CandidateSetGenerator(avishkar_conf, mrna_clip_rdd, mirna_rdd)
    output_rdd = generator.generate().flatMap(lambda x: convert_to_csv(*x))
    for row in output_rdd.take(10):
        print row
    # output_rdd.saveAsTextFile(args.output_file)
    return 0

if __name__ == '__main__':
    sys.exit(main())
