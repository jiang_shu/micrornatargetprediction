from pyspark import SparkContext
import sys
import argparse
from pyspark import StorageLevel


RDD_PARTITIONS = 80


def parse_args():
    argparser = argparse.ArgumentParser(
        description="Fixes labels of features with those in the data file"
    )
    argparser.add_argument("data_file", metavar='data_file',
                           help='HDFS path to data file. '
                           'File contains mrna, mirna, loc, and label data')
    argparser.add_argument("features_file", metavar='features_file',
                           help='HDFS path to features file.')
    argparser.add_argument("pos_output_file", metavar='pos_output_file',
                           help='File to write the positive output to.')
    argparser.add_argument("neg_output_file", metavar='neg_output_file',
                           help='File to write the positive output to.')
    argparser.add_argument("--increment-loc", dest='incr_loc',
                           action='store_true',
                           help='If specified then convert a loc (a, b) '
                           'in the data file to (a+1, b) before merging '
                           'records')
    args = argparser.parse_args()
    return args


def process_data_row(x, incr_loc=False):
    items = x.strip().split(",")
    mrna, mirna, loc, clip = items[0], items[1], items[2], items[5]
    loc = tuple(map(int, loc.split("-")))
    if incr_loc is True:
        loc = (loc[0] + 1, loc[1])

    return ((mrna, mirna, loc), clip)


def process_feature_row(x):
    items = x.strip().split(",")
    mrna, mirna, loc, features = items[1], items[2], items[3], items[4:]
    feature = ",".join(features)
    loc = tuple(map(int, loc.split("-")))
    return ((mrna, mirna, loc), feature)


def merge_row(x):
    mrna, mirna, loc = x[0]
    clip = [y for y in x[1][0]]
    feature_list = set([y for y in x[1][1]])
    if len(feature_list) == 0 or len(clip) == 0:
        return None

    if len(clip) > 1:
        print clip
        raise Exception('Data contains more than 1 record for: (%s, %s, %d-%d)'
                        % (mrna, mirna, loc[0], loc[1]))
    if len(feature_list) > 1:
        print feature_list
        raise Exception('Multiple features for: (%s, %s, %d-%d)'
                        % (mrna, mirna, loc[0], loc[1]))
    clip = str(clip[0])
    feature = feature_list.pop()
    loc = "%d-%d" % (loc[0], loc[1])
    row = [clip, mrna, mirna, loc, feature]
    return row


def relabel_data(sc, data_file, features_file,
                 pos_output_file, neg_output_file,
                 incr_loc=False):
    data_rdd = sc.textFile(data_file, RDD_PARTITIONS)
    features_rdd = sc.textFile(features_file, RDD_PARTITIONS)
    data_rdd = data_rdd\
        .map(lambda x: process_data_row(x, incr_loc=incr_loc))

    features_rdd = features_rdd\
        .map(lambda x: process_feature_row(x))

    output = data_rdd.cogroup(features_rdd)\
        .map(merge_row)\
        .filter(lambda x: x is not None)\
        .repartition(RDD_PARTITIONS)\
        .persist(StorageLevel.MEMORY_AND_DISK)

    pos_output = output.filter(lambda x: x[0] == '1')
    neg_output = output.filter(lambda x: x[0] == '0')
    pos_output.map(lambda x: ",".join(x)).saveAsTextFile(pos_output_file)
    neg_output.map(lambda x: ",".join(x)).saveAsTextFile(neg_output_file)


def main():
    args = parse_args()
    sc = SparkContext(appName='Relabel data')
    relabel_data(sc, args.data_file, args.features_file,
                 args.pos_output_file, args.neg_output_file,
                 incr_loc=args.incr_loc)


if __name__ == '__main__':
    sys.exit(main())
