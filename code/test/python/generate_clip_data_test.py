from Bio.Seq import Seq
import unittest
import mock
from rna import MicroRNA, MessengerRNA
from generate_clip_data import get_labeled_target_locations, generate_data
import generate_clip_data
from tempfile import NamedTemporaryFile
import os
import re


def invoke_rna_hybrid(mrna, mirna, energy_threshold=-15.0,
                      helix_constraint=None):
    return None


def parse_rna_hybrid_output(output, enforce_seed_match=False):
    mfe_list = [-16., -17., -15., -19., -20.]

    loc_list = [(100, 150), (100, 150), (150, 200),
                (200, 230), (101, 140)]

    seed_type_list = [None, (0, 7), (0, 7), (0, 6), (1, 7)]
    return mfe_list, loc_list, seed_type_list


class TestFeatureGen(unittest.TestCase):
    def setUp(self):
        self.clip_locations = [(90, 150), (250, 300), (200, 240)]
        self.expected_output = [
            ((100, 150), 1, -17., (0, 7)),
            ((150, 200), 0, -15., (0, 7)),
            ((200, 230), 1, -19., (0, 6)),
            ((101, 140), 1, -20., (1, 7))
        ]
        self.output_file = NamedTemporaryFile(mode='w', delete=False)

    def tearDown(self):
        os.unlink(self.output_file.name)

    def validate_output(self, output):
        self.assertEqual(len(output), len(self.expected_output))
        self.assertItemsEqual(output, self.expected_output)

    #####################################################################
    # Test labelling of clip data
    @mock.patch('generate_clip_data.parse_rna_hybrid_output',
                side_effect=parse_rna_hybrid_output)
    @mock.patch('generate_clip_data.invoke_rna_hybrid',
                side_effect=invoke_rna_hybrid)
    def test_get_labeled_target_locations(self, func1, func2):

        mirna = MicroRNA('test_mirna', Seq('AUUGCUUUCA'))
        mrna = MessengerRNA('test_mrna', Seq('AUUCUAAACA'))
        labeled_locs = get_labeled_target_locations(
            mirna, mrna=mrna,clip_locations=self.clip_locations
        )
        self.validate_output(labeled_locs)

    ######################################################################
    # Test batch generation of data
    @mock.patch('generate_clip_data.parse_rna_hybrid_output',
                side_effect=parse_rna_hybrid_output)
    @mock.patch('generate_clip_data.invoke_rna_hybrid',
                side_effect=invoke_rna_hybrid)
    def test_generate_data(self, func1, func2):
        mrna_clip_locs = {}
        mirna_list = []

        for i in range(10):
            mrna = MessengerRNA('test_mrna_%d' % i, Seq('AUUCUAAACA'))
            mirna = MicroRNA('test_mirna_%d' % i, Seq('AUUGCUUUCA'))
            mrna_clip_locs[mrna] = self.clip_locations
            mirna_list.append(mirna)

        generate_data(mirna_list, mrna_clip_locs, self.output_file.name)
        self.output_file.close()

        output = {}
        with open(self.output_file.name, 'rU') as fp:
            for line in fp:
                items = line.strip().split(",")
                mrna, mirna = items[0], items[1]
                loc = tuple(map(int, items[2].split("-")))
                mfe = float(items[3])
                if items[4] == "None":
                    seed_type = None
                else:
                    seed_type = tuple(map(int, items[4].split("-")))
                clip = int(items[5])

                record = (loc, clip, mfe, seed_type)
                m = re.search(r'test_mrna_(\d+)', mrna)
                mrna_id = int(m.group(1))
                m = re.search(r'test_mirna_(\d+)', mirna)
                mirna_id = int(m.group(1))
                if (mrna_id, mirna_id) not in output:
                    output[(mrna_id, mirna_id)] = []

                output[(mrna_id, mirna_id)].append(record)

        self.assertEqual(len(output), 100)
        for i in range(10):
            for j in range(10):
                self.validate_output(output[(i, j)])

if __name__ == '__main__':
    unittest.main()
