import sys


def main():
    if len(sys.argv) != 4:
        print "Usage: %s <all file> <computed file> <output file>" % sys.argv[0]
    all_file = sys.argv[1]
    computed_file = sys.argv[2]
    output_file = sys.argv[3]

    computed_data = set([])

    print "Reading computed data"

    with open(computed_file, 'rU') as fp:
        for line in fp:
            mrna, mirna, loc = line.strip().split(",")
            loc = loc.split("-")
            loc = int(loc[0]) - 1, int(loc[1])
            computed_data.add((mrna, mirna, loc))
    print "Done."
    print "Read %d records" % len(computed_data)

    print "Reading from %s to compute records remaining" % all_file
    print "Writing to %s remaining records" % output_file
    count = 0
    all_count = 0
    with open(all_file, 'rU') as fp, open(output_file, 'w') as ofp:
        for line in fp:
            all_count += 1
            line = line.strip()
            mrna, mirna, loc, _, _, _ = line.split(",")
            loc = loc.split("-")
            loc = int(loc[0]), int(loc[1])
            if (mrna, mirna, loc) not in computed_data:
                ofp.write("%s\n" % line)
            else:
                count += 1

    print "Total number of records:", all_count
    print "Records matched in computed data:", count


if __name__ == '__main__':
    sys.exit(main())
