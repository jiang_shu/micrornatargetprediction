import sys
import numpy as np
from pipeline import Pipeline
from parallelize import run_func_parallely
from sklearn.cross_validation import KFold
from train_clip import train
from predict_clip import predict
from train_clip_mv import train_mv
from predict_clip_mv import predict_mv
import time
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve
from functools import partial
from batch_file_reader import BatchFileReader
from uuid import uuid4
import train_predict_spark
from numpy import random
import argparse


def parse_arguments():
    argparser = argparse.ArgumentParser(
        description="Performs 10 fold crossvalidation"
    )
    argparser.add_argument("pos_file", metavar='pos_file',
                           help='path to positive data')
    argparser.add_argument("neg_file", metavar='neg_file',
                           help='path to negative data')
    argparser.add_argument("seed_enrichment_file",
                           metavar='seed_enrichment_file',
                           help='path to seed enrichment file')
    argparser.add_argument("output_file", metavar='output_file',
                           help='File to write the output to')
    argparser.add_argument(
        "--model", default='FN', choices=['FN', 'MV'],
        help='type of model multivariate (MV) or functional (FN, default)'
    )
    argparser.add_argument(
        '--dataset', default='',
        help='Used to write to dataset specific DFS paths'
    )
    argparser.add_argument(
        '--save', metavar='file', dest='save_fname', default=None,
        help='File to save the preprocessed data to'
    )
    argparser.add_argument(
        '--cache-pca', metavar='path', dest='pca_cache_path', default=None,
        help='Path where to cache PCA objs to.'
        ' If not provided PCA obj are recomputed everytime during training.'
    )
    args = argparser.parse_args()
    return args


def compute_performance(y_actual, y_pred, prob=None):
    ''' Compute accuracy and recall '''
    conf = confusion_matrix(y_actual, y_pred)
    tp = float(conf[1][1])
    tn = float(conf[0][0])
    fp = float(conf[0][1])
    fn = float(conf[1][0])
    if tp == 0:
        accuracy = 0
        recall = 0
    else:
        accuracy = (tp/(tp + fp))
        recall = (tp/(tp + fn))
    if prob is None:
        positive = tp + fn
        negative = tn + fp
        fpr = fp/negative
        tpr = tp/positive
    else:
        # Compute fpr, tpr using sklearn roc_curve
        fpr, tpr, _ = roc_curve(y_actual, prob[:, 0], pos_label=1)

    return (accuracy, recall, fpr, tpr)


def parse_str_array(str_arr, delimiter=" ", conv_func=float):
    return np.array([conv_func(x) for x in str_arr.split(delimiter)])


def extract_columns(row, delimiter=","):
    # Exracts columns from a single row.
    row = row.strip().split(delimiter)
    row = row[5:]

    delg = [parse_str_array(row[0]),
            parse_str_array(row[1])]
    deldelg = [parse_str_array(row[2]),
               parse_str_array(row[3])]
    au = [parse_str_array(row[4]),
          parse_str_array(row[5])]

    consv = map(float, row[6:9])
    site_region = row[9]
    rel_site_loc = float(row[10])
    seed_match_type = row[11]
    site_len = int(row[12])

    return [delg, deldelg, au, consv, site_region, rel_site_loc,
            seed_match_type, site_len]


def preprocess_data_row(row, seed_enrichment_map=None, seed_sites=False):
    # Do some processing like filtering rows. Converting nan values etc.
    cols = extract_columns(row)
    # Convert nans in delg, deldelg, seed
    cols[0] = map(np.nan_to_num, cols[0])
    cols[1] = map(np.nan_to_num, cols[1])
    cols[2] = map(np.nan_to_num, cols[2])

    # Convert the seed type to numeric attribute
    seed_pval = seed_enrichment_map.get(cols[6], 1.0)
    seed_enrichment = 1 - seed_pval
    cols[6] = seed_enrichment

    if seed_sites:
        # We are interested in seed sites.
        # Filter out seedless sites
        # if seed consv is nan then we have a seedless site
        if np.isnan(cols[3][1]):
            return None
    else:
        # We are interested in seedless sites
        # Filter out seed sites
        if not np.isnan(cols[3][1]):
            return None
        cols[3] = cols[3][0]

    return tuple(cols)


def preprocess_data(data_file, seed_enrichment_map=None, max_rows=sys.maxsize):
    print "Reading data from %s" % data_file
    data = []
    with BatchFileReader(data_file, batch_size=max_rows) as reader:
        reader.read(data)
    print "Done."

    print "Preprocessing data."
    t1 = time.time()
    pipeline_func = partial(preprocess_data_row,
                            seed_enrichment_map=seed_enrichment_map)
    pipeline_func.__name__ = 'preprocess_data_row'
    pipeline = (Pipeline())\
        .add_step(pipeline_func)

    data = run_func_parallely(pipeline.execute,
                              data,
                              kwparams={'for_each': True},
                              combine_func=lambda l: [x for sublist in l
                                                      for x in sublist])
    data = zip(*data)
    delg = zip(*data[0])
    delg = map(np.vstack, delg)
    deldelg = zip(*data[1])
    deldelg = map(np.vstack, deldelg)
    au = zip(*data[2])
    au = map(np.vstack, au)

    consv = data[3]
    if type(consv[0]) == float:
        consv = np.array(consv)
        consv = consv.reshape((consv.size, 1))
    else:
        consv = np.vstack(consv)

    categorical_features = np.array(data[4])  # region type
    categorical_features = categorical_features.reshape(
        (categorical_features.size, 1))
    other_features = map(np.array, data[5:])
    other_features = map(lambda x: x.reshape((x.size, 1)), other_features)
    other_features = np.hstack(other_features)
    numeric_features = np.hstack((consv, other_features))
    t2 = time.time()
    print "Done (%f sec) " % (t2 - t1)

    return (delg, deldelg, au, categorical_features, numeric_features)


def array_to_str(arr, delimiter=" "):
    return delimiter.join([str(x) for x in arr])


def save_data(name, features):
    filename = '../../../data/%s' % name
    print "Saving data to:", filename

    delg, deldelg, au, cat_features, numeric_features =\
        features[0], features[1], features[2], features[3], features[4]

    header = "delg_site,delg_seed,deldelg_site,deldelg_seed"\
        ",au_site,au_seed,site_region,consv_site"

    if numeric_features.shape[1] == 6:
        header += ",consv_seed,consv_offseed"
    header += ",rel_site_loc,seed_match_type,site_len"

    with open(filename, 'w') as fp:
        fp.write("%s\n" % header)
        for feature in zip(delg[0], delg[1], deldelg[0], deldelg[1],
                           au[0], au[1], cat_features, numeric_features):
            fp.write("%s,%s,%s,%s,%s,%s" % (
                array_to_str(feature[0]),  # degG site
                array_to_str(feature[1]),  # delG seed
                array_to_str(feature[2]),  # deldelG site
                array_to_str(feature[3]),  # deldelG seed
                array_to_str(feature[4]),  # au site
                array_to_str(feature[5])  # au seed
            ))
            fp.write(",%s" % feature[6][0])  # region
            fp.write(",%s\n" % array_to_str(feature[7],
                                            delimiter=","))  # numeric features


def read_pos_seed_enrichment(fname):
    print "Reading seed enrichment"
    t1 = time.time()
    seed_enrichment = {}
    with open(fname, 'rU') as fp:
        for line in fp:
            line = line.strip().split(",")
            seed_enrichment[line[0]] = float(line[1])
    t2 = time.time()
    print "Done (%f sec)." % (t2 - t1)
    return seed_enrichment


def train_predict_mv(train_y,
                     train_data,
                     test_y,
                     test_data,
                     model_params=None):

    print "Beginning training (Multivariate)."
    t1 = time.time()
    prob = model_params['prob']
    svm_model = train_mv(train_y, *train_data, prob=prob)
    t2 = time.time()
    print "Done (%f sec)." % (t2 - t1)
    print "Coefficients:"
    print svm_model

    print "Beginning prediction."
    t1 = time.time()
    y_prob = None
    y_pred = predict_mv(test_data[0], test_data[1], test_data[2], test_data[3],
                        test_data[4], svm_model, prob=prob)
    if prob:
        y_pred, y_prob = y_pred[0], y_pred[1]
    t2 = time.time()
    print "Done (%f sec)." % (t2 - t1)

    accuracy, recall, fpr, tpr = compute_performance(test_y, y_pred,
                                                     prob=y_prob)
    return (accuracy, recall, fpr, tpr)


def train_predict(train_y,
                  train_data,
                  test_y,
                  test_data,
                  model_params=None):

    print "Beginning training (Functional)."
    t1 = time.time()
    (svm_model, therm_pca_pair, au_pca_pair) = train(train_y, *train_data,
                                                     **model_params)
    t2 = time.time()
    print "Coefficients:"
    print svm_model
    print "Done (%f sec)." % (t2 - t1)

    print "Beginning prediction."
    t1 = time.time()
    y_prob = None
    y_pred = predict(test_data[0], test_data[1], test_data[2], test_data[3],
                     test_data[4], svm_model, therm_pca_pair, au_pca_pair,
                     **model_params)

    if model_params['prob']:
        y_pred, y_prob = y_pred[0], y_pred[1]
    t2 = time.time()
    print "Done (%f sec)." % (t2 - t1)

    accuracy, recall, fpr, tpr = compute_performance(test_y, y_pred,
                                                     prob=y_prob)

    return (accuracy, recall, fpr, tpr)


def get_feature_slice(features, indices):
    # Return slices of each feature contained in the features list
    feature_slice = []
    feature_slice.append(map(lambda x: x[indices, :], features[0]))  # delg
    feature_slice.append(map(lambda x: x[indices, :], features[1]))  # deldelg
    feature_slice.append(map(lambda x: x[indices, :], features[2]))  # au
    feature_slice += map(lambda x: x[indices, :], features[3:])  # rest
    return feature_slice


def combine_pos_neg_data(pos_data_list, neg_data_list):
    data_list = []
    data_list.append(map(np.vstack, zip(pos_data_list[0], neg_data_list[0])))
    data_list.append(map(np.vstack, zip(pos_data_list[1], neg_data_list[1])))
    data_list.append(map(np.vstack, zip(pos_data_list[2], neg_data_list[2])))
    data_list += map(np.vstack, zip(pos_data_list[3:], neg_data_list[3:]))
    return data_list


def crossvalidate(pos_features,
                  neg_features,
                  model='FN',
                  model_params=None,
                  save_fname=None,
                  dataset=None,
                  pca_cache_path=None):

    len_pos = pos_features[0][0].shape[0]
    len_neg = neg_features[0][0].shape[0]
    pos_y = np.repeat(1, len_pos)
    neg_y = np.repeat(0, len_neg)
    pos_kf = KFold(len_pos, 10)
    neg_kf = KFold(len_neg, 10)
    pos_ind = [(train, test) for train, test in pos_kf]
    neg_ind = [(train, test) for train, test in neg_kf]

    if save_fname is not None:
        save_data(save_fname+'_pos', pos_features)
        save_data(save_fname+'_neg', neg_features)

    acc, rec, fpr, tpr = [], [], [], []

    for ind in zip(pos_ind, neg_ind):
        pos_train_ind = ind[0][0]
        pos_test_ind = ind[0][1]
        neg_train_ind = ind[1][0]
        neg_test_ind = ind[1][1]

        pos_train_data = get_feature_slice(pos_features, pos_train_ind)
        pos_test_data = get_feature_slice(pos_features, pos_test_ind)
        neg_train_data = get_feature_slice(neg_features, neg_train_ind)
        neg_test_data = get_feature_slice(neg_features, neg_test_ind)

        y_train = np.hstack((pos_y[pos_train_ind], neg_y[neg_train_ind]))
        y_test = np.hstack((pos_y[pos_test_ind], neg_y[neg_test_ind]))

        train_data = combine_pos_neg_data(pos_train_data, neg_train_data)
        test_data = combine_pos_neg_data(pos_test_data, neg_test_data)

        if model == 'FN':
            accuracy, recall, fpr_i, tpr_i = train_predict_spark.train_predict(
                y_train, train_data,
                y_test, test_data,
                model_params=model_params,
                dataset=dataset,
                pca_cache_path=pca_cache_path
            )
        else:
            accuracy, recall, fpr_i, tpr_i = train_predict_mv(
                y_train, train_data,
                y_test, test_data,
                model_params=model_params
            )

        print "Accuracy = %f, Recall = %f" % (accuracy, recall)
        acc.append(accuracy)
        rec.append(recall)
        fpr.append(fpr_i)
        tpr.append(tpr_i)

    return (acc, rec, tpr, fpr)


def print_results(output_file, acc, rec, tpr, fpr):
    with open(output_file, 'w') as ofp:
        ofp.write("Mean Accuracy = %f, Mean Recall = %f\n"
                  % (np.mean(acc), np.mean(rec)))

        if type(tpr[0]) == float:
            ofp.write("Mean fpr = %f\n" % np.mean(fpr))
            ofp.write("Mean tpr = %f\n" % np.mean(tpr))
        else:
            random_str = str(uuid4())
            tpr_file = "/tmp/tpr_%s" % random_str
            fpr_file = "/tmp/fpr_%s" % random_str
            ofp.write("Writing tpr to:\n", tpr_file)
            with open(tpr_file, 'w') as fp:
                for tpr_i in tpr:
                    fp.write("%s\n" % array_to_str(tpr_i, delimiter=","))

            ofp.write("Writing fpr to:\n", fpr_file)
            with open(fpr_file, 'w') as fp:
                for fpr_i in fpr:
                    fp.write("%s\n" % array_to_str(fpr_i, delimiter=","))


def main():
    args = parse_arguments()
    seed_enrichment_map = read_pos_seed_enrichment(args.seed_enrichment_file)

    print "Reading data from file"
    t1 = time.time()
    pos_data = preprocess_data(args.pos_file,
                               seed_enrichment_map=seed_enrichment_map)
    neg_data = preprocess_data(args.neg_file,
                               seed_enrichment_map=seed_enrichment_map)
    t2 = time.time()
    print "Done (%f sec)." % (t2 - t1)

    num_pos, num_neg = pos_data[0][0].shape[0], neg_data[0][0].shape[0]
    print "Positive data: ", num_pos
    print "Negative data: ", num_neg

    model_params = {
        'therm_basis': 20,
        'au_basis': 15,
        'therm_components': 3,
        'au_components': 15,
        'prob': True,
    }

    # If one of the dataset is larger than the other then randomly sample
    # 10 times from the larger dataset.
    for i in range(10):
        save_fname = None
        if args.save_fname is not None:
            save_fname += '.%d' % i

        if num_pos > num_neg:
            print "Sampling from positive data"
            sample_ind = random.choice(num_pos, num_neg, replace=False)
            pos_data_sample = get_feature_slice(pos_data, sample_ind)
            neg_data_sample = neg_data
        else:
            print "Sampling from negative data"
            pos_data_sample = pos_data
            sample_ind = random.choice(num_neg, num_pos, replace=False)
            neg_data_sample = get_feature_slice(neg_data, sample_ind)
        results = crossvalidate(pos_data_sample, neg_data_sample,
                                model=args.model, model_params=model_params,
                                save_fname=save_fname,
                                dataset=args.dataset,
                                pca_cache_path=args.pca_cache_path
                                )
        print_results(args.output_file, *results)

    '''
    therm_basis = [5, 10, 20, 30]
    au_basis = [5, 9, 15]
    therm_comp = [3, 9, 12, 15]
    au_comp = [3, 9, 12, 15]
    all_results = []
    model_params = {}
    model_params['prob'] = False

    params = []

    for (therm_b, au_b, therm_c, au_c) in itertools.product(therm_basis,
                                                            au_basis,
                                                            therm_comp,
                                                            au_comp):
        if (au_c > au_b) or (therm_c > therm_b):
            continue

        model_params['therm_basis'] = therm_b
        model_params['au_basis'] = au_b
        model_params['therm_components'] = therm_c
        model_params['au_components'] = au_c

        results = crossvalidate(pos_data, neg_data,
                                model=model, model_params=model_params)

        results = map(np.mean, results)
        all_results.append(results)
        params.append((therm_b, au_b, therm_c, au_c))

        print "Params:", model_params, " result:", results

    print "therm_basis,au_basis,therm_comps,au_comps,accuracy,recall,tpr,fpr"

    for res, param in zip(all_results, params):
        print "%s,%s" % (array_to_str(param), array_to_str(res))
    '''


if __name__ == '__main__':
    sys.exit(main())
