from Bio import SeqIO
from Bio.Seq import Seq
import sys

'''
    Generate miRNA and sequence for the HITS-CLIP experiment
    based on the seed motif from supplimentary data.
'''


def get_all_motifs(motif_8mer):
    '''
        Get all possible 6/7/8 mer seed motifs from the given 8mer seed motif
    '''
    motifs = []
    motifs.append(motif_8mer)
    # 7 mers
    motifs.append(motif_8mer[:7])  # 1 - 7
    motifs.append(motif_8mer[1:])  # 2 - 8
    # 6 mers
    motifs.append(motif_8mer[:6])  # 1 - 6
    motifs.append(motif_8mer[1:7])  # 2 - 7
    motifs.append(motif_8mer[2:])  # 3 - 8

    return motifs


def main():
    seed_motif_file = '../../../data/HITS-CLIP/mir_seed_sequences.txt'
    mir_fasta_file = '../../../data/mirnas/mature.fa'
    output_file = '../../../data/HITS-CLIP/hits_clip_mirnas.txt'
    mir_map = {}

    with open(seed_motif_file, 'rU') as fp:
        for line in fp:
            for mir_seed_seq in get_all_motifs(line.strip()):
                mir_seed_seq = Seq(mir_seed_seq)
                mir_seed_seq = mir_seed_seq.transcribe().reverse_complement()
                mir_map[str(mir_seed_seq)] = []

    for record in SeqIO.parse(mir_fasta_file, "fasta"):
        name = record.id.split(" ")[0]
        seq = str(record.seq)
        for seed_seq in get_all_motifs(seq[:8]):
            if name.startswith('mmu') and seed_seq in mir_map:
                mir_map[seed_seq].append((name, seq))

    with open(output_file, 'w') as ofp:
        for mir_seed in sorted(mir_map.keys(), key=lambda x: str(Seq(x).reverse_complement().back_transcribe())):
            print str(Seq(mir_seed).reverse_complement().back_transcribe())
            for (name, seq) in mir_map[mir_seed]:
                print "\t", name
                ofp.write("%s,%s\n" % (name, seq))


if __name__ == '__main__':
    sys.exit(main())
