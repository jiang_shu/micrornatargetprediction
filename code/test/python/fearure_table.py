import sys
import numpy as np
from pipeline import Pipeline
import preprocess
from parallelize import run_func_parallely
import time
import re


def usage():
    print "%s <input file> <#cols>" % sys.argv[0]


def main():
    if len(sys.argv) != 3:
        usage()
        return 1
    data_file = sys.argv[1]

    print "Reading data from file"
    with open(data_file, 'rU') as fp:
        data = fp.readlines()
    print "Done."

    if sys.argv[2] == 'max_len':
        pipeline = (Pipeline())\
            .add_step(preprocess.remove_unwanted_chars, delimiter=',')\
            .add_step(preprocess.get_slice,
                      from_index=4, delimiter=' ')\
            .add_step(lambda x: len(x))
        data_len = run_func_parallely(pipeline.execute,
                                      data,
                                      kwparams={'for_each': True},
                                      combine_func=lambda l: [x for sublist in l
                                                              for x in sublist])
        max_data_len = max(data_len)
        print "Maximum data length is ", max_data_len
    else:
        max_data_len = int(sys.argv[2])

    pipeline = (Pipeline())\
        .add_step(preprocess.remove_unwanted_chars, delimiter=',')\
        .add_step(preprocess.get_slice,
                  from_index=4, delimiter=' ', as_numeric=True)\
        .add_step(preprocess.fit_length, desired_len=max_data_len)

    print "Preprocessing data."
    t1 = time.time()
    data = run_func_parallely(pipeline.execute,
                              data,
                              kwparams={'for_each': True})
    t2 = time.time()
    print "Done (%f sec)." % (t2 - t1)

    m = re.search('(.*)[.].*', data_file)
    out_data_file = m.group(1)

    out_data_file = "%s_%d.csv" % (out_data_file, max_data_len)
    print "Writing to files: ", out_data_file
    np.savetxt(out_data_file, data, delimiter=',')
    print "Done"

if __name__ == '__main__':
    sys.exit(main())
