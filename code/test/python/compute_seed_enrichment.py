import sys
from batch_file_reader import BatchFileReader
from scipy.stats import binom_test
import argparse


def parse_args():
    argparser = argparse.ArgumentParser(
        description="Computes seed enrichment from an alignment file"
    )
    argparser.add_argument('input', help='input file name')
    argparser.add_argument('output', help='output file name')
    options = argparser.add_mutually_exclusive_group(required=True)
    options.add_argument('--alignments', action='store_true', dest='alignments',
                         help='Indicates that specified file contains a '
                         'list of alignments')
    options.add_argument('--counts', action='store_true', dest='counts',
                         help='Indicates that specified file contains '
                         'pairs of (alignment,count) values')

    return argparser.parse_args()


def parse_row(x):
    y = x.strip().split(",")
    return (y[0], int(y[1]))


def main():
    args = parse_args()

    input_file = args.input
    output_file = args.output
    alignment_counts = {}
    total_count = 0

    if args.alignments is True:
        alignment_data = []
        with BatchFileReader(input_file,
                             process_row=lambda x: x.strip()) as reader:
            reader.read(alignment_data)
        print "Read %d records" % len(alignment_data)

        alignment_counts = {}
        total_count = len(alignment_data)
        print "number of records:", total_count

        for alignment in alignment_data:
            count = alignment_counts.get(alignment, 0)
            alignment_counts[alignment] = count + 1

    elif args.counts is True:
        alignment_data = []
        with BatchFileReader(input_file,
                             process_row=parse_row) as reader:
            reader.read(alignment_data)
        print "Read %d records" % len(alignment_data)
        for  (al, c) in alignment_data:
            alignment_counts[al] = c
            total_count += c
    else:
        raise Exception('Illegal option.')

    print "Computing pvalues."

    pvalues = {}
    # Compute pvalue
    for alignment, count in alignment_counts.iteritems():
        p = 0.25 ** len(alignment)
        pvalues[alignment] = binom_test(count, total_count, p)

    print "Writing output to:", output_file
    with open(output_file, 'w') as fp:
        for alignment, pval in sorted(pvalues.iteritems(),
                                      key=lambda (x, y): (y, x)):
            fp.write("%s,%f\n" % (alignment, pval))

    return 0


if __name__ == '__main__':
    sys.exit(main())
