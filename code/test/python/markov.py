import numpy as np
from numpy import random
import sys
import time


def get_tuple(item):
    if type(item) == np.ndarray or type(item) == list:
        return tuple(item)
    else:
        return (item, )


def get_counts(data):
    count = {}
    for i in range(data.shape[0]):
        row = get_tuple(data[i])
        c = count.get(row, 0.0)
        count[row] = c + 1

    for k in count.keys():
        count[k] /= float(data.shape[0])
    return count


def get_probability(chain, trans_prob):
    p = np.log(trans_prob[0].get(get_tuple(chain[0]), 0.0))
    for i in range(1, chain.size):
        p += np.log(trans_prob[i].get((chain[i-1], chain[i]), 0.0))

    return p


def sample_char(prob_map):
    cumsum = 0
    r = random.rand()
    c, p = 0.0, 0.0
    for char, prob in prob_map.iteritems():
        cumsum += prob
        if r < cumsum:
            return char, prob
        c, p = char, prob
    return c, p


def get_cond_prob(cond_char, cond_char_prob, prob_map):
    cond_prob = {}
    for (char1, char2) in prob_map.keys():
        if char1 != cond_char:
            continue
        cond_prob[char2] = prob_map[(char1, char2)]/cond_char_prob
    return cond_prob


def get_marginal_prob(char, index, prob_map):
    mar_prob = 0
    for key, prob in prob_map.iteritems():
        if key[index] == char:
            mar_prob += prob
    return mar_prob


def sample(trans_probs, count):
    random.seed(seed=int(time.time()))
    all_samples = []
    for k in range(count):
        samples = []
        char, prob = sample_char(trans_probs[0])
        samples.append(char[0])
        for i in range(1, len(trans_probs)):
            prob_map = get_cond_prob(char, prob, trans_probs[i])
            char, prob = sample_char(prob_map)
            samples.append(char)
            prob = get_marginal_prob(char, 0, trans_probs[i])
        all_samples.append(samples)

    all_samples = np.array(all_samples)
    return all_samples


def main():
    pos_data = np.genfromtxt('../../../data/crossdata_pos.csv', delimiter=",")
    neg_data = np.genfromtxt('../../../data/crossdata_neg.csv', delimiter=",")

    pos_alignments = pos_data[:,55:]
    neg_alignments = neg_data[:,55:]

    print "Computing transition probabilities"
    trans_prob = []
    trans_prob.append(get_counts(pos_alignments[:, 0]))

    for i in range(1, pos_alignments.shape[1]):
        trans_prob.append(get_counts(pos_alignments[:, (i-1, i)]))
    print "Done."

    print "Computing probabilities."
    pos_probs = []
    for i in range(pos_alignments.shape[0]):
        pos_probs.append(get_probability(pos_alignments[i,:], trans_prob))
    pos_probs = np.array(pos_probs)
    neg_probs = []
    for i in range(neg_alignments.shape[0]):
        neg_probs.append(get_probability(neg_alignments[i,:], trans_prob))
    neg_probs = np.array(neg_probs)
    print "Done."

    np.savetxt("../../../data/clip_pos_probs.csv", pos_probs,
               fmt="%.3f", delimiter=",")
    np.savetxt("../../../data/clip_neg_probs.csv", neg_probs,
               fmt="%.3f", delimiter=",")

    samples = sample(trans_prob, 100)
    np.savetxt("../../../data/clip_samples.csv", samples,
               fmt="%d", delimiter=",")

if __name__ == '__main__':
    sys.exit(main())
