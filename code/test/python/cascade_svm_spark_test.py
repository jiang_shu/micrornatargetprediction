import sys
import numpy as np
from cascade_svm_spark import CascadeSvmSpark
from pyspark import SparkContext
from tempfile import NamedTemporaryFile
from sklearn.svm import SVC
import time


def get_misclassification_error(model, rdd):
    misclassified = rdd\
        .map(lambda x: 0 if int(x[0]) == int(model.predict(x[1:])[0]) else 1)\
        .reduce(lambda x, y: x+y)
    total = rdd.count()

    return misclassified/float(total)


def get_prob(model, rdd):
    probs = rdd.map(lambda x: [x[0], model.predict_proba(x[1:])]).collect()
    probs = np.vstack(probs)
    return probs


def label_func(x):
    return 1.0 if ((x[0] - 2)**2 + (x[1] - 2)**2 <= 4 or
                   (x[0] + 2)**2 + (x[1] + 2)**2 <= 6) else 0.0

def train_predict(sc, rdd, *params, **kw_params):
    t1 = time.time()
    svm_model = CascadeSvmSpark.train(sc, rdd, *params, **kw_params)
    t2 = time.time()
    diff = t2 - t1
    error = get_misclassification_error(svm_model, rdd)
    return (diff, error)



def main():
    sc = SparkContext(appName="Cascade SVM Test")
    splits = 80

    data = 4.0 * np.random.randn(100000, 2)
    compute_prob = False
    labels = np.atleast_2d([label_func(x) for x in data])
    labelled_data = np.hstack((labels.T, data))
    labelled_data_rdd = sc.parallelize(labelled_data, splits)

    for i in range(1, 6):
        diff, error = train_predict(sc, labelled_data_rdd, iterations=i,
                                    gamma=0.01)

        print "Training size:", data.shape[0]
        print "Number of partitions:", splits
        print "Iterations:", i
        print "Parallel training error:", error
        print "Parallel training time %.3f sec" % diff

    clf = SVC(C=1.0, gamma=0.01)
    t1 = time.time()
    clf.fit(data, labels.T)
    t2 = time.time()
    diff2 = t2 - t1
    error2 = get_misclassification_error(clf, labelled_data_rdd)

    print "Serial training error:", error2
    print "Serial training time %.3f sec" % diff2

    if compute_prob is True:
        print "Computing probabilities."
        probs = get_prob(svm_model, labelled_data_rdd)
        temp_fp = NamedTemporaryFile(mode='w', delete=False)
        print "Writing probabilities to:", temp_fp.name
        np.savetxt(temp_fp, probs, fmt='%.6f')

    return 0

if __name__ == '__main__':
    sys.exit(main())
