import unittest
import numpy as np
import copy
from feature_transformers import FeatureTransformer, FeatureTransformerSeed,\
    NanTransformer, NanMeanTransformer, StandardScaler
from record import Record


SITE_REGION_TYPES = ['3UTR', '5UTR', 'CDS']


def generate_data_row(dtype='seedless', seed_site=False,
                      with_nan=False, data_mean=0.0,
                      label=None, record_id=None):

    delg = [np.random.randn(27) + data_mean, np.random.randn(27) + data_mean]
    deldelg = [np.random.randn(27) + data_mean, np.random.randn(27) + data_mean]
    au = [np.random.randn(27) + data_mean, np.random.randn(27) + data_mean]
    if with_nan is True:
        delg[0][np.random.randint(0, 27, 13)] = np.nan
        delg[1][np.random.randint(0, 27, 13)] = np.nan
        deldelg[0][np.random.randint(0, 27, 13)] = np.nan
        deldelg[1][np.random.randint(0, 27, 13)] = np.nan
        au[0][np.random.randint(0, 27, 13)] = np.nan
        au[1][np.random.randint(0, 27, 13)] = np.nan

    if not seed_site:
        consv, rel_site_loc, seed_enrichment = np.random.rand(3)
    else:
        consv = np.random.rand(3)
        rel_site_loc, seed_enrichment = np.random.rand(2)

    site_region = SITE_REGION_TYPES[np.random.randint(0, 3)]
    site_len = np.random.randint(22, 40)

    feature_list = [delg, deldelg, au, consv, site_region, rel_site_loc,
                    seed_enrichment, site_len]
    return Record(feature_list, label=label, record_id=record_id)


class FeatureTransformerTest(unittest.TestCase):
    def setUp(self):
        self._data = []
        for i in range(10):
            self._data.append(generate_data_row(label=1,
                                                record_id='mRNA:%d' % i))

        self._model_params = dict(basis=10)
        self.feature_transformer = FeatureTransformer(
            categories=SITE_REGION_TYPES,
            basis=10
        )
        self._transformed_data = []
        for datum in self._data:
            self._transformed_data.append(
                self.feature_transformer.transform(datum)
            )

    def test_transform(self):
        expected_feature_len = self._model_params['basis'] * 6 + 4
        self.assertEqual(len(self._transformed_data), len(self._data))
        for datum in self._transformed_data:
            numeric_feature, cat_feature = datum[0], datum[1]
            self.assertEqual(numeric_feature.size, expected_feature_len)
            self.assertEqual(cat_feature.size, 3)

    def test_cat_feature(self):
        for i in range(len(self._data)):
            site_region = self._data[i][4]
            if site_region == '3UTR':
                self.assertEqual(tuple(self._transformed_data[i][1]), (1, 0, 0))
            elif site_region == '5UTR':
                self.assertEqual(tuple(self._transformed_data[i][1]), (0, 1, 0))
            else:
                self.assertEqual(tuple(self._transformed_data[i][1]), (0, 0, 1))

    def test_transform_seed(self):
        self.feature_transformer = FeatureTransformerSeed(
            categories=SITE_REGION_TYPES,
            basis=10
        )
        tdata = []
        for i in range(10):
            row = generate_data_row(seed_site=True, label=1,
                                    record_id='mRNA:%d' % i)
            trow = self.feature_transformer.transform(row)
            tdata.append(trow[0])
            self.assertEqual(trow[0].size, 66)
            self.assertEqual(trow[1].size, 3)

        tdata = np.vstack(tdata)
        self.assertEqual(tdata.shape, (10, 66))
        self.assertEqual(type(tdata), np.ndarray)
        self.assertEqual(tdata.dtype, np.float64)


class NanTransformerTest(unittest.TestCase):
    def setUp(self):
        self._train_data = []
        self._test_data = []
        for i in range(10):
            label = np.random.randint(0, 2)
            data_row = generate_data_row(with_nan=True, label=label,
                                         record_id='mRNA:%d' % i)
            self._train_data.append(data_row)

            label = np.random.randint(0, 2)
            data_row = generate_data_row(with_nan=True, label=label,
                                         record_id='mRNA:%d' % i)
            self._test_data.append(data_row)

        self.nan_transformer = NanTransformer()
        self._transformed_train_data = []
        self._transformed_test_data = []
        for train_datum, test_datum in zip(self._train_data, self._test_data):
            self._transformed_train_data.append(
                self.nan_transformer.transform_train(copy.deepcopy(train_datum))
            )
            self._transformed_test_data.append(
                self.nan_transformer.transform_test(copy.deepcopy(test_datum))
            )

    def test_transform_train(self):
        for i in range(len(self._train_data)):
            feature = self._train_data[i]
            tfeature = self._transformed_train_data[i]
            self.assertEqual(tfeature.label(), feature.label())

            for fn_i in range(3):
                for fn_j in range(2):
                    nan_indices = np.where(np.isnan(feature[fn_i][fn_j]))[0]
                    self.assertGreater(nan_indices.size, 0)
                    s = np.sum(
                        tfeature[fn_i][fn_j][nan_indices]
                    )
                    self.assertEqual(s, 0)
                    nan_vals = np.where(np.isnan(tfeature[fn_i][fn_j]))[0]
                    self.assertEqual(nan_vals.size, 0)

    def test_transform_test(self):
        for i in range(len(self._test_data)):
            feature = self._test_data[i]
            tfeature = self._transformed_test_data[i]
            self.assertEqual(feature.label(), tfeature.label())

            for fn_i in range(3):
                for fn_j in range(2):
                    nan_indices = np.where(np.isnan(feature[fn_i][fn_j]))[0]
                    self.assertGreater(nan_indices.size, 0)
                    s = np.sum(
                        tfeature[fn_i][fn_j][nan_indices]
                    )
                    self.assertEqual(s, 0)
                    nan_vals = np.where(np.isnan(tfeature[fn_i][fn_j]))[0]
                    self.assertEqual(nan_vals.size, 0)


class NanMeanTransformerTest(unittest.TestCase):
    def setUp(self):
        self._data = []
        self._means = []
        self.mu_pos = 0
        self.mu_neg = 100
        for i in range(10):
            label = np.random.randint(0, 2)
            data_mean = self.mu_neg if label == 0 else self.mu_pos
            data_row = generate_data_row(with_nan=True, data_mean=data_mean,
                                         label=label, record_id='mRNA:%d' % i)
            self._data.append(data_row)

        for i in range(6):
            self._means.append((np.random.randn(27) + self.mu_neg,
                                np.random.randn(27) + self.mu_pos))

    def test_transform_train(self):
        transformer = NanMeanTransformer(self._means)
        for i in range(len(self._data)):
            feature = self._data[i]
            label = feature.label()
            tfeature = transformer.transform_train(
                copy.deepcopy(feature)
            )
            self.assertEqual(feature.label(), tfeature.label())

            for fn_i in range(3):
                for fn_j in range(2):
                    nan_indices = np.where(np.isnan(feature[fn_i][fn_j]))[0]
                    self.assertGreater(nan_indices.size, 0)
                    m = np.mean(
                        tfeature[fn_i][fn_j][nan_indices]
                    )
                    if label == 0:
                        # Check if mean is within 5 std of actual mean
                        self.assertTrue(self.mu_neg - 5 <= m <= self.mu_neg + 5)
                    else:
                        self.assertTrue(self.mu_pos - 5 <= m <= self.mu_pos + 5)
                    nan_vals = np.where(np.isnan(tfeature[fn_i][fn_j]))[0]
                    self.assertEqual(nan_vals.size, 0)

    def test_transform_test(self):
        transformer = NanMeanTransformer(self._means)
        for i in range(len(self._data)):
            feature = self._data[i]
            label = feature.label()
            tfeature = transformer.transform_test(
                copy.deepcopy(feature)
            )
            self.assertEqual(feature.label(), tfeature.label())

            for fn_i in range(3):
                for fn_j in range(2):
                    nan_indices = np.where(np.isnan(feature[fn_i][fn_j]))[0]
                    self.assertGreater(nan_indices.size, 0)
                    m = np.mean(
                        tfeature[fn_i][fn_j][nan_indices]
                    )
                    if label == 0:
                        # Check if mean is within 5 std of actual mean
                        self.assertTrue(self.mu_neg - 5 <= m <= self.mu_neg + 5)
                    else:
                        self.assertTrue(self.mu_pos - 5 <= m <= self.mu_pos + 5)
                    nan_vals = np.where(np.isnan(tfeature[fn_i][fn_j]))[0]
                    self.assertEqual(nan_vals.size, 0)

    def test_transform_test_with_infer_mean(self):
        transformer = NanMeanTransformer(self._means, infer_test_mean=True)
        for i in range(len(self._data)):
            feature = self._data[i]
            tfeature = transformer.transform_test(
                copy.deepcopy(feature)
            )
            label = feature.label()
            for fn_i in range(3):
                for fn_j in range(2):
                    nan_indices = np.where(np.isnan(feature[fn_i][fn_j]))[0]
                    self.assertGreater(nan_indices.size, 0)
                    m = np.mean(
                        tfeature[fn_i][fn_j][nan_indices]
                    )
                    if label == 0:
                        # Check if mean is within 5 std of actual mean
                        self.assertTrue(self.mu_neg - 5 <= m <= self.mu_neg + 5)
                    else:
                        self.assertTrue(self.mu_pos - 5 <= m <= self.mu_pos + 5)
                    nan_vals = np.where(np.isnan(tfeature[fn_i][fn_j]))[0]
                    self.assertEqual(nan_vals.size, 0)


class Rdd:
    def __init__(self, data):
        self._data = copy.deepcopy(data)

    def map(self, func):
        return Rdd(map(func, self._data))

    def reduce(self, func):
        return reduce(func, self._data)


class TestStandardScaler(unittest.TestCase):
    def setUp(self):
        # Set the seed so that we can test mean/std deterministically.
        np.random.seed(282629734)
        self._means = np.arange(10)
        self._std = np.random.rand(10)*5
        self._data = []
        for i in range(10):
            self._data.append(
                np.random.randn(100, 1)*self._std[i] + self._means[i])
        self._data = Rdd(np.hstack(self._data))

    def test_fit(self):
        scaler = StandardScaler()
        scaler = scaler.fit(self._data)
        expected_mean = np.mean(self._data._data, axis=0)
        expected_std = np.std(self._data._data, axis=0)
        for i in range(expected_mean.size):
            self.assertAlmostEqual(expected_mean[i], scaler._mean[i])
            self.assertAlmostEqual(expected_std[i], scaler._std[i])

    def test_transform(self):
        scaler = StandardScaler()
        scaler = scaler.fit(self._data)
        transformed_data = []
        for data_row in self._data._data:
            transformed_data.append(scaler.transform(data_row))

        transformed_data = np.vstack(transformed_data)
        tmean = np.mean(transformed_data, axis=0)
        tstd = np.std(transformed_data, axis=0)
        for i in range(tmean.size):
            self.assertAlmostEqual(tmean[i], 0.0)
            self.assertAlmostEqual(tstd[i], 1.0)


if __name__ == '__main__':
    unittest.main()
