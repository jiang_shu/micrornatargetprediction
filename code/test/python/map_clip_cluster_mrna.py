'''
    Read output produced by NCBI blast and prints the following:
        <mrna>,<start ind>,<end ind>,<region>,<cluster peak>,<seq>
    If multiple mrna transcript correspond to the gene then the longest one
    is selected.
'''
import sys
from redisdao import RedisDao


def parse_ncbi_output(fname):
    cluster_map = {}  # Map the CLIP cluster seq to mRNA with longest transcript
    count = 0
    try:
        count += 1
        with open(fname, 'rU') as fp, RedisDao() as redis_dao:
            for line in fp:
                line = line.strip()
                if not line:
                    continue
                items = line.split(",")
                mrna_name = items[1].split("|")[-2]
                # Ignore predicted mRNA transcripts.
                if mrna_name.startswith("X"):
                    continue
                mrna_name = mrna_name.split(".")[0]
                mrna_obj = redis_dao.get_messenger_rna_from_name(mrna_name,
                                                                 assembly='mm9')

                if mrna_obj is None:
                    print "Could not find sequence for mRNA: %s" % mrna_name
                    continue

                cluster_id = items[0].split("|")[-1]

                mrna_start, mrna_end = int(items[-4]), int(items[-3])
                # indices returned by NCBI are 1 based on includes both ends
                mrna_start -= 1  # convert to 0 based index
                mrna_end -= 1

                if cluster_id in cluster_map:
                    mrna = cluster_map[cluster_id][0]
                    if len(mrna._seq) > len(mrna_obj._seq):
                        continue
                cluster_map[cluster_id] = (mrna_obj, mrna_start, mrna_end,
                                           items[0])
    except:
        print "Caught exception at line %d" % count
        raise
    return cluster_map


def print_data(cluster_mrna_map, output_file):
    with open(output_file, 'w') as ofp:
        for cluster_id in sorted(cluster_mrna_map.keys(),
                                 key=lambda x: (cluster_mrna_map[x][0]._name,
                                                cluster_mrna_map[x][1],
                                                cluster_mrna_map[x][2])):
            items = cluster_mrna_map[cluster_id]
            mrna = items[0]
            mrna_start, mrna_end = items[1], items[2]
            desc_items = cluster_id.split("_")
            region, cluster_peak = desc_items[-2], desc_items[-1]
            ofp.write("%s,%d,%d,%s,%s\n" % (mrna._name, mrna_start, mrna_end,
                                            region, cluster_peak))


def main():
    ncbi_dataset = '../../../data/HITS-CLIP/mm9_ncbi_seq_mrna_map.csv'
    output_file = '../../../data/HITS-CLIP/mm9_mrna_dataset.csv'
    cluster_mrna_map = parse_ncbi_output(ncbi_dataset)
    print_data(cluster_mrna_map, output_file)


if __name__ == '__main__':
    sys.exit(main())
