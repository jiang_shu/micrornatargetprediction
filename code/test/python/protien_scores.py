"""
    Given a file containing mRNA, miRNA, loc, score
    and another file containing mRNA,miRNA,protien fold change information
    output file containing mRNA, miRNA, score, fold change.
"""

import sys
import numpy as np


def get_max_score(score_file):
    mrna_mirna_data = {}
    with open(score_file, 'rU') as sfp:
        for line in sfp:
            line = line.strip()
            mrna, mirna, loc, _, _, score = line.split(",")
            score = float(score)
            if (mrna, mirna) not in mrna_mirna_data:
                mrna_mirna_data[(mrna, mirna)] = {}

            if loc not in mrna_mirna_data[(mrna, mirna)]:
                mrna_mirna_data[(mrna, mirna)][loc] = []

            mrna_mirna_data[(mrna, mirna)][loc].append(score)

    for (mrna, mirna) in mrna_mirna_data.keys():
        for loc in mrna_mirna_data[(mrna, mirna)].keys():
            mrna_mirna_data[(mrna, mirna)][loc] =\
                np.mean(mrna_mirna_data[(mrna, mirna)][loc])

        max_score = np.max(mrna_mirna_data[(mrna, mirna)].values())
        mrna_mirna_data[(mrna, mirna)] = max_score
    return mrna_mirna_data


def get_score_fold_change(score_file, fold_change_file, mirna_name):
    mrna_mirna_data = get_max_score(score_file)
    mrna_score_fold_change = {}
    with open(fold_change_file, 'rU') as fp:
        for line in fp:
            line = line.strip()
            mrna, fold_change = line.split(",")
            fold_change = float(fold_change)
            if (mrna, mirna_name) in mrna_mirna_data:
                score = mrna_mirna_data[(mrna, mirna_name)]
                mrna_score_fold_change[(mrna, mirna_name)] = (score,
                                                              fold_change)
    return mrna_score_fold_change


def main():
    if len(sys.argv) != 5:
        print "Usage: %s <score file> "\
            "<fold change file> <mirna_name> <output file>" % sys.argv[0]
        return 1

    score_file = sys.argv[1]
    fold_change_file = sys.argv[2]
    mirna_name = sys.argv[3]
    output_file = sys.argv[4]

    score_fold_change = get_score_fold_change(
        score_file, fold_change_file, mirna_name
    )
    with open(output_file, 'w') as ofp:
        for key, val in sorted(score_fold_change.iteritems(),
                               key=lambda (k, v): v[0], reverse=True):
            ofp.write("%s,%s,%f,%f\n" % (key[0], key[1], val[0], val[1]))

    return 0


if __name__ == '__main__':
    sys.exit(main())
