import sys
import os.path
import shutil
import argparse


def visit(arg, dirname, files):
    dst = arg[1]
    ignore_part = arg[0]

    if dirname.endswith(".prob"):
        for fname in files:
            path_components = dirname.split("/")
            start_index = path_components.index(ignore_part) + 1
            dst_fname = "_".join(path_components[start_index:]) + "_%s" % (fname)
            dst_path = os.path.join(dst, dst_fname)
            src_path = os.path.join(dirname, fname)
            print "Copying %s => %s" % (src_path, dst_path)
            shutil.copy(os.path.join(dirname, fname), dst_path)


def main():
    argparser = argparse.ArgumentParser(
        description="Finds file ending with .prob and copies them"
        " in dest directory in a non-overwriting way."
    )
    argparser.add_argument('src', help='Source directory')
    argparser.add_argument('dst', help='Destination directory')
    argparser.add_argument('--ignore-before', metavar='<path component>',
                           default='', dest='ignore_part',
                           help='Ignore parts of the path before this when '
                           'constructing destination file names'
                           )
    args = argparser.parse_args()

    os.path.walk(args.src, visit, (args.ignore_part, args.dst))


if __name__ == '__main__':
    sys.exit(main())
