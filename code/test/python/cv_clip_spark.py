import sys
import numpy as np
from sklearn.cross_validation import KFold
import time
import train_predict_spark
from train_predict_spark import DEFAULT_REG_PARAM
import argparse
from pyspark import SparkContext
# from pyspark import StorageLevel
from batch_file_reader import BatchFileReader
from preprocess_spark import parse_str_array, \
    preprocess_data
import gc
from collections import OrderedDict
import itertools
from functools import partial


RANDOM_SEED = 13
RDD_PARTITIONS = 80
# The first choice should be ALL
REGION_CHOICES = set(['3UTR', '5UTR', 'CDS'])


def parse_arguments():
    argparser = argparse.ArgumentParser(
        description="Functional MiRNA Target Prediction"
    )

    subparsers = argparser.add_subparsers(
        title='Modes of operation', help='sub-command help.',
        dest='subparser_name'
    )
    # Parameters for Crosss Validation
    cv_parser = subparsers.add_parser(
        'crossvalidate', help='Compute Cross-Validation Performance'
    )
    cv_parser.add_argument(
        "--model-params",
        default=[20, 1.0, DEFAULT_REG_PARAM, 'LINEAR', 0.01],
        nargs='*',
        dest='model_params',
        help='model parameters: #basis: Defaults to (20), '
        'C: penalty for error term for kernel model (defaults to 1.0), '
        'reg_param: Regularization parameter for linear model'
        'kernel: (defaults to LINEAR), '
        'kernel_params: gamma for RBF (defaults to 0.01)'
    )
    cv_parser.add_argument("pos_file", metavar='pos_file',
                           help='path to positive data')
    cv_parser.add_argument("neg_file", metavar='neg_file',
                           help='path to negative data')
    cv_parser.add_argument(
        '--cv-runs', metavar='K', type=int, dest='cv_runs', default=10,
        help='K of the k-fold corssvalidation'
    )

    # Parameters for Tune
    tune_parser = subparsers.add_parser(
        'tune', help='Tune parameters using Cross-Validation '
        'Evaluate FN model at a grid of different parameter values.'
    )
    tune_parser.add_argument('--nbasis', dest='nbasis', default=[20], nargs='*',
                             help='Number of basis functions.')
    tune_parser.add_argument("--kernel", dest='kernel',
                             choices=['LINEAR', 'RBF'], default='LINEAR',
                             help='Type of kernel to use.')
    tune_parser.add_argument("--C", dest='C', nargs='*', default=[1.0],
                             help='The values of C for RBF kernel')
    tune_parser.add_argument("--gamma", dest='gamma', nargs='*', default=[0.01],
                             help='The values of gamma to use for RBF kernel')
    tune_parser.add_argument("--reg-param", dest='reg_param', nargs='*',
                             default=[DEFAULT_REG_PARAM],
                             help='Regularization parameters to use for '
                             'linear model')
    tune_parser.add_argument("pos_file", metavar='pos_file',
                             help='path to positive data')
    tune_parser.add_argument("neg_file", metavar='neg_file',
                             help='path to negative data')
    tune_parser.add_argument(
        '--cv-runs', metavar='K', type=int, dest='cv_runs', default=10,
        help='K of the k-fold corssvalidation'
    )

    # Parameters for Train/Predict
    predict_parser = subparsers.add_parser(
        'predict', help='Train and Predict on different datasets'
    )
    predict_parser.add_argument(
        "--model-params",
        default=[20, 1.0, DEFAULT_REG_PARAM, 'LINEAR', 0.01],
        nargs='*',
        dest='model_params',
        help='model parameters: #basis: Defaults to (20), '
        'C: penalty for error term for kernel model (defaults to 1.0), '
        'reg_param: Regularization parameter for linear model'
        'kernel: (defaults to LINEAR), '
        'kernel_params: gamma for RBF (defaults to 0.01)'
    )
    predict_parser.add_argument("pos_file", metavar='pos_train_file',
                                help='path to positive train data')
    predict_parser.add_argument("neg_file", metavar='neg_train_file',
                                help='path to negative train data')
    predict_parser.add_argument("pos_test_file", metavar='pos_test_file',
                                help='path to positive test data')
    predict_parser.add_argument("neg_test_file", metavar='neg_test_file',
                                help='path to negative test data')

    # Common options for all parsers
    argparser.add_argument(
        '--save', metavar='file', dest='save_fname', default=None,
        help='File to save the preprocessed data to'
    )
    argparser.add_argument("seed_enrichment_file",
                           metavar='seed_enrichment_file',
                           help='path to seed enrichment file')
    argparser.add_argument("output_file", metavar='output_file',
                           help='File to write the output to')

    argparser.add_argument(
        "--model", default='FN',
        choices=['FN', 'FN_PCA', 'MV'],
        help='type of model. FN (default): Functional therm and AU, '
        'FN_PCA: Functional features along with PCA '
        'MV: Multivariate features for all.'
    )
    argparser.add_argument(
        "--seed-sites", action='store_true',
        dest='seed_sites',
        help='Filter seed sites. By defaults only uses seedless sites'
    )
    argparser.add_argument(
        "--region", default='ALL', choices=list(REGION_CHOICES) + ['ALL'],
        dest='region',
        help='Filter a given region. By default filters all region.'
    )
    argparser.add_argument(
        "--cluster-assignment", metavar='<file>',
        help='Path to CSV file that defines cluster assignment. '
        'The CSV file contains key value pairs where the key represents '
        'the key by which rows are assigned to clusters and  the value '
        'represents the cluster to which a row should be assigned. '
        'The values are converted to numeric cluster indices.',
        dest='cluster_assn_file', default=None
    )
    argparser.add_argument(
        "--train-iters", metavar='<iterations>', type=int,
        default=train_predict_spark.DEFAULT_ITERATIONS,
        dest="train_iters",
        help='# Training iterations.'
    )
    argparser.add_argument(
        "--mean-file", metavar='<mean file>', default=None, dest="mean_file",
        help='File containing mean curves which are used to replace NaN values'
    )
    argparser.add_argument(
        "--infer-test-mean", action='store_true', dest="infer_test_mean",
        help='Infer the mean to use for test points rather than directly '
        'using the appropriate mean based on Test Label'
    )
    argparser.add_argument(
        '--dataset', default='',
        help='Used to write to dataset specific DFS paths'
    )
    argparser.add_argument(
        '--save-train-data',
        dest='save_train_data', action='store_true',
        help='Turns on whether or not to save transformed training/test data'
    )
    argparser.add_argument(
        '--cache-pca', metavar='path', dest='pca_cache_path', default=None,
        help='Path where to cache PCA objs to.'
        ' If not provided PCA obj are recomputed everytime during training.'
    )
    argparser.add_argument(
        '--subsample-runs', type=int, dest='subsample_runs', default=10,
        help='Number of times to subsample from the larger dataset'
    )
    argparser.add_argument(
        '--sample-frac', dest='sample_frac', default=1.0,
        help='The amount of positive data to subsample. Defaults to 1.0 '
        'which doesnt subsample positive data'
    )
    args = argparser.parse_args()
    # Print arguments
    print "Arguments:"
    for k, v in vars(args).iteritems():
        print k, " => ", v

    return args


def get_metrics(conf):
    ''' Compute various performance results given the confusion matrix '''
    error = (conf[0, 1] + conf[1, 0]) / (np.sum(conf))
    tp = float(conf[0, 0])
    tn = float(conf[1, 1])
    fp = float(conf[1, 0])
    fn = float(conf[0, 1])
    if tp == 0:
        accuracy, recall = 0.0, 0.0
        fpr, tpr = 0.0, 0.0
    else:
        accuracy = (tp/(tp + fp))
        recall = (tp/(tp + fn))
        positive = tp + fn
        negative = tn + fp
        fpr = fp/negative
        tpr = tp/positive
    return (accuracy, recall, fpr, tpr, error)


def array_to_str(arr, delimiter=" "):
    return delimiter.join([str(x) for x in arr])


def save_data(name, features):
    filename = '../../../data/%s' % name
    print "Saving data to:", filename

    delg, deldelg, au, cat_features, numeric_features =\
        features[0], features[1], features[2], features[3], features[4]

    header = "delg_site,delg_seed,deldelg_site,deldelg_seed"\
        ",au_site,au_seed,site_region,consv_site"

    if numeric_features.shape[1] == 6:
        header += ",consv_seed,consv_offseed"
    header += ",rel_site_loc,seed_match_type,site_len"

    with open(filename, 'w') as fp:
        fp.write("%s\n" % header)
        for feature in zip(delg[0], delg[1], deldelg[0], deldelg[1],
                           au[0], au[1], cat_features, numeric_features):
            fp.write("%s,%s,%s,%s,%s,%s" % (
                array_to_str(feature[0]),  # degG site
                array_to_str(feature[1]),  # delG seed
                array_to_str(feature[2]),  # deldelG site
                array_to_str(feature[3]),  # deldelG seed
                array_to_str(feature[4]),  # au site
                array_to_str(feature[5])  # au seed
            ))
            fp.write(",%s" % feature[6][0])  # region
            fp.write(",%s\n" % array_to_str(feature[7],
                                            delimiter=","))  # numeric features


def read_pos_seed_enrichment(fname):
    print "Reading seed enrichment"
    t1 = time.time()
    seed_enrichment = {}
    with open(fname, 'rU') as fp:
        for line in fp:
            line = line.strip().split(",")
            seed_enrichment[line[0]] = float(line[1])
    t2 = time.time()
    print "Done (%f sec)." % (t2 - t1)
    return seed_enrichment


def sample_rows(features, indices):
    # Returns rows from @features present in @indices
    return features.zipWithIndex()\
        .filter(lambda (r, i): True if i in indices else False)\
        .map(lambda x: x[0])


def combine_pos_neg_data(pos_data, neg_data):
    combined_data = pos_data.union(neg_data)
    combined_data = combined_data.coalesce(RDD_PARTITIONS)
    return combined_data


def train_predict(sc, pos_train_data, neg_train_data,
                  pos_test_data, neg_test_data,
                  model_params=None, save_fname=None, save_train_data=False,
                  cluster_assignment=None,
                  dataset=None, pca_cache_path=None,
                  curve_means=None, infer_test_mean=False,
                  train_iters=train_predict_spark.DEFAULT_ITERATIONS,
                  seed_sites=False
                  ):

    train_data = combine_pos_neg_data(pos_train_data, neg_train_data)
    test_data = combine_pos_neg_data(pos_test_data, neg_test_data)

    conf_matrix = train_predict_spark.train_predict(
        sc, train_data, test_data,
        model_params=model_params,
        cluster_assignment=cluster_assignment,
        dataset=dataset,
        pca_cache_path=pca_cache_path,
        save_train_data=save_train_data,
        curve_means=curve_means,
        infer_test_mean=infer_test_mean,
        seed_sites=seed_sites
    )
    print "Confusion matrix:", conf_matrix[0, 0], conf_matrix[0, 1],\
        conf_matrix[1, 0], conf_matrix[1, 1]
    acc, recall, fpr, tpr, _ = get_metrics(conf_matrix)
    print "Accuracy - Recall: %.3f - %.3f" % (acc, recall)
    print "TPR - FPR: %.3f - %.3f" % (tpr, fpr)
    return conf_matrix


def print_results(ofp, conf_matrices):
    # Write the header
    ofp.write("Accuracy,Recall,TPR,FPR,Misclassification\n")

    for conf_matrix in conf_matrices:
        acc, recall, fpr, tpr, error = get_metrics(conf_matrix)
        ofp.write("%.3f,%.3f,%.3f,%.3f,%.3f\n" % (acc, recall, tpr, fpr, error))


def get_tune_params(args):
    tune_params = OrderedDict([])
    tune_params['nbasis'] = map(int, args.nbasis)
    tune_params['C'] = map(float, args.C)
    tune_params['reg_param'] = map(float, args.reg_param)
    tune_params['gamma'] = map(float, args.gamma)
    return tune_params


def get_model_params(model, param_list):
    model_params = {
        'basis': int(param_list[0]),
        'C': float(param_list[1]),
        'reg_param': float(param_list[2]),
        'kernel': param_list[3],
        'kernel_params': {},
        'prob': True,
    }
    # model takes the following vals: 'FN', 'FN_PCA', 'MV'
    print "Model type:", model
    if model == 'FN_PCA':
        raise Exception('Model FN_PCA not implemented currently.')
    elif model == 'MV':
        model_params['basis'] = -1

    if not (param_list[3] == 'LINEAR' or param_list[3] == 'RBF'):
        raise Exception('Only LINEAR and RBF kernels are supported')

    if param_list[3] == 'RBF':
        if len(param_list) >= 5 and param_list[4] != 'None':
            gamma = float(param_list[4])
            model_params['kernel_params']['gamma'] = gamma

    print "Model params:"
    for k, v in sorted(model_params.iteritems(), key=lambda (x, y): x):
        print k, "=>", v

    return model_params


def print_tune_results(ofp, results, header=[]):
    # Print the header
    ofp.write("%s,Accuracy,Recall,TPR,FPR,Misclassification\n" %
              ",".join(header))
    for key in results.keys():
        params = ",".join([str(x) for x in key])
        for conf_matrix in results[key]:
            acc, recall, fpr, tpr, error = get_metrics(conf_matrix)
            ofp.write("%s,%.3f,%.3f,%.3f,%.3f,%.3f\n" %
                      (params, acc, recall, tpr, fpr, error))


def subsample_data(pos_data, neg_data, num_pos, num_neg, seed,
                   sample_frac=1.0):
    pos_data_sample = pos_data
    neg_data_sample = neg_data

    # Compute sample size with respect to the smaller dataset
    if num_pos > num_neg:
        sample_size = sample_frac * num_neg
    else:
        sample_size = sample_frac * num_pos

    pos_data_sample = pos_data.sample(False, sample_size/num_pos, seed)
    neg_data_sample = neg_data.sample(False, sample_size/num_neg, seed)

    return pos_data_sample, neg_data_sample


def read_cluster_assignment(fname):
    if fname is None:
        return None
    cluster_assignment = []
    reader = BatchFileReader(fname, process_row=lambda x: x.strip().split(","))
    reader.read(cluster_assignment)

    distinct_vals = set([v for k, v in cluster_assignment])
    cluster_id_map = dict([(k, i) for i, k in enumerate(sorted(distinct_vals))])
    print "Cluster IDS:"
    for key in sorted(cluster_id_map.keys()):
        print "%s -> %d" % (key, cluster_id_map[key])

    cluster_assignment_map = {}
    print "Cluster Assignment:"
    for key, val in cluster_assignment:
        cluster_assignment_map[key] = cluster_id_map[val]
        print "%s -> %d" % (key, cluster_assignment_map[key])
    return cluster_assignment_map


def read_mean_curves(fname):
    temp = []
    curve_means = []
    reader = BatchFileReader(fname)
    reader.read(temp)
    curve_means = [None]*(len(temp)/2)
    for i in range(len(temp)/2):
        curve_means[i] = (parse_str_array(temp[2*i]),
                          parse_str_array(temp[2*i + 1]))
    del temp
    return curve_means


class ClipPipeline:
    def __init__(self, spark_context, args):
        self._sc = spark_context
        self._args = args
        self._ofp = open(self._args.output_file, 'w')
        self._curve_means = None
        self._cluster_assignment = None
        self._initialize()

    def __del__(self):
        self._ofp.close()

    def _initialize(self):
        # set numpy random seed so that we have deterministic behavior acorss
        # runs
        np.random.seed(RANDOM_SEED)
        self._generate_new_random_seed()
        self._sample_frac = self._args.sample_frac
        if self._args.subparser_name == 'predict' or \
                self._args.subparser_name == 'crossvalidate':
            self._model_params = get_model_params(self._args.model,
                                                  self._args.model_params)
        else:
            self._tune_params = get_tune_params(self._args)

        self._seed_enrichment_map = read_pos_seed_enrichment(
            self._args.seed_enrichment_file
        )

        if self._args.mean_file is not None:
            print "Using curve means to replace NaNs"
            self._curve_means = read_mean_curves(self._args.mean_file)

        if self._args.cluster_assn_file is not None:
            self._cluster_assignment =\
                read_cluster_assignment(self._args.cluster_assn_file)

        if self._args.region == 'ALL':
            self._region = None
        else:
            self._region = set([self._args.region])

    def _generate_new_random_seed(self):
        self._rand_seed = np.random.randint(0, 10000)

    def _read_data(self, hdfs_fname):
        print "Reading data from file:", hdfs_fname
        t1 = time.time()
        data_rdd = self._sc.textFile(hdfs_fname, RDD_PARTITIONS)
        data_rdd = preprocess_data(
            data_rdd, seed_sites=self._args.seed_sites,
            region=self._region, seed_enrichment_map=self._seed_enrichment_map)
        data_rdd = data_rdd.repartition(RDD_PARTITIONS)
        t2 = time.time()
        print "Done (%f sec)." % (t2 - t1)
        return data_rdd

    def _read_subsampled_data(self, pos_fname, neg_fname):
        pos_data = self._read_data(pos_fname)
        neg_data = self._read_data(neg_fname)
        num_pos = pos_data.count()
        num_neg = neg_data.count()

        pos_sampled_data, neg_sampled_data = subsample_data(
            pos_data, neg_data, num_pos, num_neg, self._rand_seed,
            sample_frac=self._sample_frac)

        return pos_sampled_data, neg_sampled_data

    def _crossvalidate(self):
        cv_k = self._args.cv_runs
        print "Performing %d-fold crossvalidation" % cv_k
        pos_data, neg_data = self._read_subsampled_data(
            self._args.pos_file, self._args.neg_file)

        len_pos = pos_data.count()
        len_neg = neg_data.count()

        pos_kf = KFold(len_pos, cv_k)
        neg_kf = KFold(len_neg, cv_k)
        pos_ind = [(train, test) for train, test in pos_kf]
        neg_ind = [(train, test) for train, test in neg_kf]

        if self._args.save_fname is not None:
            save_data(self._args.save_fname+'_pos', pos_data)
            save_data(self._args.save_fname+'_neg', neg_data)

        conf_matrices = []
        for ind in zip(pos_ind, neg_ind):
            pos_train_ind = ind[0][0]
            pos_test_ind = ind[0][1]
            neg_train_ind = ind[1][0]
            neg_test_ind = ind[1][1]

            pos_train_data = sample_rows(pos_data, pos_train_ind)
            pos_test_data = sample_rows(pos_data, pos_test_ind)
            neg_train_data = sample_rows(neg_data, neg_train_ind)
            neg_test_data = sample_rows(neg_data, neg_test_ind)

            conf_matrix = train_predict(
                self._sc,
                pos_train_data, neg_train_data,
                pos_test_data, neg_test_data,
                model_params=self._model_params, dataset=self._args.dataset,
                cluster_assignment=self._cluster_assignment,
                pca_cache_path=self._args.pca_cache_path,
                save_train_data=self._args.save_train_data,
                curve_means=self._curve_means,
                infer_test_mean=self._args.infer_test_mean,
                seed_sites=self._args.seed_sites
            )
            conf_matrices.append(conf_matrix)
        return conf_matrices

    def _train_predict(self):
        # Write the header
        self._ofp.write("Accuracy,Recall,TPR,FPR\n")

        pos_train_data, neg_train_data = self._read_subsampled_data(
            self._args.pos_file, self._args.neg_file)
        pos_test_data, neg_test_data = self._read_subsampled_data(
            self._args.pos_test_file, self._args.neg_test_file)

        conf_matrix = train_predict(
            self._sc,
            pos_train_data, neg_train_data,
            pos_test_data, neg_test_data,
            model_params=self._model_params, dataset=self._args.dataset,
            cluster_assignment=self._cluster_assignment,
            pca_cache_path=self._args.pca_cache_path,
            save_train_data=self._args.save_train_data,
            curve_means=self._curve_means,
            infer_test_mean=self._args.infer_test_mean,
            seed_sites=self._args.seed_sites
        )
        return [conf_matrix]

    def _tune(self):
        results = OrderedDict()
        for p in itertools.product(*self._tune_params.values()):
            self._model_params = get_model_params(
                'FN', [p[0], p[1], p[2], self._args.kernel, p[3]]
            )
            # We don't want to compute probabilities during CV.
            self._model_params['prob'] = False
            conf_matrices = self._crossvalidate()
            results[p] = conf_matrices
        return results

    def run(self):
        if self._args.subparser_name == 'predict':
            action = self._train_predict
            process_results = print_results
        elif self._args.subparser_name == 'crossvalidate':
            action = self._crossvalidate
            process_results = print_results
        elif self._args.subparser_name == 'tune':
            action = self._tune
            process_results = partial(print_tune_results,
                                      header=self._tune_params.keys())

        t1 = time.time()
        for i in range(self._args.subsample_runs):
            self._generate_new_random_seed()
            results = action()
            process_results(self._ofp, results)
        t2 = time.time()
        print "Total time: %.3f minutes" % ((t2 - t1)/60.0)


def main():
    gc.set_debug(gc.DEBUG_STATS)
    args = parse_arguments()
    sc = SparkContext(appName="Avishkar")
    clip_pipeline = ClipPipeline(sc, args)
    clip_pipeline.run()


if __name__ == '__main__':
    sys.exit(main())
