import numpy as np
import matplotlib.pyplot as plt
import os.path

COLORS=['#67412C', '#C056C5', '#9AC4B9', '#4F405E', '#CC5835',
        '#82D35B', '#547645', '#C75F7E', '#8385CB', '#CEB452']
MARKERS=['d', '>', '*', '<', 'D',
         '^', 'o', 's', 'v', 'p']
FONTSIZE = 18
MARKERSIZE = 8
WIDTH = 0.35       # the width of the bars
ROOT_PATH = '/Users/asish/Workspace/Academic/Research/Projects/MicroRnaTargetPrediction/data/experiments/'

CLUSTER_NAMES = ['miR-7', 'miR-25', 'miR-103', 'miR-15',
                 'miR-10', 'miR-106', 'miR-19', 'miR-320',
                 'miR-30', 'let-7']

def plot_linear(ax, *files):
    all_data = []
    for f in files:
        data = np.loadtxt(f, delimiter=',', skiprows=1)
        all_data.append(data)

    all_data = np.vstack(all_data)
    all_data = all_data.reshape((len(files), 5, -1))
    ind = np.arange(len(files))
    m, s = np.mean(all_data[:, :, -1], axis=1), np.std(all_data[:, :, -1], axis=1)
    ax.bar(ind, m, WIDTH, color=COLORS[3], yerr=s)


def plot_non_linear(ax, *files):
    all_data = []
    for f in files:
        data = np.loadtxt(f, delimiter=',', skiprows=1)
        all_data.append(data)

    all_data = np.vstack(all_data)
    all_data = all_data.reshape((len(files), 5, -1))
    ind = np.arange(len(files))
    m, s = np.mean(all_data[:, :, -1], axis=1), np.std(all_data[:, :, -1], axis=1)
    ax.bar(ind + WIDTH, m, WIDTH, color=COLORS[4], yerr=s)


def plot_all():
    fig, ax = plt.subplots()
    plot_linear(
        ax, os.path.join(ROOT_PATH, 'linear_smv_results/results_linear_cluster_1.txt'),
        os.path.join(ROOT_PATH, 'linear_smv_results/results_linear_cluster_2.txt'),
        os.path.join(ROOT_PATH, 'linear_smv_results/results_linear_cluster_3.txt'),
        os.path.join(ROOT_PATH, 'linear_smv_results/results_linear_cluster_4.txt'),
        os.path.join(ROOT_PATH, 'linear_smv_results/results_linear_cluster_5.txt'),
        os.path.join(ROOT_PATH, 'linear_smv_results/results_linear_cluster_6.txt'),
        os.path.join(ROOT_PATH, 'linear_smv_results/results_linear_cluster_7.txt'),
        os.path.join(ROOT_PATH, 'linear_smv_results/results_linear_cluster_8.txt'),
        os.path.join(ROOT_PATH, 'linear_smv_results/results_linear_cluster_9.txt'),
        os.path.join(ROOT_PATH, 'linear_smv_results/results_linear_cluster_10.txt'),
    )
    plot_non_linear(
        ax, os.path.join(ROOT_PATH, 'kernel_svm_exps/best_results_cluster_1.txt'),
        os.path.join(ROOT_PATH, 'kernel_svm_exps/best_results_cluster_2.txt'),
        os.path.join(ROOT_PATH, 'kernel_svm_exps/best_results_cluster_3.txt'),
        os.path.join(ROOT_PATH, 'kernel_svm_exps/best_results_cluster_4.txt'),
        os.path.join(ROOT_PATH, 'kernel_svm_exps/best_results_cluster_5.txt'),
        os.path.join(ROOT_PATH, 'kernel_svm_exps/best_results_cluster_6.txt'),
        os.path.join(ROOT_PATH, 'kernel_svm_exps/best_results_cluster_7.txt'),
        os.path.join(ROOT_PATH, 'kernel_svm_exps/best_results_cluster_8.txt'),
        os.path.join(ROOT_PATH, 'kernel_svm_exps/best_results_cluster_9.txt'),
        os.path.join(ROOT_PATH, 'kernel_svm_exps/best_results_cluster_10.txt'),
    )

    ax.set_ylabel('Mean test misclassification rate', fontsize=FONTSIZE)
    ax.set_xlabel('Cluster', fontsize=FONTSIZE)
    ax.set_xticks(np.arange(10) + WIDTH)
    ax.set_xticklabels(CLUSTER_NAMES, fontsize=FONTSIZE)
    ax.set_yticks(np.linspace(0, 0.4, 9))
    ax.set_yticklabels(np.linspace(0, 0.4, 9), fontsize=FONTSIZE)
    ax.legend(['Linear Avishkar ', 'Non-linear Avishkar '], loc='lower left', fontsize=FONTSIZE)
