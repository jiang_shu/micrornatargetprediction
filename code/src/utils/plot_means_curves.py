import matplotlib.pyplot as plt
import numpy as np
from batch_file_reader import BatchFileReader
import sys


POS_COL = 'red'
NEG_COL = 'black'
FONTSIZE = 12


def parse_str_array(sarr, delimiter=" "):
    return np.array([float(x) for x in sarr.strip().split(delimiter)])


def plot(ax, pos_mean, neg_mean, pos_std, neg_std, title='', sense_marker=False):
    t = np.arange(-13, 14)

    ax.plot(t, pos_mean, label='$\mu_{+}(t)$ (positive examples)', color=POS_COL, linewidth=1)
    ax.plot(t, neg_mean, label='$\mu_{-}(t)$ (negative examples)', color=NEG_COL, linewidth=1)
    ax.plot(t, pos_mean + pos_std, label='$\mu_{+}(t) + \sigma_{+}(t)$',
            color=POS_COL, linestyle=':', linewidth=1)
    ax.plot(t, neg_mean + neg_std, label='$\mu_{-}(t)+ \sigma_{-}(t)$',
            color=NEG_COL, linestyle=':', linewidth=1)

    if sense_marker is True:
        ax2 = ax.twiny()
        ax2.set_xticks([-13, 13])
        ax2.set_xticklabels(['5\'', '3\''])
    '''
    ax.fill_between(t, pos_mean + pos_std, pos_mean - pos_std,
                    facecolor=POS_COL, alpha=0.5, linewidth=0.1)
    ax.fill_between(t, neg_mean + neg_std, neg_mean - neg_std,
                    facecolor=NEG_COL, alpha=0.5, linewidth=0.1)
    '''
    ax.set_xticks(t[::2])
    ax.set_title(title, fontsize=FONTSIZE)
    plt.setp(ax.get_yticklabels(), fontsize=FONTSIZE)


def main():
    mean_file = sys.argv[1]
    var_file = sys.argv[2]

    means = []
    reader = BatchFileReader(mean_file, process_row=parse_str_array)
    reader.read(means)

    variances = []
    reader = BatchFileReader(var_file, process_row=parse_str_array)
    reader.read(variances)
    stds = map(lambda x: np.sqrt(x), variances)

    _, axs = plt.subplots(3, 2, sharex='col')
    plot(axs[0][0], means[0], means[1], stds[0], stds[1],
         title='$\Delta G (site)$', sense_marker=True)
    plot(axs[0][1], means[2], means[3], stds[2], stds[3],
         title='$\Delta G (seed)$', sense_marker=True)
    plot(axs[1][0], means[4], means[5], stds[4], stds[5],
         title='$\Delta \Delta G (site)$')
    plot(axs[1][1], means[6], means[7], stds[6], stds[7],
         title='$\Delta \Delta G (seed)$')
    plot(axs[2][0], means[8], means[9], stds[8], stds[9],
         title='$AU (site)$')
    plot(axs[2][1], means[10], means[11], stds[10], stds[11],
         title='$AU (seed)$')
    axs[2][0].set_xlabel('t (window offset)', fontsize=FONTSIZE)
    axs[2][1].set_xlabel('t (window offset)', fontsize=FONTSIZE)

    plt.setp(axs[2][0].get_xticklabels(), fontsize=FONTSIZE - 2)
    plt.setp(axs[2][1].get_xticklabels(), fontsize=FONTSIZE - 2)

    # plt.legend()
    axs[0][0].legend(bbox_to_anchor=(0, 1.6, 2.2, 0.102),
               fontsize=FONTSIZE, mode="expand",
               borderaxespad=0., ncol=2)
    plt.savefig('test.pdf', bbox_inches='tight')


if __name__ == '__main__':
    sys.exit(main())
