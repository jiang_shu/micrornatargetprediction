'''
    Plots average ROC curve by reading multiple tpr and fpr values from files.
'''
from scipy.interpolate import interp1d
import numpy as np
import sys
from batch_file_reader import BatchFileReader
import argparse
import os
import os.path
from sklearn.metrics import roc_curve
from cStringIO import StringIO
import re


def process_row(line):
    line = line.strip()
    if not line:
        return None
    else:
        items = line.split(",")
        # The last two items correspond to label and scores.
        return tuple(items[-2:])


def get_mean_roc(fpr_list, tpr_list):
    fpr_ind = np.linspace(0, 1, 200)
    tpr_interpd = []  # Interploated tpr values
    for fpr_i, tpr_i in zip(fpr_list, tpr_list):
        f = interp1d(fpr_i, tpr_i)
        tpr_interpd.append(f(fpr_ind))

    return (fpr_ind, np.mean(tpr_interpd, 0))


def get_fpr_tpr_list(input_file):
    reader = BatchFileReader(input_file, process_row=process_row)
    data = []
    reader.read(data)
    fpr_list, tpr_list = zip(*data)
    fpr_list = [np.array(x) for x in fpr_list]
    tpr_list = [np.array(x) for x in tpr_list]
    return fpr_list, tpr_list


def compute_fpr_tpr(input_file, top_n=-1):
    data = []
    if type(input_file) == str:
        print "Computing fpr/tpr for file:", input_file
        reader = BatchFileReader(input_file, process_row=process_row)
        reader.read(data)
    else:
        # Treate input_file as a file pointer.
        print "Computing fpr/tpr for combined file:", input_file[0]
        data = map(process_row, input_file[1])

    data = filter(lambda x: x is not None, data)
    labels, probs = zip(*data)
    labels = np.array([int(x) for x in labels])
    probs = np.array([float(x) for x in probs])
    probs = np.nan_to_num(probs)

    if top_n > 0:
        top_n_val = np.percentile(probs, 100 - top_n)
        bottom_n_val = np.percentile(probs, top_n)
        top_n_indices = probs >= top_n_val
        bottom_n_indices = probs <= bottom_n_val
        probs = np.hstack((probs[top_n_indices], probs[bottom_n_indices]))
        labels = np.hstack((labels[top_n_indices], labels[bottom_n_indices]))

    fpr, tpr, _ = roc_curve(labels, probs, pos_label=1)
    return fpr, tpr


def array_to_str(arr, delimiter=" "):
    return delimiter.join([str(x) for x in arr])


def get_input_files(input_path, combine_parts=False):
    if combine_parts is False:
        for f in os.listdir(input_path):
            full_path = os.path.join(input_path, f)
            if os.path.isfile(full_path):
                yield full_path
    else:
        print "Combining parts."
        file_maps = {}
        for f in os.listdir(input_path):
            full_path = os.path.join(input_path, f)
            if not os.path.isfile(full_path):
                continue
            name = re.search(r'^(.*)part.*', f).group(1)
            if name not in file_maps:
                print "Processing name:", name
                file_maps[name] = StringIO()
            with open(full_path, 'rU') as fp:
                for line in fp:
                    file_maps[name].write("%s" % line)

        for k, v in file_maps.iteritems():
            v.reset()
            yield (k, v)


def main():
    argparser = argparse.ArgumentParser(
        description='Given a path containg probability files where each '
        'file contains labels and probability scores, returns the average '
        'fpr and tpr across all files. If a single file is specified as '
        'input then assumes that the file contains list fpr and tpr values '
        'in two columns and computes the average fpr and tpr values.'
    )
    argparser.add_argument('--flag', default='path', choices=['file', 'path'],
                           help='Specifies if the argument is to be treated '
                           'as a file or a path (default)')
    argparser.add_argument('arg', help='A file containing list of fpr and '
                           'tpr vaulues or a path containing files of '
                           '(label, probability) values.')
    argparser.add_argument('output_file', metavar='output_file',
                           help='file where output is written.')
    argparser.add_argument('--combine-parts', action='store_true',
                           dest='combine_parts',
                           help='Combines different parts of the file into'
                           ' one before computing fpr, tpr.')
    argparser.add_argument('--top-n', dest='top_n', type=int, default=-1,
                           metavar='N',
                           help='Compute TPR/FPR for top N (in percentage) '
                           'of scores.')

    args = argparser.parse_args()

    if args.flag == 'file':
        input_file = args.arg
        fpr_list, tpr_list = get_fpr_tpr_list(input_file)
        mean_fpr, mean_tpr = get_mean_roc(fpr_list, tpr_list)
    else:
        input_path = args.arg
        fpr_list, tpr_list = [], []
        for input_file in get_input_files(input_path,
                                          combine_parts=args.combine_parts):
            fpr_i, tpr_i = compute_fpr_tpr(input_file,
                                           top_n=args.top_n)
            fpr_list.append(fpr_i)
            tpr_list.append(tpr_i)

        mean_fpr, mean_tpr = get_mean_roc(fpr_list, tpr_list)

    with open(args.output_file, 'w') as ofp:
        ofp.write("%s,%s\n" % (array_to_str(mean_fpr),
                               array_to_str(mean_tpr)))


if __name__ == '__main__':
    sys.exit(main())
