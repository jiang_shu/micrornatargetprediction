from batch_file_reader import BatchFileReader
import argparse
import random
import os.path
import sys


def parse_arguments():
    argparser = argparse.ArgumentParser(
        description='Generate 10 probability files (label, score) '
        'that have equal number of positive and negative examples '
        'from a file containing mRNA,miRNA,loc,score,CLIP data'
    )

    argparser.add_argument("input_file", help='Input data file')
    argparser.add_argument("output_path",
                           help='Path where output files are to be written')
    argparser.add_argument("--prefix", help='File prefix',
                           default='prob', dest='prefix')
    args = argparser.parse_args()
    return args


def read_label_score(fname):
    reader = BatchFileReader(fname)
    data = []
    reader.read(data)
    label_score = []
    for line in data:
        line = line.strip()
        if not line:
            continue
        items = line.split(",")
        score, label = float(items[-2]), int(items[-1])
        label_score.append((label, score))

    return label_score


def write_csv(data, ofp):
    for label, score in data:
        ofp.write("%d,%f\n" % (label, score))


def generate_prob_files(fname, output_path, output_prefix):
    label_score = read_label_score(fname)
    pos_data = filter(lambda x: x[0] == 1, label_score)
    neg_data = filter(lambda x: x[0] == 0, label_score)

    print "# Positive examples:", len(pos_data)
    print "# Negative examples:", len(neg_data)

    # Generate 10 data files with equal number of positive and negative data
    pos_data_len = len(pos_data)
    neg_data_len = len(neg_data)

    if pos_data_len <= neg_data_len:
        large_data = neg_data
        small_data = pos_data
    else:
        large_data = pos_data
        small_data = neg_data
    small_data_len = len(small_data)

    for i in range(10):
        large_data_sample = random.sample(large_data, small_data_len)
        dest = os.path.join(output_path, output_prefix + '_%d' % i)
        print "Writing labels and scores to:", dest
        with open(dest, 'w') as ofp:
            write_csv(large_data_sample, ofp)
            write_csv(small_data, ofp)


def main():
    args = parse_arguments()
    generate_prob_files(args.input_file, args.output_path, args.prefix)


if __name__ == '__main__':
    sys.exit(main())
