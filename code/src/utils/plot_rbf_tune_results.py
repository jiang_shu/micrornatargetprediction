import numpy as np
import matplotlib.pyplot as plt
import os.path

COLORS=['#67412C', '#C056C5', '#9AC4B9', '#4F405E', '#CC5835',
        '#82D35B', '#547645', '#C75F7E', '#8385CB', '#CEB452']
MARKERS=['d', '>', '*', '<', 'D',
         '^', 'o', 's', 'v', 'p']
FONTSIZE = 18
MARKERSIZE = 8

ROOT_PATH = '/Users/asish/Workspace/Academic/Research/Projects/MicroRnaTargetPrediction/data/experiments/'

CLUSTER_NAMES = ['miR-7', 'miR-25', 'miR-103', 'miR-15',
                 'miR-10', 'miR-106', 'miR-19', 'miR-320',
                 'miR-30', 'let-7']

def plot_files(ax, *files):
    count = 0

    for f in files:
        readcols = (3, 8)
        if count < 5:
            readcols = (0, 6) # Corresponds to gamma and error
        
        data = np.loadtxt(f, delimiter=',', skiprows=1, usecols=readcols)
        rows = data.shape[0]
        data = data.reshape((rows/5, 5, -1))
        mean_error = np.mean(data[:, :, 1], axis=1)
        std_error = np.std(data[:, :, 1], axis=1)
        ax.errorbar(np.arange(5), mean_error[:5], color=COLORS[count], yerr=std_error[:5],
                    label=CLUSTER_NAMES[count], linewidth=2.0)
        count += 1


def plot_tune_results():
    fig, ax = plt.subplots()
    ax.margins(0.08)
    plot_files(ax, os.path.join(ROOT_PATH, 'kernel_svm_exps/results_cluster_1.txt'),
         os.path.join(ROOT_PATH, 'kernel_svm_exps/results_cluster_2.txt'),
         os.path.join(ROOT_PATH, 'kernel_svm_exps/results_cluster_3.txt'),
         os.path.join(ROOT_PATH, 'kernel_svm_exps/results_cluster_4.txt'),
         os.path.join(ROOT_PATH, 'kernel_svm_exps/results_cluster_5.txt'),
         os.path.join(ROOT_PATH, 'kernel_svm_exps/results_cluster_6.txt'),
         os.path.join(ROOT_PATH, 'kernel_svm_exps/results_cluster_7.txt'),
         os.path.join(ROOT_PATH, 'kernel_svm_exps/results_cluster_8.txt'),
         os.path.join(ROOT_PATH, 'kernel_svm_exps/results_cluster_9.txt'),
         os.path.join(ROOT_PATH, 'kernel_svm_exps/results_cluster_10.txt'))

    ax.axhline(0.34315803548700002, 0, 4, color='black', linestyle='--', linewidth=2.0, label='Linear')
    ax.set_ylabel('Mean test misclassification rate', fontsize=FONTSIZE)
    ax.set_xlabel(r'$\gamma$', fontsize=FONTSIZE)
    ax.set_xticks(np.arange(5))
    ax.set_xticklabels([0.0001, 0.001, 0.01, 0.1, 1.0], fontsize=FONTSIZE)
    ax.set_yticks(np.linspace(0.10, 0.50, 9))
    ax.set_yticklabels(np.linspace(0.10, 0.50, 9), fontsize=FONTSIZE)
    ax.legend(loc='best', fontsize=FONTSIZE)
    
