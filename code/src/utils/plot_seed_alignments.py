import os.path
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats


FONTSIZE = 22
REPO = '/Users/asish/Workspace/Academic/Research/'\
    'Projects/MicroRnaTargetPrediction/data/'


def get_count(fname):
    alignment_counts = {}
    with open(fname, 'rU') as fp:
        for line in fp:
            pattern, count = line.strip().split(",")
            count = int(count)
            alignment_counts[pattern] = count
    return alignment_counts


def normalize_counts(alignment_counts):
    alignment_sum = sum(alignment_counts.values())
    for k in alignment_counts.keys():
        alignment_counts[k] /= float(alignment_sum)
    return alignment_counts


def get_common_counts(alignment_counts1, alignment_counts2):
    common_patterns = set(alignment_counts1.keys()).intersection(alignment_counts2.keys())
    common_counts = {}
    for a in common_patterns:
            common_counts[a] = (alignment_counts1[a], alignment_counts2[a])
    return common_counts

def plot_alignments():
    hits_clip_alignments = normalize_counts(get_count(os.path.join(REPO, 'HITS-CLIP/hits_clip_all_alignments.csv')))
    vclip_alignments = normalize_counts(get_count(os.path.join(REPO, 'V-CLIP/v_clip_all_alignments.csv')))
    common_counts = get_common_counts(hits_clip_alignments, vclip_alignments)
    common_count_list = [(x[0], x[1][0], x[1][1]) for x in sorted(common_counts.iteritems(), key=lambda x: x[1])]
    x = [hits_clip_alignments[k] for k in common_counts.keys()]
    y = [vclip_alignments[k] for k in common_counts.keys()]
    area = np.pi * 5**2
    plt.scatter(x, y, s=area, c='red', alpha=0.5)
    for i in range(1, 11):
        plt.annotate(common_count_list[-i][0], xy=common_count_list[-i][1:],
                xytext=(common_count_list[-i][1]+0.0001, common_count_list[-i][2]), fontsize=FONTSIZE)
    plt.axis([0, 0.025, 0, 0.040])
    plt.xticks(np.arange(0, 0.0275, 0.0025), fontsize=FONTSIZE)
    plt.yticks(np.arange(0, 0.045, 0.005), fontsize=FONTSIZE)
    plt.xlabel('Mouse', fontsize=FONTSIZE)
    plt.ylabel('Human', fontsize=FONTSIZE)
    corr = stats.pearsonr(x, y)
    plt.title(r'Correlation Coefficient $(\rho)$ = %.3f' % corr[0], fontsize=FONTSIZE)

