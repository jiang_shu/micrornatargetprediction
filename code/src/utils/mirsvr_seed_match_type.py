"""
    Given mirsvr data file having the following format:
        NM_016917,mmu-let-7g-3p,2720-2741,-0.0528,0
        NM_029402,mmu-let-7g-3p,3260-3281,-0.0089,0
        NM_025341,mmu-let-7g-3p,1726-1747,-0.0054,0
        NM_181405,mmu-let-7g-3p,2864-2885,-0.0039,1
        NM_019998,mmu-let-7g-3p,1330-1344,-0.0011,0
    outputs whether the seed match is canonical (C) or non-canonical (N)
    i.e:
        NM_016917,mmu-let-7g-3p,2720-2741,C,-0.0528,0
        NM_029402,mmu-let-7g-3p,3260-3281,N,-0.0089,0
"""

# RNAhybrid executabe
RNAHYBRID = "../../third-party/bin/RNAhybrid"

import sys
from pipeline import Pipeline
from parallelize import run_func_parallely
from batch_file_reader import BatchFileReader
from redisdao import RedisDao
from functools import partial
import os
from tempfile import NamedTemporaryFile
import subprocess


def invoke_rna_hybrid(mrna_seq, mirna_seq,
                      energy_threshold=None,
                      helix_constraint=None):
    executable = RNAHYBRID

    # TODO: Change the dataset type (3utr_human). But do we need to change
    # if we are not interested in the p-value?
    args = "-c -s 3utr_human -m %d" % len(mrna_seq)
    if helix_constraint is not None:
        args += " -f %d,%d" % (helix_constraint[0], helix_constraint[1])
    if energy_threshold is not None:
        args += " -e %0.3f" % energy_threshold

    if os.name == 'nt':
        # We are running on windows, so use files.
        fp = NamedTemporaryFile(mode='w', delete=True)
        fp.write(">mrna\n")
        fp.write("%s\n" % mrna_seq)
        fp.flush()
        cmd = "%s %s -t %s %s" % (executable, args, fp.name, mirna_seq)
        output = subprocess.check_output(cmd, shell=True)
        fp.close()
    else:
        cmd = "%s %s %s %s" % (executable, args, mrna_seq, mirna_seq)
        output = subprocess.check_output(cmd, shell=True)

    return output


def parse_output(output):
    output = output.strip()
    output = output.split(":")
    if not output or not output[0]:
        return None

    mrna_mismatch, mrna_match, mirna_match, mirna_mismatch =\
        output[7], output[8], output[9], output[10]

    max_align_len = [mrna_mismatch, mrna_match, mirna_mismatch, mirna_match]
    max_align_len = max(map(lambda x: len(x), max_align_len))
    mrna_mismatch += " " * (max_align_len - len(mrna_mismatch))
    mrna_match += " " * (max_align_len - len(mrna_match))
    mirna_mismatch += " " * (max_align_len - len(mirna_mismatch))
    mirna_match += " " * (max_align_len - len(mirna_match))

    seed_match = zip(mrna_match, mirna_match,
                     mrna_mismatch, mirna_mismatch)[::-1]
    start_ind = 0
    non_canonical = False

    while(seed_match[start_ind][0] == " " or
          seed_match[start_ind][1] == " "):
        start_ind += 1

    end_ind = start_ind
    while((seed_match[end_ind][0] != " " and
           seed_match[end_ind][1] != " ") or
          (seed_match[end_ind][2] != " " and
           seed_match[end_ind][3] != " ")):

        if (seed_match[end_ind][0] == 'G' and
            seed_match[end_ind][1] == 'U') or\
            (seed_match[end_ind][0] == 'U' and
             seed_match[end_ind][1] == 'G'):
            if end_ind < 7:
                non_canonical = True
            else:
                break
        elif (seed_match[end_ind][2] != ' ' and
              seed_match[end_ind][3] != ' '):
            if end_ind < 7:
                non_canonical = True
            else:
                break

        if end_ind >= 7:
            break
        end_ind += 1

    start_ind += 1
    seed_type_str = "%d-%d" % (start_ind, end_ind)
    if non_canonical or (end_ind - start_ind + 1) < 6:
        seed_type_str += "*"
    return seed_type_str


def get_seed_match_type(record, dao=None, assembly='mm9'):
    record = record.strip()
    try:
        mrna_name, mirna_name, loc, score, label = record.split(",")
    except:
        print "Error in line:"
        print record
        return None

    mrna = dao.get_messenger_rna_from_name(mrna_name, assembly=assembly)
    mirna = dao.get_micro_rna_from_name(mirna_name)
    if mrna is None or mirna is None:
        print "Couldn't get sequence for either:", mrna, " or:", mirna
        return None

    loc = map(int, loc.split("-"))
    loc[0] -= 1
    loc[1] += 1
    output = invoke_rna_hybrid(str(mrna._seq[loc[0]:loc[1]]),
                               str(mirna._seq))
    seed_type = parse_output(output)
    loc = "%d-%d" % (loc[0], loc[1])
    return (mrna_name, mirna_name, loc, seed_type, score, label)


def main():
    if len(sys.argv) != 4:
        print "Usage: %s <input file> <assembly> <output file>" % (sys.argv[0])
        return 1

    input_file = sys.argv[1]
    assembly = sys.argv[2]
    output_file = sys.argv[3]

    data = []
    with BatchFileReader(input_file, batch_size=1000, verbose=True) as reader,\
            open(output_file, 'w') as ofp,\
            RedisDao() as dao:

        func = partial(get_seed_match_type, dao=dao, assembly=assembly)
        func.__name__ = 'get_seed_match_type'
        pipeline = (Pipeline())\
            .add_step(func)

        while True:
            lines_remaining = reader.read(data)
            output_records =\
                run_func_parallely(
                    pipeline.execute,
                    data,
                    kwparams={'for_each': True},
                    combine_func=lambda l: [x for sublist in l
                                            for x in sublist]
                )
            for rec in output_records:
                ofp.write("%s\n" % ",".join(rec))

            del data[:]
            if not lines_remaining:
                break

if __name__ == '__main__':
    sys.exit(main())
