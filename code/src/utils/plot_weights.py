import os
import os.path
import numpy as np
import re
import matplotlib.pyplot as plt
from fda import smooth_spline

REPO = "/Users/asish/Workspace/Academic/Research/Projects/"\
    "MicroRnaTargetPrediction"
WIDTH = 0.5


def regex_match(fname, regex):
    if not regex:
        return True

    m = re.search(regex, fname)
    if m:
        return True
    return False


def increment_along(value, incr):
    return value + np.sign(value) * incr


def annotate_range(start, end, height, label, incr=0.01, label_incr=0.03):
    plt.annotate('', xy=(start + WIDTH/2.0, increment_along(height, incr)),
                 xytext=(end + WIDTH/2.0, increment_along(height, incr)),
                 arrowprops=dict(arrowstyle="<->"))

    plt.annotate(label, xy=(start + 2, increment_along(height, label_incr)),
                 xytext=(start + 2, increment_along(height, label_incr)),
                 fontsize=14)


def annotate_point(x, y, tx, ty, label):
    plt.annotate(label, xy=(x + WIDTH/2.0, increment_along(y, 0.01)),
                 xytext=(tx, ty),
                 arrowprops=dict(arrowstyle="->"),
                 fontsize=14)


def get_max(arr, range):
    max_val = np.max(arr[range[0]: range[1]])
    min_val = np.min(arr[range[0]: range[1]])
    if abs(max_val) > abs(min_val):
        return max_val
    else:
        return min_val


def get_weights(path, reg_expr=''):
    '''
        Given a path containing weight files. Read the weights and return
        a matrix of weights where rows are weights from different runs
        and columns are weights of different features.
    '''
    weights = []
    for fname in os.listdir(path):
        full_path = os.path.join(path, fname)
        if os.path.isfile(full_path) and regex_match(full_path, reg_expr):
            print "Reading from :", full_path
            weight = np.genfromtxt(full_path)
            weights.append(weight)
    weights = np.vstack(weights)

    return weights


def parse_str_array(sarr, delim=" "):
    return np.array([float(x) for x in sarr.strip().split(delim)])


def plot_weights(weights,
                 delg_site_coeff_range, delg_seed_coeff_range,
                 deldelg_site_coeff_range, deldelg_seed_coeff_range,
                 au_site_coeff_range, au_seed_coeff_range,
                 seed_site=False):

    weight_mean = np.mean(weights, axis=0)
    weight_std = np.std(weights, axis=0)

    ind = np.arange(0, weight_mean.size)
    plt.bar(ind, weight_mean, WIDTH, color='r', yerr=weight_std)

    # Now annotate plot
    max_delg_site = get_max(weight_mean, delg_site_coeff_range)
    max_delg_seed = get_max(weight_mean, delg_seed_coeff_range)
    max_deldelg_site = get_max(weight_mean, deldelg_site_coeff_range)
    max_deldelg_seed = get_max(weight_mean, deldelg_seed_coeff_range)
    max_au_site = get_max(weight_mean, au_site_coeff_range)
    max_au_seed = get_max(weight_mean, au_seed_coeff_range)

    annotate_range(delg_site_coeff_range[0], delg_site_coeff_range[1],
                   max_delg_site, r'$\Delta G$ site coeffs')
    annotate_range(delg_seed_coeff_range[0], delg_seed_coeff_range[1],
                   max_delg_seed, r'$\Delta G$ seed coeffs', label_incr=0.09)
    annotate_range(deldelg_site_coeff_range[0], deldelg_site_coeff_range[1],
                   max_deldelg_site, r'$\Delta\Delta G$ site coeffs',
                   label_incr=0.09)
    annotate_range(deldelg_seed_coeff_range[0], deldelg_seed_coeff_range[1],
                   max_deldelg_seed, r'$\Delta\Delta G$ seed coeffs',
                   label_incr=0.09)
    annotate_range(au_site_coeff_range[0], au_site_coeff_range[1],
                   max_au_site, r'AU site coeffs')
    annotate_range(au_seed_coeff_range[0], au_seed_coeff_range[1],
                   max_au_seed, r'AU seed coeffs')

    point_ind = au_seed_coeff_range[1]
    annotate_point(point_ind, weight_mean[point_ind], 126, 0.17,
                   'conservation')
    point_ind += 1
    if seed_site is True:
        annotate_point(point_ind, weight_mean[point_ind], 126, 0.24,
                       'seed consv')
        point_ind += 1
        annotate_point(point_ind, weight_mean[point_ind], 126, 0.1,
                       'out-seed consv')
        point_ind += 1

    annotate_point(point_ind, weight_mean[point_ind], 101, -0.22,
                   'rel site location')
    point_ind += 1
    annotate_point(point_ind, weight_mean[point_ind], 127, 0.35,
                   'seed enrichment')

    point_ind += 1
    annotate_point(point_ind, weight_mean[point_ind], 109, -0.54,
                   'site length')

    point_ind += 1
    annotate_point(point_ind, weight_mean[point_ind], 130, -0.07,
                   '3\'UTR')
    point_ind += 1
    annotate_point(point_ind, weight_mean[point_ind], 130, -0.14,
                   '5\'UTR')
    point_ind += 1
    annotate_point(point_ind, weight_mean[point_ind], 130, -0.21,
                   'CDS')
    plt.yticks(np.arange(-0.8, 0.4, 0.2), fontsize=14)
    plt.xlim([-0.5, 145])
    plt.ylim([-0.7, 0.4])
    plt.xticks([])

    ret = dict(
        cds=weight_mean[-1],
        utr5=weight_mean[-2],
        utr3=weight_mean[-3],
        site_length=weight_mean[-4],
        seed_enrichment=weight_mean[-5],
        rel_site_loc=weight_mean[-6]
    )
    for i in range(delg_site_coeff_range[1] - delg_site_coeff_range[0]):
        ret['delg_site_coeff_%d' % i] = weight_mean[delg_site_coeff_range[0] + i]
        ret['delg_seed_coeff_%d' % i] = weight_mean[delg_seed_coeff_range[0] + i]
        ret['deldelg_site_coeff_%d' % i] = weight_mean[deldelg_site_coeff_range[0] + i]
        ret['deldelg_seed_coeff_%d' % i] = weight_mean[deldelg_seed_coeff_range[0] + i]
        ret['au_site_coeff_%d' % i] = weight_mean[au_site_coeff_range[0] + i]
        ret['au_seed_coeff_%d' % i] = weight_mean[au_seed_coeff_range[0] + i]

    if seed_site is True:
        ret['out_seed_consv'] = weight_mean[-7]
        ret['seed_consv'] = weight_mean[-8]
        ret['consv'] = weight_mean[-9]
    else:
        ret['consv'] = weight_mean[-7]

    return ret


def plot_all_weights():
    seed_weights = get_weights(
        os.path.join(REPO,
                     'data/experiments/output/V-CLIP-SEED-CV'),
        reg_expr=r'.*[.]weights$'
    )
    seedless_weights = get_weights(
        os.path.join(REPO,
                     'data/experiments/output/V-CLIP-SEEDLESS-CV'),
        reg_expr=r'.*[.]weights$'
    )

    plt.subplot(2, 1, 1)
    plt.title('Seed sites')
    seed_w = plot_weights(seed_weights, (0, 20), (20, 40),
                          (40, 60), (60, 80), (80, 100), (100, 120),
                          seed_site=True)
    plt.subplot(2, 1, 2)
    plt.title('Seedless sites')
    seedless_w = plot_weights(seedless_weights, (0, 20), (20, 40),
                              (40, 60), (60, 80), (80, 100), (100, 120),
                              seed_site=False)
    return seed_w, seedless_w
