import numpy as np
import matplotlib.pyplot as plt
import os.path
from sklearn.metrics import auc
from matplotlib import rc

rc('text', usetex=True)

REPO = '/Users/asish/Workspace/Academic/Research/'\
    'Projects/MicroRnaTargetPrediction/data/experiments/output'
FONTSIZE = 18
MARKERSIZE = 8

COLORS=['#67412C', '#C056C5', '#9AC4B9', '#4F405E', '#CC5835',
        '#82D35B', '#547645', '#C75F7E', '#8385CB', '#CEB452']
MARKERS=['d', '>', '*', '<', 'D',
         '^', 'o', 's', 'v', 'p']

METHOD_IND = {
        '3UTR': 0,
        'CDS': 1,
        '5UTR': 2,
        'AV4': 3,
        'AV5': 4,
        'MIRZA': 5,
        'MIRSVR': 6,
        'PITA': 7,
        'STARMIR': 8,
        'TARGETSCAN': 9
        }


def parse_arr(sarr, delimiter=" "):
    return np.array([float(x) for x in sarr.split(delimiter)])


def read_fpr_tpr(fname):
    with open(fname) as fp:
        line = fp.readline()
        line = line.strip()
        fpr, tpr = line.split(",")
        fpr = parse_arr(fpr)
        tpr = parse_arr(tpr)
        tpr[0] = 0.0

    return fpr, tpr


def plot_roc(ax, fpr, tpr, method_name, label):
    ind = METHOD_IND[method_name]
    ax.plot(fpr, tpr, color=COLORS[ind], linewidth=2.0, linestyle='-')
    marker_pos = np.argmax(tpr - fpr)

    label += " [AUC: %.2f] " % auc(fpr, tpr)
    ax.plot(fpr[marker_pos], tpr[marker_pos], color=COLORS[ind], linewidth=2.0, marker=MARKERS[ind],
            markersize=MARKERSIZE, label=label)


def plot_human_seedless_roc(ax):
    file_name = os.path.join(
        REPO, 'V-CLIP/V-CLIP-3UTR-CV/v_clip_3utr_fpr_tpr.csv'
    )
    fpr, tpr = read_fpr_tpr(file_name)
    plot_roc(ax, fpr, tpr, '3UTR', r'3\' UTR 10-fold CV')


    file_name = os.path.join(
        REPO, 'V-CLIP/V-CLIP-CDS-CV/v_clip_cds_fpr_tpr.csv'
    )
    fpr, tpr = read_fpr_tpr(file_name)
    plot_roc(ax, fpr, tpr, 'CDS', r'CDS 10-fold CV')

    file_name = os.path.join(
        REPO, 'V-CLIP/V-CLIP-5UTR-CV/v_clip_5utr_fpr_tpr.csv'
    )
    fpr, tpr = read_fpr_tpr(file_name)
    plot_roc(ax, fpr, tpr, '5UTR', r'5\' UTR 10-fold CV')

    # Baseline
    ax.plot([0, 1], [0, 1], 'k--',
             label='baseline [AUC: 0.5]')

    ax.legend(loc='lower right', fontsize=FONTSIZE - 8)
    ax.set_title('human, seedless', fontsize=FONTSIZE)


def plot_human_seed_roc(ax):
    file_name = os.path.join(
        REPO, 'V-CLIP/V-CLIP-3UTR-SEED-CV/v_clip_3utr_seed_fpr_tpr.csv'
    )
    fpr, tpr = read_fpr_tpr(file_name)
    plot_roc(ax, fpr, tpr, '3UTR', r'3\' UTR 10-fold CV')


    file_name = os.path.join(
        REPO, 'V-CLIP/V-CLIP-CDS-SEED-CV/v_clip_cds_seed_fpr_tpr.csv'
    )
    fpr, tpr = read_fpr_tpr(file_name)
    plot_roc(ax, fpr, tpr, 'CDS', r'CDS 10-fold CV')

    file_name = os.path.join(
        REPO, 'V-CLIP/V-CLIP-5UTR-SEED-CV/v_clip_5utr_seed_fpr_tpr.csv'
    )
    fpr, tpr = read_fpr_tpr(file_name)
    plot_roc(ax, fpr, tpr, '5UTR', r'5\' UTR 10-fold CV')

    # Baseline
    ax.plot([0, 1], [0, 1], 'k--',
             label='baseline [AUC: 0.5]')

    ax.legend(loc='lower right', fontsize=FONTSIZE - 8)
    ax.set_title('human, seed', fontsize=FONTSIZE)

def plot_all():
    fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
    plot_human_seed_roc(ax1)
    plot_human_seedless_roc(ax2)

    ax2.set_xlabel('FPR', fontsize=FONTSIZE)
    ax1.set_xlabel('FPR', fontsize=FONTSIZE)
    ax1.set_ylabel('TPR', fontsize=FONTSIZE)

    plt.setp(ax1.get_yticklabels(), fontsize=FONTSIZE - 2)
    plt.setp(ax1.get_xticklabels(), fontsize=FONTSIZE - 2)
    plt.setp(ax2.get_xticklabels(), fontsize=FONTSIZE - 2)
    fig.set_tight_layout(dict(pad=0.1, h_pad=0.1, w_pad=0.1))
    return fig

