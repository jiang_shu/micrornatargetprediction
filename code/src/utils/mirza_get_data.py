"""
    Gets mRNA,miRNA,loc,score for MIRZA data
"""

import sys
import argparse
from redisdao import RedisDao


MRNA_SEQ_LENGTH = 50
ASSEMBLY = 'hg18'


def parse_result(input_file, output_file, compute_label=True):
    count = 0
    with open(output_file, 'w') as ofp,\
            open(input_file, 'rU') as fp:
        for line in fp:
            count += 1
            line = line.strip()
            try:
                items = line.split("\t")
                score, rid, mirna = items[1], items[7], items[8]
                score = float(score)
                rid = rid.split("|")
                mrna, loc_start, loc_end = rid[:3]
                if compute_label is True:
                    label = rid[-1]
            except:
                print "Error parsing line:", count
                print line
                continue
            if compute_label is True:
                ofp.write("%s,%s,%s-%s,%f,%s\n" % (mrna, mirna,
                                                   loc_start, loc_end, score,
                                                   label))
            else:
                ofp.write("%s,%s,%s-%s,%f\n" % (mrna, mirna,
                                                   loc_start, loc_end, score))

def adjust_loc(loc, non_negative=True, max_end=None):
    loc_length = loc[1] - loc[0] + 1
    diff = MRNA_SEQ_LENGTH - loc_length
    loc[0] -= diff/2
    loc[1] += diff - diff/2
    if non_negative is True and loc[0] < 0:
        loc = [0, loc[1] - loc[0]]
    if max_end is not None and loc[1] > max_end:
        loc[0] -= loc[1] - max_end
        loc[1] = max_end
    return loc


def generate_mrna_data(input_file, output_file):
    count = 0
    with RedisDao() as dao,\
            open(output_file, 'w') as ofp,\
            open(input_file, 'rU') as fp:
        for line in fp:
            count += 1
            line = line.strip()
            try:
                items = line.split(",")
                mrna, loc = items[0], items[2]
                loc = map(int, loc.split("-"))
                loc[0] -= 1
            except:
                print "Error in line:", count
                print line
                continue
            mrna = dao.get_messenger_rna_from_name(mrna, assembly=ASSEMBLY)
            loc = adjust_loc(loc, non_negative=True, max_end=len(mrna._seq))
            if loc[1] - loc[0] + 1 != MRNA_SEQ_LENGTH:
                print "Couldn't adjust location for:", line
                print "adjusted loc:", loc
                continue
            items.insert(3, "%s-%s" % (loc[0], loc[1]))
            seq = mrna._seq[loc[0]:loc[1]]
            ofp.write(">%s\n" % "|".join(items))
            ofp.write("%s\n" % seq)


def generate_mirna_data(mirna_expression_file, mirna_file, output_file):
    mirna_expression = {}
    with open(mirna_expression_file, 'rU') as fp:
        for line in fp:
            line = line.strip()
            mirna, expression = line.split("\t")
            mirna_expression[mirna] = expression

    with open(mirna_file, 'rU') as fp,\
            open(output_file, 'w') as ofp:
        for line in fp:
            line = line.strip()
            mirna = line
            if mirna.endswith("3p"):
                mirna = mirna.rstrip("-3p")
            elif mirna.endswith("5p"):
                mirna = mirna.rstrip("-5p")
            score = mirna_expression.get(mirna, 'nan')
            ofp.write("%s\t%s\n" % (line, score))
