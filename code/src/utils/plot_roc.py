import numpy as np
import matplotlib.pyplot as plt
import os.path
from sklearn.metrics import auc
from matplotlib import rc

rc('text', usetex=True)

REPO = '/Users/asish/Workspace/Academic/Research/'\
    'Projects/MicroRnaTargetPrediction/data/experiments/output'
FONTSIZE = 18
MARKERSIZE = 8

COLORS=['#67412C', '#C056C5', '#9AC4B9', '#4F405E', '#CC5835',
        '#82D35B', '#547645', '#C75F7E', '#8385CB', '#CEB452']
MARKERS=['d', '>', '*', '<', 'D',
         '^', 'o', 's', 'v', 'p']

METHOD_IND = {
        'AV1': 0,
        'AV2': 1,
        'AV3': 2,
        'AV4': 3,
        'AV5': 4,
        'MIRZA': 5,
        'MIRSVR': 6,
        'PITA': 7,
        'STARMIR': 8,
        'TARGETSCAN': 9
        }


def parse_arr(sarr, delimiter=" "):
    return np.array([float(x) for x in sarr.split(delimiter)])


def read_fpr_tpr(fname):
    with open(fname) as fp:
        line = fp.readline()
        line = line.strip()
        fpr, tpr = line.split(",")
        fpr = parse_arr(fpr)
        tpr = parse_arr(tpr)
        tpr[0] = 0.0

    return fpr, tpr


def plot_roc(ax, fpr, tpr, method_name, label):
    ind = METHOD_IND[method_name]
    ax.plot(fpr, tpr, color=COLORS[ind], linewidth=2.0, linestyle='-')
    marker_pos = np.argmax(tpr - fpr)

    label += " [AUC: %.2f] " % auc(fpr, tpr)
    ax.plot(fpr[marker_pos], tpr[marker_pos], color=COLORS[ind], linewidth=2.0, marker=MARKERS[ind],
            markersize=MARKERSIZE, label=label)


def plot_human_3utr_roc(ax):
    seed_file = os.path.join(
        REPO, 'V-CLIP/V-CLIP-SEED-CV/fpr_tpr.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(seed_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'AV1', r'\textbf{Avishkar} 10-fold CV')

    seed_file = os.path.join(
        REPO, 'V-CLIP/V-CLIP-3UTR-SEED/fpr_tpr.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(seed_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'AV3', r'\textbf{Avishkar} 10-fold CV (3\' UTR)')

    seed_file = os.path.join(
        REPO, 'V-CLIP/V-CLIP-TEST-3UTR-SEED/fpr_tpr.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(seed_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'AV5', r'\textbf{Avishkar} Mouse train (3\' UTR)')

    seed_file = os.path.join(
        REPO, 'V-CLIP/comptetition/MIRZA/mirza_results/mean_v_clip_fpr_tpr_canonical.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(seed_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'MIRZA', 'MIRZA')

    mirsvr_file = os.path.join(
        REPO, 'V-CLIP/comptetition/MiRSVR/fpr_tpr_seed.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(mirsvr_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'MIRSVR', 'mirSVR (3\' UTR)')

    pita_file = os.path.join(
        REPO, 'V-CLIP/comptetition/PITA/fpr_tpr.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(pita_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'PITA', 'PITA (3\' UTR)')

    yeding_file = os.path.join(
        REPO, 'V-CLIP/comptetition/TargetS/fpr_tpr_seed.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(yeding_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'STARMIR', 'STarMir')

    # Baseline
    ax.plot([0, 1], [0, 1], 'k--',
             label='baseline [AUC: 0.5]')

    ax.legend(loc='lower right', fontsize=FONTSIZE - 8)
    ax.set_title('human, seed', fontsize=FONTSIZE)


def plot_mouse_3utr_roc(ax):
    seed_file = os.path.join(
        REPO, 'HITS-CLIP/HITS-CLIP-SEED-CV/fpr_tpr.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(seed_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'AV1', r'\textbf{Avishkar} 10-fold CV')

    seed_file = os.path.join(
        REPO, 'HITS-CLIP/HITS-CLIP-3UTR-SEED/fpr_tpr.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(seed_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'AV3', r'\textbf{Avishkar} 10-fold CV (3\' UTR)')

    seed_file = os.path.join(
        REPO, 'HITS-CLIP/HITS-CLIP-TEST-3UTR-SEED/fpr_tpr.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(seed_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'AV5', r'\textbf{Avishkar} Human train (3\' UTR)')

    mirsvr_file = os.path.join(
        REPO, 'HITS-CLIP/comptetition/MiRSVR/fpr_tpr_seed.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(mirsvr_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'MIRSVR', 'mirSVR (3\' UTR)')

    pita_file = os.path.join(
        REPO, 'HITS-CLIP/comptetition/PITA/fpr_tpr.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(pita_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'PITA', 'PITA (3\' UTR)')

    pita_file = os.path.join(
        REPO, 'HITS-CLIP/comptetition/TargetScan/fpr_tpr.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(pita_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'TARGETSCAN', 'TargetScan (3\' UTR)')

    yeding_file = os.path.join(
        REPO, 'HITS-CLIP/comptetition/TargetS/fpr_tpr_seed.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(yeding_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'STARMIR', 'STarMir')

    # Baseline
    ax.plot([0, 1], [0, 1], 'k--',
             label='baseline [AUC: 0.5]')

    ax.legend(loc='lower right', fontsize=FONTSIZE - 8)
    ax.set_title('mouse, seed', fontsize=FONTSIZE)

def plot_mouse_seedless_roc(ax):
    seed_file = os.path.join(
        REPO, 'HITS-CLIP/HITS-CLIP-SEEDLESS-CV/fpr_tpr.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(seed_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'AV1', r'\textbf{Avishkar} 10-fold CV')

    seed_file = os.path.join(
        REPO, 'HITS-CLIP/HITS-CLIP-TEST/fpr_tpr.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(seed_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'AV5', r'\textbf{Avishkar} Human train')

    seed_file = os.path.join(
        REPO, 'HITS-CLIP/comptetition/TargetS/fpr_tpr_seedless.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(seed_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'STARMIR', 'STarMir')

    mirsvr_file = os.path.join(
        REPO, 'HITS-CLIP/comptetition/MiRSVR/fpr_tpr_seedless.csv'
    )
    fpr_seedless, tpr_seedless = read_fpr_tpr(mirsvr_file)
    plot_roc(ax, fpr_seedless, tpr_seedless, 'MIRSVR', 'mirSVR (3\' UTR)')

    # Baseline
    ax.plot([0, 1], [0, 1], 'k--',
             label='baseline [AUC: 0.5]')

    ax.legend(loc='lower right', fontsize=FONTSIZE - 8)
    ax.set_title('mouse, seedless', fontsize=FONTSIZE)


def plot_human_seedless_roc(ax):
    seed_file = os.path.join(
        REPO, 'V-CLIP/V-CLIP-SEEDLESS-CV/fpr_tpr.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(seed_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'AV1', r'\textbf{Avishkar} 10-fold CV')

    seed_file = os.path.join(
        REPO, 'V-CLIP/V-CLIP-TEST/fpr_tpr.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(seed_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'AV5', r'\textbf{Avishkar} Mouse train')

    mirsvr_file = os.path.join(
        REPO, 'V-CLIP/comptetition/MiRSVR/fpr_tpr_seedless.csv'
    )
    fpr_seedless, tpr_seedless = read_fpr_tpr(mirsvr_file)
    plot_roc(ax, fpr_seedless, tpr_seedless, 'MIRSVR', 'mirSVR (3\' UTR)')

    seed_file = os.path.join(
        REPO, 'V-CLIP/comptetition/MIRZA/mirza_results/mean_v_clip_fpr_tpr_noncanonical.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(seed_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'MIRZA', 'MIRZA')

    seed_file = os.path.join(
        REPO, 'V-CLIP/comptetition/TargetS/fpr_tpr_seedless.csv'
    )
    fpr_seed, tpr_seed = read_fpr_tpr(seed_file)
    plot_roc(ax, fpr_seed, tpr_seed, 'STARMIR', 'STarMir')

    # Baseline
    ax.plot([0, 1], [0, 1], 'k--',
             label='baseline [AUC: 0.5]')

    ax.legend(loc='lower right', fontsize=FONTSIZE - 8)
    ax.set_title('human, seedless', fontsize=FONTSIZE)


def plot_all():
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex='col', sharey='row')
    plot_mouse_3utr_roc(ax1)
    plot_mouse_seedless_roc(ax2)
    plot_human_3utr_roc(ax3)
    plot_human_seedless_roc(ax4)

    ax3.set_xlabel('FPR', fontsize=FONTSIZE)
    ax4.set_xlabel('FPR', fontsize=FONTSIZE)
    ax1.set_ylabel('TPR', fontsize=FONTSIZE)
    ax3.set_ylabel('TPR', fontsize=FONTSIZE)

    plt.setp(ax3.get_xticklabels(), fontsize=FONTSIZE - 2)
    plt.setp(ax4.get_xticklabels(), fontsize=FONTSIZE - 2)
    plt.setp(ax1.get_yticklabels(), fontsize=FONTSIZE - 2)
    plt.setp(ax3.get_yticklabels(), fontsize=FONTSIZE - 2)
    fig.set_tight_layout(dict(pad=0.1, h_pad=0.1, w_pad=0.1))
    return fig

