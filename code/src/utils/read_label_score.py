import numpy as np
from sklearn.metrics import roc_curve
from sklearn.metrics import auc


def read_label_score(fname, filter_non_nan=False):
    label_scores = []
    count = 0
    with open(fname, 'rU') as fp:
        for line in fp:
            count += 1
            items = line.strip().split(",")
            if not items:
                continue
            try:
                label_score = (int(items[-1]), float(items[-2]))
            except:
                print "Error in line:", count
                print line
                continue

            if filter_non_nan is False:
                label_scores.append(label_score)
            elif filter_non_nan is True and not np.isnan(label_score[1]):
                label_scores.append(label_score)
    label_scores = np.array(label_scores)
    return label_scores


def exp_transform(label_scores):
    non_nan_ind = ~ np.isnan(label_scores[:, 1])
    label_scores[non_nan_ind, 1] = 1.0/( 1 + np.exp( label_scores[non_nan_ind, 1] ) )
    return label_scores


def get_min(label_scores):
    non_nan_ind = ~ np.isnan(label_scores[:, 1])
    return np.min(label_scores[non_nan_ind, 1])


def replace_nan(label_scores, min_val):
    nan_ind = np.isnan(label_scores[:, 1])
    label_scores[nan_ind, 1] = min_val
    return label_scores
    

def get_str_arr(arr):
    return " ".join([str(x) for x in arr])


def write_fpr_tpr(input_file, output_file, filter_non_nan=False, exp_trans=False):
    label_scores = read_label_score(input_file, filter_non_nan=filter_non_nan)
    if exp_trans is True:
        label_scores = exp_transform(label_scores)

    if filter_non_nan is False:
        min_val = get_min(label_scores)
        label_scores = replace_nan(label_scores, min_val)
    fpr, tpr, _ = roc_curve(label_scores[:, 0], label_scores[:, 1])
    auc_value = auc(fpr, tpr)
    print "AUC:", auc_value

    with open(output_file, 'w') as fp:
        fp.write("%s,%s\n" % (get_str_arr(fpr), get_str_arr(tpr)))
