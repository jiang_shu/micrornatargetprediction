from batch_fasta_file_reader import BatchFastaFileReader
import numpy as np
import sys


def get_label(name):
    return name[-1]


def toss_coin(prob):
    return np.random.rand() <= prob


def sample_fasta(input_file, num_pos, num_neg, splits=10):
    """
        Reads fasta format records from @input_file.
        Writes @splits number of output files.
        Samples from negative data. Writes positive and
        sampled negative data into one of the randomly chosen output files.
        Each of the output file contains on an average equal number of positive
        and neg data.
    """
    output_fps = []
    for i in range(splits):
        output_file_name = input_file + '.part_%d' % i
        output_fps.append(open(output_file_name, 'w'))

    data = []
    prob = float(num_pos)/num_neg
    with BatchFastaFileReader(input_file, batch_size=1000) as reader:
        while True:
            remaining_data = reader.read(data)
            for d in data:
                label = get_label(d.id)
                if label == '0' and toss_coin(prob) is False:
                    continue

                ofp = output_fps[np.random.randint(0, splits)]
                ofp.write(">%s\n" % d.id)
                ofp.write("%s\n" % d.seq)

            del data[:]
            if remaining_data is False:
                break

def main():
    if len(sys.argv) != 3:
        print "Usage: %s <input file> <output file>" % (sys.argv[0])
        return 1
    input_file = sys.argv[1]
    output_file = sys.argv[2]
    sample_fasta(input_file, output_file)
    return 0

if __name__ == '__main__':
    sys.exit(main())
