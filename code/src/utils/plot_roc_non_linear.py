import numpy as np
import matplotlib.pyplot as plt
import os.path
from sklearn.metrics import roc_curve
from sklearn.metrics import auc

COLORS=['#67412C', '#C056C5', '#9AC4B9', '#4F405E', '#CC5835',
        '#82D35B', '#547645', '#C75F7E', '#8385CB', '#CEB452']
MARKERS=['d', '>', '*', '<', 'D',
         '^', 'o', 's', 'v', 'p']
FONTSIZE = 18
MARKERSIZE = 8

ROOT_PATH = '/Users/asish/Workspace/Academic/Research/Projects/MicroRnaTargetPrediction/data/experiments/'

CLUSTER_NAMES = ['miR-7', 'miR-25', 'miR-103', 'miR-15',
                 'miR-10', 'miR-106', 'miR-19', 'miR-320',
                 'miR-30', 'let-7']


def plot_roc():
    linear_file = os.path.join(ROOT_PATH, 'vclip_linear_all_probs.csv')
    non_linear_file = os.path.join(ROOT_PATH, 'vclip_cluster_all_probs.csv')
    linear_label_prob = np.loadtxt(linear_file, delimiter=',', usecols=(4,5))
    non_linear_label_prob = np.loadtxt(non_linear_file, delimiter=',', usecols=(4,5))
    l_fpr, l_tpr, _ = roc_curve(linear_label_prob[:, 0], linear_label_prob[:, 1])
    nl_fpr, nl_tpr, _ = roc_curve(non_linear_label_prob[:, 0], non_linear_label_prob[:, 1])
    l_auc = auc(l_fpr, l_tpr)
    nl_auc = auc(nl_fpr, nl_tpr)

    fig, ax = plt.subplots()
    # ax.margins(0.04)
    ax.plot(nl_fpr, nl_tpr, color=COLORS[4],
            label='Non-linear Avishkar 5-fold CV [AUC: %.3f]' % nl_auc,
            linewidth=2)
    ax.plot(l_fpr, l_tpr, color=COLORS[3],
            label='Linear Avishkar 5-fold CV [AUC: %.3f]' % l_auc,
            linewidth=2)
    ax.plot([0, 1], [0, 1], color='black',
            label='Baseline [AUC: 0.5]',
            linewidth=2, linestyle='--')

    ax.set_ylabel('TPR', fontsize=FONTSIZE)
    ax.set_xlabel('FPR', fontsize=FONTSIZE)
    ax.set_xticks(np.linspace(0, 1, 6))
    ax.set_xticklabels(np.linspace(0, 1, 6), fontsize=FONTSIZE)
    ax.set_yticks(np.linspace(0, 1, 6))
    ax.set_yticklabels(np.linspace(0, 1, 6), fontsize=FONTSIZE)
    ax.legend(loc='lower right', fontsize=FONTSIZE)
    
