"""
    Functional PCA
"""
from thunder import RowMatrix
from thunder import PCA
import numpy as np
from fda import smooth_spline
import copy


def get_numbered_row(index, itr):
    """ For each element in the iterator @itr return
        a pair (id, array) where id is the unique id identifying
        the record
    """
    count = 0
    for x in itr:
        count += 1
        yield ((index, count), np.array(x))


def to_row_matrix(rdd):
    matrix = rdd.mapPartitionsWithIndex(get_numbered_row)
    return RowMatrix(matrix)


class FPCA:
    def __init__(self, nbasis=20, k=20, n=100):
        """
            @nbasis: Number of cubic B-spline basis functions to use
            to smooth data
            @k: The number of principal components.
            @n: The number of discrete points to compute
        """
        if k <= 0:
            raise Exception("Number of principal components (k) should"
                            " be greater than 0")
        self._k = k
        self._n = n
        self._nbasis = nbasis
        self._x = np.linspace(0, 1, n)
        self._model = None

    def fit(self, rdd, compute_explained_variance_ratio=False):
        """
            Computes the functional principal components by first smoothing
            data using B-spline smoothing. Then discretizes the function
            and computes multivariate PCA on the discrete data.
        """
        smoothed_rdd = rdd.map(self._smooth_data)
        row_matrix = to_row_matrix(smoothed_rdd)
        row_matrix.cache()
        row_matrix.center(1)
        model = PCA(k=self._k).fit(row_matrix)
        model.scores = row_matrix.times(model.comps.T)
        self.comps = copy.copy(model.comps)

        if compute_explained_variance_ratio is True:
            self.total_variance_ = np.sum(row_matrix.variance())
            self.explained_variance_ = model.scores.variance()
            self.explained_variance_ratio_ =\
                self.explained_variance_/self.total_variance_

    def transform(self, x):
        """
            Project the point x into the space spanned by the principal
            components and return the projection score
        """
        return np.dot(self.comps, x)

    def _smooth_data(self, y):
        spl = smooth_spline(y)
        return spl(self._x)
