import sys
import numpy as np
import rpy2.robjects as robjects
import rpy2
from rpy2.robjects.numpy2ri import numpy2ri
rpy2.robjects.numpy2ri.activate()
from r_interface import create_r_func
from parallelize import run_func_parallely
from set_model_params import set_model_params
from sklearn.feature_extraction import DictVectorizer


def usage():
    print '''
    %s <positive mfe file> <positive mfe_open file> <positive seed file>
    <negative mfe file> <negative mfe_open file> <negative seed file>
    <output model file> <output pca file>''' % sys.argv[0]


def get_onehot_repr(data):
    '''
        Given a list categorical features convert them to
        onehot encoding.
    '''
    vectorizer = DictVectorizer(sparse=False)
    data_dict = [{1: x} for x in data]
    data = vectorizer.fit_transform(data_dict)
    names = vectorizer.get_feature_names()
    return (names, data)


def get_onehot_parallel(data):
    # TODO: For now this is serial. If this becomes a bottleneck then we'll
    # parallelize later.
    return get_onehot_repr(data)

    '''
    names, data = run_func_parallely(get_onehot_repr, list(data),
                                     combine_func=lambda x: zip(*x))
    print names
    data = np.vstack(data)
    return names[0], data
    '''


def encode_categorical_features(categorical_features):
    # Converts categorical features to their one-hot encoding.
    cat_features = []
    for i in range(categorical_features.shape[1]):
        _, cat_feature = get_onehot_parallel(categorical_features[:, i])
        cat_features.append(cat_feature)

    if len(cat_features) > 1:
        cat_features = np.hstack(cat_features)
    else:
        cat_features = cat_features[0]
    return cat_features


def array_to_str(arr):
    return "".join([str(x) for x in arr])


def get_principal_components(fdata, num_basis=20, components=10):
    set_model_params(num_basis=num_basis, num_principal_components=components)
    smooth_fd_func = create_r_func('smoothFd.R')
    data_fd = run_func_parallely(smooth_fd_func, fdata)
    # We need to combine the splits to create on fd object
    create_fd_func = create_r_func('createFd.R')
    data_fd = create_fd_func(data_fd)
    # Get principal components
    get_principal_components_func = create_r_func('computePca.R')
    pca_r = get_principal_components_func(data_fd)
    return pca_r


def train(y, delg, deldelg, au,
          categorical_features,
          numeric_features,
          therm_basis=20,  # For delg and deldelg combined
          au_basis=10,
          therm_components=15,  # For delg and deldelg combined
          au_components=5,
          prob=False,
          ):

    # thermodynamic feature
    therm_site = np.hstack((delg[0], deldelg[0]))
    therm_seed = np.hstack((delg[1], deldelg[1]))

    # Smooth training data
    print "Getting principal components for therm (site)"
    pca_therm_site_r = get_principal_components(therm_site,
                                                num_basis=therm_basis,
                                                components=therm_components)
    print "Getting principal components for therm (seed)"
    pca_therm_seed_r = get_principal_components(therm_seed,
                                                num_basis=therm_basis,
                                                components=therm_components)

    # Do the same thing for the AU composition.
    print "Getting principal components for au (site)"
    pca_au_site_r = get_principal_components(au[0], num_basis=au_basis,
                                             components=au_components)

    print "Getting principal components for au (seed)"
    pca_au_seed_r = get_principal_components(au[1], num_basis=au_basis,
                                             components=au_components)

    # Converting categorical features.
    print "Getting one-hot encoding of categorical features."

    categorical_features = encode_categorical_features(categorical_features)

    all_features = np.hstack((categorical_features, numeric_features))

    print "Starting training."
    set_model_params(svm_kernel='linear', svm_cost=100,
                     svm_prob=prob, save_train_data=False)

    # Train SVM Model
    svm_func = create_r_func('trainSvmClip.R')
    svm_model = svm_func(y,
                         pca_therm_site_r[0], pca_therm_seed_r[0],
                         pca_au_site_r[0], pca_au_seed_r[0],
                         all_features)
    print "Done."

    return(svm_model,
           (pca_therm_site_r, pca_therm_seed_r),
           (pca_au_site_r, pca_au_seed_r)
           )


def main():
    # TODO: Fix this
    if len(sys.argv) != 9:
        usage()
        return 1
    set_model_params()
    pos_mfe = np.genfromtxt(sys.argv[1], delimiter=',')
    pos_mfe_open = np.genfromtxt(sys.argv[2], delimiter=',')
    pos_seed = np.genfromtxt(sys.argv[3], delimiter=',')
    neg_mfe = np.genfromtxt(sys.argv[4], delimiter=',')
    neg_mfe_open = np.genfromtxt(sys.argv[5], delimiter=',')
    neg_seed = np.genfromtxt(sys.argv[6], delimiter=',')
    svm_model_file = sys.argv[7]
    pca_file = sys.argv[8]
    (svm_model, pca) = train(pos_mfe, pos_mfe_open, pos_seed,
                             neg_mfe, neg_mfe_open, neg_seed)

    # Save the SVM Model and PCA
    assign_func = robjects.r('assign')
    assign_func('svm.model', svm_model)
    assign_func('pca', pca)
    robjects.r('save(svm.model, file="%s")' % svm_model_file)
    robjects.r('save(pca, file="%s")' % pca_file)
    return 0


if __name__ == '__main__':
    sys.exit(main())
