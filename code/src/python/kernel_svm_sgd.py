"""
    Bugeted SGD for kernel SVM
"""
import numpy as np
from functools import partial
from itertools import cycle
from abc import ABCMeta, abstractmethod


DEF_REG_PARAM = 0.1
DEF_RBF_KERNEL_SIGMA = 0.01
TOLERANCE = 0.0001
BUDGETING_POLICIES = set(['REMOVE', 'MERGE'])
KERNEL_PRECISION = 1000000.0

### Note: reg_param = 2/(N * C) where N is the number of data points
### and C is penalty for misclassification.

def get_dimension(x):
    if type(x) == np.ndarray:
        return x.size
    else:
        # Assume list
        return len(x)


def golden_search(func, low, high, tolerance=TOLERANCE, operation='MIN'):
    """
        Golden search minimization.
    """
    mul = 1.0
    # If we want to maximize then minimize -func instead
    if operation == 'MAX':
        mul = -1.0

    gr = (3.0 - np.sqrt(5.0))/2.0
    cr = 1 - gr
    ax, bx, cx = low, (gr*low + cr*high), high
    x0, x3 = ax, cx
    if np.abs(cx - bx) > np.abs(bx - ax):
        x1 = bx
        x2 = bx + cr * (cx - bx)
    else:
        x2 = bx
        x1 = bx - cr * (bx - ax)

    f1, f2 = mul * func(x1), mul * func(x2)
    while np.abs(x3 - x0) > tolerance*(np.abs(x1) + np.abs(x2)):
        if f2 < f1:
            x0, x1, x2, x3 = x1, x2, (gr*x2 + cr*x3), x3
            f1, f2 = f2, mul * func(x2)
        else:
            x0, x1, x2, x3 = x0, (cr*x0 + gr*x1), x1, x2
            f1, f2 = mul * func(x1), f1

    if f1 < f2:
        return x1
    else:
        return x2


def optimization_objective(m, k12, h):
    """ k12 is the kernel value for points x1 and x2 """
    return (m * (k12**((1-h)**2))) + ((1 - m) * (k12**(h**2)))


class Kernel(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def eval(self, x1, x2):
        pass

    def __call__(self, x1, x2):
        return self.eval(x1, x2)


class CachedKernel(Kernel):
    __metaclass__ = ABCMeta

    def __init__(self):
        self._cache = {}

    @abstractmethod
    def cache_key(self, x1, x2):
        pass

    def _cached_eval(self, x1, x2):
        key = self.cache_key(x1, x2)
        if key in self._cache:
            return self._cache[key]

        val = self.eval(x1, x2)
        self._cache[key] = val
        return val

    def __call__(self, x1, x2):
        return self._cached_eval(x1, x2)


class RBFKernel(Kernel):
    def __init__(self, gamma=DEF_RBF_KERNEL_SIGMA):
        Kernel.__init__(self)
        self._gamma = gamma

    def eval(self, x1, x2):
        diff = x1 - x2
        return np.exp(-self._gamma*(np.dot(diff, diff)))


class CachedRBFKernel(CachedKernel):
    def __init__(self, gamma=DEF_RBF_KERNEL_SIGMA):
        CachedKernel.__init__(self)
        self._gamma = gamma

    def eval(self, x1, x2):
        diff = x1 - x2
        return np.exp(-self._gamma*(np.dot(diff, diff)))

    def cache_key(self, x1, x2):
        diff = x1 - x2
        key = int(np.dot(diff, diff) * KERNEL_PRECISION + 0.5)
        return key


class LearningRate(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def rate(self, t):
        pass

    def __call__(self, t):
        return self.rate(t)


class PegasosLearningRate(LearningRate):
    """
        The Pegasos learning rate is given by:
        eta = 1/(lambda * t) where lambda is the regularization parameter
    """
    def __init__(self, reg_param=DEF_REG_PARAM):
        self._reg_param = reg_param

    def rate(self, t):
        return 1.0/(self._reg_param * t)


class BudgetingPolicy(object):
    __metaclass__ = ABCMeta

    def __init__(self, kernel, coeffs, svs, sv_labels):
        self._kernel = kernel
        self._support_vectors = svs
        self._support_vector_labels = sv_labels
        self.coeffs = coeffs

    def __call__(self, sv_count, w, beta):
        """ @sv_count: Number of support vectors
            @w: Vector of kernels computed with the data points and SVs
        """
        return self._get_index(sv_count, w, beta)

    @abstractmethod
    def _get_index(self, sv_count, w, beta):
        """
            Returns the index of the support vector to remove
        """
        pass

    def _get_duplicate_point(self, w):
        """
            Givent the kernel vector for the query point.
            Find the duplicate element
        """
        # Find if we have a vector that is almost same as the point x
        d = w * KERNEL_PRECISION + 0.5
        d = d.astype(int)
        remove_index = np.where(d == KERNEL_PRECISION)[0]
        if remove_index.size != 0:
            return remove_index[0]
        return -1


class RemovalBudgetingPolicy(BudgetingPolicy):
    """ Removes a support vector with minimum coeff """
    def _get_index(self, sv_count, w, beta):
        remove_index = self._get_duplicate_point(w)
        if remove_index > 0:
            return remove_index

        # We didn't find a duplicate vector
        remove_index = np.argmin(np.abs(self.coeffs[:sv_count]))
        return remove_index


class MergeBudgetingPolicy(BudgetingPolicy):
    def _get_index(self, sv_count, w, beta):
        remove_index = self._get_duplicate_point(w)
        if remove_index > 0:
            self.coeffs[remove_index] += beta
            return -1

        # First find the support vector with smallest alpha
        min_index = np.argmin(np.abs(self.coeffs[:sv_count]))
        sv1 = self._support_vectors[min_index]
        sv1_label = self._support_vector_labels[min_index]
        alpha1 = self.coeffs[min_index]
        h_max = 0.0
        sv2_index = 0
        for i in range(sv_count):
            if i == min_index:
                continue
            sv2 = self._support_vectors[i]
            sv2_label = self._support_vector_labels[i]
            alpha2 = self.coeffs[i]

            if sv1_label * sv2_label < 0.0:
                # We only want to maximize over positive indices
                continue

            # Search for h that maximizes:
            # m * kernel((1-h) * sv1, (1-h) * sv2) +
            # (1-m) * kernel(h * sv1, h * sv2)
            m = alpha1/(alpha1 + alpha2)
            objective_func = partial(
                optimization_objective, m, self._kernel(sv1, sv2)
            )

            hc = golden_search(objective_func, 0.0, 1.0, operation='MAX')

            if hc >= h_max:
                h_max = hc
                sv2_index = i

        # print "Merging [%.3f] (%d, %d)" % (h_max, min_index, sv2_index)

        sv2 = self._support_vectors[sv2_index]
        alpha2 = self.coeffs[sv2_index]
        z = h_max*sv1 + (1 - h_max)*sv2
        alpha_z = alpha1*self._kernel(sv1, z) +\
            alpha2*self._kernel(sv2, z)

        # Replace the first support vector by the merged vector
        # and return the index of the 2nd vector as the vector to be
        # replaced.
        self.coeffs[min_index] = alpha_z
        self._support_vector_labels[min_index] = sv1_label
        self._support_vectors[min_index] = z
        return sv2_index


class KernelSvmBsgd(object):
    def __init__(self, budget, reg_param,
                 learning_rate=None,
                 kernel=CachedRBFKernel(),
                 budgeting_policy='REMOVE'):
        """
            @budget: Maximum number of support vectors
            @reg_param: Regularization parameter (lambda)
            @learning_rate: Learning rate. A function of t, t >= 1. Defaults
            to Pegasos learning rate: 1/(reg_param * t)
            @kernel: Kernel function. Defaults to Cached RBF kernel
            @kernel_params: Parameters that are passed to the kernel function
            e.g. gamma for RBF kernel.
        """
        if budgeting_policy not in BUDGETING_POLICIES:
            raise Exception("Valid budgeting policies are:",
                            "/".join(BUDGETING_POLICIES))

        if not isinstance(kernel, Kernel):
            raise Exception("kernel must be an instance of Kernel class")

        if (not (isinstance(kernel, RBFKernel) or
                 isinstance(kernel, CachedRBFKernel))) and\
                budgeting_policy != 'REMOVE':

            raise Exception("Kernels other than the RBF kernel only support"
                            "REMOVE policy for budgeting support vectors")

        self._reg_param = float(reg_param)
        self._budget = budget
        self._learning_rate = learning_rate
        self._kernel = kernel

        if self._learning_rate is None:
            self._learning_rate = PegasosLearningRate(reg_param=reg_param)

        # The alphas for the support vectors
        self.coeffs = np.zeros((budget, 1))

        # We can't initialize the array here becuase we don't
        # know the dimension of the data points. So wait until
        # we receive the first data point.
        self._support_vectors = None
        self._support_vector_labels = np.zeros((budget, 1))
        self._t = 1
        self._sv_count = 0

        # Reinitilize this once we've initialized the support_vectors list
        self._budgeting_policy = budgeting_policy

    def _compute_kernel_vector(self, x):
        kv = np.array([self._kernel(x1, x2) for (x1, x2) in
                       zip(self._support_vectors[:self._sv_count], cycle([x]))])
        return kv

    def _loss(self, y, kv):
        l = y * np.dot(kv, self.coeffs[:self._sv_count])  # yW'x
        l = 1 - l
        return l

    def _add_sv(self, y, x, beta, w):
        index = self._sv_count
        if self._sv_count >= self._budget:
            index = self._budgeting_policy(self._sv_count, w, beta)
        else:
            self._sv_count += 1

        if index >= 0:
            """ Adds support vector """
            self._support_vectors[index] = x
            self._support_vector_labels[index] = y
            self.coeffs[index] = beta

    def _add_point(self, y, x):
        if self._support_vectors is None:
            beta = y * self._learning_rate(self._t)
            self._support_vectors = np.zeros((self._budget, get_dimension(x)))
            self._add_sv(y, x, beta, None)
            self._t += 1

            # Initialize the budgeting policy now.
            if self._budgeting_policy == 'REMOVE':
                self._budgeting_policy = RemovalBudgetingPolicy(
                    self._kernel,
                    self.coeffs, self._support_vectors,
                    self._support_vector_labels
                )
            elif self._budgeting_policy == 'MERGE':
                self._budgeting_policy = MergeBudgetingPolicy(
                    self._kernel,
                    self.coeffs, self._support_vectors,
                    self._support_vector_labels
                )
            else:
                raise Exception('Budgeting policy %s not implemented' %
                                self._budgeting_policy)
            return

        # (1 - lambda * eta):
        m = (1.0 - self._reg_param*self._learning_rate(self._t))
        self.coeffs[:self._sv_count] = self.coeffs[:self._sv_count] * m
        w = self._compute_kernel_vector(x)
        if self._loss(y, w) > 0:
            beta = y * self._learning_rate(self._t)
            self._add_sv(y, x, beta, w)
        self._t += 1

    def get_support_vectors(self):
        # Convert labels to 0/1 instead of -1/+1
        labels = np.atleast_2d(
            map(lambda y: 0.0 if y[0] < 0.0 else y[0],
                self._support_vector_labels[:self._sv_count])
        )
        svs = np.hstack((labels.T, self._support_vectors[:self._sv_count]))
        return svs

    def train(self, data):
        for y, x in data:
            # Convert label to -1.0 and 1.0
            y = -1.0 if int(y) == 0 else y
            self._add_point(y, x)

    def predict(self, x):
        w = self._compute_kernel_vector(x)
        f = np.dot(w, self.coeffs[:self._sv_count])
        y = np.sign(f)
        # Reconvert label to 0/1
        y = 0.0 if y < 0.0 else y
        return y
