from functools import partial
from collections import OrderedDict
import numpy as np


def is_valid(data):
    return not ((data is None) or
                (type(data) == list and len(data) == 0) or
                (type(data) == np.ndarray and np.size(data) == 0))


class Pipeline:
    def __init__(self):
        self.__pipeline_steps = OrderedDict()

    def add_step(self, func, **kwargs):
        self.__pipeline_steps[func.__name__] = partial(func, **kwargs)
        return self

    def execute(self, data, for_each=False):
        # @for_each indicates if the operation is to be performed for
        # each element in data
        if not for_each:
            return self.__execute_single(data)
        else:
            processed_data = []
            count = 0
            for datum in data:
                try:
                    datum = self.__execute_single(datum)
                    if is_valid(datum):
                        processed_data.append(datum)
                except:
                    print "Encountered error in line: ", count
                    raise
                count += 1
            # print "Filtered out %d items." % (len(data) - len(processed_data))
            return processed_data

    def __execute_single(self, data):
        ''' Execute the pipeline for a single data point '''
        for func_name, func in self.__pipeline_steps.iteritems():
            try:
                data = func(data)
            except:
                print "Encoutered error while executing function: ", func_name
                print data
                raise
        return data
