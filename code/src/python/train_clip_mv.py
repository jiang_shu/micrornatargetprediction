'''
    Multivariate model for microRNA Target Prediction using CLIP data
'''
import sys
import numpy as np
import rpy2.robjects as robjects
import rpy2
from rpy2.robjects.numpy2ri import numpy2ri
rpy2.robjects.numpy2ri.activate()
from r_interface import create_r_func
from set_model_params import set_model_params
from train_clip import encode_categorical_features


def usage():
    print '''
    %s <positive mfe file> <positive mfe_open file> <positive seed file>
    <negative mfe file> <negative mfe_open file> <negative seed file>
    <output model file> <output pca file>''' % sys.argv[0]


def array_to_str(arr):
    return "".join([str(x) for x in arr])


def train_mv(y, delg, deldelg, au,
             categorical_features,
             numeric_features,
             prob=False):

    # thermodynamic feature
    therm_site = np.hstack((delg[0], deldelg[0]))
    therm_seed = np.hstack((delg[1], deldelg[1]))

    # Converting categorical features.
    categorical_features = encode_categorical_features(categorical_features)

    all_features = np.hstack((categorical_features, numeric_features))

    print "Starting training."
    set_model_params(svm_kernel='linear', svm_cost=100,
                     svm_prob=prob, save_train_data=False)

    # Train SVM Model
    svm_func = create_r_func('trainSvmClipMv.R')
    svm_model = svm_func(y, therm_site, therm_seed, au[0], au[1],
                         all_features)
    print "Done."

    return svm_model


def main():
    # TODO: Fix this
    if len(sys.argv) != 9:
        usage()
        return 1
    set_model_params()
    pos_mfe = np.genfromtxt(sys.argv[1], delimiter=',')
    pos_mfe_open = np.genfromtxt(sys.argv[2], delimiter=',')
    pos_seed = np.genfromtxt(sys.argv[3], delimiter=',')
    neg_mfe = np.genfromtxt(sys.argv[4], delimiter=',')
    neg_mfe_open = np.genfromtxt(sys.argv[5], delimiter=',')
    neg_seed = np.genfromtxt(sys.argv[6], delimiter=',')
    svm_model_file = sys.argv[7]
    pca_file = sys.argv[8]
    (svm_model, pca) = train_mv(pos_mfe, pos_mfe_open, pos_seed,
                                neg_mfe, neg_mfe_open, neg_seed)

    # Save the SVM Model and PCA
    assign_func = robjects.r('assign')
    assign_func('svm.model', svm_model)
    assign_func('pca', pca)
    robjects.r('save(svm.model, file="%s")' % svm_model_file)
    robjects.r('save(pca, file="%s")' % pca_file)
    return 0


if __name__ == '__main__':
    sys.exit(main())
