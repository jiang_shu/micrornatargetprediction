import os.path
import os
import rpy2.robjects as robjects
import rpy2
from rpy2.robjects.numpy2ri import numpy2ri
rpy2.robjects.numpy2ri.activate()

# The environment variable R_SRC_PATH can be set to override this.
DEFAULT_RPATH = '../R/'


def create_r_func(filename):
    rpath = os.getenv('R_SRC_PATH', DEFAULT_RPATH)
    filename = os.path.join(rpath, filename)
    # Creates a r function from the filename
    func = None
    with open(filename, 'rU') as fp:
        func = fp.read()
    if not func:
        raise Exception('Could not read file: %s' % filename)
    return robjects.r(func)
