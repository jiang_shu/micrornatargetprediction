'''
    Input: File with mrna-mirna clip data.
    Outputs the following features:
        a) With window length ~22 and centered around the entire target region
            1) del G curve
            2) del del G curve
            3) AU composition curve
            4) mean conservation curve
        b) For window length =8 and centered around seed match region
            1) del G curve
            2) del del G curve
            3) AU composition curve
            5) mean conservation curve
        c) Region in which the site is present: 3UTR, 5UTR, CDS
        d) Location of target from the end of the region (range: 0 to 1)
        e) Enrichment of seed type for both seed and seedless sites.
        f) Size of target site

    Possible future features:
        1) Number of seed matches withing the seed region.
        2) Avg binding of the top N alignments within the site.
'''

import sys
from redisdao import RedisDao
from batch_file_reader import BatchFileReader
import multiprocessing
import numpy as np
from functools import partial
from pipeline import Pipeline
from parallelize import run_func_parallely
import subprocess
import re
import traceback


MATCH, MISMATCH, GAP, WOBBLE = 1, 2, 3, 4
WINDOWS = 13  # Number of windows
WINDOW_LEN_SITE = 46  # TODO
WINDOW_LEN_SEED = 9
PROCESSORS = multiprocessing.cpu_count()  # Number of processors
SEED_TYPES = [(1, 8), (1, 7), (2, 8), (2, 7), (1, 6), (3, 8), None]

# RNAhybrid executabe
RNAHYBRID = "../../third-party/bin/RNAhybrid"
# RNAfold executable
RNAFOLD = "RNAfold"

# TODO: hg19 to hg18 hack
mrna_exclusion_list = set([])


def invoke_rna_hybrid(mrna_seq, mirna_seq,
                      energy_threshold=None,
                      helix_constraint=None):
    executable = RNAHYBRID
    # TODO: Change the dataset type (3utr_human). But do we need to change
    # if we are not interested in the p-value?
    args = "-c -b 1 -s 3utr_human"
    if helix_constraint is not None:
        args += " -f %d,%d" % (helix_constraint[0], helix_constraint[1])
    if energy_threshold is not None:
        args += " -e %0.3f" % energy_threshold
    cmd = "%s %s %s %s" % (executable, args, mrna_seq, mirna_seq)
    output = subprocess.check_output(cmd, shell=True)
    return output


def get_align_site(output):
    # parse RNA hybrid output and get the site within the mRNA where
    # the miRNA is aligned to.
    output = output.strip()
    output = output.split(":")

    if not output or not output[0]:
        return None

    mrna_mismatch, mrna_match, mirna_match, mirna_mismatch =\
        output[7], output[8], output[9], output[10]

    max_align_len = [mrna_mismatch, mrna_match, mirna_mismatch, mirna_match]
    max_align_len = max(map(lambda x: len(x), max_align_len))
    mrna_mismatch += " " * (max_align_len - len(mrna_mismatch))
    mrna_match += " " * (max_align_len - len(mrna_match))
    mirna_mismatch += " " * (max_align_len - len(mirna_mismatch))
    mirna_match += " " * (max_align_len - len(mirna_match))

    mrna_align_len = 0
    for i in range(max_align_len):
        # If both mismatch and match are not gaps then increment
        # the mrna align length.
        if mrna_mismatch[i] == " " and mrna_match[i] == " ":
            continue
        mrna_align_len += 1

    site_start = int(output[6])
    site_end = site_start + mrna_align_len
    return (site_start, site_end)


def get_seed_match_type(mrna_mismatch, mrna_match,
                        mirna_mismatch, mirna_match):
    # Returns the alignment of the first 8 (from the 5' end)
    # nucleotide positions of the miRNA

    max_align_len = [mrna_mismatch, mrna_match, mirna_mismatch, mirna_match]
    max_align_len = max(map(lambda x: len(x), max_align_len))
    mrna_mismatch += " " * (max_align_len - len(mrna_mismatch))
    mrna_match += " " * (max_align_len - len(mrna_match))
    mirna_mismatch += " " * (max_align_len - len(mirna_mismatch))
    mirna_match += " " * (max_align_len - len(mirna_match))

    mirna_start_offset = 0  # accounts for gaps at the beginning of miRNA.
    for i in range(1, max_align_len):
        if mirna_mismatch[-i] != " " or mirna_match[-i] != " ":
            break
        mirna_start_offset += 1

    mirna_aligned_chars = 0
    alignment = []

    for i in range(1 + mirna_start_offset, max_align_len):
        if mirna_match[-i] == ' ':
            if mirna_mismatch[-i] != ' ' and mrna_mismatch[-i] != ' ':
                alignment.append(MISMATCH)
            else:
                alignment.append(GAP)
        elif (mirna_match[-i] == 'G' and mrna_match[-i] == 'U') or\
             (mrna_match[-i] == 'G' and mirna_match[-i] == 'U'):
            alignment.append(WOBBLE)
        else:
            alignment.append(MATCH)

        if mirna_match[-i] != ' ' or mirna_mismatch[-i] != ' ':
            mirna_aligned_chars += 1

        if mirna_aligned_chars >= 8:
            break

    alignment = array_to_str(alignment[::-1], delimiter="")
    return alignment


def parse_rna_hybrid_output(output, seed_type=False):
    # Parse output to get the following features:
    #   Mean free energy (mfe) of duplex.
    #   alignment of the seed region of miRNA if @seed_type is True

    output = output.strip()
    output = output.split(":")
    if not output or not output[0]:
        return None
    mfe = float(output[4])
    if not mfe < 0.0:
        return None

    if seed_type is False:
        return mfe

    mrna_mismatch, mrna_match, mirna_match, mirna_mismatch =\
        output[7], output[8], output[9], output[10]
    seed_match_type = get_seed_match_type(
        mrna_mismatch, mrna_match, mirna_mismatch, mirna_match
    )

    return (mfe, seed_match_type)


def compute_delg_window(mrna_seq, mirna_seq, seed_type=None):
    delg = 0.0
    if seed_type is not None:
        output = invoke_rna_hybrid(mrna_seq, mirna_seq,
                                   helix_constraint=seed_type)
        parsed_out = parse_rna_hybrid_output(output)
        if parsed_out is not None:
            delg = parsed_out
        return delg

    for seed_type in SEED_TYPES:
        output = invoke_rna_hybrid(mrna_seq, mirna_seq,
                                   helix_constraint=seed_type)
        parsed_out = parse_rna_hybrid_output(output)
        if parsed_out is not None:
            delg = parsed_out
            break
    return delg


def compute_delg(mrna_seq, mirna_seq, site_loc, site_seed_type, window_len):
    mrna_len = len(mrna_seq)
    delg = np.repeat(np.nan, WINDOWS*2 + 1)
    delg[WINDOWS] = compute_delg_window(mrna_seq[site_loc[0]:site_loc[1]],
                                        mirna_seq,
                                        site_seed_type)
    for i in range(WINDOWS):
        start = site_loc[1] + (i * window_len)
        end = min(mrna_len, start + window_len)
        delg[WINDOWS + i + 1] = compute_delg_window(
            mrna_seq[start:end],
            mirna_seq
        )
        if end == mrna_len:
            break

    for i in range(WINDOWS):
        end = site_loc[0] - (i * window_len)
        start = max(0, end - window_len)
        delg[WINDOWS - i - 1] = compute_delg_window(
            mrna_seq[start:end],
            mirna_seq
        )
        if start == 0:
            break
    return delg


def invoke_rna_fold(mrna_seq):
    executable = RNAFOLD
    cmd = "echo %s | %s --noPS" % (mrna_seq, executable)
    output = subprocess.check_output(cmd, shell=True)
    return output


def parse_rna_fold_output(output):
    mfe_open = 0.0
    output = output.split('\n')
    m = re.search(r'\(\s*([-0-9]*[.]{0,1}[0-9]+)\)$', output[1])
    if m:
        mfe_open = float(m.group(1))
    else:
        print output
        raise Exception("Couldn't parse output")
    return mfe_open


def get_seed_match_site(mrna_seq, mirna_seq, site_loc, seed_type):
    '''
        Returns the seed match site given a mrna and mirna. If the
        seed_type is not None then returns the seed match site in the
        mRNA by searching for seed seq (reverse comp)
        from the end of the mRNA site.
        If the seed_type is None then returns the location of the best
        possible hybridization of nucleotides 1-8 of mirna with mrna
        Note: The seed_site interval returned is a open on the right.
    '''
    if seed_type is not None:
        # Just do a string search to find the @seed_type within @site_loc
        seed_seq = mirna_seq[seed_type[0] - 1: seed_type[1]]
        seed_seq = str(seed_seq.reverse_complement())
        mrna_site_seq = str(mrna_seq[site_loc[0]: site_loc[1]])
        pos = [m.start() for m in re.finditer(seed_seq, mrna_site_seq)]
        seed_site = (pos[-1] + site_loc[0],
                     pos[-1] + site_loc[0] + seed_type[1] - seed_type[0] + 1)
    else:
        seed_seq = mirna_seq[:8]  # find the best match
        output = invoke_rna_hybrid(mrna_seq[site_loc[0]:site_loc[1]],
                                   seed_seq)
        seed_site = get_align_site(output)
        seed_site = (seed_site[0] + site_loc[0],
                     seed_site[1] + site_loc[0])
        if seed_site is None:
            raise Exception('Could not align miRNA seed seq')
    return seed_site


def compute_curve(func, mrna_seq, site_loc, window_len):
    mrna_len = len(mrna_seq)
    data = np.repeat(np.nan, WINDOWS*2 + 1)
    data[WINDOWS] = func(
        mrna_seq[site_loc[0]:site_loc[1]],
    )
    for i in range(WINDOWS):
        start = site_loc[1] + (i * window_len)
        end = min(mrna_len, start + window_len)
        data[WINDOWS + i + 1] = func(
            mrna_seq[start:end],
        )
        if end == mrna_len:
            break

    for i in range(WINDOWS):
        end = site_loc[0] - (i * window_len)
        start = max(0, end - window_len)
        data[WINDOWS - i - 1] = func(
            mrna_seq[start:end],
        )
        if start == 0:
            break
    return data


def compute_mfe_open_window(mrna_seq):
    output = invoke_rna_fold(mrna_seq)
    return parse_rna_fold_output(output)


def compute_deldelg(mrna_seq, delg, site_loc, window_len):
    mfe_open = compute_curve(compute_mfe_open_window, mrna_seq,
                             site_loc, window_len)
    deldelg = delg - mfe_open
    return deldelg


def compute_au_window(mrna_seq):
    au_content = [1 if x == 'A' or x == 'U' else 0 for x in mrna_seq]
    au_content = sum(au_content)
    return float(au_content)/float(len(mrna_seq))


def compute_au(mrna_seq, site_loc, window_len):
    return compute_curve(compute_au_window, mrna_seq, site_loc, window_len)


def compute_consv(dao, mrna, mirna, site_loc, seed_type, seed_site,
                  assembly='hg18'):

    consv = dao._handle.hget('features:%s:%s' % (assembly, mrna._name),
                             'consv_seq')
    seed_consv = np.nan
    offseed_consv = np.nan
    consv = np.array([float(x) for x in consv.split()])
    consv = np.array(consv[site_loc[0]:site_loc[1]])

    if seed_type is not None:
        site_len = site_loc[1] - site_loc[0]
        seed_indices = np.repeat(False, site_len)
        seed_indices[seed_site[0] - site_loc[0]:
                     seed_site[1] - site_loc[0]] = True
        seed_consv = np.mean(consv[seed_indices])
        offseed_consv = np.mean(consv[~seed_indices])

    consv = np.mean(consv)
    return (consv, seed_consv, offseed_consv)


# Returns true if smaller location (@loc)
# is contained within the larger location (@other_loc)
def contains(loc, other_loc):
    ''' Returns True of @loc is contained within @other_loc '''
    if loc[0] >= other_loc[0] and loc[1] <= other_loc[1]:
        return True
    return False


# Returns the percentage (wrt smaller interval) of overlap between 2 intervals.
def overlap_perct(interval, other_interval):
    interval_size = min(interval[1] - interval[0],
                        other_interval[1] - interval[0])
    overlap_size = min(interval[1], other_interval[1]) -\
        max(interval[0], other_interval[0])
    overlap_size = max(0, overlap_size)
    return float(overlap_size)/interval_size


def compute_misc_features(dao, mrna, mirna, site_loc, seed_type,
                          seed_enrichment_map, assembly):
    # site_region, rel_site_loc, seed_type_enrichment, site_size

    site_len = site_loc[1] - site_loc[0]

    # get the site region
    utr5_range, utr3_range, cds_range = dao._handle.hmget(
        'features:%s:%s' % (assembly, mrna._name),
        ['utr5_range', 'utr3_range', 'cds_range'])
    utr5_range = tuple(map(int, utr5_range.split("-")))
    utr3_range = tuple(map(int, utr3_range.split("-")))
    cds_range = tuple(map(int, cds_range.split("-")))

    if overlap_perct(site_loc, utr5_range) >= 0.95:
        site_region_str, site_region = "5UTR", utr5_range
    elif overlap_perct(site_loc, utr3_range) >= 0.95:
        site_region_str, site_region = "3UTR", utr3_range
    elif overlap_perct(site_loc, cds_range) >= 0.96:
        site_region_str, site_region = "CDS", cds_range
    else:
        site_region = "UNKNOWN"

    # get the relative location of the site within the region
    rel_site_loc = float(site_loc[0] - site_region[0]) / \
        (site_region[1] - site_region[0] + 1)
    rel_site_loc = max(0, rel_site_loc)  # make 0 if negative
    rel_site_loc = min(1, rel_site_loc)  # make 1 if > 1

    # get alignment
    output = invoke_rna_hybrid(mrna._seq[site_loc[0]: site_loc[1]],
                               mirna._seq, helix_constraint=seed_type)
    _, alignment = parse_rna_hybrid_output(output, seed_type=True)
    # seed_enrichment = seed_enrichment_map[alignment] # TODO
    seed_enrichment = alignment

    return (site_region_str, rel_site_loc, seed_enrichment, site_len)


def compute_features_wrap(input_line, dao=None, assembly=None,
                          enrichment_map=None):
    try:
        features = compute_features(input_line, dao=dao, assembly=assembly,
                                    enrichment_map=enrichment_map)
        return features
    except:
        print "Caught exception while processing line: "
        print input_line
        print "Exception:"
        print traceback.format_exc()
        print "Ignoring."
        return None


def compute_features(input_line, dao=None, assembly=None, enrichment_map=None):
    mrna, mirna, site_loc, _, seed_type, clip = input_line.strip().split(",")
    if mrna in mrna_exclusion_list:
        print mrna, "in exclusion list. Ignoring"
        return None

    mrna = dao.get_messenger_rna_from_name(mrna, assembly=assembly)
    mirna = dao.get_micro_rna_from_name(mirna)
    mirna_seed_seq = mirna._seq[:8]
    site_loc = map(int, site_loc.split("-"))
    site_loc = tuple(site_loc)
    clip = int(clip)

    if seed_type != "None":
        seed_type = tuple(map(int, seed_type.split("-")))
    else:
        seed_type = None

    seed_site = get_seed_match_site(mrna._seq, mirna._seq, site_loc, seed_type)
    delg_site = compute_delg(mrna._seq, mirna._seq,
                             site_loc, seed_type, WINDOW_LEN_SITE)
    delg_seed = compute_delg(mrna._seq, mirna_seed_seq,
                             seed_site, seed_type, WINDOW_LEN_SEED)

    deldelg_site = compute_deldelg(mrna._seq, delg_site,
                                   site_loc, WINDOW_LEN_SITE)
    deldelg_seed = compute_deldelg(mrna._seq, delg_seed,
                                   seed_site, WINDOW_LEN_SEED)

    au_site = compute_au(mrna._seq, site_loc, WINDOW_LEN_SITE)
    au_seed = compute_au(mrna._seq, seed_site, WINDOW_LEN_SEED)

    # consv is a tuple of 3 elements: site consv, seed consv, offseed consv
    consv = compute_consv(dao, mrna, mirna,
                          site_loc, seed_type, seed_site,
                          assembly=assembly)

    misc_features = compute_misc_features(dao, mrna, mirna, site_loc,
                                          seed_type, enrichment_map,
                                          assembly)

    features = (delg_site, delg_seed, deldelg_site, deldelg_seed,
                au_site, au_seed, consv, misc_features)
    return (mrna._name, mirna._name, site_loc, seed_site, clip, features)


def array_to_str(arr, delimiter=" "):
    return delimiter.join([str(x) for x in arr])


def write_features(output_fp,
                   features_batch):

    for features in features_batch:
        mrna_name, mirna_name, site, seed_site, clip =\
            features[0], features[1], features[2], features[3], features[4]
        site = (site[0] + 1, site[1])  # 1 based, closed interval
        seed_site = (seed_site[0] + 1, seed_site[1])  # 1 based closed interval
        output_fp.write("%d,%s,%s,%s,%s," % (
            clip,
            mrna_name,  # mrna name
            mirna_name,  # mirna name
            array_to_str(site, delimiter="-"),  # site
            array_to_str(seed_site, delimiter="-")  # seed site
        ))
        features = features[5]
        output_fp.write("%s,%s,%s,%s,%s,%s," % (
            array_to_str(features[0]),  # degG site
            array_to_str(features[1]),  # delG seed
            array_to_str(features[2]),  # deldelG site
            array_to_str(features[3]),  # deldelG seed
            array_to_str(features[4]),  # au site
            array_to_str(features[5]),  # au seed
        ))

        # write conservation
        output_fp.write("%f,%f,%f," % (features[6][0],  # consv site
                                       features[6][1],  # consv seed
                                       features[6][2]))  # consv offseed

        # write misc features
        output_fp.write("%s,%f,%s,%d\n" % (
            features[7][0],  # site region
            features[7][1],  # rel site loc
            array_to_str(features[7][2], delimiter=""),  # enrichment of seed match TODO
            features[7][3]  # site len
        ))


def read_seed_enrichment_data(seed_enrichment_file):
        print "Reading seed enrichment data form %s" % seed_enrichment_file
        seed_enrichment_map = {}
        # TODO:
        '''
        with open(seed_enrichment_file, 'rU') as fp:
            for line in fp:
                seed_type, enrichment = line.strip().split(",")
                enrichment = float(enrichment)
                seed_enrichment_map[seed_type] = enrichment
        '''
        return seed_enrichment_map


def compute_features_batch(input_file, seed_enrichment_file, output_file,
                           assembly='hg18'):

    with open(output_file, 'w') as output_fp,\
            BatchFileReader(input_file, batch_size=PROCESSORS,
                            verbose=True) as reader,\
            RedisDao() as dao:

        seed_enrichment_map = read_seed_enrichment_data(seed_enrichment_file)

        batch_data = []
        feature_func = partial(compute_features_wrap, dao=dao,
                               assembly=assembly,
                               enrichment_map=seed_enrichment_map)
        feature_func.__name__ = 'compute_features'

        while True:
            lines_remaining = reader.read(batch_data)

            pipeline = (Pipeline())\
                .add_step(feature_func)

            all_features =\
                run_func_parallely(
                    pipeline.execute,
                    batch_data,
                    kwparams={'for_each': True},
                    combine_func=lambda l: [x for sublist in l
                                            for x in sublist]
                )
            write_features(output_fp,
                           all_features)

            del batch_data[:]

            if not lines_remaining:
                break


def main():
    mrna_mirna_dataset = sys.argv[1]
    seed_enrichment_file = sys.argv[2]
    output_file = sys.argv[3]
    assembly = sys.argv[4]

    if assembly == 'hg19':
        # hg19 to hg18 hack
        # hg19 is not supported. So we consider only those mRNAs in hg18
        # that are present in hg19 and have the same sequence.
        global mrna_exclusion_list
        exclusion_list = []
        print "Reading hg19 exclusion list"
        with BatchFileReader('../../../data/human/hg19_mrna_exclusion.txt')\
                as reader:
            reader.read(exclusion_list)
            exclusion_list = map(lambda x: x.strip(), exclusion_list)
            mrna_exclusion_list = set(exclusion_list)
        assembly = 'hg18'

    if assembly != 'hg18' and assembly != 'mm9':
        raise Exception('Only hg18/mm9 assemblies are supported.'
                        ' Got %s' % assembly)

    compute_features_batch(mrna_mirna_dataset, seed_enrichment_file,
                           output_file, assembly=assembly)


if __name__ == '__main__':
    sys.exit(main())
