import sys
import numpy as np
import rpy2.robjects as robjects
import rpy2
from rpy2.robjects.numpy2ri import numpy2ri
rpy2.robjects.numpy2ri.activate()
from r_interface import create_r_func
from parallelize import run_func_parallely
from set_model_params import set_model_params
from train_clip import encode_categorical_features


def usage():
    print ''' %s <test mfe file> <test mfe open file > <test seed file>
    <model file> <pca file>''' % sys.argv[0]


def combine_pred_func(pred):
    pred = map(lambda x: (np.array(x[0]), np.array(x[1])), pred)
    pred, prob = zip(*pred)
    pred = np.hstack(pred)
    prob = np.vstack(prob)
    print pred.shape, prob.shape

    return pred, prob


def get_projection_scores(data, pca_obj, num_basis=20, components=10):

    set_model_params(num_basis=num_basis,
                     num_principal_components=components)
    smooth_fd_func = create_r_func('smoothFd.R')
    data_fd = run_func_parallely(smooth_fd_func,
                                 data,
                                 combine=False)

    print "Projecting test curves along principal components"
    project_fd_func = create_r_func('projectFd.R')
    # data_scores are used for prediction
    data_scores = run_func_parallely(project_fd_func,
                                     data_fd,
                                     params=[pca_obj],
                                     split=False,
                                     combine=lambda l: [x for sublist in l
                                                        for x in sublist])
    print "Done."
    return data_scores


def predict(delg, deldelg, au,
            categorical_features,
            numeric_features,
            svm_model,
            therm_pca_pair, au_pca_pair,
            therm_basis=20,  # For delg and deldelg combined
            au_basis=10,
            therm_components=15,  # For delg and deldelg combined
            au_components=5,
            prob=False):

    therm_site = np.hstack((delg[0], deldelg[0]))
    therm_seed = np.hstack((delg[1], deldelg[1]))
    au_site, au_seed = au[0], au[1]

    pca_therm_site, pca_therm_seed = therm_pca_pair[0], therm_pca_pair[1]
    pca_au_site, pca_au_seed = au_pca_pair[0], au_pca_pair[1]

    # Smooth test data
    print "Getting scores for thermodynamic features (site)"
    therm_site_scores = get_projection_scores(therm_site,
                                              pca_therm_site,
                                              num_basis=therm_basis,
                                              components=therm_components)
    print "Getting scores for thermodynamic features (seed)"
    therm_seed_scores = get_projection_scores(therm_seed,
                                              pca_therm_seed,
                                              num_basis=therm_basis,
                                              components=therm_components)
    print "Getting scores for AU (site)"
    au_site_scores = get_projection_scores(au_site,
                                           pca_au_site,
                                           num_basis=au_basis,
                                           components=au_components)
    print "Getting scores for AU (seed)"
    au_seed_scores = get_projection_scores(au_seed,
                                           pca_au_seed,
                                           num_basis=au_basis,
                                           components=au_components)
    print "Done."

    print "Getting one-hot encoding of categorical features."
    categorical_features = encode_categorical_features(categorical_features)

    data = np.hstack((therm_site_scores, therm_seed_scores,
                      au_site_scores, au_seed_scores,
                      categorical_features, numeric_features))

    set_model_params(svm_kernel='linear', svm_cost=100,
                     svm_prob=prob, save_train_data=False)

    print "Beginning prediction."
    predict_svm_func = create_r_func('predictSvmClip.R')
    y_pred, y_prob = run_func_parallely(predict_svm_func,
                                        data,
                                        params=[svm_model],
                                        split=True,
                                        combine_func=combine_pred_func)

    print "Done."
    if prob:
        return y_pred, y_prob
    return y_pred


def main():
    if len(sys.argv) != 6:
        usage()
        return 1

    set_model_params()

    mfe = np.genfromtxt(sys.argv[1], delimiter=',')
    mfe_open = np.genfromtxt(sys.argv[2], delimiter=',')
    seed = np.genfromtxt(sys.argv[3], delimiter=',')

    # This returns the name of the variable as a list.
    svm_model = robjects.r('load("%s")' % sys.argv[4])
    svm_model = robjects.globalenv[svm_model[0]]

    # The pca object is a list having the following components:
    # 1) The PCA fd object of training data (data.pca)
    # 2) Number of principal components (num.principal.components)
    # 3) Mean fd (data.mean.fd)
    pca = robjects.r('load("%s")' % sys.argv[5])
    pca = robjects.globalenv[pca[0]]

    y_pred = predict(mfe, mfe_open, seed, svm_model, pca)
    print " ".join([str(x) for x in y_pred])

if __name__ == '__main__':
    sys.exit(main())
