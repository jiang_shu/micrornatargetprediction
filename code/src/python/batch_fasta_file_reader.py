from batch_file_reader import BatchFileReader
from Bio import SeqIO


class BatchFastaFileReader(BatchFileReader):
    def reset_iterator(self):
        self._itr = SeqIO.parse(self._fname, 'fasta')
        self._total_count = 0

    def close_iterator(self):
        self._itr = None

