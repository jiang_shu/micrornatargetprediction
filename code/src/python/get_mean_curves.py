from pyspark import SparkContext
import numpy as np
import sys
import argparse


RDD_PARTITIONS = 80


def parse_str_array(str_arr, delimiter=" ", conv_func=float):
    return np.array([conv_func(x) for x in str_arr.split(delimiter)])


def is_seed_type(x, seed_type='ALL'):
    return (seed_type == 'ALL') or\
        (seed_type == 'SEEDLESS' and np.isnan(float(x[7]))) or\
        (seed_type == 'SEED' and (not np.isnan(float(x[7]))))


def get_mean_curve(rdd, col_index):
    rdd = rdd.map(lambda x: parse_str_array(x[col_index]))\
        .filter(lambda x: not np.isnan(np.sum(x)))\
        .repartition(RDD_PARTITIONS)

    rdd_sum, rdd_sum_sq, rdd_count = rdd.map(lambda x: (x, x**2, 1))\
        .reduce(lambda x, y: [i + j for (i, j) in zip(x, y)])

    smean = rdd_sum/float(rdd_count)
    svar = rdd_sum_sq/float(rdd_count) - smean**2
    return smean, svar


def array_to_str(arr, delimiter=" "):
    return delimiter.join([str(x) for x in arr])


def main():
    argparser = argparse.ArgumentParser(
        description="Compute Mean Curve"
    )
    argparser.add_argument("pos_file", metavar='pos_file',
                           help='path to positive data')
    argparser.add_argument("neg_file", metavar='neg_file',
                           help='path to negative data')
    argparser.add_argument("output_file", metavar='output_file',
                           help='File to write the output to')
    argparser.add_argument("--type", default='ALL', dest='seed_type',
                           choices=['SEED', 'SEEDLESS', 'ALL'],
                           help='The type of data to compute mean '
                           'curves for. Defaults to ALL')

    args = argparser.parse_args()
    sc = SparkContext(appName="Mean curves")

    pos_data_rdd = sc.textFile(args.pos_file, RDD_PARTITIONS)\
        .map(lambda x: x.strip().split(",")[5:])\
        .filter(lambda x: is_seed_type(x, seed_type=args.seed_type))\
        .persist()
    neg_data_rdd = sc.textFile(args.neg_file, RDD_PARTITIONS)\
        .map(lambda x: x.strip().split(",")[5:])\
        .filter(lambda x: is_seed_type(x, seed_type=args.seed_type))\
        .persist()

    pos_delg_site = get_mean_curve(pos_data_rdd, 0)
    neg_delg_site = get_mean_curve(neg_data_rdd, 0)
    pos_delg_seed = get_mean_curve(pos_data_rdd, 1)
    neg_delg_seed = get_mean_curve(neg_data_rdd, 1)
    pos_deldelg_site = get_mean_curve(pos_data_rdd, 2)
    neg_deldelg_site = get_mean_curve(neg_data_rdd, 2)
    pos_deldelg_seed = get_mean_curve(pos_data_rdd, 3)
    neg_deldelg_seed = get_mean_curve(neg_data_rdd, 3)
    pos_au_site = get_mean_curve(pos_data_rdd, 4)
    neg_au_site = get_mean_curve(neg_data_rdd, 4)
    pos_au_seed = get_mean_curve(pos_data_rdd, 5)
    neg_au_seed = get_mean_curve(neg_data_rdd, 5)

    with open(args.output_file + ".mean", 'w') as ofp:
        ofp.write("%s\n" % array_to_str(pos_delg_site[0]))
        ofp.write("%s\n" % array_to_str(neg_delg_site[0]))
        ofp.write("%s\n" % array_to_str(pos_delg_seed[0]))
        ofp.write("%s\n" % array_to_str(neg_delg_seed[0]))
        ofp.write("%s\n" % array_to_str(pos_deldelg_site[0]))
        ofp.write("%s\n" % array_to_str(neg_deldelg_site[0]))
        ofp.write("%s\n" % array_to_str(pos_deldelg_seed[0]))
        ofp.write("%s\n" % array_to_str(neg_deldelg_seed[0]))
        ofp.write("%s\n" % array_to_str(pos_au_site[0]))
        ofp.write("%s\n" % array_to_str(neg_au_site[0]))
        ofp.write("%s\n" % array_to_str(pos_au_seed[0]))
        ofp.write("%s\n" % array_to_str(neg_au_seed[0]))

    with open(args.output_file + ".var", 'w') as ofp:
        ofp.write("%s\n" % array_to_str(pos_delg_site[1]))
        ofp.write("%s\n" % array_to_str(neg_delg_site[1]))
        ofp.write("%s\n" % array_to_str(pos_delg_seed[1]))
        ofp.write("%s\n" % array_to_str(neg_delg_seed[1]))
        ofp.write("%s\n" % array_to_str(pos_deldelg_site[1]))
        ofp.write("%s\n" % array_to_str(neg_deldelg_site[1]))
        ofp.write("%s\n" % array_to_str(pos_deldelg_seed[1]))
        ofp.write("%s\n" % array_to_str(neg_deldelg_seed[1]))
        ofp.write("%s\n" % array_to_str(pos_au_site[1]))
        ofp.write("%s\n" % array_to_str(neg_au_site[1]))
        ofp.write("%s\n" % array_to_str(pos_au_seed[1]))
        ofp.write("%s\n" % array_to_str(neg_au_seed[1]))

if __name__ == '__main__':
    sys.exit(main())
