from kernel_svm_sgd import KernelSvmBsgd, RBFKernel
import time
import numpy as np
from pyspark.mllib.regression import LabeledPoint


RDD_PARTITIONS = 80


class KernelSvmBsgdSpark(KernelSvmBsgd):
    def train(self, data):
        t1 = time.time()
        for x in data:
            # Convert label to -1.0 and 1.0
            y = -1.0 if int(x.label) == 0 else x.label
            self._add_point(y, x.features)
        t2 = time.time()
        print "Done (%f sec)." % (t2 - t1)


class KernelSVMWithBSGD:
    @staticmethod
    def train(sc, rdd, budget, reg_param, iterations=1, local_iters=1,
              **kernel_params):

        print "Training partitions."
        svs = None
        num_partitions = rdd.getNumPartitions()

        for i in range(iterations):
            print "Starting iteration:", i
            models = [None]*num_partitions

            for j in range(local_iters):
                print "Starting local iteration:", j

                models = rdd.mapPartitionsWithIndex(
                    lambda i, itr: KernelSVMWithBSGD._train_partition(
                        i, itr, budget, reg_param,
                        support_vectors=svs,
                        model=models[i],
                        **kernel_params
                    )
                ).collect()

                models = zip(*sorted(models, key=lambda (i, m): i))[1]
                print "Done (local iteration %d)." % j

            print "Reducing support vectors."
            support_vectors = map(lambda m: m.get_support_vectors(), models)
            support_vectors = np.vstack(support_vectors)
            model = KernelSVMWithBSGD._reduce_svs(sc, support_vectors,
                                                  budget, reg_param,
                                                  **kernel_params)
            svs = model.get_support_vectors()

            print "Done (iteration %d)." % i

        return model

    @staticmethod
    def _reduce_svs(sc, svs, budget, reg_param, **kernel_params):
        """
            Reduces support vectors in a tree fashion.
        """
        partitions = RDD_PARTITIONS
        num_svs = svs.shape[0]
        print "# Support vectors:", num_svs

        while partitions > 2:
            partitions = partitions / 2
            print "Current partitions:", partitions
            rdd = sc.parallelize(svs, partitions)\
                .map(lambda x: LabeledPoint(x[0], x[1:]))

            svs = rdd.mapPartitionsWithIndex(
                lambda index, itr: KernelSVMWithBSGD._train_partition(
                    index, itr, num_svs, reg_param, **kernel_params)
            ).map(lambda m: m[1].get_support_vectors()).collect()

            svs = np.vstack(svs)
            num_svs = svs.shape[0]
            print "# Support vectors:", num_svs

        model = KernelSvmBsgd(num_svs, reg_param, budgeting_policy='REMOVE',
                              kernel=RBFKernel(**kernel_params))

        support_vectors = [(x[0], x[1:]) for x in svs]
        model.train(support_vectors)
        print "Done"
        support_vectors = model.get_support_vectors()
        print "# Support vectors (combined) :", support_vectors.shape[0]
        return model

    @staticmethod
    def _train_partition(index, itr, budget, reg_param, support_vectors=None,
                         model=None, **kernel_params):

        if model is None:
            print "Creating model for partition:", index
            model = KernelSvmBsgdSpark(budget, reg_param,
                                       budgeting_policy='REMOVE',
                                       kernel=RBFKernel(**kernel_params))

        if support_vectors is not None:
            # First train on the support vectors
            support_vectors = map(lambda x: LabeledPoint(x[0], x[1:]),
                                  support_vectors)
            model.train(support_vectors)

        model.train(itr)

        return [(index, model)]
