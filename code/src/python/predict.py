import sys
import numpy as np
import rpy2.robjects as robjects
import rpy2
from rpy2.robjects.numpy2ri import numpy2ri
rpy2.robjects.numpy2ri.activate()
from r_interface import create_r_func
from parallelize import run_func_parallely
from set_model_params import set_model_params


def usage():
    print ''' %s <test mfe file> <test mfe open file > <test seed file>
    <model file> <pca file>''' % sys.argv[0]


def predict(mfe, mfe_open, seed, svm_model, pca_obj):
    # Smooth test data
    print "Smoothing test data"
    smooth_fd_func = create_r_func('smoothFd.R')

    seed_max = np.max(seed, axis=1)
    seed_max = np.reshape(seed_max, (np.size(seed_max), 1))
    seed = seed / np.repeat(seed_max, seed.shape[1], axis=1)
    del_del_g = mfe - mfe_open
    weighted_del_del_g = del_del_g
    weighted_del_g = mfe
    data_test = np.hstack((weighted_del_g, weighted_del_del_g))
    data_test_fd = run_func_parallely(smooth_fd_func,
                                      data_test,
                                      combine=False)
    print "Done."

    print "Projecting test curves along principal components"
    project_fd_func = create_r_func('projectFd.R')
    # data_scores are used for prediction
    data_scores = run_func_parallely(project_fd_func,
                                     data_test_fd,
                                     params=[pca_obj],
                                     split=False,
                                     combine=False)
    print "Done."

    print "Beginning prediction."
    predict_svm_func = create_r_func('predictSvm.R')
    y_pred = run_func_parallely(predict_svm_func,
                                data_scores,
                                params=[svm_model],
                                split=False,
                                combine=True,
                                combine_func=np.hstack
                                )
    print "Done."

    return y_pred


def main():
    if len(sys.argv) != 6:
        usage()
        return 1

    set_model_params()

    mfe = np.genfromtxt(sys.argv[1], delimiter=',')
    mfe_open = np.genfromtxt(sys.argv[2], delimiter=',')
    seed = np.genfromtxt(sys.argv[3], delimiter=',')

    # This returns the name of the variable as a list.
    svm_model = robjects.r('load("%s")' % sys.argv[4])
    svm_model = robjects.globalenv[svm_model[0]]

    # The pca object is a list having the following components:
    # 1) The PCA fd object of training data (data.pca)
    # 2) Number of principal components (num.principal.components)
    # 3) Mean fd (data.mean.fd)
    pca = robjects.r('load("%s")' % sys.argv[5])
    pca = robjects.globalenv[pca[0]]

    y_pred = predict(mfe, mfe_open, seed, svm_model, pca)
    print " ".join([str(x) for x in y_pred])

if __name__ == '__main__':
    sys.exit(main())
