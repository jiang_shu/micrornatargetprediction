'''
    Uses a sliding window to compute the optimal delta G (dg) score
    between a mrna window and mirna. Also outputs the number of seed matches,
    mismatches and gaps. Invokes the code RNAhybrid to compute the delta G
    score.
'''
import sys
import subprocess
import re
import math
from multiprocessing import Process, Queue
import multiprocessing
import time
from redisdao import RedisDao
import numpy as np

MRNA_MAX_LENGTH = sys.maxsize  # Get features for mRNA having length at most this.
PROCESSORS = multiprocessing.cpu_count()  # Number of processors
MAX_LENGTH = sys.maxsize  # Maximum length of the curve to generate
MIN_LENGTH = 100  # Minimum length of curve to generate
WINDOW_LENGTH = None  # If none then use WINDOW_LENGTH_MULTIPLIER* len(microRNA)
WINDOW_LENGTH_MULTIPLIER = 1

# RNAhybrid executabe
RNAHYBRID = "../../third-party/bin/RNAhybrid"
# RNAfold executable
RNAFOLD = "RNAfold"


def invoke_rna_hybrid(mrna_seq, mirna_seq, helix_constraint=False):
    executable = RNAHYBRID
    # TODO: Change the dataset type (3utr_human). But do we need to change
    # if we are not interested in the p-value?
    args = "-b 1 -s 3utr_human"
    if helix_constraint:
        args += " -f 2,7"
    cmd = "%s %s %s %s" % (executable, args, mrna_seq, mirna_seq)
    output = subprocess.check_output(cmd, shell=True)
    return output


def invoke_rna_fold(mrna_seq):
    executable = RNAFOLD
    cmd = "echo %s | %s --noPS" % (mrna_seq, executable)
    output = subprocess.check_output(cmd, shell=True)
    return output


def parse_rna_fold_output(output):
    mfe_open = np.nan
    output = output.split('\n')
    m = re.search(r'\(\s*([-0-9]*[.]{0,1}[0-9]+)\)$', output[1])
    if m:
        mfe_open = float(m.group(1))
    else:
        print output
        raise Exception("Couldn't parse output")
    return mfe_open


def parse_rna_hybrid_output(output):
    # Parse output to get the following features:
    #   Mean free energy (mfe) of duplex.
    #   Number of seed matches.
    #   Number of non-seed matches.
    m = re.search(r'mfe: (-{0,1}[0-9]+[.]{0,1}\d*) .*', output)
    (mfe, seed_matches, non_seed_matches) = (np.nan, np.nan, np.nan)
    if m:
        mfe = m.group(1)
    m = re.search(r'miRNA  3\' ([ACTGU ]+) 5\'', output)
    if m:
        mirna_seq = m.group(1)
        # Get the number of matches from the miRNA sequence
        # spaces represent matches. So find spaces
        match_ind = [m1.start() for m1 in re.finditer(' ', mirna_seq)]
        seed_matches = sum(map(lambda x: 1 if x >= 1 and x <= 6 else 0,
                               match_ind))
        non_seed_matches = len(match_ind) - seed_matches
    return (mfe, seed_matches, non_seed_matches)


def get_features(pid, mrna, mirna, queue):
    # Run a sliding window of length twice of mirna and generate features
    len_mirna = len(mirna)
    len_mrna = len(mrna)
    win_length = WINDOW_LENGTH_MULTIPLIER * len_mirna
    if WINDOW_LENGTH:
        win_length = WINDOW_LENGTH
    mfe = []
    mfe_helix = []
    mfe_open = []
    seed_matches = []
    non_seed_matches = []

    if len_mrna < win_length:
        return

    # Sliding window
    for i in range(0, len_mrna - win_length + 1):
        # Get the mfe for the duplex.
        mrna_part = mrna[i:i+win_length]
        output = invoke_rna_hybrid(mrna_part, mirna)
        features = parse_rna_hybrid_output(output)
        mfe.append(float(features[0]))
        seed_matches.append(features[1])
        non_seed_matches.append(features[2])

        output = invoke_rna_hybrid(mrna_part, mirna, helix_constraint=True)
        features = parse_rna_hybrid_output(output)
        mfe_helix.append(float(features[0]))

        # Get the mfe open (DeltaG_open) for the mrna
        output = invoke_rna_fold(mrna_part)
        output = parse_rna_fold_output(output)
        mfe_open.append(output)
    queue.put((pid, mfe, mfe_helix, mfe_open, seed_matches, non_seed_matches))


def get_features_parallel(mrna, mirna):
    len_mirna = len(mirna)
    len_mrna = len(mrna)
    # The window size is twice the lenth of micrna
    win_length = WINDOW_LENGTH_MULTIPLIER * len_mirna
    if WINDOW_LENGTH:
        win_length = WINDOW_LENGTH
    if len_mrna - win_length < (MIN_LENGTH - 1):
        return None

    print "mRNA length: %d, microRNA length: %d" % (len_mrna, len_mirna)
    max_len = min(MAX_LENGTH, len_mrna - win_length + 1)
    batch_size = math.trunc(max_len / PROCESSORS)
    queue = Queue()
    procs = []
    for i in range(PROCESSORS - 1):
        start_index = i * batch_size
        end_index = ((i + 1) * batch_size) + win_length - 1
        p = Process(target=get_features,
                    args=(i,
                          mrna[start_index:end_index],
                          mirna,
                          queue)
                    )
        procs.append(p)

    start_index = (PROCESSORS - 1) * batch_size
    end_index = max_len + win_length - 1
    p = Process(target=get_features,
                args=(PROCESSORS - 1,
                      mrna[start_index:end_index],
                      mirna,
                      queue)
                )
    procs.append(p)
    for p in procs:
        p.start()

    mfe = [None]*PROCESSORS
    mfe_helix = [None]*PROCESSORS
    mfe_open = [None]*PROCESSORS
    seed_matches = [None]*PROCESSORS
    non_seed_matches = [None]*PROCESSORS

    for i in range(PROCESSORS):
        ret = queue.get()
        pid = ret[0]
        mfe[pid] = ret[1]
        mfe_helix[pid] = ret[2]
        mfe_open[pid] = ret[3]
        seed_matches[pid] = ret[4]
        non_seed_matches[pid] = ret[5]

    mfe = np.concatenate(mfe)
    mfe_helix = np.concatenate(mfe_helix)
    mfe_open = np.concatenate(mfe_open)
    seed_matches = np.concatenate(seed_matches)
    non_seed_matches = np.concatenate(non_seed_matches)
    return (mfe.ravel(),
            mfe_helix.ravel(),
            mfe_open.ravel(),
            seed_matches.ravel(),
            non_seed_matches.ravel())


def get_target_list(dao, target_type, max_len=0):
    print "Getting target list of type: ", target_type
    target_list = []
    if target_type == 'positive_mirwalk':
        target_list = \
            dao.get_validated_mrna_mirna_pairs(dataset='mirwalk')

    elif target_type == 'positive_psilac':
        target_list = \
            dao.get_validated_mrna_mirna_pairs(dataset='psilac')

    elif target_type == 'negative_mirwalk':
        target_list = \
            dao.get_negative_mrna_mirna_pairs(dataset='mirwalk')

    elif target_type == 'negative_psilac':
        target_list = \
            dao.get_negative_mrna_mirna_pairs(dataset='psilac')

    elif target_type == 'positive_targetminer_train':
        target_list = \
            dao.get_validated_mrna_mirna_pairs(
                dataset='targetminer:train')

    elif target_type == 'positive_targetminer_test':
        target_list = \
            dao.get_validated_mrna_mirna_pairs(
                dataset='targetminer:test')

    elif target_type == 'negative_targetminer_train':
        target_list = \
            dao.get_validated_mrna_mirna_pairs(
                dataset='targetminer:train:neg')

    elif target_type == 'negative_targetminer_test':
        target_list = \
            dao.get_validated_mrna_mirna_pairs(
                dataset='targetminer:test:neg')

    else:
        raise Exception('Invalid target type %s. Shoudld be one of'
                        'positive_mirwalk, positive_psilac, '
                        'negative_mirwalk, negative_psilac.')
    if max_len > 0:
        print "Returning %d items out of %d total items." % (max_len,
                                                             len(target_list))
        target_list = target_list[:min(max_len, len(target_list))]
    return target_list


def write_features(target_type,
                   mfe_file,
                   mfe_helix_file,
                   mfe_open_file,
                   seed_match_file,
                   non_seed_match_file,
                   max_len=0):
    with open(mfe_file, 'w') as mfe_fp,\
        open(mfe_helix_file, 'w') as mfe_helix_fp,\
        open(mfe_open_file, 'w') as mfe_open_fp,\
        open(seed_match_file, 'w') as seed_fp,\
        open(non_seed_match_file, 'w') as non_seed_fp,\
        RedisDao() as redis_dao:

        count = 0
        ignored = 0
        print "Getting features for target: ", target_type
        print "Using %d processors" % PROCESSORS
        t1 = time.time()
        for mrna, mirna in get_target_list(redis_dao, target_type, max_len):
            print "Getting features for: %s - %s" % (mrna, mirna)
            mrna = redis_dao.get_messenger_rna_from_name(mrna)
            mirna = redis_dao.get_micro_rna_from_name(mirna)
            if not mrna or not mirna:
                print "Could not get sequence for %s-%s" % (mrna, mirna)
                ignored += 1
                print "Ignored: ", ignored
                continue

            if len(mrna._seq) > MRNA_MAX_LENGTH:
                print "Length of mrna (%d) exceeds threshold %d. Skipping. " % (
                    len(mrna._seq),
                    MRNA_MAX_LENGTH
                )
                continue

            features = get_features_parallel(str(mrna._seq), str(mirna._seq))

            if not features:
                ignored += 1
                print "Could not get feature for %s-%s" % (mrna, mirna)
                print "Ignored: ", ignored
                continue

            print np.shape(features[1])
            mfe_fp.write(
                "%s,%s,%s\n" % (mrna,
                                mirna,
                                " ".join(map(lambda x: str(x), features[0]))
                                ))
            mfe_helix_fp.write(
                "%s,%s,%s\n" % (mrna,
                                mirna,
                                " ".join(map(lambda x: str(x), features[1]))
                                ))
            mfe_open_fp.write(
                "%s,%s,%s\n" % (mrna,
                                mirna,
                                " ".join(map(lambda x: str(x), features[2]))
                                ))
            seed_fp.write(
                "%s,%s,%s\n" % (mrna,
                                mirna,
                                " ".join(map(lambda x: str(x), features[3]))
                                ))
            non_seed_fp.write(
                "%s,%s,%s\n" % (mrna,
                                mirna,
                                " ".join(map(lambda x: str(x), features[4]))
                                ))
            count += 1

        t2 = time.time()
        print "Time elapsed: ", (t2 - t1)


def main():
    write_features('positive_psilac',
                   '../../../data/pos_psilac_mfe.txt',
                   '../../../data/pos_psilac_mfe_helix.txt',
                   '../../../data/pos_psilac_mfe_open.txt',
                   '../../../data/pos_psilac_seed.txt',
                   '../../../data/pos_psilac_nonseed.txt'
                   )
    write_features('negative_psilac',
                   '../../../data/neg_psilac_mfe.txt',
                   '../../../data/neg_psilac_mfe_helix.txt',
                   '../../../data/neg_psilac_mfe_open.txt',
                   '../../../data/neg_psilac_seed.txt',
                   '../../../data/neg_psilac_nonseed.txt'
                   )
    '''
    write_features('positive_targetminer_train',
                   '../../../data/pos_targetminer_train_mfe.txt',
                   '../../../data/pos_targetminer_train_mfe_helix.txt',
                   '../../../data/pos_targetminer_train_mfe_open.txt',
                   '../../../data/pos_targetminer_train_seed.txt',
                   '../../../data/pos_targetminer_train_nonseed.txt'
                   )
    write_features('positive_targetminer_test',
                   '../../../data/pos_targetminer_test_mfe.txt',
                   '../../../data/pos_targetminer_test_mfe_helix.txt',
                   '../../../data/pos_targetminer_test_mfe_open.txt',
                   '../../../data/pos_targetminer_test_seed.txt',
                   '../../../data/pos_targetminer_test_nonseed.txt'
                   )
    write_features('negative_targetminer_train',
                   '../../../data/neg_targetminer_train_mfe.txt',
                   '../../../data/neg_targetminer_train_mfe_helix.txt',
                   '../../../data/neg_targetminer_train_mfe_open.txt',
                   '../../../data/neg_targetminer_train_seed.txt',
                   '../../../data/neg_targetminer_train_nonseed.txt'
                   )
    write_features('negative_targetminer_test',
                   '../../../data/neg_targetminer_test_mfe.txt',
                   '../../../data/neg_targetminer_test_mfe_helix.txt',
                   '../../../data/neg_targetminer_test_mfe_open.txt',
                   '../../../data/neg_targetminer_test_seed.txt',
                   '../../../data/neg_targetminer_test_nonseed.txt'
                   )
    '''

if __name__ == '__main__':
    sys.exit(main())
