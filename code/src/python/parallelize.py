import sys
from multiprocessing import Process, Queue
import multiprocessing
import numpy as np


PROCESSORS = multiprocessing.cpu_count()  # Number of processors


def process(pid, queue, func, data, params, kwparams):
    ret = func(data, *params, **kwparams)
    queue.put((pid, ret))
    sys.exit(0)


def split_data(data):
    processors = PROCESSORS
    if type(data) == np.ndarray:
        data_len = np.size(data, 0)
        if data_len < PROCESSORS:
            processors = data_len

        end_ind = data_len - data_len % processors
        data_splits = np.split(data[:end_ind], processors, axis=0)
        # If the size of data is not exactly divisble by the
        # number of processors then add the remaining data to the last split.
        if end_ind != data_len:
            data_splits[-1] = np.vstack((data_splits[-1], data[end_ind:]))
        return data_splits
    elif type(data) == list:
        data_len = len(data)
        if data_len < PROCESSORS:
            processors = data_len
        data_splits = [None]*processors
        batch_size = data_len/processors
        for i in range(processors - 1):
            start_ind = i*batch_size
            end_ind = (i + 1)*batch_size
            data_splits[i] = data[start_ind:end_ind]
        data_splits[-1] = data[(processors - 1)*batch_size:]
        return data_splits


def run_func_parallely(func,
                       data,
                       params=[],
                       kwparams={},
                       split=True,
                       combine=True,
                       combine_func=np.vstack):
    if split:
        # Split the data.
        data_splits = split_data(data)
    else:
        # The data is already split i.e. a list
        if len(data) > PROCESSORS:
            raise Exception('Data has incorrect number of partitions:'
                            '%d, expected partitions: %d.'
                            % (len(data), PROCESSORS))
        data_splits = data

    processors = len(data_splits)
    queue = Queue()
    procs = []
    for i in range(processors):
        p = Process(target=process,
                    args=(i,
                          queue,
                          func,
                          data_splits[i],
                          params,
                          kwparams)
                    )
        procs.append(p)

    for p in procs:
        p.start()

    processed_data = [None] * processors
    for i in range(processors):
        ret = queue.get()
        pid = ret[0]
        processed_data[pid] = ret[1]

    if combine:
        # Recombine the splits to create one array
        processed_data = \
            [x for x in processed_data if x is not None and len(x) is not 0]
        processed_data = combine_func(processed_data)

    return processed_data
