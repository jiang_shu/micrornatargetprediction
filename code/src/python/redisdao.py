import redis
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from rna import MicroRNA, MessengerRNA

HOST = 'localhost'
PORT = 6379


class RedisDao(object):
    ''' Data access object for RNAs '''
    def __init__(self):
        self._handle = redis.StrictRedis(host=HOST, port=PORT, db=0)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self._cleanup()

    def _cleanup(self):
        return
        # Save the database
        # self._handle.save()

    def get_validated_mrna_mirna_pairs(self, dataset='mirwalk'):
        # Returns experimentally vaildated messengerRNA-microRNA pairs.
        # This function only returns the names of the mRNA and miRNAs.
        # The dataset specifies which dataset to use (mirWalk/pSILAC)
        pairs = self._handle.smembers(dataset+':target:list')
        target_list = map(lambda x: x.split(','), pairs)
        target_list = sorted(target_list, key=lambda x: (x[0], x[1]))
        return target_list

    def get_negative_mrna_mirna_pairs(self, dataset='mirwalk'):
        # Returns negative messengerRNA-microRNA pairs.
        # This function only returns the names of the mRNA and miRNAs.
        # The dataset specifies which dataset to use (mirWalk/pSILAC)
        pairs = self._handle.smembers(dataset+':neg:target:list')
        target_list = map(lambda x: x.split(','), pairs)
        target_list = sorted(target_list, key=lambda x: (x[0], x[1]))
        return target_list

    def get_messenger_rna_from_name(self, name, assembly="hg19"):
        seq = self._handle.get('seq:%s:%s' % (assembly, name))
        if not seq:
            return None
        seq = Seq(seq, IUPAC.unambiguous_dna)
        seq = seq.transcribe()
        return MessengerRNA(name, seq)

    def get_micro_rna_from_name(self, name):
        seq = self._handle.get('seq:'+name)
        if not seq:
            return None
        seq = Seq(seq, IUPAC.unambiguous_rna)
        return MicroRNA(name, seq)
