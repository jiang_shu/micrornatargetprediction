import kernel_svm_sgd
from kernel_svm_sgd import KernelSvmBsgd
from kernel_svm_sgd import CachedRBFKernel
from parallelize import run_func_parallely
from pipeline import Pipeline
import numpy as np
import copy
import sys
import time

# from tempfile import NamedTemporaryFile


def get_misclassification_error(model, labelled_data):
    labels, data = zip(*map(lambda x: (x[0], x[1]), labelled_data))
    data = list(data)
    preds = predict(model, data)
    misclassified = 0
    total = 0
    for x1, x2 in zip(preds, labels):
        if int(x1) != int(x2):
            misclassified += 1
        total += 1
    return misclassified/float(total)


def train_batch(data, support_vectors=None, budget=None, reg_param=None,
                sigma=kernel_svm_sgd.DEF_RBF_KERNEL_SIGMA,
                budgeting_policy='REMOVE',
                batch_iterations=1):

    kernel = CachedRBFKernel(sigma)
    model = KernelSvmBsgd(budget, reg_param,
                          kernel=kernel,
                          budgeting_policy=budgeting_policy)

    for i in range(batch_iterations):

        # If there are support vectors then first train on them.
        if support_vectors is not None:
            data = support_vectors + data

        # np.random.shuffle(data)
        model.train(data)

    svs = model.get_support_vectors()
    return svs


def train(data, budget, reg_param, sigma,
          budgeting_policy='REMOVE', iterations=1,
          batch_iterations=1):

    data = copy.deepcopy(data)
    t1 = time.time()
    model = None
    support_vectors = None
    for i in range(iterations):
        pipeline = Pipeline().add_step(train_batch,
                                       support_vectors=support_vectors,
                                       budget=budget,
                                       reg_param=reg_param,
                                       sigma=sigma,
                                       budgeting_policy=budgeting_policy,
                                       batch_iterations=batch_iterations
                                       )

        # Support vectors
        svs = run_func_parallely(
            pipeline.execute, data,
        )

        support_vectors = [(x[0], x[1:]) for x in svs]
        num_svs = len(support_vectors)
        print "Support vectors:", num_svs

        model = KernelSvmBsgd(num_svs, reg_param, kernel=CachedRBFKernel(sigma),
                              budgeting_policy=budgeting_policy)

        model.train(support_vectors)
        svs = model.get_support_vectors()

        # Save the model to a temp file
        # temp_file = NamedTemporaryFile(delete=False)
        # print "Writing model to:", temp_file.name
        # coeffs = np.atleast_2d(model.coeffs[:model._sv_count])
        # out = np.hstack((svs, coeffs))
        # np.savetxt(temp_file, out, fmt="%.3f", delimiter=",")
        # pickle.dump(model, temp_file)

        support_vectors = [(x[0], x[1:]) for x in svs]
        print "# Support vectors (combined):", len(support_vectors)

        error = get_misclassification_error(model, data)
        print "Misclassification rate: %.6f" % error

    t2 = time.time()
    print "Training time: %f sec." % (t2 - t1)
    return model


def predict(model, data):
    pipeline = Pipeline().add_step(lambda x: model.predict(x))
    preds = run_func_parallely(
        pipeline.execute, data, kwparams=dict(for_each=True),
        combine_func=lambda l: [x for s in l for x in s]
    )
    return preds


def label_func(x):
    return 1.0 if ((x[0] - 2)**2 + (x[1] - 2)**2 <= 4 or
                   (x[0] + 2)**2 + (x[1] + 2)**2 <= 6) else 0.0


def main():
    data = 4.0 * np.random.randn(10000, 2)
    labelled_data = [(label_func(x), x) for x in data]
    model = train(labelled_data, 50, 0.00001, 0.1,
                  budgeting_policy='REMOVE',
                  iterations=1,
                  batch_iterations=5)

    data = 4.0 * np.random.randn(10000, 2)
    labelled_data = [(label_func(x), x) for x in data]
    error = get_misclassification_error(model, labelled_data)
    print "Test error:", error

    return 0

if __name__ == '__main__':
    sys.exit(main())
