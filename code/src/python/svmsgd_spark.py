from pyspark import SparkContext
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.classification import SVMWithSGD
import sys
import os.path
import numpy as np
from tempfile import NamedTemporaryFile
import subprocess
import functools
import argparse


DEFAULT_ITERATIONS = 100
HDFS_OUTPUT_PATH = '/microrna/output/'
HADOOP_PATH = '~/hadoop-2.5.1'


def parse_point(line):
    values = [float(x) for x in line.strip().split(",")]
    return LabeledPoint(values[0], values[1:])


def get_metric(line):
    ''' Compute accuracy and recall '''
    y_actual, y_pred = line[0], line[1]
    tp = tn = fp = fn = 0.0
    y_actual = int(y_actual)
    y_pred = int(y_pred)
    if y_actual == 1:
        if y_pred == 1:
            tp = 1
        else:
            fn = 1
    else:
        if y_pred == 1:
            fp = 1
        else:
            tn = 1

    return np.array([tp, fp, tn, fn])


def write_to_hdfs(arr, fname):
    # Writes the array to HDFS
    o_fpath = os.path.join(HDFS_OUTPUT_PATH, fname)
    with NamedTemporaryFile(mode='w') as temp_fp:
        np.savetxt(temp_fp, arr)
        temp_fp.flush()

        # Write temp file to HDFS
        cmd = 'cd %s; cat %s | ./bin/hdfs dfs -put -f - %s' % (HADOOP_PATH,
                                                               temp_fp.name,
                                                               o_fpath)
        subprocess.check_call(cmd, shell=True)


def create_hdfs_path(hdfs_path):
    cmd = "cd %s; ./bin/hdfs dfs -mkdir -p %s" % (HADOOP_PATH,
                                                  hdfs_path)
    subprocess.check_call(cmd, shell=True)


def train_svm(train_data, train_file,
              iterations=DEFAULT_ITERATIONS, regParam=0.001):
    model = SVMWithSGD.train(train_data,
                             iterations=iterations,
                             regParam=regParam)

    # Compute training error.
    labelsAndPreds = train_data.map(lambda x: (x.label,
                                               model.predict(x.features)))
    trainErr = labelsAndPreds.filter(lambda (v, p): v != p).count()
    trainErr /= float(train_data.count())

    # Print training metrics
    print "Training error:", trainErr
    weights_file = os.path.basename(train_file) + ".weights"
    print "Writing weights to:", weights_file
    write_to_hdfs(model.weights, weights_file)
    return model


def get_logistic_prob(features, weights=None, intercept=0.0):
    prob = 1.0 / (1 + np.exp(-(features.dot(weights) + intercept)))
    return prob


def predict(model, test_data, test_file, compute_prob=False):
    labelsAndPreds = test_data.map(lambda x: (x.label,
                                              model.predict(x.features)))
    testErr = labelsAndPreds.filter(lambda (v, p): v != p).count()
    testErr /= float(test_data.count())

    tp, fp, tn, fn = labelsAndPreds.map(get_metric).reduce(lambda x, y: x + y)
    accuracy = (tp/(tp + fp))
    recall = (tp/(tp + fn))
    positive = tp + fn
    negative = tn + fp
    fpr = fp/negative
    tpr = tp/positive

    # Write predictions and labels for test data to HDFS.
    ofile_name = os.path.basename(test_file) + ".pred"
    ofile_path = os.path.join(HDFS_OUTPUT_PATH, ofile_name)
    print "Writing labels and predictions to:", ofile_path
    labelsAndPreds.saveAsTextFile(ofile_path)

    # Print test metrics
    print "Test error:", testErr
    print "Accuracy: %.3f" % accuracy
    print "Recall: %.3f" % recall
    print "TPR: %.3f" % tpr
    print "FPR: %.3f" % fpr

    if not compute_prob:
        return

    # Compute probabilities using the weights and logistic function
    compute_prob = functools.partial(get_logistic_prob,
                                     weights=model.weights,
                                     intercept=model.intercept)
    labels_and_prob = test_data.map(lambda x: (x.label,
                                               compute_prob(x.features)))

    # Write probabilities to HDFS file
    ofile_name = os.path.basename(test_file) + ".prob"
    ofile_path = os.path.join(HDFS_OUTPUT_PATH, ofile_name)
    print "Writing labels and probability scores to:", ofile_path
    labels_and_prob\
        .map(lambda x: "%d,%f" % (x[0], x[1]))\
        .saveAsTextFile(ofile_path)


def main():
    argparser = argparse.ArgumentParser(
        description="Learn a SVM using the provided data. Evaluate the"
        "model on the test data."
    )
    argparser.add_argument("train_file", help='HDFS path to training file')
    argparser.add_argument("test_file", help='HDFS path to test file')
    argparser.add_argument("--iter", help='#iterations for SGD', default=-1,
                           metavar='N', type=int, dest='iterations')
    argparser.add_argument("--prob", help='Additionally compute probabilities',
                           action="store_true")
    argparser.add_argument("--dataset", default='',
                           help='Write outputs to path with the given suffix')
    args = argparser.parse_args()

    train_file = args.train_file
    test_file = args.test_file
    iterations = DEFAULT_ITERATIONS
    if args.iterations < 0:
        iterations = args.iterations
    compute_prob = args.prob
    print "Iterations: %d" % iterations
    print "Compute probabilities: %s" % compute_prob

    global HDFS_OUTPUT_PATH
    HDFS_OUTPUT_PATH = os.path.join(HDFS_OUTPUT_PATH, args.dataset)
    create_hdfs_path(HDFS_OUTPUT_PATH)

    sc = SparkContext(appName="svm_sgd")
    train_data = sc.textFile(train_file).map(parse_point)
    test_data = sc.textFile(test_file).map(parse_point)

    model = train_svm(train_data, train_file,
                      iterations=iterations, regParam=0.001)

    predict(model, test_data, test_file, compute_prob=compute_prob)


if __name__ == '__main__':
    sys.exit(main())
