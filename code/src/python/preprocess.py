import re
import sys


def remove_unwanted_chars(line, delimiter=None):
    '''
        Removes spaces.
        Replaces "][" by delimiter.
        splits the line by delimiter
    '''
    if not line:
        return line
    line = line.strip()
    if "[" in line:
        line = re.sub('\s+', lambda m: "", line)
        line = re.sub('\]\[', lambda m: delimiter, line)
        line = re.sub('\[|\]', lambda m: "", line)
    line = line.split(delimiter)
    return line


def get_slice(line,
              from_index=None,
              to_index=None,
              delimiter=None,
              as_numeric=False):
    '''
        Get a slice from the specfied line.
        If @delimiter is specified then the function first gets
        the slice and then further split the resulting string by delimiter
        If @as_numeric is True then return the slice as a numeric vector.
    '''
    if not line:
        return line
    line = line[from_index:to_index]
    if delimiter and len(line) == 1:
        line = line[0].split(delimiter)
    if as_numeric:
        line = [float(x) for x in line]
    return line


def fit_length(data, desired_len=0, beginning=False):
    '''
        Fixes @data to length @length. If the length of data
        is less than @length then repeats the end (@beginning = False)
        of the data otherwise truncates the data. If @beginning = True
        then repeats from the beginning.
    '''
    if not data:
        return data
    data_len = len(data)
    datal = data
    if data_len < desired_len:
        # repeat
        times = int(desired_len) / data_len  # repeat this many times
        add_index = desired_len % data_len  # then add the reaming.
        if not beginning:
            add_index = - (add_index)
            add_index = data_len if add_index == 0 else add_index
            datal = datal * times + datal[add_index:]
        else:
            datal = datal[0:add_index] + datal * times
    elif data_len > desired_len:
        datal = datal[0:desired_len]

    return datal


def filter_by_length(data, atleast=0, atmost=sys.maxsize):
    '''
        Keep data that have length >= @atleast and <= @atmost
    '''
    if len(data) >= atleast and len(data) <= atmost:
        return data
    return None
