import numpy as np
import subprocess
import pickle
import os.path
from pyspark import StorageLevel
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.classification import SVMWithSGD
from clustered_svm_spark import ClusterSvmModel
import shortuuid
import os
from tempfile import NamedTemporaryFile
import functools
from feature_transformers import NanTransformer, NanMeanTransformer,\
    StandardScaler, FeatureTransformer, FeatureTransformerSeed
from record import Record


HDFS_URL = 'hdfs://pdsl4.cs.purdue.edu:8000'
HDFS_DATA_PATH = '/microrna/data/'
CODE_PATH = '~/Workspace/MicroRnaTargetPrediction/code/src/python'
SITE_REGION_TYPES = ['3UTR', '5UTR', 'CDS']
RDD_PARTITIONS = 80
DEFAULT_ITERATIONS = 200
DEFAULT_REG_PARAM = 0.001
DEFAULT_C = 1.0
DEFAULT_KERNEL = 'LINEAR'
HDFS_OUTPUT_PATH = '/home/aghoshal/microrna/output/'
HADOOP_PATH = '/home/aghoshal/hadoop-2.6.0'
KERNEL_TYPES = set(['LINEAR', 'RBF'])


def create_hdfs_path(hdfs_path):
    cmd = "ssh aghoshal@pdsl4.cs.purdue.edu"\
        " \"cd ~/hadoop-2.5.1; bin/hdfs dfs -mkdir -p %s\"" % hdfs_path

    subprocess.check_call(cmd, shell=True)


def cache_pca_object(pca_obj, cache_file):
    if cache_file is None:
        return
    print "Caching to file:", cache_file
    with open(cache_file, 'w') as fp:
        pickle.dump(pca_obj, fp)


def load_pca_from_cache(cache_file):
    if cache_file is None or not os.path.exists(cache_file):
        return None
    print "Loading PCA obj from file:", cache_file
    with open(cache_file, 'rU') as fp:
        pca_obj = pickle.load(fp)

    return pca_obj


def array_to_str(arr, delimiter=","):
    return delimiter.join([str(x) for x in arr])


def save_rdd(data_rdd, dataset, fname):
    hdfs_dst_path = os.path.join(HDFS_DATA_PATH, dataset)
    hdfs_fname = os.path.join(hdfs_dst_path, fname)
    hdfs_fname = HDFS_URL + hdfs_fname
    print "Saving data to:", hdfs_fname
    data_rdd.saveAsTextFile(hdfs_fname)


def write_to_hdfs(arr, fname):
    # Writes the array to HDFS
    o_fpath = os.path.join(HDFS_OUTPUT_PATH, fname)
    with NamedTemporaryFile(mode='w') as temp_fp:
        np.savetxt(temp_fp, arr, fmt="%.6f")
        temp_fp.flush()

        # Write temp file to HDFS
        cmd = 'cd %s; cat %s | ./bin/hdfs dfs -put -f - %s' % (HADOOP_PATH,
                                                               temp_fp.name,
                                                               o_fpath)
        subprocess.check_call(cmd, shell=True)


def assign_to_cluster(x, cluster_assignment_map=None):
    """
    Cluster by microRNA family
    """
    mirna_name = x.rid()[1]
    return cluster_assignment_map.get(mirna_name, -1)


def get_train_features(spark_context,
                       train_data,
                       basis=20,
                       prob=False,
                       pca_cache_path=None,  # Path where to cache pca obj to.
                       curve_means=None,
                       seed_sites=False,
                       ):

    nan_transformer = NanTransformer()
    if curve_means is not None:
        print "Using means to replace NaN"
        # Use the mean to replace NaN
        nan_transformer = NanMeanTransformer(curve_means)

    train_data = train_data.map(lambda x: nan_transformer.transform_train(x))

    if seed_sites is True:
        feature_transformer = FeatureTransformerSeed(
            categories=SITE_REGION_TYPES,
            basis=basis,
            prob=False,
        )
    else:
        feature_transformer = FeatureTransformer(
            categories=SITE_REGION_TYPES,
            basis=basis,
            prob=False,
        )

    # This returns (label, (numeric_feature, categorical_features))
    train_data = train_data\
        .map(lambda x: Record(
            feature_transformer.transform(x),
            label=x.label(),
            record_id=x.rid()
        ))

    numeric_features = train_data.map(lambda x: x[0])
    scaler = StandardScaler().fit(numeric_features)

    train_data = train_data.map(
        lambda x: Record(
            np.hstack((scaler.transform(x[0]), x[1])),
            label=x.label(),
            record_id=x.rid()
        )
    )

    return train_data


def get_test_features(spark_context,
                      test_data,
                      therm_pca_pair,
                      basis=20,
                      curve_means=None,
                      infer_test_mean=False,
                      seed_sites=False,
                      prob=False):

    nan_transformer = NanTransformer()
    if curve_means is not None:
        # Use the mean to replace NaN
        print "Using means to replace NaN"
        print "infer_test_mean is:", infer_test_mean
        nan_transformer = NanMeanTransformer(curve_means,
                                             infer_test_mean=infer_test_mean)

    test_data = test_data.map(lambda x: nan_transformer.transform_train(x))

    if seed_sites is True:
        feature_transformer = FeatureTransformerSeed(
            categories=SITE_REGION_TYPES,
            basis=basis,
            prob=False,
        )
    else:
        feature_transformer = FeatureTransformer(
            categories=SITE_REGION_TYPES,
            basis=basis,
            prob=False,
        )

    # This returns (label, (numeric_feature, categorical_features))
    test_data = test_data\
        .map(
            lambda x: Record(
                feature_transformer.transform(x),
                label=x.label(),
                record_id=x.rid()
            )
        )

    numeric_features = test_data.map(lambda x: x[0])
    scaler = StandardScaler().fit(numeric_features)

    test_data = test_data.map(
        lambda x: Record(
            np.hstack((scaler.transform(x[0]), x[1])),
            label=x.label(),
            record_id=x.rid()
        )
    )
    return test_data


def update_error(l, p, error, total):
    e = 1 if l != p else 0
    error.add(e)
    total.add(1)


def get_misclassification_error(sc, rdd):
    misclassified = sc.accumulator(0)
    total = sc.accumulator(1)
    rdd.foreach(lambda (x, p): update_error(x.label(), p, misclassified, total))

    return misclassified.value/float(total.value)


def update_confusion(label, pred, confusion_matrix):
    label, pred = int(label), int(pred)
    if label == 1:
        if label == pred:
            confusion_matrix[0].add(1)
        else:
            confusion_matrix[1].add(1)
    else:
        if label == pred:
            confusion_matrix[3].add(1)
        else:
            confusion_matrix[2].add(1)


def get_confusion_matrix(sc, rdd):
    confusion_matrix = []
    for i in range(4):
        confusion_matrix.append(sc.accumulator(i))

    rdd.foreach(lambda (x, p): update_confusion(x.label(), p, confusion_matrix))
    confusion_matrix = [[confusion_matrix[0].value, confusion_matrix[1].value],
                        [confusion_matrix[2].value, confusion_matrix[3].value]]
    confusion_matrix = np.array(confusion_matrix)
    confusion_matrix = confusion_matrix.astype(float)
    return confusion_matrix


def train_svm(spark_context, train_data, output_file_prefix,
              cluster_assignment=None,
              kernel='LINEAR',
              iterations=DEFAULT_ITERATIONS,
              C=DEFAULT_C,
              reg_param=DEFAULT_REG_PARAM,
              compute_prob=False,
              **kernel_params):

    if kernel not in KERNEL_TYPES:
        raise Exception('Not a valid kernel. '
                        'Accepted kernel types are:'
                        ','.join(KERNEL_TYPES)
                        )

    if kernel == 'LINEAR':
        # Spark expects data to be as LabeledPoint so convert to LabeledPoint
        if cluster_assignment is not None:
            # Extract data points that belong to one of the clusters.
            cluster_assignment_func = functools.partial(
                assign_to_cluster, cluster_assignment_map=cluster_assignment
            )
            train_data = train_data.filter(
                lambda x: cluster_assignment_func(x) >= 0
            )
            train_data = train_data.repartition(RDD_PARTITIONS)

        train_data_labeled = train_data.map(
            lambda x: LabeledPoint(x.label(), x._feature_list)
        )
        model = SVMWithSGD.train(train_data_labeled,
                                 iterations=iterations,
                                 regParam=reg_param,
                                 intercept=True)
        labels_preds = train_data.map(
            lambda x: (x, model.predict(x._feature_list))
        )

    elif kernel == 'RBF':
        num_clusters = len(set(cluster_assignment.values()))
        cluster_assignment_func = functools.partial(
            assign_to_cluster, cluster_assignment_map=cluster_assignment
        )
        model = ClusterSvmModel(num_clusters, cluster_assignment_func,
                                C=C, prob=compute_prob, **kernel_params)
        model.train(spark_context, train_data, use_threads=True)

        labels_preds = model.predict(train_data)

    # Compute training error.
    error = get_misclassification_error(
        spark_context, labels_preds
    )

    # Print training metrics
    print "Training error:", error

    if kernel == 'LINEAR':
        weights_file = output_file_prefix + ".weights"
        print "Writing weights to:", weights_file
        write_to_hdfs(model.weights, weights_file)
    else:
        # Write support vectors to file
        # TODO: Save support vectors for 'RBF' kernel?
        # support_vector_file = output_file_prefix + ".support"
        # print "Writing support vectors to:", support_vector_file
        # write_to_hdfs(model.get_support_vectors(), support_vector_file)
        pass

    return model


def get_logistic_prob(features, weights=None, intercept=0.0):
    prob = 1.0 / (1 + np.exp(-(features.dot(weights) + intercept)))
    return prob


def predict(spark_context, model, test_data, output_file_prefix,
            cluster_assignment=None,
            kernel='LINEAR', compute_prob=False):

    if kernel == 'LINEAR':
        if cluster_assignment is not None:
            # Extract data points that belong to one of the clusters.
            cluster_assignment_func = functools.partial(
                assign_to_cluster, cluster_assignment_map=cluster_assignment
            )
            test_data = test_data.filter(
                lambda x: cluster_assignment_func(x) >= 0
            )
            test_data = test_data.repartition(RDD_PARTITIONS)
        labels_preds = test_data.map(
            lambda x: (x, model.predict(x._feature_list))
        )
    elif kernel == 'RBF':
        labels_preds = model.predict(test_data)

    # Compute test error.
    error = get_misclassification_error(
        spark_context, labels_preds
    )

    # Print training metrics
    print "Test error:", error
    conf_matrix = get_confusion_matrix(spark_context, labels_preds)

    # Write predictions and labels for test data to HDFS.
    ofile_name = output_file_prefix + ".pred"
    ofile_path = os.path.join(HDFS_OUTPUT_PATH, ofile_name)
    print "Writing labels and predictions to:", ofile_path
    labels_preds.saveAsTextFile(ofile_path)
    labels_preds.unpersist()

    if not compute_prob:
        return conf_matrix

    if kernel == 'LINEAR':
        # Compute probabilities using the weights and logistic function
        compute_prob = functools.partial(get_logistic_prob,
                                         weights=model.weights,
                                         intercept=model.intercept)
        labels_and_prob = test_data.map(
            lambda x: (x, compute_prob(x._feature_list))
        )
    else:
        # For RBF Kernel. Compute the probabilities using the model.
        labels_and_prob = model.predict_proba(test_data)

    # Write probabilities to HDFS file
    ofile_name = output_file_prefix + ".prob"
    ofile_path = os.path.join(HDFS_OUTPUT_PATH, ofile_name)
    print "Writing labels and probability scores to:", ofile_path
    labels_and_prob\
        .map(lambda (x, p): "%s,%d,%f" % (",".join(x.rid()), x.label(), p))\
        .saveAsTextFile(ofile_path)

    return conf_matrix


def train_predict(spark_context,
                  train_data,
                  test_data,
                  model_params=None,
                  cluster_assignment=None,
                  dataset='',
                  pca_cache_path=None,
                  save_train_data=False,
                  curve_means=None,
                  train_iters=DEFAULT_ITERATIONS,
                  infer_test_mean=False,
                  seed_sites=False,
                  ):

    uid = shortuuid.uuid()
    output_file_prefix = os.path.join(dataset, uid)

    train_data = get_train_features(
        spark_context, train_data,
        basis=model_params['basis'],
        prob=model_params['prob'],
        pca_cache_path=pca_cache_path,
        curve_means=curve_means,
        seed_sites=seed_sites
    )
    train_data = train_data.persist(StorageLevel.MEMORY_ONLY)
    if save_train_data is True:
        save_rdd(train_data, dataset, "train_"+uid)

    model = train_svm(spark_context,
                      train_data, output_file_prefix,
                      cluster_assignment=cluster_assignment,
                      C=model_params['C'],
                      reg_param=model_params['reg_param'],
                      kernel=model_params['kernel'],
                      compute_prob=model_params['prob'],
                      **model_params['kernel_params']
                      )
    train_data.unpersist()

    test_data = get_test_features(
        spark_context, test_data, None,
        basis=model_params['basis'],
        curve_means=curve_means,
        infer_test_mean=infer_test_mean,
        seed_sites=seed_sites,
        prob=model_params['prob']
    )
    test_data = test_data.persist(StorageLevel.MEMORY_ONLY)
    if save_train_data is True:
        save_rdd(test_data, dataset, "test_"+uid)

    metrics = predict(spark_context, model, test_data, output_file_prefix,
                      cluster_assignment=cluster_assignment,
                      kernel=model_params['kernel'],
                      compute_prob=model_params['prob'])
    test_data.unpersist()
    return metrics
