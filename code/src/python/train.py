import sys
import numpy as np
import rpy2.robjects as robjects
import rpy2
from rpy2.robjects.numpy2ri import numpy2ri
rpy2.robjects.numpy2ri.activate()
from r_interface import create_r_func
from parallelize import run_func_parallely
from set_model_params import set_model_params


def usage():
    print '''
    %s <positive mfe file> <positive mfe_open file> <positive seed file>
    <negative mfe file> <negative mfe_open file> <negative seed file>
    <output model file> <output pca file>''' % sys.argv[0]


def train(pos_mfe, pos_mfe_open, pos_seed,
          neg_mfe, neg_mfe_open, neg_seed):
    pos_y = np.repeat(1, np.size(pos_mfe, 0))
    neg_y = np.repeat(-1, np.size(neg_mfe, 0))

    # Training data is seed weighted Delta Delta G score
    # i.e. (mfe - mfe_open)*(seed/max(seed))
    seed = pos_seed
    seed_max = np.max(seed, axis=1)
    seed_max = np.reshape(seed_max, (np.size(seed_max), 1))
    pos_seed = pos_seed / np.repeat(seed_max, seed.shape[1], axis=1)
    pos_delta_delta_g = pos_mfe - pos_mfe_open
    pos_weighted_del_g = pos_mfe
    pos_weighted_del_del_g = pos_delta_delta_g

    seed = neg_seed
    seed_max = np.max(seed, axis=1)
    seed_max = np.reshape(seed_max, (np.size(seed_max), 1))
    neg_seed = neg_seed / np.repeat(seed_max, seed.shape[1], axis=1)
    neg_delta_delta_g = neg_mfe - neg_mfe_open
    neg_weighted_del_g = neg_mfe
    neg_weighted_del_del_g = neg_delta_delta_g

    data_x = np.vstack((np.hstack((pos_weighted_del_g, pos_weighted_del_del_g)),
                        np.hstack((neg_weighted_del_g, neg_weighted_del_del_g)))
                       )
    data_y = np.hstack((pos_y, neg_y))

    # Smooth training data
    smooth_fd_func = create_r_func('smoothFd.R')
    data_x_fd = run_func_parallely(smooth_fd_func, data_x)
    # We need to combine the splits to create on fd object
    create_fd_func = create_r_func('createFd.R')
    data_x_fd = create_fd_func(data_x_fd)

    # Get principal components
    get_principal_components_func = create_r_func('computePca.R')
    pca_r = get_principal_components_func(data_x_fd)

    # Train SVM Model
    svm_func = create_r_func('trainSvm.R')
    svm_model = svm_func(data_y, pca_r[0])
    return(svm_model, pca_r)


def main():
    if len(sys.argv) != 9:
        usage()
        return 1
    set_model_params()
    pos_mfe = np.genfromtxt(sys.argv[1], delimiter=',')
    pos_mfe_open = np.genfromtxt(sys.argv[2], delimiter=',')
    pos_seed = np.genfromtxt(sys.argv[3], delimiter=',')
    neg_mfe = np.genfromtxt(sys.argv[4], delimiter=',')
    neg_mfe_open = np.genfromtxt(sys.argv[5], delimiter=',')
    neg_seed = np.genfromtxt(sys.argv[6], delimiter=',')
    svm_model_file = sys.argv[7]
    pca_file = sys.argv[8]
    (svm_model, pca) = train(pos_mfe, pos_mfe_open, pos_seed,
                             neg_mfe, neg_mfe_open, neg_seed)

    # Save the SVM Model and PCA
    assign_func = robjects.r('assign')
    assign_func('svm.model', svm_model)
    assign_func('pca', pca)
    robjects.r('save(svm.model, file="%s")' % svm_model_file)
    robjects.r('save(pca, file="%s")' % pca_file)
    return 0


if __name__ == '__main__':
    sys.exit(main())
