"""
    Encapsulates a record containing a label, row_id and feature list
"""
import numpy as np


class Record:
    def __init__(self, feature_list, label=None, record_id=None):
        self._feature_list = feature_list
        self._label = label
        self._record_id = record_id

    def __getitem__(self, index):
        return self._feature_list[index]

    def __len__(self):
        return len(self._feature_list)

    def __setitem__(self, key, value):
        self._feature_list[key] = value

    def __delitem__(self, index):
        del self._feature_list[index]

    def __iter__(self):
        return iter(self._feature_list)

    def __array__(self):
        return np.array(self._feature_list)

    def __eq__(self, other):
        return self._record_id == other.rid()

    def __hash__(self):
        return hash(self._record_id)

    def label(self):
        return self._label

    def rid(self):
        return self._record_id


class LabeledDataPoint:
    def __init__(self, features, label=None, record_id=None):
        self.label = label
        self.rid = record_id
        self.features = features
