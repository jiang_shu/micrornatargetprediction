'''
    Input: File with mrna-mirna clip data.
    Outputs the following features:
        a) With window length ~22 and centered around the entire target region
            1) del G curve
            2) del del G curve
            3) AU composition curve
            4) mean conservation curve
        b) For window length =8 and centered around seed match region
            1) del G curve
            2) del del G curve
            3) AU composition curve
            5) mean conservation curve
        c) Region in which the site is present: 3UTR, 5UTR, CDS
        d) Location of target from the end of the region (range: 0 to 1)
        e) Enrichment of seed type for both seed and seedless sites.
        f) Size of target site

    Possible future features:
        1) Number of seed matches withing the seed region.
        2) Avg binding of the top N alignments within the site.
'''
import sys
import subprocess
import re
import math
from multiprocessing import Process, Queue
import multiprocessing
import time
from redisdao import RedisDao
import numpy as np
from functools import partial


PROCESSORS = multiprocessing.cpu_count()  # Number of processors
WINDOW_LENGTH = 10  # If none then use WINDOW_LENGTH_MULTIPLIER* len(microRNA)
WINDOWS = 13  # Number of windows to get data for
WINDOW_LENGTH_MULTIPLIER = 1

# RNAhybrid executabe
RNAHYBRID = "../../third-party/bin/RNAhybrid"
# RNAfold executable
RNAFOLD = "RNAfold"

MATCH, MISMATCH, GAP, WOBBLE = 1, 2, 3, 4


def get_au_content(mrna_seq):
    au_content = [1 if x == 'A' or x == 'U' else 0 for x in mrna_seq]
    au_content = sum(au_content)
    return float(au_content)/float(len(mrna_seq))


def invoke_rna_hybrid(mrna_seq, mirna_seq, helix_constraint="-f 2,7"):
    executable = RNAHYBRID
    # TODO: Change the dataset type (3utr_human). But do we need to change
    # if we are not interested in the p-value?
    args = "-c -b 1 -s 3utr_human"
    if helix_constraint:
        args += " " + helix_constraint
    cmd = "%s %s %s %s" % (executable, args, mrna_seq, mirna_seq)
    output = subprocess.check_output(cmd, shell=True)
    return output


def invoke_rna_fold(mrna_seq):
    executable = RNAFOLD
    cmd = "echo %s | %s --noPS" % (mrna_seq, executable)
    output = subprocess.check_output(cmd, shell=True)
    return output


def parse_rna_fold_output(output):
    mfe_open = np.nan
    output = output.split('\n')
    m = re.search(r'\(\s*([-0-9]*[.]{0,1}[0-9]+)\)$', output[1])
    if m:
        mfe_open = float(m.group(1))
    else:
        print output
        raise Exception("Couldn't parse output")
    return mfe_open


def parse_rna_hybrid_output(output, get_alignment=False):
    # Parse output to get the following features:
    #   Mean free energy (mfe) of duplex.
    #   Alignment as a vector of
    #   matches (1), mismatches (2), gap (3) and GU wobble (4)
    output = output.split(":")
    mfe = float(output[4])
    if not get_alignment:
        return mfe
    alignment = []
    start = int(output[6])
    for i in range(len(output[-2])):
        if output[-2][i] == ' ':
            if output[-1][i] != ' ' and output[-4][i] != ' ':
                alignment.append(MISMATCH)
            else:
                alignment.append(GAP)
        elif (output[-2][i] == 'G' and output[-3][i] == 'U') or\
             (output[-3][i] == 'G' and output[-2][i] == 'U'):
            alignment.append(WOBBLE)
        else:
            alignment.append(MATCH)
    return (mfe, start, alignment)


def get_features(pid, mrna, mirna, queue):
    len_mrna = len(mrna)

    mfe = []
    mfe_helix = []
    mfe_open = []
    au_content = []

    # Sliding window
    i = 0
    while (i + WINDOW_LENGTH) <= len_mrna:
        # Get the mfe for the duplex.
        mrna_part = mrna[i:(i + WINDOW_LENGTH)]
        output = invoke_rna_hybrid(mrna_part, mirna, helix_constraint=None)
        features = parse_rna_hybrid_output(output)
        mfe.append(float(features))

        output = invoke_rna_hybrid(mrna_part, mirna)
        features = parse_rna_hybrid_output(output)
        mfe_helix.append(float(features))

        # Get the mfe open (DeltaG_open) for the mrna
        output = invoke_rna_fold(mrna_part)
        output = parse_rna_fold_output(output)
        mfe_open.append(output)
        i += WINDOW_LENGTH

        # Get AU Content
        au_content.append(get_au_content(mrna_part))

    queue.put((pid, mfe, mfe_helix, mfe_open, au_content))


def get_start_end_indices(mrna, mirna, site_pos):
    len_mrna = len(mrna)

    site_pos_len = site_pos[1] - site_pos[0]
    if site_pos_len < WINDOW_LENGTH:
        diff = WINDOW_LENGTH - site_pos_len
        site_pos = (site_pos[0] - diff/2, site_pos[1] + (diff - diff/2))
    elif site_pos_len > WINDOW_LENGTH:
        mid = site_pos[0] + site_pos_len/2
        site_pos = (mid - WINDOW_LENGTH/2,
                    mid + (WINDOW_LENGTH - WINDOW_LENGTH/2))
    if site_pos[0] < 0:
        site_pos = (0, site_pos[1] - site_pos[0])
    elif site_pos[1] > len_mrna:
        site_pos = (site_pos[0] - site_pos[1] + len_mrna, len_mrna)

    start_ind = site_pos[0] + np.arange(1, (WINDOWS + 1)) * (-WINDOW_LENGTH)
    valid_ind = np.where(start_ind >= 0)
    if np.size(valid_ind) <= 0:
        start_ind = site_pos[0]
    else:
        start_ind = start_ind[np.max(valid_ind)]

    end_ind = site_pos[1] + np.arange(1, (WINDOWS + 1)) * WINDOW_LENGTH
    valid_ind = np.where(end_ind <= len_mrna)
    if np.size(valid_ind) <= 0:
        end_ind = site_pos[1]
    else:
        end_ind = end_ind[np.max(valid_ind)]
    return (start_ind, end_ind, site_pos)


def fix_length(data, site=None, start=None, end=None):
    prefix_len = WINDOWS - (site[0] - start)/WINDOW_LENGTH
    suffix_len = WINDOWS - (end - site[1])/WINDOW_LENGTH
    prefix = np.repeat(np.nan, prefix_len)
    suffix = np.repeat(np.nan, suffix_len)
    return np.concatenate((prefix, data, suffix))


def get_features_parallel(redis_dao, mrna, mirna, site_pos):
    mrna_seq = str(mrna._seq)
    mirna_seq = str(mirna._seq)
    len_mirna = len(mirna_seq)
    len_mrna = len(mrna_seq)
    if site_pos[0] > len_mrna or site_pos[1] > len_mrna:
        print "site [%d, %d] outside mrna of length %d" % (site_pos[0],
                                                           site_pos[1],
                                                           len_mrna)
        return None

    start_ind, end_ind, site_pos = get_start_end_indices(mrna_seq,
                                                         mirna_seq, site_pos)

    print "mRNA length: %d, microRNA length: %d" % (len_mrna, len_mirna)
    batch_size = math.trunc(
        (end_ind - start_ind) / (WINDOW_LENGTH * PROCESSORS))

    queue = Queue()
    procs = []
    for i in range(PROCESSORS - 1):
        start_index = start_ind + i * batch_size * WINDOW_LENGTH
        end_index = start_ind + ((i + 1) * batch_size * WINDOW_LENGTH)
        p = Process(target=get_features,
                    args=(i,
                          mrna_seq[start_index:end_index],
                          mirna_seq,
                          queue)
                    )
        procs.append(p)

    start_index = start_ind + (PROCESSORS - 1) * batch_size * WINDOW_LENGTH
    end_index = end_ind

    p = Process(target=get_features,
                args=(PROCESSORS - 1,
                      mrna_seq[start_index:end_index],
                      mirna_seq,
                      queue)
                )
    procs.append(p)
    for p in procs:
        p.start()

    mfe = [None]*PROCESSORS
    mfe_helix = [None]*PROCESSORS
    mfe_open = [None]*PROCESSORS
    au_content = [None]*PROCESSORS

    for i in range(PROCESSORS):
        ret = queue.get()
        pid = ret[0]
        mfe[pid] = ret[1]
        mfe_helix[pid] = ret[2]
        mfe_open[pid] = ret[3]
        au_content[pid] = ret[4]

    mfe = np.concatenate(mfe)
    fix_length_func = partial(fix_length, site=site_pos,
                              start=start_ind,
                              end=end_ind)
    mfe = fix_length_func(mfe)
    mfe_helix = np.concatenate(mfe_helix)
    mfe_helix = fix_length_func(mfe_helix)
    mfe_open = np.concatenate(mfe_open)
    mfe_open = fix_length_func(mfe_open)
    au_content = np.concatenate(au_content)
    au_content = fix_length_func(au_content)

    min_len = min([mfe.size, mfe_helix.size, mfe_open.size, au_content.size])
    if min_len < (2*WINDOWS + 1):
        raise Exception("Got length %d < %d" % (min_len, 2*WINDOWS + 1))

    output = invoke_rna_hybrid(mrna_seq[site_pos[0]:site_pos[1]],
                               mirna_seq)
    _, start, alignment = parse_rna_hybrid_output(output, get_alignment=True)
    if len(alignment) == 0:
        output = invoke_rna_hybrid(mrna_seq[site_pos[0]:site_pos[1]],
                                   mirna_seq, helix_constraint=None)
        _, start, alignment = parse_rna_hybrid_output(output,
                                                      get_alignment=True)
    start += site_pos[0]
    end = start + len(alignment)
    consv = redis_dao._handle.hget('features:hg18:'+mrna._name,
                                   'consv_seq')
    consv = np.array([float(x) for x in consv.split()])
    consv = consv[start:end]

    return (mfe.ravel(),
            mfe_helix.ravel(),
            mfe_open.ravel(),
            au_content.ravel(),
            consv,
            alignment)


def str_from_array(arr):
    return " ".join([str(x) for x in arr])


def get_site_offset(mrna, site_end, target_mismatch, target_match):
    offset = None
    print "Getting offset for: ", mrna._name
    mrna_region = [x1 if x1 != " " else x2 for x1, x2 in zip(target_mismatch,
                                                             target_match)]
    mrna_region = "".join(mrna_region)
    mrna_region = re.sub(r"\s+", "", mrna_region)
    mrna_region = mrna_region.strip("N")
    region_end = mrna._seq.find(mrna_region)
    if region_end > 0:
        region_end += len(mrna_region)
        offset = region_end - site_end
    return offset


def write_features(fname):
    offset = None
    with open(fname, 'rU') as input_fp,\
        open('../../../data/pos_clip_3utr_offset_seed_features.txt', 'w') as pos_train_fp,\
        open('../../../data/neg_clip_3utr_offset_seed_features.txt', 'w') as neg_train_fp,\
        RedisDao() as redis_dao:

        output_fps = {
            'pos_train': pos_train_fp,
            'neg_train': neg_train_fp,
        }
        ignored = 0
        header = True

        print "Using %d processors" % PROCESSORS

        t1 = time.time()
        for line in input_fp:
            key = None
            if header:
                header = False
                continue
            line = line.strip()
            line = line.split("\t")
            mrna, mirna, site_pos, seed_pos, clip, target_mismatch, target_match =\
                line[0], line[1], line[3], line[4], line[40], line[42], line[43]
            site_pos = site_pos.split("-")
            site_pos = map(int, site_pos)
            seed_pos = seed_pos.split("-")
            seed_pos = map(int, seed_pos)

            print "Getting features for: %s - %s" % (mrna, mirna)
            mrna = redis_dao.get_messenger_rna_from_name(mrna, assembly="hg18")
            mirna = redis_dao.get_micro_rna_from_name(mirna)

            if not mrna or not mirna:
                print "Could not get sequence for %s-%s" % (mrna, mirna)
                ignored += 1
                print "Ignored: ", ignored
                continue
            mirna._seq = mirna._seq[:8] #only take the seed region of the mirna.

            offset = get_site_offset(mrna, site_pos[1], target_mismatch, target_match)
            if offset is None:
                print "Could not get offset for %s" % mrna
                ignored += 1
                print "Ignored: ", ignored
                continue
            site_pos = site_pos[0] + offset, site_pos[1] + offset
            seed_pos = seed_pos[0] + offset, seed_pos[1] + offset

            if clip == "1":
                # Write to pos_train_features file
                key = 'pos_train'

            elif clip == "0":
                key = 'neg_train'

            else:
                print "Got clip value of %s. Ignoring." % clip
                ignored += 1
                continue


            features = get_features_parallel(redis_dao,
                                             mrna,
                                             mirna,
                                             seed_pos)

            if not features:
                ignored += 1
                print "Could not get feature for %s-%s" % (mrna, mirna)
                print "Ignored: ", ignored
                continue

            if key:
                # mrna, mirna, mfe, mfe_helix, mfe_open, au_content
                # consv, alignment, offset
                output_fps[key].write("%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % (
                    mrna, mirna,
                    str_from_array(features[0]),
                    str_from_array(features[1]),
                    str_from_array(features[2]),
                    str_from_array(features[3]),
                    str_from_array(features[4]),
                    str_from_array(features[5]),
                    str(offset)
                ))

        t2 = time.time()
        print "Time elapsed: ", (t2 - t1)


def main():
    write_features('../../../data/CLIP/Human-UTR3-Seed-All.txt')

if __name__ == '__main__':
    sys.exit(main())
