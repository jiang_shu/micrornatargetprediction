'''
    Code to create and populate redis database.
'''

import sys
import redis
from Bio import SeqIO


# populate microRNA
def populate_microrna_list(redis_handle):
    for record in SeqIO.parse('../../../data/mirnas/mature.fa', 'fasta'):
        name = record.id.split('.')[0]
        seq = str(record.seq)
        redis_handle.sadd('microrna:list', name)
        redis_handle.set('seq:'+name, seq)
        print "Processed microRNA: ", name


def populate_microrna_target_miner_list(redis_handle):
    for record in SeqIO.parse(
            '../../../data/TargetMiner/hsa_mirbase_list.txt',
            'fasta'):

        name = record.id.split('.')[0]
        seq = str(record.seq)
        redis_handle.sadd('microrna:list', name)
        redis_handle.set('seq:'+name, seq)
        print "Processed microRNA: ", name


# populate mrna
def populate_mrna_target_miner_list(redis_handle):
    existing_mrnas = redis_handle.smembers('mrna:list')
    for record in SeqIO.parse(
            '../../../data/TargetMiner/hg18_3utr.txt',
            'fasta'):

        try:
            name = record.description.split(' ')[1]
        except:
            print "Error processing record: \n", record
            continue
        if name not in existing_mrnas:
            seq = record.seq.transcribe()  # Covert the alphabet to AUCG
            seq = str(record.seq)
            redis_handle.sadd('mrna:list', name)
            redis_handle.set('seq:'+name, seq.upper())
            print "Processed mrna: ", name


# populate mrna
def populate_mrna_list(redis_handle, assembly="hg19"):
    if assembly == "hg18":
        filename = '../../../data/human/ucsc_ref_genes_mrna_hg18_seq'
    elif assembly == "hg19":
        filename = '../../../data/human/ucsc_ref_genes_mrna_hg19_seq'
    elif assembly == "mm9":
        filename = '../../../data/mouse/mm9_ref_gene_seq'
    else:
        raise Exception("Unknown assembly: %s" % assembly)

    print "Using assembly %s and file: %s" % (assembly, filename)
    for record in SeqIO.parse(filename,
                              'fasta'):
        name = record.id.split('.')[0]
        seq = record.seq.transcribe()  # Covert the alphabet to AUCG
        seq = str(record.seq)
        redis_handle.sadd('mrna:list:'+assembly, name)
        redis_handle.set('seq:%s:%s' % (assembly, name), seq.upper())
        print "Processed mrna: ", name


# populate targets
def populate_targets(redis_handle):
    with open('../../../data/validated_targets.txt', 'rU') as fp:
        for line in fp:
            mrna, microrna = line.strip().split(',')
            redis_handle.sadd('mirwalk:target:'+mrna, microrna)
            redis_handle.sadd('mirwalk:target:'+microrna, mrna)
            redis_handle.sadd('mirwalk:target:list', line.strip())


def populate_psilac_targets(redis_handle):
    # Map microRNA name to standard name
    micrna_map = {
        'LET-7B': 'hsa-let-7b-5p',
        'MIR-30A-5P': 'hsa-miR-30a-5p',
        'MIR-1': 'hsa-miR-1-3p',
        'MIR-155': 'hsa-miR-155-5p',
        'MIR-16': 'hsa-miR-16-5p'
    }

    # Delete existing keys
    for line in redis_handle.smembers('psilac:target:list'):
        (mrna, microrna) = line.split(',')
        if redis_handle.exists('psilac:target:'+mrna):
            redis_handle.delete('psilac:target:'+mrna)
        if redis_handle.exists('psilac:target:'+microrna):
            redis_handle.delete('psilac:target:'+microrna)
    redis_handle.delete('psilac:target:list')

    with open('../../../data/psilac/downregulated_targets', 'rU') as fp:
        for line in fp:
            mrna, microrna = line.strip().split(',')
            microrna = micrna_map[microrna]
            redis_handle.sadd('psilac:target:'+mrna, microrna)
            redis_handle.sadd('psilac:target:'+microrna, mrna)
            redis_handle.sadd('psilac:target:list', "%s,%s" % (mrna, microrna))


def populate_neg_psilac_targets(redis_handle):
    # Map microRNA name to standard name
    micrna_map = {
        'LET-7B': 'hsa-let-7b-5p',
        'MIR-30A-5P': 'hsa-miR-30a-5p',
        'MIR-1': 'hsa-miR-1-3p',
        'MIR-155': 'hsa-miR-155-5p',
        'MIR-16': 'hsa-miR-16-5p'
    }

    # Delete existing keys
    for line in redis_handle.smembers('psilac:neg:target:list'):
        (mrna, microrna) = line.split(',')
        if redis_handle.exists('psilac:neg:target:'+mrna):
            redis_handle.delete('psilac:neg:target:'+mrna)
        if redis_handle.exists('psilac:neg:target:'+microrna):
            redis_handle.delete('psilac:neg:target:'+microrna)
    redis_handle.delete('psilac:neg:target:list')

    with open('../../../data/psilac/not_downregulated_targets', 'rU') as fp:
        for line in fp:
            mrna, microrna = line.strip().split(',')
            microrna = micrna_map[microrna]
            redis_handle.sadd('psilac:neg:target:'+mrna, microrna)
            redis_handle.sadd('psilac:neg:target:'+microrna, mrna)
            redis_handle.sadd('psilac:neg:target:list',
                              "%s,%s" % (mrna, microrna))


# populate negative targets
def populate_negative_targets(redis_handle):
    with open('../../../data/negative_targets.txt', 'rU') as fp:
        for line in fp:
            mrna, microrna = line.strip().split(',')
            redis_handle.sadd('mirwalk:neg:target:'+mrna, microrna)
            redis_handle.sadd('mirwalk:neg:target:'+microrna, mrna)
            redis_handle.sadd('mirwalk:neg:target:list', line.strip())


def populate_target_miner_train_targets(redis_handle):
    # Delete existing keys
    for line in redis_handle.smembers('targetminer:train:target:list'):
        microrna, mrna = line.split(',')
        if redis_handle.exists('targetminer:train:target:'+mrna):
            redis_handle.delete('targetminer:train:target:'+mrna)

        if redis_handle.exists('targetminer:train:target:'+microrna):
            redis_handle.delete('targetminer:train:target:'+microrna)

    redis_handle.delete('targetminer:train:target:list')

    with open('../../../data/TargetMiner/targetminer_train_pos.csv',
              'rU') as fp:
        for line in fp:
            microrna, mrna = line.strip().split(',')
            redis_handle.sadd('targetminer:train:target:'+mrna, microrna)
            redis_handle.sadd('targetminer:train:target:'+microrna, mrna)
            redis_handle.sadd('targetminer:train:target:list', "%s,%s" %
                              (mrna, microrna))


def populate_target_miner_test_targets(redis_handle):
    # Delete existing keys
    for line in redis_handle.smembers('targetminer:test:target:list'):
        (microrna, mrna) = line.split(',')
        if redis_handle.exists('targetminer:test:target:'+mrna):
            redis_handle.delete('targetminer:test:target:'+mrna)

        if redis_handle.exists('targetminer:test:target:'+microrna):
                redis_handle.delete('targetminer:test:target:'+microrna)

    redis_handle.delete('targetminer:test:target:list')

    with open('../../../data/TargetMiner/bio_verified_positive_test.txt',
              'rU') as fp:

        for line in fp:
            microrna, mrna = line.strip().split(',')
            redis_handle.sadd('targetminer:test:target:'+mrna, microrna)
            redis_handle.sadd('targetminer:test:target:'+microrna, mrna)
            redis_handle.sadd('targetminer:test:target:list', "%s,%s" %
                              (mrna, microrna))


def populate_target_miner_train_negative_targets(redis_handle):
    # Delete existing keys
    for line in redis_handle.smembers('targetminer:train:neg:target:list'):
        microrna, mrna = line.split(',')
        if redis_handle.exists('targetminer:train:neg:target:'+mrna):
            redis_handle.delete('targetminer:train:neg:target:'+mrna)

        if redis_handle.exists('targetminer:train:neg:target:'+microrna):
            redis_handle.delete('targetminer:train:neg:target:'+microrna)

    redis_handle.delete('targetminer:train:neg:target:list')

    with open('../../../data/TargetMiner/targetminer_train_neg.csv',
              'rU') as fp:
        for line in fp:
            microrna, mrna = line.strip().split(',')
            redis_handle.sadd('targetminer:train:neg:target:'+mrna, microrna)
            redis_handle.sadd('targetminer:train:neg:target:'+microrna, mrna)
            redis_handle.sadd('targetminer:train:neg:target:list', "%s,%s" %
                              (mrna, microrna))


def populate_target_miner_test_negative_targets(redis_handle):
    # Delete existing keys
    for line in redis_handle.smembers('targetminer:test:neg:target:list'):
        (microrna, mrna) = line.split(',')
        if redis_handle.exists('targetminer:test:neg:target:'+mrna):
            redis_handle.delete('targetminer:test:neg:target:'+mrna)

        if redis_handle.exists('targetminer:test:neg:target:'+microrna):
            redis_handle.delete('targetminer:test:neg:target:'+microrna)

    redis_handle.delete('targetminer:test:neg:target:list')

    with open('../../../data/TargetMiner/bio_verified_negative_test.txt',
              'rU') as fp:

        for line in fp:
            microrna, mrna = line.strip().split(',')
            redis_handle.sadd('targetminer:test:neg:target:'+mrna, microrna)
            redis_handle.sadd('targetminer:test:neg:target:'+microrna, mrna)
            redis_handle.sadd('targetminer:test:neg:target:list', "%s,%s" %
                              (mrna, microrna))


def populate_mrna_features(redis_handle, assembly='hg19'):
    fname = None
    if assembly == 'hg18':
        fname = '../../../data/human/mrna_hg18_features.txt'
    elif assembly == 'mm9':
        fname = '../../../data/mouse/mrna_mm9_features.txt'
    else:
        raise Exception('Assembly %s currently not supported' % assembly)
    with open(fname, 'rU') as fp:
        for line in fp:
            line = line.strip()
            mrna, utr5_range, utr3_range, cds_range, consv_seq = \
                line.split(",")
            key = 'features:%s:%s' % (assembly, mrna)
            value_map = {'utr5_range': utr5_range,
                         'utr3_range': utr3_range,
                         'cds_range': cds_range,
                         'consv_seq': consv_seq}
            redis_handle.delete('features:'+mrna)
            redis_handle.hmset(key, value_map)


def main():
    redis_handle = redis.StrictRedis(host='localhost', port=6379, db=0)
    populate_mrna_list(redis_handle, assembly="mm9")
    populate_mrna_list(redis_handle, assembly="hg19")
    populate_mrna_list(redis_handle, assembly="hg18")
    populate_microrna_list(redis_handle)
    populate_mrna_features(redis_handle, assembly='hg18')
    populate_mrna_features(redis_handle, assembly='mm9')
    redis_handle.save()


if __name__ == '__main__':
    sys.exit(main())
