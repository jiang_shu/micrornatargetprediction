import rpy2
from rpy2.robjects.numpy2ri import numpy2ri
rpy2.robjects.numpy2ri.activate()
from r_interface import create_r_func


def set_model_params(num_basis=10,
                     register_curves=False,
                     num_principal_components=10,
                     svm_kernel='radial',
                     svm_gamma=1,
                     svm_cost=111,
                     svm_prob=False,
                     chunk_size=100,
                     gamma1=50,
                     save_train_data=False):

    set_model_params_func = create_r_func('setModelParams.R')
    params = [num_basis, register_curves, num_principal_components,
              svm_kernel, svm_gamma, svm_cost, svm_prob, chunk_size, gamma1,
              save_train_data]
    set_model_params_func(params)
