from r_interface import create_r_func
import numpy as np


def cluster_data(data):
    r_cluster_func = create_r_func('../R/cluster.R')
    clusters = r_cluster_func(data)
    return clusters[0]


def assign_other_to_cluster(cluster_assignments, main_id, other_id):
    other_id_map = {} # Maps ids to their index in the list
    other_assignment = [np.nan]*len(other_id)

    for i in range(len(other_id)):
        other_id_map[other_id[i]] = i

    for i in range(len(cluster_assignments)):
        if main_id[i] in other_id_map:
            other_id_ind = other_id_map[main_id[i]]
            other_assignment[other_id_ind] = cluster_assignments[i]

    other_assignment = np.array(other_assignment)
    print "Couldn't assign %d out of %d to clusters" % (
        np.size(np.where(np.isnan(other_assignment))), np.size(other_assignment)
    )
    return other_assignment


def cluster(pos_id, pos_data,
            neg_id, neg_data,
            by = 'POSITIVE'):
    '''
        Clusters pos_data (or neg_data if by is NEGATIVE)
        and assigns the neg_data (or pos_data) to the clusters where
        neg_id matches a pos_id in that cluster.
    '''
    if not (by == 'POSITIVE' or by == 'NEGATIVE' or by == 'ALL'):
        raise Exception('by should be one of the following:'
                        'POSITIVE/NEGATIVE/ALL'
                        )
    print "Clustering by %s examples" % by
    if by == 'POSITIVE':
        pos_cluster_assignments = cluster_data(pos_data)
        neg_cluster_assignments = assign_other_to_cluster(
            pos_cluster_assignments, pos_id, neg_id)
    elif by == 'NEGATIVE':
        neg_cluster_assignments = cluster_data(neg_data)
        pos_cluster_assignments = assign_other_to_cluster(
            neg_cluster_assignments, neg_id, pos_id)
    else:
        pos_count = pos_data.shape[0]
        all_data = np.vstack((pos_data, neg_data))
        all_assignments = cluster_data(all_data)
        all_assignments = np.array(all_assignments)
        return (all_assignments[0:pos_count],
                all_assignments[pos_count:])

    return (pos_cluster_assignments, neg_cluster_assignments)
