import MySQLdb as mdb
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from rna import MicroRNA, MessengerRNA


_DBNAME = 'genome'
_PASSWD = 'qx8xyjhwao'


class RNADao(object):
    ''' Data access object for RNAs '''
    def __init__(self):
        # open a database connection
        print "Opening a database connection."
        self._con = mdb.connect(passwd=_PASSWD, db=_DBNAME)
        # get a cursor from the connection
        self._cur = self._con.cursor()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self._cleanup()


    def _cleanup(self):
        # Close cursor and connection
        print "Closing database connections."
        self._cur.close()
        self._con.close()

    def get_validated_mrna_mirna_pairs(self):
        # Returns experimentally vaildated messengerRNA-microRNA pairs.
        # This function only returns the names of the mRNA and miRNAs.
        query = '''
            select distinct a.name as mrna_name, b.mirna_name as mirna_name\
            from ucsc_refgene_mrna a inner join validated_mirna_mrna_map b\
            on (a.name2 = b.gene_name);
            '''
        results = self._cur.execute(query)
        print "Retrieved %d rows" % results
        return self._cur.fetchall()

    def get_messenger_rna_from_name(self, name):
        # Returns a mRNA instance from its name
        query = '''select sequence from ucsc_refgene_mrna_seq\
            where name='%s';''' % name
        self._cur.execute(query)
        seq = self._cur.fetchone()
        if not seq:
            return None
        seq = Seq(seq[0].upper(), IUPAC.unambiguous_dna)
        # The data has ATCG alphabets
        # We need to transcribe to translate to RNA
        seq = seq.transcribe()
        return MessengerRNA(name, seq)

    def get_micro_rna_from_name(self, name):
        # Returns a miRNA instance from its name
        query = '''select sequence from mirna_seq\
            where name='%s';''' % name
        self._cur.execute(query)
        seq = self._cur.fetchone()
        if not seq:
            return None
        seq = Seq(seq[0].upper(), IUPAC.unambiguous_rna)
        return MicroRNA(name, seq)
