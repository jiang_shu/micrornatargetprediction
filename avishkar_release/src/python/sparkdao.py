"""
    DAO interface to access data using Spark SQL
"""
from avishkarconf import AvishkarConf
from pyspark import SparkContext
from pyspark.sql import SQLContext, Row
from rna import MessengerRNA, MicroRNA
import re


def get_row_rdd(rdd, col_names):
    return rdd.map(lambda x: Row(**dict(zip(col_names, x.split(",")))))


def quote_strings(str_list):
    return map(lambda x: '\'%s\'' % x, str_list)


class SparkDao:
    def __init__(self, avishkar_conf, spark_context):
        self._sc = spark_context
        self.sql_context = SQLContext(self._sc)

        # Rgister all files as tables.
        # Each section describes a file
        for table_name in avishkar_conf.sparkdao_tables():
            hdfs_path, col_names =\
                avishkar_conf.sparkdao_table_details(table_name)
            print "Registering %s as table: %s" % (hdfs_path, table_name)

            rdd = self._sc.textFile(hdfs_path)
            row_rdd = get_row_rdd(rdd, col_names)
            schema = self.sql_context.inferSchema(row_rdd)
            schema.registerTempTable(table_name)

    def get_mrna_sequences(self, assembly):
        """
            Returns RDD of @MessengerRNA objects
            assembly specifies the assembly to be used.
            The table name is generated from assembly as assembly_MRNA_SEQ
        """
        table_name = "%s_MRNA_SEQ" % assembly.upper()
        print "Retrieving data from table:", table_name
        result = self.sql_context.sql('select * from %s' % table_name)
        result = result.map(lambda r: MessengerRNA(r.MRNA_NAME, r.SEQ))
        return result

    def get_mirna_sequences(self, mirna_regex=None, mirna_names=None):
        """
            Returns RDD of @MicroRNA objects
        """
        query = 'select * from MIRNA_SEQ'
        result = self.sql_context.sql(query)
        result = result.map(lambda r: MicroRNA(r.MIRNA_NAME, r.SEQ))
        if mirna_regex is not None:
            result = result.filter(
                lambda m: re.search(mirna_regex, m._name) is not None
            )
        elif mirna_names is not None:
            mirna_names = set(mirna_names)
            result = result.filter(lambda m: m._name in mirna_names)
        return result

    def get_mrna_features(self, assembly):
        """
            Returns the mRNA features:
                mrna_name, (5' UTR start, 5' UTR end),
                (3' UTR start, 3' UTR end), (CDS start, CDS end),
                consv_seq
        """
        mrna_table = '%s_MRNA_FEATURES' % assembly.upper()
        result = self.sql_context.sql(
            'select * from %s' % (mrna_table)
        )
        result = result.map(
            lambda x: [x.MRNA_NAME, (x.UTR5_START, x.UTR5_END),
                       (x.UTR3_START, x.UTR3_END),
                       (x.CDS_START, x.CDS_END),
                       x.CONSV_SEQ]
        )
        return result

    def get_clip_locations(self, dataset_name):
        '''
            Returns RDD of mRNA name, List<CLIP locations>
        '''
        results = self.sql_context.sql('select * from %s' % dataset_name)
        results = results.map(
            lambda x: (x.MRNA_NAME, (x.START, x.END))
        )
        return results.groupByKey()

    def get_mrna_clip_data(self, assembly, dataset_name):
        '''
            Returns tuples of (@MessengerRNA, List<CLIP locations>)
            where a CLIP Location is in turn a tuple.
        '''
        mrna_rdd = self.get_mrna_sequences(assembly).map(lambda m: (m._name, m))
        clip_rdd = self.get_clip_locations(dataset_name)
        result_rdd = mrna_rdd.join(clip_rdd).map(lambda (_, (m, l)): (m, l))
        return result_rdd


if __name__ == '__main__':
    sc = SparkContext(appName="SparkDao Test")
    avishkar_conf = AvishkarConf()
    dao = SparkDao(avishkar_conf, sc)

    for mirna in dao.get_mirna_sequences(
            mirna_names=['hsa-let-7a-5p', 'hsa-let-7a-3p']).collect():
        print mirna
