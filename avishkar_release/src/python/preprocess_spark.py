import numpy as np
from functools import partial
import time
from record import Record


def parse_str_array(str_arr, delimiter=" ", conv_func=float):
    return np.array([conv_func(x) for x in str_arr.split(delimiter)])


def extract_columns(row, delimiter=",", seed_enrichment_map=None):
    # Exracts columns from a single row.
    row = row.strip().split(delimiter)
    label = float(row[0])
    row_id = tuple(row[1:5])
    row = row[5:]

    delg = [parse_str_array(row[0]),
            parse_str_array(row[1])]
    deldelg = [parse_str_array(row[2]),
               parse_str_array(row[3])]
    au = [parse_str_array(row[4]),
          parse_str_array(row[5])]

    consv = map(float, row[6:9])
    site_region = row[9]
    rel_site_loc = float(row[10])
    seed_match_type = row[11]
    site_len = int(row[12])

    # Convert the seed type to numeric attribute
    seed_pval = seed_enrichment_map.get(seed_match_type, 1.0)
    seed_enrichment = 1 - seed_pval

    features = [delg, deldelg, au, consv, site_region, rel_site_loc,
                seed_enrichment, site_len]

    return Record(features, label=label, record_id=row_id)


def filter_rows(row, seed_sites=False, region=None):
    is_seed_site = not np.isnan(row[3][1])
    seed_match = (seed_sites == is_seed_site)
    region_val = row[-4]
    region_match = (region is None) or (region_val in region)
    if seed_sites is False:
        # Remove seed and offseed conservation from feature list
        row[3] = row[3][0]
    if seed_match and region_match:
        return row
    else:
        return None


def preprocess_data(data,
                    seed_sites=False,
                    region=None,
                    seed_enrichment_map=None):

    print "Preprocessing data."
    t1 = time.time()
    extract_cols_func = partial(extract_columns, delimiter=",",
                                seed_enrichment_map=seed_enrichment_map)
    filter_rows_func = partial(filter_rows, seed_sites=seed_sites,
                               region=region)
    data = data.map(extract_cols_func).map(filter_rows_func)
    data = data.filter(lambda x: x is not None)
    t2 = time.time()
    print "Done (%f sec) " % (t2 - t1)

    return data
