import numpy as np
from fda import smooth_spline


def set_index(index, arr_size):
    arr = np.zeros(arr_size)
    arr[index] = 1
    return arr


class NanTransformer:
    def transform_train(self, features):
        features[0] = map(lambda y: np.nan_to_num(y), features[0])
        features[1] = map(lambda y: np.nan_to_num(y), features[1])
        features[2] = map(lambda y: np.nan_to_num(y), features[2])
        return features

    def transform_test(self, x):
        return self.transform_train(x)


class NanMeanTransformer(NanTransformer):
    def __init__(self, means, infer_test_mean=False):
        # @means is an array of tuples where
        # arr[i] = (+ve mean for ith feature, -ve mean for ith feature)
        self._means = means
        if infer_test_mean is False:
            self.transform_test = self.transform_train

    def _replace_nan(self, x, mean_arr):
        nan_indices = np.where(np.isnan(x))
        x[nan_indices] = mean_arr[nan_indices]
        return x

    def transform_train(self, features):
        label = features.label()
        for i in range(3):
            features[i] = map(lambda (j, f):
                              self._replace_nan(
                                  f, self._means[2*i + j][int(label)]),
                              enumerate(features[i]))

        return features

    def _diff(self, x, mean_arr):
        non_nan_ind = np.where(~ np.isnan(x))[0]
        diff = x[non_nan_ind] - mean_arr[non_nan_ind]
        return diff.dot(diff)

    def _infer_label(self, x, means_arr):
        diff_neg = self._diff(x, means_arr[0])
        diff_pos = self._diff(x, means_arr[1])
        return 1 if diff_pos <= diff_neg else 0

    def transform_test(self, features):
        # Infer label based on DelG site
        label = self._infer_label(features[0][0], self._means[0])
        for i in range(3):
            features[i] = map(lambda (j, f):
                              self._replace_nan(f, self._means[2*i + j][label]),
                              enumerate(features[i]))

        return features


class StandardScaler:
        def __init__(self):
                self._mean = 0
                self._std = 1

        def fit(self, rdd):
                ssum, sum_sq, scount = rdd.map(lambda x: [x, x**2, 1])\
                    .reduce(lambda x, y: [i + j for (i, j) in zip(x, y)])
                self._mean = ssum/float(scount)
                self._std = np.sqrt(sum_sq/float(scount) - (self._mean ** 2))
                return self

        def transform(self, data):
                return (data - self._mean)/self._std


class FeatureTransformer:
    '''
        Transforms multivariate features to functions features.
        Transform categorical features to one hot encoding.
    '''
    def __init__(self, categories=[], **model_params):
        self._model_params = model_params
        self._category_map = {}
        for i, v in enumerate(categories):
            self._category_map[v] = i
        self._num_categories = len(categories)

    def _get_other_features(self, x):
        return [x[i] for i in (3, 5, 6, 7)]

    def transform(self, x):
        delg_site, delg_seed = x[0][0], x[0][1]
        deldelg_site, deldelg_seed = x[1][0], x[1][1]
        au_site, au_seed = x[2][0], x[2][1]
        fn_features = [delg_site, delg_seed,
                       deldelg_site, deldelg_seed,
                       au_site, au_seed]
        site_region = x[4]
        other_features = self._get_other_features(x)

        # smooth functional data if needed
        if self._model_params['basis'] > 0:
            for i in range(len(fn_features)):
                fn_features[i] = smooth_spline(
                    fn_features[i], nbasis=self._model_params['basis']
                ).get_coeffs()

        fn_features = np.hstack(fn_features)

        site_region = set_index(self._category_map[site_region],
                                self._num_categories)

        numeric_features = np.hstack((fn_features,
                                      other_features
                                      ))

        return (numeric_features, site_region)


class FeatureTransformerSeed(FeatureTransformer):
    def _get_other_features(self, x):
        y = [x[3][0], x[3][1], x[3][2]]
        y += [x[i] for i in (5, 6, 7)]
        return y
