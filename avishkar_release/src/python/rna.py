''' Contains abstractions for microRNAs and mRNAs '''

from Bio.Seq import Seq
import re


class RNA(object):
    def __init__(self, name, seq):
        self._name = name
        self._seq = Seq(seq)

    def __str__(self):
        # Print the name and first 3 and last 3 letters of the sequence
        return "%s, %s...%s" % (self._name, self._seq[0:3], self._seq[-3:])

    def __eq__(self, y):
        return (self._name == y._name and str(self._seq) == str(y._seq))

    def __hash__(self):
        return hash((self._name, str(self._seq)))


class MicroRNA(RNA):
    def __init__(self, name, seq):
        super(MicroRNA, self).__init__(name, seq)

    def get_seed_region(self):
        # Returns the seed region of the microRNA.
        # The seed region is defined as positions of nucleotides
        # from 2 to 8 from the beginning (i.e. 5' end).
        # Returns the seed region as Seq object.
        return self._seq[1:7]

    def get_aug_seed_region(self):
        # Returns the augmented seed region i.e.
        # The seed region plus two nucleotides on either sides.
        return self._seq[0::8]


class MessengerRNA(RNA):
    def __init__(self, name, seq):
        super(MessengerRNA, self).__init__(name, seq)

    def get_seed_matches(self, seed_seq):
        # Returns a list of indices on the mrna sequence
        # where there is an exact match of the seed sequence.
        # First we need to take the reverse complement of the seed region
        # because we are looking for complementary matches.
        seed_comp = seed_seq.reverse_complement()
        regex_pattern = '(?=%s)' % seed_comp
        matched_indices = [m.start() for m in re.finditer(regex_pattern,
                                                          str(self._seq))]
        return matched_indices

    def print_matches(self, mirna, matched_indices):
        # Print the matched duplex
        match_map = {
            ('A', 'U'): '|',
            ('U', 'A'): '|',
            ('G', 'C'): '|',
            ('C', 'G'): '|',
            ('G', 'U'): ':',
            ('U', 'G'): ':'
        }
        mirna_len = len(mirna._seq)
        for index in matched_indices:
            start_pos = index - (mirna_len - 7)
            s1 = self._seq[start_pos:(start_pos+mirna_len)]
            s2 = mirna._seq
            matches = map(lambda (x, y): match_map.get((x, y), ' '),
                          zip(list(s1), list(s2[::-1])))
            matches = "".join(matches)
            print "5' %s (mRNA)\n   %s\n3' %s (miRNA)" % (s1, matches,
                                                          s2[::-1])
            print
