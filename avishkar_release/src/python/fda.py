from scipy.interpolate import LSQUnivariateSpline
import numpy as np


def smooth_spline(y, nbasis=20):
    x = np.linspace(0, 1, y.size)
    norder = 4  # For cubic spline
    nbreaks = nbasis - norder + 2
    breaks = np.linspace(x[0], x[-1], nbreaks)
    breaks = breaks[1:-1]  # Use only the interior points
    spl = LSQUnivariateSpline(x, y, breaks)
    return spl
