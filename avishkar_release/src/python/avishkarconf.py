from ConfigParser import SafeConfigParser
import os
import os.path


# Environment variables needed by the program.
ENV_VARIABLES = ['AVISHKAR_HOME']


class AvishkarConf:
    def __init__(self):
        # Only take the required environment variables from os.environ
        dict_type = dict(map(lambda k: (k, os.environ[k]), ENV_VARIABLES))
        self._main_parser = SafeConfigParser(dict_type)
        self._main_parser.read(os.path.join(os.environ['AVISHKAR_HOME'],
                                            'conf/avishkar.conf'))
        self._sparkdao_parser = SafeConfigParser(dict_type)
        self._sparkdao_parser.read(os.path.join(os.environ['AVISHKAR_HOME'],
                                                'conf/sparkdao.conf'))

    def rnahybrid_executable(self):
        """ Returns path of the RNAhybird executable """
        return self._main_parser.get('THIRD-PARTY', 'rna_hybrid')

    def rnafold_executable(self):
        """ Returns path of the RNAfold executable """
        return self._main_parser.get('THIRD-PARTY', 'rna_fold')

    def sparkdao_table_details(self, table_name):
        """ Given the table name returns the path and column names """
        if not self._sparkdao_parser.has_section(table_name):
            raise Exception('No configuration exists for table %s' % table_name)
        hdfs_path = self._sparkdao_parser.get(table_name, 'hdfs_path')
        col_names = self._sparkdao_parser.get(table_name, 'col_names')\
            .split(',')
        return (hdfs_path, col_names)

    def sparkdao_tables(self):
        """ Returns all tables defined in the config file """
        return self._sparkdao_parser.sections()
