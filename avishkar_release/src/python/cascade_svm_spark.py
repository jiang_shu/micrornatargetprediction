"""
    Implements Parallel (Cascading) SVM .
"""

import numpy as np
from sklearn.svm import SVC
import itertools
from record import Record


MIN_MODELS = 1 # Minimum number of models to use for CompositeSVM
MAX_SV_PER_PARTITION = 20000
EPSILON = 0.0000001


def get_diff(x, y):
    diff = x - y
    return np.dot(diff, diff)


class SvmModel(object):
    def __init__(self, C=1.0, prob=False, **params):
        self.model = SVC(C=C, probability=prob, **params)

    def train(self, data_list):
        """
            Expects a list of @Record intances where each record has the label
            attribute set
        """
        # Convert from list to array
        labels = np.array([x.label() for x in data_list])
        features = np.vstack(data_list)

        self.model.fit(features, labels)
        self.support_vectors_ = [data_list[i] for i in self.model.support_]

    def predict(self, x):
        preds = self.model.predict(x)
        return preds

    def predict_proba(self, x):
        probs = self.model.predict_proba(x)
        # Return the probability of the positive class
        return [p[1] for p in probs]

    def get_support_vectors(self):
        return self.support_vectors_


class CascadeSvmSpark(object):
    @staticmethod
    def get_numbered_row(index, itr):
        """ For each element in the iterator @itr return
            a pair (id, array) where id is the unique id identifying
            the record
        """
        count = 0
        for x in itr:
            count += 1
            yield Record(np.array(x), label=x[0], record_id=(index, count))

    @staticmethod
    def train(sc, rdd, C=1.0, iterations=1, prob=False, to_record_array=False,
              **kernel_params):
        """
            @sc: Spark Context
            @rdd: Data to train on (RDD)
            @C: The error penalty for misclassified points.
            @iterations: Number of iterations over the cascade of SVM
            @prob: Learn probability scores using Platt scaling.
            @to_record_array: If True then converts the input to @Record array
            before training otherwise assumes that input data is already a
            list of @Records.
            @kernel_params: Paramters of the kernel function. E.g. gamma for
            RBF kernel.
        """
        if to_record_array is True:
            rdd = rdd.mapPartitionsWithIndex(CascadeSvmSpark.get_numbered_row)
            # TODO: See if we need to cache this RDD. But in the current use
            # case we don't invoke the code with to_record_array set to True.

        svs = sc.broadcast(None)
        num_partitions = rdd.getNumPartitions()
        print "Data partitions:", num_partitions
        final_partitions = 1

        for i in range(iterations):
            print "Starting iteration:", i
            if i == iterations - 1:
                # This is the last iteration so the final partitions
                # in reduce_svs should be 2 becuase we'll execute the
                # final reduction at the master node.
                final_partitions = 2

            svs_rdd = rdd.mapPartitions(
                lambda itr: CascadeSvmSpark._train_partition_svs(
                    itr, C=C,
                    support_vectors=svs,
                    **kernel_params
                )
            )
            print "Getting distinct support vectors."
            svs_rdd = CascadeSvmSpark._get_distinct_support_vectors(svs_rdd)

            print "Reducing support vectors."
            svs.unpersist(blocking=True)
            svs = CascadeSvmSpark._reduce_svs(svs_rdd, C=C,
                                              final_partitions=final_partitions,
                                              **kernel_params).collect()
            svs = sc.broadcast(svs)
            print "Done (iteration %d)." % i

        svs = svs.value
        model = SvmModel(C=C, prob=prob, **kernel_params)
        model.train(svs)
        num_svs = len(model.get_support_vectors())
        print "# Support vectors (combined) :", num_svs
        return model

    @staticmethod
    def _get_distinct_support_vectors(svs_rdd):
        new_rdd = svs_rdd.map(lambda x: (x.rid(), x))\
            .reduceByKey(lambda x, _: x)\
            .map(lambda (_, x): x)
        return new_rdd

    @staticmethod
    def _reduce_svs(svs, C=1.0, final_partitions=2, **kernel_params):
        """
            Reduces support vectors in a tree fashion.
            Outputs an rdd of reduced support vectors.
        """
        # svs.cache()
        partitions = svs.getNumPartitions()
        num_svs = svs.count()

        print "Current partitions:", partitions
        print "# Support vectors:", num_svs
        sv_per_partition = (2.0 * num_svs)/partitions
        print "Support Vectors per partition for next round:", sv_per_partition

        while sv_per_partition <= MAX_SV_PER_PARTITION\
                and partitions/2 >= final_partitions:

            partitions = partitions / 2
            svs = svs.repartition(partitions)
            print "Current partitions:", partitions

            svs = svs.mapPartitions(
                lambda itr: CascadeSvmSpark._train_partition_svs(
                    itr, C=C, support_vectors=None,
                    **kernel_params)
            )

            num_svs = svs.count()
            print "# Support vectors:", num_svs
            sv_per_partition = (2.0 * num_svs)/partitions
            print "Support Vectors per partition for next round:",\
                sv_per_partition

        return svs

    @staticmethod
    def _get_sv_from_model(mitr):
        """
            Given an iterator of models (The iterator should have only one
            model) Return it's support vectors
        """
        models = [m for m in mitr]
        print "Number of models in partition:", len(models)
        model = models[0]
        return model.get_support_vectors()

    @staticmethod
    def _train_partition_svs(itr, C=1.0, support_vectors=None,
                             **kernel_params):
        model = CascadeSvmSpark._train_partition(
            itr, C=C, support_vectors=support_vectors,
            **kernel_params
        )
        model = model[0]
        svs = model.get_support_vectors()
        return svs

    @staticmethod
    def _merge_support_vectors(data, svs):
        print "Data size:", len(data)
        print "SV Size:", len(svs)
        new_data = set([])
        for x in itertools.chain(data, svs):
            new_data.add(x)
        print "After merging:", len(new_data)
        return list(new_data)

    @staticmethod
    def _train_partition(itr, C=1.0, support_vectors=None,
                         **kernel_params):

        model = SvmModel(C=C, kernel='rbf', **kernel_params)
        data = [x for x in itr]

        if support_vectors is not None and support_vectors.value is not None:
            svs = support_vectors.value
            data = CascadeSvmSpark._merge_support_vectors(data, svs)

        model.train(data)
        return [model]
