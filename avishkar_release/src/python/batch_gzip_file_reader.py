from batch_file_reader import BatchFileReader
import gzip


class BatchGZipFileReader(BatchFileReader):
    def reset_iterator(self):
        self._itr = gzip.open(self._fname)
        self._total_count = 0

    def close_iterator(self):
        self._itr.close()
        self._itr = None
